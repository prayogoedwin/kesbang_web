<?php


function is_login(){
    $ci = get_instance();
    if($ci->session->userdata('status') != 1 || $ci->session->userdata('username') == ''){
        redirect('publik');
    }else{
        return true;
    }
}

function is_admin($email){
    $ci = get_instance();
    $admin = $ci->db->query("SELECT role_akses FROM turin_admin_xyz WHERE emailx = '$email'")->row();
    if($ci->session->userdata('status') == 1 && $admin->role_akses == 25){
        redirect('dashboard');
    }else{
        return true;
    }
}

function encode_url($url){
    $random1 = substr(sha1(rand()), 0, 40);
    $random2 = substr(md5(rand()), 0, 20);
    $ret = base64_encode($random1.$url.$random2);

    return strtr(
        $ret,
        array(
            '+' => '.',
            '=' => '-',
            '/' => '~'
        )
    );
}

function decode_url($url){
    $a = base64_decode($url);
    $hitung = strlen($a);
    $x = $hitung - 60;
    $y = $x + 20;
    $c = substr($a,-$y);
    return substr($c, 0, $x);
}

function cek_realparams($table, $kolom, $param){
    $ci = get_instance();
    $query = $ci->db->query(
            "SELECT $kolom
            FROM $table
            WHERE $kolom = '$param'"
        );
    if ($query->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
    }

    function filter_input_a(){
		$x = '<script>';
		$a = '<script>How are you?</script>';
		if (strpos($a, $x) !== false) {
			return 1;
		}else{
			return 0;
		}
	}

    function filter_input_b($data){
        $data = trim(htmlentities(strip_tags($data)));

        if (get_magic_quotes_gpc())
          $data = stripslashes($data);

        $data = htmlspecialchars($data);
        return $data;
    }

    function filter_input_c($a){
         $b = '[removed]';
         if ( strstr( $a, $b ) ) {
            return 1;
          } else {
            return 0;
          }
    }

    function filter_output($data){
        $newstr = filter_var($data, FILTER_SANITIZE_STRING);
        return $newstr;
    }

    function is_blok($ip){
        $ci = get_instance();
        $a = $ci->db->query("SELECT COUNT(ips) AS x FROM turin_ip_jahat WHERE ips = '$ip' AND DATE(created_at) = DATE(NOW())")->row();
        if($a->x > 0){
            redirect('https://www.polri.go.id/');
        }else{
            return true;
        }
    }

    function cek_email_is_gagal_3($email){
        $ci = get_instance();

        $m = $ci->db->escape($email);

            $a = $ci->db->query("SELECT COUNT(emaily) AS x FROM turin_log_activity WHERE emaily = $m AND status = 'X'")->row();
            $b = $ci->db->query("SELECT emaily, created_at FROM turin_log_activity WHERE emaily = $m order by id DESC")->row();
            $daynow = date('Y-m-d');
            
            if($b){
                $day = date('Y-m-d', strtotime($b->created_at));
                if($a->x > 3 && $daynow == $day ){
                    return true;
                }else{
                    return false;
                }
            }else{
                 return false;
            }
    }

    // function cek_ip_is_gagal_3($ip){
    //  $ci = get_instance();
    //  $m = $ci->db->escape($ip);
    //         $a = $ci->db->query("SELECT COUNT(ips) AS x FROM turin_log_activity WHERE ips = $m AND status = 'X'")->row();
    //         $b = $ci->db->query("SELECT ips, created_at FROM turin_log_activity WHERE ips = $m order by id DESC")->row();
    //         $daynow = date('Y-m-d');
    //         $day = date('Y-m-d', strtotime($b->created_at));
    //         if($a->x > 3 && $daynow == $day ){
    //             return true;
    //         }else{
    //             return false;
    //         }

    // }

    function get_id_user($username){
        $ci = get_instance();
        $a = $ci->db->query("SELECT id FROM turin_admin_xyz WHERE emailx = '$username'")->row();
        if($a == TRUE){
            return $a->id;
        }else{
            return 0;
        }
    }

    function get_id_bidang($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT nama FROM turin_bidang WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a->nama;
        }else{
            return 0;
        }
    }

    function get_id_uptd($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT nama FROM turin_uptd WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a->nama;
        }else{
            return 0;
        }
    }

    function get_id_regulasi($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `url` as ur FROM turin_regulasi WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_regulasi_nama($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `nama` as ur FROM turin_regulasi WHERE `url` = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_program($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `url` as ur FROM turin_program WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_informasi($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `url` as ur FROM turin_informasi WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_laporan($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `url` as ur FROM turin_laporan WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_sop($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `url` as ur FROM turin_sop WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_pengadaan($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `url` as ur FROM turin_pengadaan WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_perencanaan($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `url` as ur FROM turin_perencanaan WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_sumber($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `url` as ur FROM turin_sumber WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_program_nama($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `nama` as ur FROM turin_program WHERE `url` = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_informasi_nama($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `nama` as ur FROM turin_informasi WHERE `url` = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_laporan_nama($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `nama` as ur FROM turin_laporan WHERE `url` = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_sop_nama($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `nama` as ur FROM turin_sop WHERE `url` = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_pengadaan_nama($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `nama` as ur FROM turin_pengadaan WHERE `url` = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_perencanaan_nama($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `nama` as ur FROM turin_perencanaan WHERE `url` = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_sumbernama($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `nama` as ur FROM turin_sumber WHERE `url` = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }


    function get_id_renstra($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `url` as ur FROM turin_renstra WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_renstra_nama($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `nama` as ur FROM turin_renstra WHERE `url` = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function get_id_anggaran($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `url` as ur FROM turin_anggaran WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

   

    function is_active($uri2){
        $ci = get_instance();
        $uri = $ci->uri->segment('2');
        if($uri2 == $uri){
            return 'active';
        }else{
            return '';
        }
    }

    function get_id_anggaran_nama($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `nama` as ur FROM turin_anggaran WHERE `url` = '$id'")->row();
        if($a == TRUE){
            return $a->ur;
        }else{
            return 0;
        }
    }

    function redirecting(){
        return redirect('https://www.polri.go.id/');
    }

    function get_by_id_galeri_foto($id)
    {
        $ci = get_instance();
        $ci->db->where('id_galeri', $id);
        $ci->db->order_by('id', 'ASC');
        $ci->db->limit('1');
        return filter_output($ci->db->get('turin_galeri_data')->row()->deskripsi);
    }

    function get_nama_galeri($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT judul FROM turin_galeri WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a->judul;
        }else{
            return 0;
        }
    }

    function get_nama($uri){
        if($uri == 'bidang' || $uri == 'uptd' || $uri == 'regulasi' || $uri == 'umk' || $uri == 'program' | $uri == 'renstra' | $uri == 'anggaran'  ){
            return  'turin_'.$uri.'_data';
            // /return $data;
        }elseif($uri == 'maklumat' || $uri == 'lhkpn' || $uri == 'lhkasn'){
            return 'turin_maklumat_lhkpn';
        }elseif($uri == 'berita' || $uri == 'profil' || $uri == 'form' ){
            return 'turin_'.$uri;
        }elseif($uri == 'struktur'){
            return 'turin_profil_'.$uri;
        }elseif($uri == 'pejabat'){
            return 'turin_profil_'.$uri;
        }elseif($uri == 'download'){
            return 'turin_'.$uri;
        }elseif($uri == 'informasi'){
            return 'turin_'.$uri.'_data';
        }

    }

    function get_nama_tab($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT `nama` FROM turin_profil_pejabat_tab WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a->nama;
        }else{
            return 0;
        }
    }

    function max_upload(){
        return '5000000';
    }

    function max_id($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT MAX(id) as `maxx` FROM turin_slide WHERE ppid = 0")->row();
        if($a == TRUE){
            if($id == $a->maxx){
                return 'active';
            }else{
                return '';
            }
        }else{
            return '';
        }
    }

    function max_id_ppid($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT MAX(id) as `maxx` FROM turin_slide WHERE ppid = 1")->row();
        if($a == TRUE){
            if($id == $a->maxx){
                return 'active';
            }else{
                return '';
            }
        }else{
            return '';
        }
    }


    function get_id_anggaran_all_kol($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT * FROM turin_anggaran WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a;
        }else{
            return 0;
        }
    }

    function get_id_program_all_kol($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT * FROM turin_program WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a;
        }else{
            return 0;
        }
    }

    function get_id_pengadaan_all_kol($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT * FROM turin_pengadaan WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a;
        }else{
            return 0;
        }
    }
    
     function get_id_ruglasi_all_kol($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT * FROM turin_regulasi WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a;
        }else{
            return 0;
        }
    }

    function get_id_laporan_all_kol($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT * FROM turin_laporan WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a;
        }else{
            return 0;
        }
    }

    function get_id_sop_all_kol($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT * FROM turin_sop WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a;
        }else{
            return 0;
        }
    }

    function get_id_informasi_all_kol($id){
        $ci = get_instance();
        $a = $ci->db->query("SELECT * FROM turin_informasi WHERE id = '$id'")->row();
        if($a == TRUE){
            return $a;
        }else{
            return 0;
        }
    }

    function get_kontak($judul){
        $ci = get_instance();
        $a = $ci->db->query("SELECT * FROM turin_kontak WHERE judul = '$judul'")->row();
        if($a == TRUE){
            return $a;
        }else{
            return 0;
        }
    }

    function get_menu(){
        $ci = get_instance();
        $a = $ci->db->query("SELECT * FROM turin_menu WHERE tipe = 'Utama' AND deleted_at IS NULL")->result();
        return $a;
    }

    function get_menu_ppid(){
        $ci = get_instance();
        $a = $ci->db->query("SELECT * FROM turin_menu WHERE tipe = 'PPID' AND deleted_at IS NULL")->result();
        return $a;
    }

    

?>
