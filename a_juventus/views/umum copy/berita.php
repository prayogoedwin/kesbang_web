<div id="boxes">
   <div style="top: 50%; left: 50%; display: none;" id="dialog" class="window">
      <div id="san">
          <img src="assets/upload/header/disnakertrans.png" width="450">
      </div>
   </div>
   <div style="width: 2478px; font-size: 32pt; color:white; height: 1202px; display: none; opacity: 0.4;" id="mask"></div>
</div>

<br/>
<br/>
   <div class="page-header full-content" style="background-color:#D3D3D3">
      <div class="container">
         <div class="row">
            <div class="col-sm-6">
               <h1><?=strtoupper($this->uri->segment('3'))?></h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb">
                  <li><a href="<?=base_url()?>"><i class="fa fa-home"></i></a></li>
                  <li class="active"><?=strtoupper($this->uri->segment('3'))?></li>
               </ol>
            </div> 
         </div>
      </div>
   </div>


		<section id="home-professor">
			<div class="container">
				
				<div id="kegiatan">
				<?php foreach($allberita AS $beritas): ?>
					
					 <div class="item">
							<div class="card card-event">
							   <div class="card-heading"><img src="<?=base_url(filter_output($beritas->deskripsi))?>" alt="" />s</div>
								 <div class="card-body">
									<div class="calendar">
										<div class="month"><?=date('M',strtotime($beritas->date))?></div>
										<div class="date"><?=date('d',strtotime($beritas->date))?></div>
									</div>
									<a href="<?=base_url('publik/berita/'.encode_url($beritas->id))?>"  class="hint--top hint--large">
										<h4><?=filter_output($beritas->judul)?></h4>
									</a>
									(<?=filter_output($beritas->tipe)?>)<br/>
									<div class="card-desk home">
									
									<?=substr(filter_output($beritas->isi),0,200)?>
									</div>
								</div>
							</div>
						</div>
				<?php endforeach; ?>
						
					</div>
			</div>
		</section>

    

		<?php include(__DIR__ . "/template/footer.php"); ?>



    </body>
</html>
