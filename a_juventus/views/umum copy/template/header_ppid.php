<body class="home4 header4">


<div id="wrapper">
<header id="header" class="site-header">
	<div class="top-header">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<nav class="top-nav">
                  <div class="top-title-nav"><a href="https://www.prakerja.go.id/" target="_blank">Daftar Kartu Prakerja</a></div>
                  <div class="top-title-nav"><a href="https://www.youtube.com/watch?v=s0WKWL0dH-I" target="_blank">Tutorial Daftar Kartu Prakerja</a></div>
					</nav> 
				</div>

				<div class="col-md-6">
					<div class="top-right">
						<div class="header-info">
							<ul class="header-social">
								<li><a href="https://www.facebook.com/nakertrans.jateng" title="facebook" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="https://twitter.com/disnaker_jateng" title="Twitter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="https://www.instagram.com/nakertrans.provjateng/" title="Instagram" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
								<li><a href="https://www.youtube.com/channel/UCWCuB6H5TwwAV64PCOdobtA" title="Youtube" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
							</ul>
						</div><!-- .header-info -->
					</div><!-- .top-right -->
				</div>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- .top-header -->   <div class="mid-header">
      <div class="container">
         <nav class="main-menu">
            <span class="mobile-btn"><i class="fa fa-bars"></i></span>
            <ul>
               

               <li class=""><a href="#">Home</a>
                  <ul class="sub-menu">     
                              <li><a href="<?=base_url('ppid')?>">Home PPID</a></li>
                              <li><a href="<?=base_url()?>">Home Web Utama</a></li>     
                  </ul>
               </li>

               <li class=""><a href="#">Profil</a>
                  <ul class="sub-menu">
                  <?php $a = $this->M_portalsz->get_profil_ppid();
                                 foreach($a AS $b):
                              ?>
                                 <li><a href="<?=base_url()?>ppid/profil/<?=encode_url($b->x)?>"><?=$b->judul?></a></li>
                              <?php endforeach; ?>

                              <?php $c = $this->M_portalsz->get_struktur_ppid();
                                 foreach($c AS $d):?>
                                 <li><a href="<?=base_url()?>ppid/kategori/struktur/<?=encode_url($d->x)?>"><?=$d->judul?></a></li>
                              <?php endforeach; ?>
                              
                              <li><a href="<?=base_url()?>ppid/pejabat">Profil Pejabat Struktural</a></li>
                              <li><a href="<?=base_url()?>ppid/kategori/maklumat">Maklumat Pelayanan</a></li>
                              <li><a href="<?=base_url()?>ppid/kategori/lhkpn">LHKPN</a></li>
                              <li><a href="<?=base_url()?>ppid/kategori/lhkasn">LHKASN</a></li>
                  </ul>
               </li>

               <li class=""><a href="#">Informasi Publik</a>
                  <ul class="sub-menu">

                  <?php $c = $this->M_portalsz->get_informasi();
                  foreach($c AS $d):?>
                  <li><a href="<?=base_url()?>ppid/category/informasi/<?=encode_url($d->id)?>"><?=$d->nama?></a></li>
                  <?php endforeach; ?>

                     
                  
                  </ul>
               </li>

               

               <li class=""><a href="#">Laporan</a>
                  <ul class="sub-menu">

                  <?php $e = $this->M_portalsz->get_laporan();
                                 foreach($e AS $f):?>
                                 <li><a href="<?=base_url()?>ppid/category/laporan/<?=encode_url($f->id)?>"><?=$f->nama?></a></li>
                              <?php endforeach; ?>
                  
                              
                             
                  </ul>
               </li>

               <li class=""><a href="#">Program&Kegiatan</a>
                  <ul class="sub-menu">
                  <?php $g = $this->M_portalsz->get_program();
                                 foreach($g AS $h):?>
                                 <li><a href="<?=base_url()?>ppid/category/program/<?=encode_url($h->id)?>"><?=$h->nama?></a></li>
                              <?php endforeach; ?>  

                              <li><a href="<?=base_url()?>ppid/daftar/perencanaan">Dokumen Perencanaan</a></li>     

                               <li><a href="<?=base_url()?>ppid/daftar/pengadaan">Pengadaan Barang & Jasa</a></li>      
                  </ul>
               </li>

               <li class=""><a href="#">Keuangan</a>
                  <ul class="sub-menu">
                  <?php $i = $this->M_portalsz->get_anggaran();
                                 foreach($i AS $j):?>
                                 <li><a href="<?=base_url()?>ppid/category/anggaran/<?=encode_url($j->id)?>"><?=$j->nama?></a></li>
                  <?php endforeach; ?>

                             
                  </ul>
               </li>


               <li class=""><a href="#">SOP</a>
                  <ul class="sub-menu">
                  <?php $k = $this->M_portalsz->get_sop();
                                 foreach($k AS $l):?>
                                 <li><a href="<?=base_url()?>ppid/category/sop/<?=encode_url($l->id)?>"><?=$l->nama?></a></li>
                              <?php endforeach; ?>  

                                
                  </ul>
               </li>
               

               <li class=""><a href="#">Formulir</a>
                  <ul class="sub-menu">
                  <?php $m = $this->M_portalsz->get_form();
                  foreach($m AS $n):
                  ?>
                  <li><a href="<?=base_url()?>ppid/form/<?=encode_url($n->x)?>"><?=$n->judul?></a></li>
               <?php endforeach; ?>
                  </ul>
               </li>

               

              


				</ul>
         </nav>

          <div class="site-brand">
            <a class="logo" href="index.html">
               <!-- <img src="<?=base_url('assets/theme/img/logo4.png')?>" alt="Dinas Perhubungan Provinsi Jawa Tengah" /> -->
                <img src="<?=base_url('assets/jateng.png')?>" width="100" alt="Dinas Perhubungan Provinsi Jawa Tengah" />
            </a>
         </div> 
      </div>
   </div>
</header>