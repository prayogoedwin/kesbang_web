<!-- library JS fancybox -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fancybox/jquery.fancybox.css">
<script src="<?=base_url()?>assets/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript">
    $("[data-fancybox]").fancybox({ });
</script>

<div id="boxes">
   <div style="top: 50%; left: 50%; display: none;" id="dialog" class="window">
      <div id="san">
          <img src="assets/upload/header/disnakertrans.png" width="450">
      </div>
   </div>
   <div style="width: 2478px; font-size: 32pt; color:white; height: 1202px; display: none; opacity: 0.4;" id="mask"></div>
</div>


   <br/>
<br/>
   <div class="page-header full-content" style="background-color:#D3D3D3">
      <div class="container">
         <div class="row">
            <div class="col-sm-6">

               <h1><?=str_replace('_',' ',strtoupper($this->uri->segment('2')))?></h1>
            
              
               
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb">
                  <li><a href="<?=base_url()?>"><i class="fa fa-home"></i></a></li>
                 
                  <li class="active"><?=str_replace('_',' ',strtoupper($this->uri->segment('2')))?></li>
                  
               </ol>
            </div> 
         </div>
      </div>
   </div>

<section id="home-video" style="margin-top:-40px">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="section-header11">
							<h2><span>Galeri Foto <?=get_nama_galeri(decode_url($this->uri->segment('3')))?></span></h2>
						</div>
						<div class="row"> 

						<?php foreach($allgaleri AS $galeris): ?>
							<div class="col-md-3">
								<div class="show-video">
                                <a href="<?=base_url(filter_output($galeris->deskripsi))?>" data-fancybox="group" data-caption="<?=filter_output($galeris->judul)?>" >
									<img class="videoThumb bg-shadow" src="<?=base_url(filter_output($galeris->deskripsi))?>">

                                    <p><?=filter_output($galeris->judul)?> <br/>
                                    <?=date('d-m-Y', strtotime($galeris->created_at))?></p>
									</a>
								</div>
	                     	</div>
						<?php endforeach; ?>

	                	</div>

	               <div class="col-md-12 lagi">
	               	<!-- <div class="row">
	               		<span class="more"><a href="<?=base_url('publik/galeri')?>" aria-label="Lihat Semua Galeri" class="hint--top hint--large">Semua Galeri</a></span>
	               	</div> -->
	               </div>
					</div>
					

						
		               	
		               	
		               	
		            </div>
		            <!-- <div id="pencet">
		            	<a href="#"><button class="tombol tombol-lg lebar blue">Semua UMK</button></a>
		         	</div> -->
					</div>
				</div>
			</div>
		</section>



   <?php include(__DIR__ . "/template/footer.php"); ?>



</body>
</html>