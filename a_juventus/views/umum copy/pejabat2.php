

<body class="home4 header4">

	
	
	
<div id="wrapper">



<br/>
<br/>
<div id="content" class="site-content grey">
   <div class="page-header full-content">
      <div class="container">
         <div class="row">
            <div class="col-sm-6">
               <h1>Profil Pejabat Struktural <small></small></h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb">
                  <li><a href="http://www.perhubungan.jatengprov.go.id/"><i class="fa fa-home"></i></a></li>
                  <li><a href="#" class="active">Profil Pejabat Struktural</a></li>
               </ol>
            </div> 
         </div>
      </div>
   </div>

   <div class="container">
      <div class="row">
         <main id="main" class="site-main col-md-12">
            <div class="VerticalTab fc_VerticalTab VerticalTab_2 tabs_ver_2">                 
               <ul class="resp-tabs-list hor_1 bg-shadow">
                                    <li class="tabs-1"><span class="tabs-text">KEPALA DINAS</span></li>   
                                    <li class="tabs-2"><span class="tabs-text">SEKRETARIAT</span></li>   
                                    <li class="tabs-3"><span class="tabs-text">BIDANG PELATIHAN KERJA DAN PRODUKTIVITAS</span></li>   
                                    <li class="tabs-4"><span class="tabs-text">BIDANG PENEMPATAN TENAGA KERJA DAN TRANSMIGRASI</span></li>   
                                    <li class="tabs-5"><span class="tabs-text">BIDANG HUBUNGAN INDUSTRIAL DAN JAMINAN SOSIAL</span></li>   
                                    <li class="tabs-6"><span class="tabs-text">BIDANG PENGAWASAN KETENAGAKERJAAN</span></li>   
                                    <li class="tabs-7"><span class="tabs-text">BALAI PELATIHAN KERJA TRANSMIGRASI</span></li>   
                                    <li class="tabs-8"><span class="tabs-text">BALAI PELAYANAN PENYELESAIAN PERSELISIHAN TENAGA KERJA</span></li>   
                                    <li class="tabs-9"><span class="tabs-text">BALAI KESELAMATAN KERJA</span></li>   
                                    <li class="tabs-10"><span class="tabs-text">BALAI LATIHAN KERJA SEMARANG 1</span></li>   
                                    <li class="tabs-11"><span class="tabs-text">BALAI LATIHAN KERJA SEMARANG 2</span></li>   
                                    <li class="tabs-12"><span class="tabs-text">BALAI LATIHAN KERJA CILACAP</span></li>
                                 </ul>
         
               <div class="resp-tabs-container hor_1">
                      <div class="fc-tab-1">   

                     <?php foreach($tab1 as $t1): ?>
                        <div class="profil-card">
                           <div class="col-sm-3">
                              <div class="row">
                                   <div class="photo"><img class="img-circle" src="<?=base_url($t1->deskripsi)?>"></div>   
                              </div>
                           </div>
                           <div class="col-md-9 border">
                              <div class="row">
                                 <div class="description">
                                    <div class="title-description">
                                       <h1><?=filter_output($t1->nama)?></h1>
                                       <h2><?=filter_output($t1->jabatan)?></h2>
                                    </div>
                                    <div class="body-description">
                                       <div class="row">
                                          <div class="col-sm-3">NIP</div><div class="col-sm-9">: <?=filter_output($t1->nip)?></div>
                                          <div class="col-sm-3">Pangkat / Golongan</div><div class="col-sm-9">: <?=filter_output($t1->pangkat_golongan)?></div>
                                          <div class="col-sm-3">Pendidikan Terakhir</div><div class="col-sm-9">: <?=filter_output($t1->pendidikan_akhir)?></div>
                                          <div class="col-sm-3">Penghargaan </div><div class="col-sm-9">: <?=filter_output($t1->diklat_akhir)?></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php endforeach; ?>

                     </div>
                     <div class="fc-tab-2">
                                                                       
                     <?php foreach($tab2 as $t2): ?>
                        <div class="profil-card">
                           <div class="col-sm-3">
                              <div class="row">
                                   <div class="photo"><img class="img-circle" src="<?=base_url($t2->deskripsi)?>"></div>   
                              </div>
                           </div>
                           <div class="col-md-9 border">
                              <div class="row">
                                 <div class="description">
                                    <div class="title-description">
                                       <h1><?=filter_output($t2->nama)?></h1>
                                       <h2><?=filter_output($t2->jabatan)?></h2>
                                    </div>
                                    <div class="body-description">
                                       <div class="row">
                                          <div class="col-sm-3">NIP</div><div class="col-sm-9">: <?=filter_output($t2->nip)?></div>
                                          <div class="col-sm-3">Pangkat / Golongan</div><div class="col-sm-9">: <?=filter_output($t2->pangkat_golongan)?></div>
                                          <div class="col-sm-3">Pendidikan Terakhir</div><div class="col-sm-9">: <?=filter_output($t2->pendidikan_akhir)?></div>
                                          <div class="col-sm-3">Penghargaan </div><div class="col-sm-9">: <?=filter_output($t2->diklat_akhir)?></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php endforeach; ?>

                        </div>
                        <div class="fc-tab-3">
                                                                       
                        <?php foreach($tab3 as $t3): ?>
                        <div class="profil-card">
                           <div class="col-sm-3">
                              <div class="row">
                                   <div class="photo"><img class="img-circle" src="<?=base_url($t3->deskripsi)?>"></div>   
                              </div>
                           </div>
                           <div class="col-md-9 border">
                              <div class="row">
                                 <div class="description">
                                    <div class="title-description">
                                       <h1><?=filter_output($t3->nama)?></h1>
                                       <h2><?=filter_output($t3->jabatan)?></h2>
                                    </div>
                                    <div class="body-description">
                                       <div class="row">
                                          <div class="col-sm-3">NIP</div><div class="col-sm-9">: <?=filter_output($t3->nip)?></div>
                                          <div class="col-sm-3">Pangkat / Golongan</div><div class="col-sm-9">: <?=filter_output($t3->pangkat_golongan)?></div>
                                          <div class="col-sm-3">Pendidikan Terakhir</div><div class="col-sm-9">: <?=filter_output($t3->pendidikan_akhir)?></div>
                                          <div class="col-sm-3">Penghargaan </div><div class="col-sm-9">: <?=filter_output($t3->diklat_akhir)?></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php endforeach; ?>

                                               
                        
                     </div>
                     <div class="fc-tab-4">
                                                                       
                     <?php foreach($tab4 as $t4): ?>
                        <div class="profil-card">
                           <div class="col-sm-3">
                              <div class="row">
                                   <div class="photo"><img class="img-circle" src="<?=base_url($t4->deskripsi)?>"></div>   
                              </div>
                           </div>
                           <div class="col-md-9 border">
                              <div class="row">
                                 <div class="description">
                                    <div class="title-description">
                                       <h1><?=filter_output($t4->nama)?></h1>
                                       <h2><?=filter_output($t4->jabatan)?></h2>
                                    </div>
                                    <div class="body-description">
                                       <div class="row">
                                          <div class="col-sm-3">NIP</div><div class="col-sm-9">: <?=filter_output($t4->nip)?></div>
                                          <div class="col-sm-3">Pangkat / Golongan</div><div class="col-sm-9">: <?=filter_output($t4->pangkat_golongan)?></div>
                                          <div class="col-sm-3">Pendidikan Terakhir</div><div class="col-sm-9">: <?=filter_output($t4->pendidikan_akhir)?></div>
                                          <div class="col-sm-3">Penghargaan </div><div class="col-sm-9">: <?=filter_output($t4->diklat_akhir)?></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php endforeach; ?>

                                               
                     </div>
                     <div class="fc-tab-5">
                                                                       
                     <?php foreach($tab5 as $t5): ?>
                        <div class="profil-card">
                           <div class="col-sm-3">
                              <div class="row">
                                   <div class="photo"><img class="img-circle" src="<?=base_url($t5->deskripsi)?>"></div>   
                              </div>
                           </div>
                           <div class="col-md-9 border">
                              <div class="row">
                                 <div class="description">
                                    <div class="title-description">
                                       <h1><?=filter_output($t5->nama)?></h1>
                                       <h2><?=filter_output($t5->jabatan)?></h2>
                                    </div>
                                    <div class="body-description">
                                       <div class="row">
                                          <div class="col-sm-3">NIP</div><div class="col-sm-9">: <?=filter_output($t5->nip)?></div>
                                          <div class="col-sm-3">Pangkat / Golongan</div><div class="col-sm-9">: <?=filter_output($t5->pangkat_golongan)?></div>
                                          <div class="col-sm-3">Pendidikan Terakhir</div><div class="col-sm-9">: <?=filter_output($t5->pendidikan_akhir)?></div>
                                          <div class="col-sm-3">Penghargaan </div><div class="col-sm-9">: <?=filter_output($t5->diklat_akhir)?></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php endforeach; ?>

                     </div>
                     <div class="fc-tab-6">
                                                                       
                     <?php foreach($tab6 as $t6): ?>
                        <div class="profil-card">
                           <div class="col-sm-3">
                              <div class="row">
                                   <div class="photo"><img class="img-circle" src="<?=base_url($t6->deskripsi)?>"></div>   
                              </div>
                           </div>
                           <div class="col-md-9 border">
                              <div class="row">
                                 <div class="description">
                                    <div class="title-description">
                                       <h1><?=filter_output($t6->nama)?></h1>
                                       <h2><?=filter_output($t6->jabatan)?></h2>
                                    </div>
                                    <div class="body-description">
                                       <div class="row">
                                          <div class="col-sm-3">NIP</div><div class="col-sm-9">: <?=filter_output($t6->nip)?></div>
                                          <div class="col-sm-3">Pangkat / Golongan</div><div class="col-sm-9">: <?=filter_output($t6->pangkat_golongan)?></div>
                                          <div class="col-sm-3">Pendidikan Terakhir</div><div class="col-sm-9">: <?=filter_output($t6->pendidikan_akhir)?></div>
                                          <div class="col-sm-3">Penghargaan </div><div class="col-sm-9">: <?=filter_output($t6->diklat_akhir)?></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php endforeach; ?>


                     </div>
                     <div class="fc-tab-7">
                                                                       
                     <?php foreach($tab7 as $t7): ?>
                        <div class="profil-card">
                           <div class="col-sm-3">
                              <div class="row">
                                   <div class="photo"><img class="img-circle" src="<?=base_url($t7->deskripsi)?>"></div>   
                              </div>
                           </div>
                           <div class="col-md-9 border">
                              <div class="row">
                                 <div class="description">
                                    <div class="title-description">
                                       <h1><?=filter_output($t7->nama)?></h1>
                                       <h2><?=filter_output($t7->jabatan)?></h2>
                                    </div>
                                    <div class="body-description">
                                       <div class="row">
                                          <div class="col-sm-3">NIP</div><div class="col-sm-9">: <?=filter_output($t7->nip)?></div>
                                          <div class="col-sm-3">Pangkat / Golongan</div><div class="col-sm-9">: <?=filter_output($t7->pangkat_golongan)?></div>
                                          <div class="col-sm-3">Pendidikan Terakhir</div><div class="col-sm-9">: <?=filter_output($t7->pendidikan_akhir)?></div>
                                          <div class="col-sm-3">Penghargaan </div><div class="col-sm-9">: <?=filter_output($t7->diklat_akhir)?></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php endforeach; ?>

                                               
                        
                     </div>
                     <div class="fc-tab-8">
                                                                       
                     <?php foreach($tab8 as $t8): ?>
                        <div class="profil-card">
                           <div class="col-sm-3">
                              <div class="row">
                                   <div class="photo"><img class="img-circle" src="<?=base_url($t8->deskripsi)?>"></div>   
                              </div>
                           </div>
                           <div class="col-md-9 border">
                              <div class="row">
                                 <div class="description">
                                    <div class="title-description">
                                       <h1><?=filter_output($t8->nama)?></h1>
                                       <h2><?=filter_output($t8->jabatan)?></h2>
                                    </div>
                                    <div class="body-description">
                                       <div class="row">
                                          <div class="col-sm-3">NIP</div><div class="col-sm-9">: <?=filter_output($t8->nip)?></div>
                                          <div class="col-sm-3">Pangkat / Golongan</div><div class="col-sm-9">: <?=filter_output($t8->pangkat_golongan)?></div>
                                          <div class="col-sm-3">Pendidikan Terakhir</div><div class="col-sm-9">: <?=filter_output($t8->pendidikan_akhir)?></div>
                                          <div class="col-sm-3">Penghargaan </div><div class="col-sm-9">: <?=filter_output($t8->diklat_akhir)?></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php endforeach; ?>

                     
                        </div>
                        <div class="fc-tab-9">
                                                                       
                        <?php foreach($tab9 as $t9): ?>
                        <div class="profil-card">
                           <div class="col-sm-3">
                              <div class="row">
                                   <div class="photo"><img class="img-circle" src="<?=base_url($t9->deskripsi)?>"></div>   
                              </div>
                           </div>
                           <div class="col-md-9 border">
                              <div class="row">
                                 <div class="description">
                                    <div class="title-description">
                                       <h1><?=filter_output($t9->nama)?></h1>
                                       <h2><?=filter_output($t9->jabatan)?></h2>
                                    </div>
                                    <div class="body-description">
                                       <div class="row">
                                          <div class="col-sm-3">NIP</div><div class="col-sm-9">: <?=filter_output($t9->nip)?></div>
                                          <div class="col-sm-3">Pangkat / Golongan</div><div class="col-sm-9">: <?=filter_output($t9->pangkat_golongan)?></div>
                                          <div class="col-sm-3">Pendidikan Terakhir</div><div class="col-sm-9">: <?=filter_output($t9->pendidikan_akhir)?></div>
                                          <div class="col-sm-3">Penghargaan </div><div class="col-sm-9">: <?=filter_output($t9->diklat_akhir)?></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php endforeach; ?>

                                               
                        

                        </div>
                        <div class="fc-tab-10">
                                                                       
                        <?php foreach($tab10 as $t10): ?>
                        <div class="profil-card">
                           <div class="col-sm-3">
                              <div class="row">
                                   <div class="photo"><img class="img-circle" src="<?=base_url($t1->deskripsi)?>"></div>   
                              </div>
                           </div>
                           <div class="col-md-9 border">
                              <div class="row">
                                 <div class="description">
                                    <div class="title-description">
                                       <h1><?=filter_output($t10->nama)?></h1>
                                       <h2><?=filter_output($t10->jabatan)?></h2>
                                    </div>
                                    <div class="body-description">
                                       <div class="row">
                                          <div class="col-sm-3">NIP</div><div class="col-sm-9">: <?=filter_output($t10->nip)?></div>
                                          <div class="col-sm-3">Pangkat / Golongan</div><div class="col-sm-9">: <?=filter_output($t10->pangkat_golongan)?></div>
                                          <div class="col-sm-3">Pendidikan Terakhir</div><div class="col-sm-9">: <?=filter_output($t10->pendidikan_akhir)?></div>
                                          <div class="col-sm-3">Penghargaan </div><div class="col-sm-9">: <?=filter_output($t10->diklat_akhir)?></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php endforeach; ?>

                                               
                        

                        </div>
                        <div class="fc-tab-11">
                                                                       
                        <?php foreach($tab11 as $t11): ?>
                        <div class="profil-card">
                           <div class="col-sm-3">
                              <div class="row">
                                   <div class="photo"><img class="img-circle" src="<?=base_url($t11->deskripsi)?>"></div>   
                              </div>
                           </div>
                           <div class="col-md-9 border">
                              <div class="row">
                                 <div class="description">
                                    <div class="title-description">
                                       <h1><?=filter_output($t11->nama)?></h1>
                                       <h2><?=filter_output($t11->jabatan)?></h2>
                                    </div>
                                    <div class="body-description">
                                       <div class="row">
                                          <div class="col-sm-3">NIP</div><div class="col-sm-9">: <?=filter_output($t11->nip)?></div>
                                          <div class="col-sm-3">Pangkat / Golongan</div><div class="col-sm-9">: <?=filter_output($t11->pangkat_golongan)?></div>
                                          <div class="col-sm-3">Pendidikan Terakhir</div><div class="col-sm-9">: <?=filter_output($t11->pendidikan_akhir)?></div>
                                          <div class="col-sm-3">Penghargaan </div><div class="col-sm-9">: <?=filter_output($t11->diklat_akhir)?></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php endforeach; ?>
                                               
                        

                        </div>
                        <div class="fc-tab-12">
                                                                       
                        <?php foreach($tab12 as $t12): ?>
                        <div class="profil-card">
                           <div class="col-sm-3">
                              <div class="row">
                                   <div class="photo"><img class="img-circle" src="<?=base_url($t12->deskripsi)?>"></div>   
                              </div>
                           </div>
                           <div class="col-md-9 border">
                              <div class="row">
                                 <div class="description">
                                    <div class="title-description">
                                       <h1><?=filter_output($t12->nama)?></h1>
                                       <h2><?=filter_output($t12->jabatan)?></h2>
                                    </div>
                                    <div class="body-description">
                                       <div class="row">
                                          <div class="col-sm-3">NIP</div><div class="col-sm-9">: <?=filter_output($t12->nip)?></div>
                                          <div class="col-sm-3">Pangkat / Golongan</div><div class="col-sm-9">: <?=filter_output($t12->pangkat_golongan)?></div>
                                          <div class="col-sm-3">Pendidikan Terakhir</div><div class="col-sm-9">: <?=filter_output($t12->pendidikan_akhir)?></div>
                                          <div class="col-sm-3">Penghargaan </div><div class="col-sm-9">: <?=filter_output($t12->diklat_akhir)?></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php endforeach; ?>
                                               
                        
                        </div>
                        <div class="fc-tab-13">
                                                                       
                        <?php foreach($tab13 as $t13): ?>
                        <div class="profil-card">
                           <div class="col-sm-3">
                              <div class="row">
                                   <div class="photo"><img class="img-circle" src="<?=base_url($t13->deskripsi)?>"></div>   
                              </div>
                           </div>
                           <div class="col-md-9 border">
                              <div class="row">
                                 <div class="description">
                                    <div class="title-description">
                                       <h1><?=filter_output($t13->nama)?></h1>
                                       <h2><?=filter_output($t13->jabatan)?></h2>
                                    </div>
                                    <div class="body-description">
                                       <div class="row">
                                          <div class="col-sm-3">NIP</div><div class="col-sm-9">: <?=filter_output($t13->nip)?></div>
                                          <div class="col-sm-3">Pangkat / Golongan</div><div class="col-sm-9">: <?=filter_output($t13->pangkat_golongan)?></div>
                                          <div class="col-sm-3">Pendidikan Terakhir</div><div class="col-sm-9">: <?=filter_output($t13->pendidikan_akhir)?></div>
                                          <div class="col-sm-3">Penghargaan </div><div class="col-sm-9">: <?=filter_output($t13->diklat_akhir)?></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php endforeach; ?>

                        </div>
                     </div>
            </div>
         </main>
      </div>
   </div>
</div>


	<?php include(__DIR__ . "/template/footer.php"); ?>



    </body>
</html>
