<div id="boxes">
   <div style="top: 50%; left: 50%; display: none;" id="dialog" class="window">
      <div id="san">
          <img src="assets/upload/header/disnakertrans.png" width="450">
      </div>
   </div>
   <div style="width: 2478px; font-size: 32pt; color:white; height: 1202px; display: none; opacity: 0.4;" id="mask"></div>
</div>

<div id="content" class="site-content grey">
	<main id="main" class="site-main">
					<section id="slide">
							<?php foreach($allslide AS $slides): ?>
							<div class="item">
							<!-- dimensi gambar 1280 × 505 -->
								<img src="<?=base_url($slides->deskripsi)?>" alt="disnakertrans" />
							</div>
							<?php endforeach; ?>
						
						
					</section>

		<section id="home-professor">
			<div class="container">
				<div class="section-header11">
					<h2><span>Berita Terbaru</span></h2>
				</div>
				<div id="kegiatan">
				<?php foreach($allberita AS $beritas): ?>
					
					 <div class="item">
							<div class="card card-event">
							   <div class="card-heading"><img src="<?=base_url(filter_output($beritas->deskripsi))?>" alt="" />s</div>
								 <div class="card-body">
									<div class="calendar">
										<div class="month"><?=date('M',strtotime($beritas->created_at))?></div>
										<div class="date"><?=date('d',strtotime($beritas->created_at))?></div>
									</div>
									<a href="<?=base_url('publik/berita/'.encode_url($beritas->id))?>"  class="hint--top hint--large">
										<h4><?=filter_output($beritas->judul)?></h4>
									</a>
									<div class="card-desk home">
									<?=substr(filter_output($beritas->isi),0,200)?>
									</div>
								</div>
							</div>
						</div>
				<?php endforeach; ?>
						
					</div>
			</div>
		</section>

		<section id="home-video">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="section-header11">
							<h2><span>Galeri DISNAKERTRANS</span></h2>
						</div>
						<div class="row"> 

						<?php foreach($allgaleri AS $galeris): ?>
							<div class="col-md-4">
								<div class="show-video">
									<a class="fancybox fancybox" href="<?=base_url('publik/foto/'.encode_url($galeris->id))?>">
									<img class="videoThumb bg-shadow" src="<?=base_url(get_by_id_galeri_foto($galeris->id))?>">
									</a>
								</div>
	                     	</div>
						<?php endforeach; ?>

	                	</div>

	               <div class="col-md-12 lagi">
	               	<div class="row">
	               		<span class="more"><a href="<?=base_url('publik/galeri')?>" aria-label="Lihat Semua Galeri" class="hint--top hint--large">Semua Galeri</a></span>
	               	</div>
	               </div>
					</div>
					<div class="col-md-4">
						<div class="section-header11">
						<h2><span>Galeri Video</span></h2>
						</div>
						<div class="widget-artikel-list">
						<?php foreach($allvideo AS $videos): ?>
							<div class="col-md-12">
								<div class="show-video">
								<iframe src="https://www.youtube.com/embed/<?=filter_output($videos->deskripsi)?>" width="350" height="200" frameborder="0" allowfullscreen></iframe>
								</div>
	                     	</div>
						<?php endforeach; ?>

						<br/>
						<br/>
						<br/>

						<div class="section-header11">
						<h2><span>UMK Jawa Tengah</span></h2>
						</div>
						<div class="widget-artikel-list">
						<?php foreach($allumk AS $umks): ?>
							<div class="item bg-shadow">
								<div class="item-content">
									<h4><a href="<?=base_url('publik/regulasi/'.encode_url($umks->id))?>"><?=filter_output($umks->judul)?></a></h4>
								</div>
							</div>
						<?php endforeach; ?>

						
		               	
		               	
		               	
		            </div>
		            <!-- <div id="pencet">
		            	<a href="#"><button class="tombol tombol-lg lebar blue">Semua UMK</button></a>
		         	</div> -->
					</div>
				</div>
			</div>
		</section>

		<section id="home-banner">
			<div class="container">
				<div id="banner">
				<?php foreach($alllink AS $links): ?>
				<div class="item"><a href="<?=$links->isi?>" target="_blank"><img src="<?=base_url($links->deskripsi)?>" alt="<?=$links->judul?>"></a></div>
				<?php endforeach; ?>
					
						<!-- <div class="item"><a href="http://grms.jatengprov.go.id" target="_blank"><img src="http://www.perhubungan.jatengprov.go.id/assets/upload/image/grmsjateng.jpg" alt="GRMS"></a></div> -->
				</div>
			</div>
        </section>

        

		<?php include(__DIR__ . "/template/footer.php"); ?>



    </body>
</html>
