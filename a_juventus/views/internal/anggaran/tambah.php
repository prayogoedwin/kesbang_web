  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Tambah Data anggaran <?=get_id_anggaran_nama($this->uri->segment('3'));?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-8">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('kelolaanggaran/aksi_tambah');  ?>
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Judul</label>

                  <div class="col-sm-10">
                    <input type="text" required name="judul" class="form-control" id="inputEmail3" placeholder="Judul">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">File</label>

                  <div class="col-sm-10">
                    <input type="file" required name="filez" class="form-control" id="inputEmail3" placeholder="File">
                  </div>
                </div>

                

               
                

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jenis anggaran</label>

                  <div class="col-sm-10">
                  <select name="anggaran" class="form-control select2" style="width: 100%;">
                  <?php foreach($alldata AS $all): 
                   $sel = NULL;
                   if($all->url == $this->uri->segment('3')){
                     $sel = 'selected';
                   }else{
                     $sel ='';
                   }
                  
                  ?>
                  <option <?=$sel?> value="<?=$all->id?>"><?=$all->nama?></option>
                  <?php endforeach; ?>
                  
                </select>
                  </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Tambah</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>

                

  


  <?php include(__DIR__ . "/../template/footer.php"); ?>
  </body>
</html>