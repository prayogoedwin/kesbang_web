<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Edit Video</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-8">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open('kelolavideo/aksi_edit');  ?>
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Judul</label>
                  <div class="col-sm-10">
                    <input type="hidden" required name="id" class="form-control" id="inputEmail3" placeholder="Usermame" value="<?=$id?>">
                    <input type="text" required name="judul" class="form-control" id="inputEmail3" placeholder="Judul" value="<?=$judul?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">ID Video</label>

                  <div class="col-sm-10">
                   
                    <input type="text" required name="deskripsi" class="form-control" id="inputEmail3" placeholder="Judul" value="<?=$deskripsi?>">
                  </div>
                </div>

                
               
                <!-- <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Level</label>

                  <div class="col-sm-10">
                  <select class="form-control select2" style="width: 100%;">
                  <option>Pilih Level User</option>
                  <option>Super Admionistrator</option>
                  <option>Administrator</option>
                  <option>Updater</option>
                  
                </select>
                  </div>
                </div> -->
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Edit</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>


  <?php include(__DIR__ . "/../template/footer.php"); ?>
  </body>
</html>