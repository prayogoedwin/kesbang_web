<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url() ?>assets/jateng.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?=$this->session->userdata('username')?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN UTAMA</li>
      
        <li><a href="<?=base_url()?>kelolauser"><i class="fa fa-users"></i> <span>Kelola User</span></a></li>
        <li><a href="<?=base_url()?>kelolamedia"><i class="fa fa-file-image-o "></i> <span>Kelola Media</span></a></li>

        <li><a href="<?=base_url()?>kelolaslide"><i class="fa fa-picture-o"></i> <span>Kelola Slide</span></a></li>

        <li><a href="<?=base_url()?>kelolalinkz"><i class="fa fa-link"></i> <span>Layanan</span></a></li>

        <li><a href="<?=base_url()?>kelolaprofil"><i class="fa fa-book"></i> <span>Profil</span></a></li>
        <li><a href="<?=base_url()?>kelolastruktur"><i class="fa fa-book"></i> <span>Struktur Organisasi</span></a></li>

        <li class="treeview">
              <a href="<?=base_url()?>kelolaanggaran">
                <i class="fa fa-file"></i> <span>Anggaran</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
              <?php $b = $this->M_portalsz->get_anggaran();
              foreach($b AS $c):
              ?>
              <li><a href="<?=base_url()?>kelolaanggaran/index/<?=$c->url?>"><i class="fa fa-file"></i> <span><?=$c->nama?></span></a></li>
              <?php endforeach; ?>
              </ul>
            </li>

            <li class="treeview ">
              <a href="<?=base_url()?>kelolaprogram">
                <i class="fa fa-file"></i> <span>Program & Kegiatan</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu ">
              <?php $x = $this->M_portalsz->get_program();
              foreach($x AS $y):
              ?>
                 <li><a href="<?=base_url()?>kelolaprogram/index/<?=$y->url?>"><i class="fa fa-file"></i> <span><?=$y->nama?></span></a></li>
              <?php endforeach; ?>
              
              </ul>
            </li>

            <li class="treeview ">
              <a href="<?=base_url()?>kelolapengadaan">
                <i class="fa fa-file"></i> <span>Pengadaan</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu ">
              <?php $j = $this->M_portalsz->get_pengadaan();
              foreach($j AS $k):
              ?>
                 <li><a href="<?=base_url()?>kelolapengadaan/index/<?=$k->url?>"><i class="fa fa-file"></i> <span><?=$k->nama?></span></a></li>
              <?php endforeach; ?>
              
              </ul>
            </li>

            <li class="treeview ">
              <a href="<?=base_url()?>kelolaregulasi">
                <i class="fa fa-file-pdf-o"></i> <span>JDIH</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu ">
              <?php $x = $this->M_portalsz->get_regulasi();
              foreach($x AS $y):
              ?>
                 <li><a href="<?=base_url()?>kelolaregulasi/index/<?=$y->url?>"><i class="fa fa-file-pdf-o"></i> <span><?=$y->nama?></span></a></li>
              <?php endforeach; ?>

              </ul>
          </li>

          <li><a href="<?=base_url()?>keloladownload"><i class="fa fa-download"></i> <span>Download</span></a></li>
          <li><a href="<?=base_url()?>kelolacovid"><i class="fa fa-snowflake-o"></i> <span>Info Covid19</span></a></li>
        
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Profil</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <li><a href="<?=base_url()?>kelolaprofil"><i class="fa fa-book"></i> <span>Profil</span></a></li>
          <li><a href="<?=base_url()?>kelolastruktur"><i class="fa fa-book"></i> <span>Struktur Organisasi</span></a></li>
          <li><a href="<?=base_url()?>kelolaprofilpejabat"><i class="fa fa-book"></i> <span>Profil Pejabat</span></a></li>
          <li><a href="<?=base_url()?>keloladatabidang"><i class="fa fa-book"></i> <span>Data Bidang</span></a></li>
          <li><a href="<?=base_url()?>kelolauptd"><i class="fa fa-book"></i> <span>UPTD</span></a></li>
          </ul>
        </li> -->

       

        

        <li><a href="<?=base_url()?>kelolaberita"><i class="fa fa-globe"></i> <span>Berita&Pengumuman</span></a></li>
        

        <li class="treeview">
          <a href="#">
            <i class="fa fa-picture-o"></i> <span>Galeri</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <!-- <li><a href="<?=base_url()?>kelolafoto"><i class="fa fa-picture-o"></i> <span>Foto Kegiatan</span></a></li> -->
          <li><a href="<?=base_url()?>kelolagaleris"><i class="fa fa-picture-o"></i> <span>Galeri</span></a></li>
          <li><a href="<?=base_url()?>kelolainfografis"><i class="fa fa-picture-o"></i> <span>Infografis</span></a></li>
          <li><a href="<?=base_url()?>kelolavideo"><i class="fa fa-picture-o"></i> <span>Video </span></a></li>
          </ul>
        </li>

        <li><a href="<?=base_url()?>kelolakontak"><i class="fa fa-phone"></i> <span>Kelola Kontak</span></a></li>
      

        <li class="header">MENU PPID</li>

        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-file"></i> <span>PPID</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <li><a href="<?=base_url()?>kelolappid"><i class="fa fa-file"></i> <span>Profil PPID </span></a></li>
          <li><a href="<?=base_url()?>kelolamaklumat"><i class="fa fa-file"></i> <span>Maklumat Pelayanan </span></a></li>
          <li><a href="<?=base_url()?>kelolalhkpn"><i class="fa fa-file"></i> <span>LHKPN</span></a></li>
          <li><a href="<?=base_url()?>kelolalhkasn"><i class="fa fa-file"></i> <span>LHKASN</span></a></li>
          </ul>
        </li> -->

          <!-- <li class="treeview ">
              <a href="<?=base_url()?>kelolaprogram">
                <i class="fa fa-file"></i> <span>Program & Kegiatan</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu ">
              <?php $x = $this->M_portalsz->get_program();
              foreach($x AS $y):
              ?>
                 <li><a href="<?=base_url()?>kelolaprogram/index/<?=$y->url?>"><i class="fa fa-file"></i> <span><?=$y->nama?></span></a></li>
              <?php endforeach; ?>
              
              </ul>
            </li> -->

            <!-- <li class="treeview ">
              <a href="<?=base_url()?>kelolaperencanaan">
                <i class="fa fa-file"></i> <span>Dokumen Perencanan</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu ">
              <?php $z = $this->M_portalsz->get_perencanaan();
              foreach($z AS $a):
              ?>
                 <li><a href="<?=base_url()?>kelolaperencanaan/index/<?=$a->url?>"><i class="fa fa-file"></i> <span><?=$a->nama?></span></a></li>
              <?php endforeach; ?>
              
              </ul>
            </li> -->

            <!-- <li class="treeview ">
              <a href="<?=base_url()?>kelolapengadaan">
                <i class="fa fa-file"></i> <span>Pengadaan</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu ">
              <?php $j = $this->M_portalsz->get_pengadaan();
              foreach($j AS $k):
              ?>
                 <li><a href="<?=base_url()?>kelolapengadaan/index/<?=$k->url?>"><i class="fa fa-file"></i> <span><?=$k->nama?></span></a></li>
              <?php endforeach; ?>
              
              </ul>
            </li> -->

            
<!-- 
            <li class="treeview">
              <a href="<?=base_url()?>kelolaanggaran">
                <i class="fa fa-file"></i> <span>Anggaran</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
              <?php $b = $this->M_portalsz->get_anggaran();
              foreach($b AS $c):
              ?>
              <li><a href="<?=base_url()?>kelolaanggaran/index/<?=$c->url?>"><i class="fa fa-file"></i> <span><?=$c->nama?></span></a></li>
              <?php endforeach; ?>
              </ul>
            </li> -->

            <li class="treeview ">
              <a href="<?=base_url()?>kelolainformasi">
                <i class="fa fa-file"></i> <span>Informasi Publik</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu ">
              <?php $d = $this->M_portalsz->get_informasi();
              foreach($d AS $e):
              ?>
                 <li><a href="<?=base_url()?>kelolainformasi/index/<?=$e->url?>"><i class="fa fa-file"></i> <span><?=$e->nama?></span></a></li>
              <?php endforeach; ?>
              
              </ul>
            </li>

            <li class="treeview ">
              <a href="<?=base_url()?>kelolalaporan">
                <i class="fa fa-file"></i> <span>Laporan</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu ">
              <?php $f = $this->M_portalsz->get_laporan();
              foreach($f AS $g):
              ?>
                 <li><a href="<?=base_url()?>kelolalaporan/index/<?=$g->url?>"><i class="fa fa-file"></i> <span><?=$g->nama?></span></a></li>
              <?php endforeach; ?>
              
              </ul>
            </li>

            <li class="treeview" >
              <a href="<?=base_url()?>kelolalasop">
                <i class="fa fa-file"></i> <span>SOP</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu ">
              <?php $h = $this->M_portalsz->get_sop();
              foreach($h AS $i):
              ?>
                 <li><a href="<?=base_url()?>kelolasop/index/<?=$i->url?>"><i class="fa fa-file"></i> <span><?=$i->nama?></span></a></li>
              <?php endforeach; ?>
              
              </ul>
            </li>
            <li><a href="<?=base_url()?>kelolaformz"><i class="fa fa-edit"></i> <span>Kelola Form</span></a></li>

            

            <li><a href="<?=base_url()?>menu"><i class="fa fa-puzzle-piece"></i> <span>Custom Menu</span></a></li>
        

        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>