<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta name="description" content="Disnakertrans merupakan dinas yang memiliki tugas dan fungsi menyelenggarakan transportasi di Provinsi Jawa Tengah"/>
     <meta name="robots" content="index, follow">
     <meta name="author" content="Disnakertrans Provinsi Jawa Tengah">
     <meta name="keywords" content="disnaker,jateng, prov , jawa tengah ,disnakertrans,provinsi ">
     <meta name="author" content="Disnakertrans Provinsi Jawa Tengah">
     <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
     <meta http-equiv="Copyright" content="Disnakertrans Provinsi Jawa Tengah">
     <meta name="revisit-after" content="3">
     <meta name="webcrawlers" content="all">
     <meta name="rating" content="general">
     <meta name="spiders" content="all">
     <meta property="og:locale" content="id_ID"/>
     <meta property="og:type" content="website"/>
     <meta property="og:title" content="Disnakertrans Provinsi Jawa Tengah"/>
     <meta property="og:description" content="Disnakertrans"/>
     <meta property="og:site_name" content="Disnakertrans Provinsi Jawa Tengah"/>
     <meta property="og:image" content="<?=base_url()?>assets/upload/disnaker.jpg"/>
     <meta property="og:image:width" content="800"/>
     <meta property="og:image:height" content="500"/>
     <link rel="icon" href="<?=base_url()?>assets/theme/img/icon.png" type="image/png">

  <title>Halaman Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">


   <!-- Select2 -->
   <link rel="stylesheet" href="<?php echo base_url() ?>assets/adminlte/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/adminlte/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

     <!-- Kode menampilkan peringatan untuk mengaktifkan javascript-->
	<!-- <div align="center"><noscript>
   <div style="position:fixed; top:0px; left:0px; z-index:3000; height:100%; width:100%; background-color:#FFFFFF">
   <div style="font-family: Arial; font-size: 17px; background-color:#00bbf9; padding: 11pt;">Mohon aktifkan javascript pada browser untuk mengakses halaman ini!</div></div>
</noscript></div> -->
 
<!--Kode untuk mencegah seleksi teks, block teks dll.-->
<!-- <script type="text/javascript">
function disableSelection(e){if(typeof e.onselectstart!="undefined")e.onselectstart=function(){return false};else if(typeof e.style.MozUserSelect!="undefined")e.style.MozUserSelect="none";else e.onmousedown=function(){return false};e.style.cursor="default"}window.onload=function(){disableSelection(document.body)}
</script> -->
 
<!--Kode untuk mematikan fungsi klik kanan di blog-->
<!-- <script type="text/javascript">
function mousedwn(e){try{if(event.button==2||event.button==3)return false}catch(e){if(e.which==3)return false}}document.oncontextmenu=function(){return false};document.ondragstart=function(){return false};document.onmousedown=mousedwn
</script> -->
 
<!-- <style type="text/css">
* : (input, textarea) {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
 
}
</style>
<style type="text/css">
img {
     -webkit-touch-callout: none;
     -webkit-user-select: none;
    }
</style> -->
 
<!--Kode untuk mencegah shorcut keyboard, view source dll.-->
<!-- <script type="text/javascript">
window.addEventListener("keydown",function(e){if(e.ctrlKey&&(e.which==65||e.which==66||e.which==67||e.which==73||e.which==80||e.which==83||e.which==85||e.which==86)){e.preventDefault()}});document.keypress=function(e){if(e.ctrlKey&&(e.which==65||e.which==66||e.which==67||e.which==73||e.which==80||e.which==83||e.which==85||e.which==86)){}return false}
</script>
<script type="text/javascript">
document.onkeydown=function(e){e=e||window.event;if(e.keyCode==123||e.keyCode==18){return false}}
</script> -->

</head>