<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Edit Profil Pejabat</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-10">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('kelolaprofilpejabat/aksi_edit');  ?>
              <div class="box-body">
              

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>

                  <div class="col-sm-10">
                  <input type="hidden" required name="id" class="form-control" id="inputEmail3" value="<?=$id?>">
                    <input type="text" required name="nama" class="form-control" id="inputEmail3" value="<?=$nama?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Jabatan</label>

                  <div class="col-sm-10">
                    <input type="text" required name="jabatan" class="form-control" id="inputEmail3" value="<?=$jabatan?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">NIP</label>

                  <div class="col-sm-10">
                    <input type="text" required name="nip" class="form-control" id="inputEmail3" value="<?=$nip?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pkt/Gol</label>

                  <div class="col-sm-10">
                    <input type="text" required name="pangol" class="form-control" id="inputEmail3" value="<?=$pangol?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pend Akhir</label>

                  <div class="col-sm-10">
                    <input type="text" required name="pend" class="form-control" id="inputEmail3" value="<?=$pend?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Diklat Akhir</label>

                  <div class="col-sm-10">
                    <input type="text" required name="diklat" class="form-control" id="inputEmail3" value="<?=$diklat?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Gambar</label>

                  <div class="col-sm-10">
                    <input type="file"  name="filez" class="form-control" id="inputEmail3" placeholder="File">
                    <iframe src = "<?=base_url($deskripsi)?>" width='500' height='400' allowfullscreen></iframe>
                    
                  </div>
                </div>       


               
                

                <!-- <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Level</label>

                  <div class="col-sm-10">
                  <select class="form-control select2" style="width: 100%;">
                  <option>Pilih Level User</option>
                  <option>Super Admionistrator</option>
                  <option>Administrator</option>
                  <option>Updater</option>
                  
                </select>
                  </div>
                </div> -->
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Edit</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>

                

  


  <?php include(__DIR__ . "/../template/footer.php"); ?>
  </body>
</html>