<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Edit LHKPN</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('kelolalhkpn/aksi_edit');  ?>
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Judul</label>

                  <div class="col-sm-10">
                    <input type="hidden" required name="id" class="form-control" id="inputEmail3" placeholder="Usermame" value="<?=$id?>">
                    <input type="text" required name="judul" class="form-control" id="inputEmail3" placeholder="Judul" value="<?=$judul?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tahun</label>

                  <div class="col-sm-10">
                    <select name="tahun" id="" class="form-control">
                    <option><?=$tahun?></option>
                      <option>2014</option>
                      <option>2015</option>
                      <option>2016</option>
                      <option>2017</option>
                      <option>2018</option>
                      <option>2019</option>
                      <option>2020</option>
                      <option>2021</option>
                      <option>2022</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">File</label>

                  <div class="col-sm-10">
                    <input type="file"  name="filez" class="form-control" id="inputEmail3" placeholder="File">
                    <iframe src = "<?=base_url($deskripsi)?>" width='500' height='400' allowfullscreen></iframe>
                    
                  </div>
                </div>               
                <!-- <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Level</label>

                  <div class="col-sm-10">
                  <select class="form-control select2" style="width: 100%;">
                  <option>Pilih Level User</option>
                  <option>Super Admionistrator</option>
                  <option>Administrator</option>
                  <option>Updater</option>
                  
                </select>
                  </div>
                </div> -->
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Edit</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>


  <?php include(__DIR__ . "/../template/footer.php"); ?>
  </body>
</html>