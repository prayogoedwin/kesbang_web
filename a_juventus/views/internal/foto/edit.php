<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Edit Foto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-8">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open('kelolafoto/aksi_edit');  ?>
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Judul</label>
                  <div class="col-sm-10">
                    <input type="hidden" required name="id" class="form-control" id="inputEmail3" placeholder="Usermame" value="<?=$id?>">
                    <input type="text" required name="judul" class="form-control" id="inputEmail3" placeholder="Judul" value="<?=$judul?>">
                  </div>
                </div>

                <div class="form-group" hidden>
                  <label for="inputEmail3" class="col-sm-2 control-label">ID Video</label>

                  <div class="col-sm-10">
                   
                    <input type="text" required name="deskripsi" class="form-control" id="inputEmail3" placeholder="Judul" value="<?=$deskripsi?>">
                  </div>
                </div>

                
               
                <!-- <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Level</label>

                  <div class="col-sm-10">
                  <select class="form-control select2" style="width: 100%;">
                  <option>Pilih Level User</option>
                  <option>Super Admionistrator</option>
                  <option>Administrator</option>
                  <option>Updater</option>
                  
                </select>
                  </div>
                </div> -->
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Edit</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>


        <!-- ////////////// -->


        <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 col-xs-12">

        <div class="box">
        <div class="box-header">
        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">Tambah Data</button>
        </div>

            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Foto</th>
                  <th>Aksi</th>

                </tr>
                </thead>
                <tbody>
                <?php
                $no = 0;
                foreach($alldata as $all):
                $no++;
                ?>

                  <td><?=$no?></td>
                  <td><img height="100px" width="300px" src="<?=base_url($all->deskripsi)?>"></td>

                <td>
                 
                  <a href="<?=base_url()?>kelolafoto/hapus2/<?=encode_url($all->id)?>"><button onclick="return confirm('Anda ingin menghapus ?')" type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash" style="font-size:12px"></i></button></a></td>

                </tr>
                <?php endforeach;?>

              </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
    </div>

    
      

    </section>
    <!-- /.content -->
  </div>






  <?php include(__DIR__ . "/../template/footer.php"); ?>

  <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Foto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <?php echo form_open_multipart('kelolafoto/aksi_upload');  ?>
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Foto</label>
                  <div class="col-sm-10">
                    <input type="hidden" name="id" class="form-control" id="inputEmail3" placeholder="Usermame" value="<?=$id?>">
                    <input type="file" required name="filez" class="form-control" id="inputEmail3" placeholder="File" >
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">

      </div>
      <div class="modal-footer">
        
        <button type="submit" class="btn btn-primary">Upload</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
    $('#example2').DataTable()
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        
        buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            'pdfHtml5'
        ]
    } );
} );
</script>

  </body>
</html>