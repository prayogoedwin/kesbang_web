<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Kesbangpol Jateng</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link rel="icon" href="<?=base_url()?>assets/theme/img/icon.png" type="image/png">
  <link href="<?=base_url()?>assets/frontend/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?=base_url()?>assets/frontend/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/frontend/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/frontend/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/frontend/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/frontend/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/frontend/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/frontend/vendor/aos/aos.css" rel="stylesheet">

  <link rel="stylesheet" href="<?=base_url()?>frontend/vendor/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />

  <!-- Template Main CSS File -->
  <link href="<?=base_url()?>assets/frontend/css/style.css" rel="stylesheet">

</head>