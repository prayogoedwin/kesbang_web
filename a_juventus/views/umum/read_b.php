
    <!-- ======= About Us Section ======= -->
     <section id="about" class="about">
      <div class="container">

        <div class="row no-gutters">
          <!-- <div class="col-lg-2 video-box" style="background-image: url('assets/upload/slide/11_2020/fcfd162b2779ed1005ff213ee145bf1a.png);">
              <img src="<?=base_url()?>assets/upload/slide/11_2020/fcfd162b2779ed1005ff213ee145bf1a.png"> -->
            <!-- <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a> 
          </div> -->

          <div class="col-lg-12 d-flex flex-column justify-content-center about-content">

             <div class="section-title">
              <h2><?=$judul?></h2>
                        
            </div> 

             <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
             <img src="<?=base_url($deskripsi)?>" style="width:100%; display: block; margin: 0 auto;" >
              <!-- <div class="icon"><i class="bx bx-fingerprint"></i></div> -->
              <!-- <h4 class="title"><a href="">Komitmen Kebangsaan</a></h4> -->
              <p class="description"><?php 
               echo html_entity_decode($isi);
               ?></p>
            </div>

            <!-- <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">NASIONALISME DAN PATRIOTISME</a></h4>
              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">WAWASAN KEBANGSAAN</a></h4>
              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">BHINNEKA TUNGGAL IKA</a></h4>
              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div> -->

          </div>
        </div>

      </div>
    </section> 
    <!-- End About Us Section -->

   

    

 



           <!-- ======= Services Section ======= -->
           <section id="services" class="services">
      <div class="container">
 
        <div class="section-title">
          <h2>Sosial Media</h2>
        </div>  

        <div class="row">
        <?php $this->load->view('umum/template/sosmed')?>
        </div>

      </div>
    </section><!-- End Services Section -->

    <?php $this->load->view('umum/template/kontak')?>

  </main><!-- End #main -->

  <?php $this->load->view('umum/template/footer')?>

  
</body>

</html>