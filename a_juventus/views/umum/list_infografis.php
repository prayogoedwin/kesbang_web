
  <style>
   @media screen and (min-width: 0px) and (max-width: 700px) {
    .navWrap{
        display:none;
    }

    .gambar{
        display:show;
    }
    
}

@media screen and (min-width: 701px) {
    .navWrap{
        display:show;
    }

    .gambar{
        display:none;
    }
}

 </style> 

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
.mySlides {display:none;}
</style>



  <main id="main">

    <!-- ======= Our Portfolio Section ======= -->
    <section id="portfolio" class="portfolio section-bg">
      <div class="container" data-aos="fade-up" data-aos-delay="100">

        <div class="section-title">
          <h2>Galeri Infografis</h2>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

       

        <div class="row portfolio-container">
          

        <?php foreach($allgaallinfografisleri AS $galeris): ?>
          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="<?=base_url($galeris->deskripsi)?>" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><?=$galeris->judul?></h4>
                <!-- <p>App</p> -->
                <div class="portfolio-links">
                  <a href="<?=base_url($galeris->deskripsi)?>" data-gall="portfolioGallery" class="venobox" title="<?=$galeris->judul?>"><i class="icofont-eye"></i></a>
                  <!-- <a href="<?=base_url('publik/foto_galeri/'.encode_url($galeris->id))?>" title="More Details"><i class="icofont-external-link"></i></a> -->
                </div>
              </div>
            </div>
          </div>
          <?php endforeach; ?>


          

          

        </div>

        <br/>

        <!-- <div class="section-title">
          
          <a href="#" class=" button btn-get-started animate__animated animate__fadeInUp scrollto"><button class="btn btn-danger">Lihat Semua Galeri Kegiatan</button></a>
        </div>  -->

      </div>
    </section><!-- End Our Portfolio Section -->



     


<?php $this->load->view('umum/template/kontak')?>

  </main><!-- End #main -->

  <?php $this->load->view('umum/template/footer')?>

  
</body>

</html>