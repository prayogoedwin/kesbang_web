
  <style>
   @media screen and (min-width: 0px) and (max-width: 700px) {
    .navWrap{
        display:none;
    }

    .gambar{
        display:show;
    }
    
}

@media screen and (min-width: 701px) {
    .navWrap{
        display:show;
    }

    .gambar{
        display:none;
    }
}

 </style> 

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
.mySlides {display:none;}
</style>



  <main id="main">





    

    <!-- ======= About Lists Section ======= -->
    <section class="about-lists">
      <div class="container">

        <div class="section-title">
          <h2>Berita Terbaru Kesbangpol</h2>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.</p> -->
        </div> 

        <div class="row no-gutters">

          <style>
            .judul_berita{
              text-align: center;
            }
            .isi{
              text-align: justify;
            }
          </style>

          <?php foreach($allberitabawah AS $beritas): ?>

          <div class="col-lg-3 col-md-4 content-item" data-aos="fade-up">
            <div class="pic"><img src="<?=base_url(filter_output($beritas->deskripsi))?>" class="img-fluid" alt=""></div>
            <br/>
            <a href="<?=base_url('publik/berita/'.encode_url($beritas->id))?>"> <h5 class="judul_berita"><?=filter_output($beritas->judul)?></h5></a>
            <p class="isi"><?=substr(filter_output($beritas->isi),0,200)?></p>
          </div>

          <?php endforeach; ?>

          <!-- <div class="col-lg-3 col-md-4 content-item" data-aos="fade-up" data-aos-delay="100">
            <div class="pic"><img src="assets/img/team/team-1.jpg" class="img-fluid" alt=""></div>
            <h5 class="judul_berita">Lorem Ipsum</h5>
            <p class="isi">Dolorem est fugiat occaecati voluptate velit esse. Dicta veritatis dolor quod et vel dire leno para dest</p>
          </div>

          <div class="col-lg-3 col-md-4 content-item" data-aos="fade-up" data-aos-delay="200">
            <div class="pic"><img src="assets/img/team/team-1.jpg" class="img-fluid" alt=""></div>
            <h5 class="judul_berita">Lorem Ipsum</h5>
            <p class="isi">Molestiae officiis omnis illo asperiores. Aut doloribus vitae sunt debitis quo vel nam quis</p>
          </div>

          <div class="col-lg-3 col-md-4 content-item" data-aos="fade-up" data-aos-delay="300">
            <div class="pic"><img src="assets/img/team/team-1.jpg" class="img-fluid" alt=""></div>
            <h5 class="judul_berita" >Lorem Ipsum</h5>
            <p class="isi">Inventore quo sint a sint rerum. Distinctio blanditiis deserunt quod soluta quod nam mider lando casa</p>
          </div> -->

          

        <hr/>

        </div>
        <br/>

        <!-- <div class="section-title">
          
          <a href="#" class=" button btn-get-started animate__animated animate__fadeInUp scrollto"><button class="btn btn-danger">Lihat Semua Berita</button></a>
        </div>  -->

      </div>
    </section>
    <!-- End About Lists Section -->

    

 



<?php $this->load->view('umum/template/kontak')?>

  </main><!-- End #main -->

  <?php $this->load->view('umum/template/footer')?>

  
</body>

</html>