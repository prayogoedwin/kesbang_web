<style>
   @media screen and (min-width: 0px) and (max-width: 700px) {
    .navWrap{
        display:none;
    }

    .gambar{
        display:show;
    }
    
}

@media screen and (min-width: 701px) {
    .navWrap{
        display:show;
    }

    .gambar{
        display:none;
    }
}

 </style> 

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
.mySlides {display:none;}
</style>

 <!-- ======= Hero Section ======= -->
  <div class="w3-content gambar">
  <?php foreach($allslide AS $sld): ?>
  <img class="mySlides img-responsive" style="width:100%;" src="<?=$sld->deskripsi?>">
  <?php endforeach; ?>
 
  </div> 

  <script>
var slideIndex = 0;
carousel();

function carousel() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none"; 
  }
  slideIndex++;
  if (slideIndex > x.length) {slideIndex = 1} 
  x[slideIndex-1].style.display = "block"; 
  setTimeout(carousel, 2000); 
}
</script>

 <div class="row navWrap">
 <div class="col-md-12" style="padding:0;margin:0">
    <section id="hero"  >
      <div class="hero-container"  style=" width:100%" >
        <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel"  >

          <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

          <div class="carousel-inner" role="listbox" > 
              
                
                <?php foreach($allslide AS $slides): ?>
                  <div class="carousel-item <?=max_id_ppid($slides->id)?>" style="width:100%; background-image: url('<?=$slides->deskripsi?>');">
            
                  </div>
                <?php endforeach; ?>
                

          </div>

        

        </div>
      </div>
      
    </section><!-- End Hero -->

    

</div>


</div>
  <main id="main">




           <!-- ======= Services Section ======= -->
<section id="services" class="services">
      <div class="container">

      <div class="row">
      <div  class="col-md-6 icon-box">
     <a href="<?=base_url()?>ppid/open_form/<?=encode_url(2)?>" class="btn btn-success">Form Permohonan Informasi</a>
     </div>
      <div class="col-md-6 icon-box">
      <a href="<?=base_url()?>ppid/open_form/<?=encode_url(4)?>" class="btn btn-warning">Form Keberatan Informasi</a>
      </div>
      </div>

      
 
        <div class="section-title">
          <h2>Sosial Media</h2>
        </div>  

        <div class="row">
        <?php $this->load->view('umum/template/sosmed')?>
         
        </div>

      </div>
	</section><!-- End Services Section -->
	

	    <!-- ======= About Lists Section ======= -->
		<section class="about-lists">
      <div class="container">

        <div class="section-title">
          <h2>Berita Terbaru Kesbangpol</h2>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.</p> -->
        </div> 

        <div class="row no-gutters">

          <style>
            .judul_berita{
              text-align: center;
            }
            .isi{
              text-align: justify;
            }
          </style>

          <?php foreach($allberitabawah AS $beritas): ?>

          <div class="col-lg-3 col-md-4 content-item" data-aos="fade-up">
            <div class="pic"><img src="<?=base_url(filter_output($beritas->deskripsi))?>" class="img-fluid" alt=""></div>
            <br/>
            <a href="<?=base_url('publik/berita/'.encode_url($beritas->id))?>"><h5 class="judul_berita"><?=filter_output($beritas->judul)?></h5></a>
            <p class="isi"><?=substr(filter_output($beritas->isi),0,200)?></p>
          </div>

          <?php endforeach; ?>

         
          

        <hr/>

        </div>
   

        <!-- <div class="section-title">
          
          <a href="#" class=" button btn-get-started animate__animated animate__fadeInUp scrollto"><button class="btn btn-danger">Lihat Semua Berita</button></a>
        </div>  -->

      </div>
    </section>
    <!-- End About Lists Section -->

  
	  <?php $this->load->view('umum/template/kontak')?>

  </main><!-- End #main -->

  <?php $this->load->view('umum/template/footer')?>

  
</body>

</html>