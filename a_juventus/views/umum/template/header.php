<body>

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="icofont-envelope"></i><a href="mailto:<?=get_kontak('Email')->deskripsi?>"><?=get_kontak('Email')->deskripsi?></a>
        <i class="icofont-phone"></i> <?=get_kontak('Telepon')->deskripsi?>
      </div>
      <div class="social-links float-right" >

      <?php 
            $array = array('13', '11', '12', '10');
            $kontak =  $this->M_portalsz->get_kontak_where_in($array);
            foreach($kontak as $ktk):
            
            ?>

<a href="<?=$ktk->deskripsi?>" class="twitter"><i class="icofont-<?=$ktk->judul?>" style="color:red"></i></a>
             

            <?php endforeach; ?>
       
      </div>
    </div>
  </section>
  

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div class="logo float-left">
        <!-- <h1 class="text-light"><a href="index.html"><span>KESBANGPOL</span></a></h1> -->
        <!-- Uncomment below if you prefer to use an image logo  -->
        <a href="<?=base_url('publik')?>"><img src="<?=base_url('assets/kesbang_logo.png')?>" alt="" class="img-fluid"></a>
      </div>


      <nav class="nav-menu float-right d-none d-lg-block">
        <ul>
          

          <li class="drop-down <?=is_active('profil')?>"><a href="#">Profil</a>
            <ul>
                  <?php $a = $this->M_portalsz->get_profil();
                     foreach($a AS $b):
                  ?>
                   <li><a href="<?=base_url()?>publik/profil/<?=encode_url($b->x)?>"><?=$b->judul?></a></li>
                  <?php endforeach; ?>
             
              <!-- <li class="drop-down"><a href="#">Drop Down 2</a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              -->
              <li><a href="#contact">Kontak</a></li>
            </ul>
          </li>
          <!-- <li><a href="#about">About Us</a></li>
          <li><a href="#services">Services</a></li>
          <li><a href="#portfolio">Portfolio</a></li>
          <li><a href="#team">Team</a></li> -->

          <li class="drop-down <?=is_active('informasi')?>"><a href="#">Layanan</a>
            <ul>
              
              <?php $pr = $this->M_portalsz->get_profil_id('8'); ?> <li><a href="<?=base_url()?>publik/profil/<?=encode_url($pr->x)?>"><?=$pr->judul?></a></li>
              <?php $st = $this->M_portalsz->get_profil_id('22'); ?> <li><a href="<?=base_url()?>publik/profil/<?=encode_url($st->x)?>"><?=$st->judul?></a></li>
              <?php $pi = $this->M_portalsz->get_form_id('2'); ?> <li><a href="<?=base_url()?>publik/open_form/<?=encode_url($pi->x)?>"><?=$pi->judul?></a></li>
              <li><a href="https://perizinan.jatengprov.go.id/frontend/bidang_perizinan?idlicense=358">Surat Keterangan Penelitian</a></li>
              
             
            </ul>
          </li>

          <li class="drop-down <?=is_active('program')?>"><a href="#">Kegiatan</a>
            <ul>
                 
             
               <li class="drop-down"><a href="#">Program</a>
                <ul>
                <?php $k = $this->M_portalsz->get_program_in();
                                 foreach($k AS $l):?>
                                  <li><a href="<?=base_url()?>publik/program/<?=encode_url($l->id)?>"><?=$l->nama?></a></li>

                  <?php endforeach; ?>
                </ul>
              </li>

              <li class="drop-down"><a href="#">Anggaran</a>
                <ul>
                <?php $i = $this->M_portalsz->get_anggaran_in();
                                 foreach($i AS $j):?>
                                  <li><a href="<?=base_url()?>publik/anggaran/<?=encode_url($j->id)?>"><?=$j->nama?></a></li>

                  <?php endforeach; ?>
                </ul>

                
              </li>
              
              <!-- <li><a href="#contact">Kontak</a></li> -->

              <li class="drop-down <?=is_active('profil')?>"><a href="#">Galeri</a>
              <ul>
              <li><a href="<?=base_url()?>publik/fotos/">Foto</a></li>
              <li><a href="<?=base_url()?>publik/videos/">Video</a></li>
              <li><a href="<?=base_url()?>publik/infografiss/">Infografis</a></li>
             
            </ul>
          </li>

          <?php $pro = $this->M_portalsz->get_program_id('23'); ?> <li><a href="<?=base_url()?>publik/program/<?=encode_url($pro->id)?>">Laporan <?=$pro->nama?></a></li>

          <li><a target="BLANK" href="http://kesbangpol.jatengprov.go.id/layar/">Agenda</a></li>

          <li><a target="BLANK" href="<?=base_url()?>publik/pengadaan/<?=encode_url(4)?>">Pengadaan Barang & Jasa</a></li>


          <!-- <li class="drop-down"><a href="#">Pengadaan Barang & Jasa</a>
            <ul>
            <?php $m = $this->M_portalsz->get_pengadaan();
                                 foreach($m AS $n):?>
                                  <li><a href="<?=base_url()?>publik/pengadaan/<?=encode_url($n->id)?>"><?=$n->nama?></a></li>

                  <?php endforeach; ?>
             
            </ul>
          </li> -->



          </ul>
        </li>


        <li class="drop-down <?=is_active('download')?> <?=is_active('regulasi')?> <?=is_active('laporan')?>"><a href="#">Download</a>
            <ul>

            <li><a href="<?=base_url('publik/download')?>">Materi</a></li>
            <li><a target="BLANK" href="https://jdih.jatengprov.go.id/">Regulasi</a></li>

            <!-- <li class="drop-down"><a href="#">Regulasi</a>
            <ul>
              <li><a target="BLANK" href="https://jdih.jatengprov.go.id/">JDIH Jawa Tengah</a></li>
              <?php $o = $this->M_portalsz->get_regulasi();
                                 foreach($o AS $p):?>
                                  <li><a href="<?=base_url()?>publik/regulasi/<?=encode_url($p->id)?>"><?=$p->nama?></a></li>

                  <?php endforeach; ?>
            </ul>
           </li> -->

           <li class="drop-down <?=is_active('sop')?>"><a href="#">SOP</a>
            <ul>
            <?php $i = $this->M_portalsz->get_sop();
                  foreach($i AS $j):?>
                   <li><a href="<?=base_url()?>publik/sop/<?=encode_url($j->id)?>"><?=$j->nama?></a></li>
                  <?php endforeach; ?>
             
            </ul>
          </li> 

          </ul>
        </li>

        <li class="drop-down <?=is_active('download')?>"><a href="#">Publikasi</a>
            <ul>

            <li><a target="BLANK" href="http://data.jatengprov.go.id/organization/badankesatuanbangsadanpolitikprovinsijawatengah">Open Data</a></li>
            <li><a target="BLANK" href="https://laporgub.jatengprov.go.id/">Laporgub</a></li>
            <li><a href="<?=base_url('publik/artikel')?>">Berita Kesbangpol</a></li>
            <li><a href="https://corona.jatengprov.go.id/">Info Covid-19</a></li>
            

            

          </ul>
        </li>

        <li class="drop-down <?=is_active('ppid')?>"><a href="#">PPID</a>
            <ul>

          

            <li><a href="https://ppid.jatengprov.go.id/">PPID Utama</a></li>
            <li><a href="<?=base_url('ppid')?>">PPID KESBANGPOL</a></li>
          

            

          </ul>
        </li>
        

        <!-- <li class="drop-down <?=is_active('download')?>"><a href="#">Indo Covid19</a>
            <ul>

          

            <li><a href="https://corona.jatengprov.go.id/">Info Covid-19 JATENG</a></li>
            <li><a href="<?=base_url('publik/infocovid19')?>">Update Covid-19 Kesbangpol</a></li>
          

            

          </ul>
        </li> -->

        <!-- <li><a href="#contact">Kontak</a></li> -->

        <li><a href="#contact">Kontak</a></li>

          




         


         

          <?php 
          $custom = get_menu();
          foreach($custom AS $vv):
          ?>
          <li><a href="<?=base_url()?>publik/custom/<?=encode_url($vv->id)?>"><?=$vv->judul?></a></li>
          <?php endforeach; ?>

        
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->