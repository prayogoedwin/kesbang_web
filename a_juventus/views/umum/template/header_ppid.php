<body>

   <!-- ======= Top Bar ======= -->
   <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="icofont-envelope"></i><a href="mailto:<?=get_kontak('Email')->deskripsi?>"><?=get_kontak('Email')->deskripsi?></a>
        <i class="icofont-phone"></i> <?=get_kontak('Telepon')->deskripsi?>
      </div>
      <div class="social-links float-right" >
      <?php 
            $array = array('13', '11', '12', '10');
            $kontak =  $this->M_portalsz->get_kontak_where_in($array);
            foreach($kontak as $ktk):
            
            ?>

<a href="<?=$ktk->deskripsi?>" class="twitter"><i class="icofont-<?=$ktk->judul?>" style="color:red"></i></a>
             

            <?php endforeach; ?>
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div class="logo float-left">
        <!-- <h1 class="text-light"><a href="index.html"><span>KESBANGPOL</span></a></h1> -->
        <!-- Uncomment below if you prefer to use an image logo  -->
        <a href="<?=base_url('ppid')?>"><img src="<?=base_url('assets/kesbang_logo.png')?>" alt="" class="img-fluid"></a>
      </div>


      <nav class="nav-menu float-right d-none d-lg-block">
        <ul>

        <li class="drop-down <?=is_active('profil')?>"><a href="#">Beranda</a>
            <ul>
                  
            <li><a href="<?=base_url('ppid')?>">Beranda Web Company Profile</a></li>
            <li><a href="<?=base_url('ppid')?>">Beranda Web PPID</a></li>
            </ul>
          </li>
          

          <li class="drop-down <?=is_active('profil')?>"><a href="#">Profil</a>
            <ul>
                  <?php $a = $this->M_portalsz->get_profil_ppid();
                     foreach($a AS $b):
                  ?>
                   <li><a href="<?=base_url()?>ppid/profil/<?=encode_url($b->x)?>"><?=$b->judul?></a></li>
                  <?php endforeach; ?>
             
              <!-- <li class="drop-down"><a href="#">Drop Down 2</a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              -->
            </ul>
          </li>

          <li class="drop-down <?=is_active('informasi')?>"><a href="#">Informasi ppid</a>
            <ul>
                 
             
               <li class="drop-down"><a href="#">Informasi Berkala</a>
                <ul>
                        <li class="drop-down"><a href="#">Profil</a>
                        <ul>
                        <?php $a = $this->M_portalsz->get_profil();
                            foreach($a AS $b):
                          ?>
                          <li><a href="<?=base_url()?>ppid/profil/<?=encode_url($b->x)?>"><?=$b->judul?></a></li>
                          <?php endforeach; ?>
                        </ul>
                      </li>

                      <li class="drop-down"><a href="#">Program Kegiatan</a>
                      <ul>
                      <?php 
                      $array = array('12');
                      $im = $this->M_portalsz->get_anggaran_where_in($array);
                                      foreach($im AS $jm):?>
                                        <li><a href="<?=base_url()?>ppid/anggaran/<?=encode_url($jm->id)?>"><?=$jm->nama?></a></li>

                        <?php endforeach; ?>

                        <?php 
                      $array = array('32', '30', '26', '31', '27', '28', '25', '35', '24', '20', '21', '36');
                      $i = $this->M_portalsz->get_program_where_in($array);
                                      foreach($i AS $j):?>
                                        <li><a href="<?=base_url()?>ppid/program/<?=encode_url($j->id)?>"><?=$j->nama?></a></li>

                        <?php endforeach; ?>
                      </ul>

                      
                    </li>


                    <li class="drop-down"><a href="#">Laporan Keuangan</a>
                      <ul>
                      

                        <?php 
                      $array = array('8', '7', '5', '3', '4', '1', '2', '6', '9');
                      $is = $this->M_portalsz->get_anggaran_where_in($array);
                                      foreach($is AS $js):?>
                                        <li><a href="<?=base_url()?>ppid/anggaran/<?=encode_url($js->id)?>"><?=$js->nama?></a></li>

                        <?php endforeach; ?>
                      </ul>

                      
                    </li>

                    <li class="drop-down"><a href="#">Ringkasan Kinerja</a>
                      <ul>
          
                        <?php 
                        $array = array('1', '2', '3', '4');
                        $g = $this->M_portalsz->get_laporan_where_in($array);
                      foreach($g AS $h):?>
                      <li><a href="<?=base_url()?>ppid/laporan/<?=encode_url($h->id)?>"><?=$h->nama?></a></li>
                      <?php endforeach; ?>
                      </ul>

                      
                    </li>


                    <li class="drop-down"><a href="#">Informasi Peraturan</a>
                      <ul>
                        <?php 
                        $array = array('6', '7');
                        $ga = $this->M_portalsz->get_informasi_where_in($array);
                      foreach($ga AS $ha):?>
                      <li><a href="<?=base_url()?>ppid/laporan/<?=encode_url($ha->id)?>"><?=$ha->nama?></a></li>
                      <?php endforeach; ?>
                      </ul>

                      
                    </li>


                    <li class="drop-down"><a href="#">Pelayanan Informasi</a>
                      <ul>
                        <?php 
                        $array = array('2', '4', '9');
                        $gs = $this->M_portalsz->get_form_where_in($array);
                      foreach($gs AS $hs):?>
                      <li><a href="<?=base_url()?>ppid/laporan/<?=encode_url($hs->id)?>"><?=$hs->judul?></a></li>
                      <?php endforeach; ?>

                      <?php 
                        $array = array('5');
                        $ge = $this->M_portalsz->get_laporan_where_in($array);
                      foreach($ge AS $he):?>
                      <li><a href="<?=base_url()?>ppid/laporan/<?=encode_url($he->id)?>"><?=$he->nama?></a></li>
                      <?php endforeach; ?>


                      </ul>

                      
                    </li>

                    <li class="drop-down"><a href="#">Aduan Pelanggaran ASN</a>
                      <ul>
                        <?php 
                        $array = array('8', '9');
                        $ga = $this->M_portalsz->get_informasi_where_in($array);
                      foreach($ga AS $ha):?>
                      <li><a href="<?=base_url()?>ppid/laporan/<?=encode_url($ha->id)?>"><?=$ha->nama?></a></li>
                      <?php endforeach; ?>
                      </ul>

                      
                    </li>

                    <li class="drop-down"><a href="#">Pengadaan Barang dan Jasa</a>
                      <ul>
                        <?php 
                        $array = array('1', '2', '3');
                        $ga = $this->M_portalsz->get_pengadaan_where_in($array);
                      foreach($ga AS $ha):?>
                      <li><a href="<?=base_url()?>ppid/pengadaan/<?=encode_url($ha->id)?>"><?=$ha->nama?></a></li>
                      <?php endforeach; ?>
                      </ul>

                      
                    </li>

                    <li class="drop-down"><a href="#">Informasi Lain</a>
                      <ul>
                        <?php 
                        $array = array('10', '11', '12', '13');
                        $ga = $this->M_portalsz->get_kontak_where_in($array);
                      foreach($ga AS $ha):?>
                      <li><a target="BLANK" href="<?=$ha->deskripsi?>"><?=$ha->judul?></a></li>
                      <?php endforeach; ?>
                      </ul>

                      
                    </li>

                </ul>
              </li>

              <li class="drop-down"><a href="#">Informasi Serta Merta</a>
                <ul>
                <li ><a target="BLANK" href="https://bpbdjateng.id/">Evakuasi Keadaan Darurat</a>
                <li ><a target="BLANK" href="https://bpbdjateng.id/kontak-bnpb-jateng/">Daftar Kontak BPBD se Jawa Tengah</a>
                <li ><a target="BLANK" href="https://corona.jatengprov.go.id/"> Covid-19</a>
               

                </ul>
              </li>
<!-- 
              <li class="drop-down"><a href="#">Informasi Setiap Saat</a>
                <ul>
                <li ><a target="BLANK" href="https://bpbdjateng.id/">Evakuasi Keadaan Darurat</a>
                <li ><a target="BLANK" href="https://bpbdjateng.id/kontak-bnpb-jateng/">Daftar Kontak BPBD se Jawa Tengah</a>
                <li ><a target="BLANK" href="https://corona.jatengprov.go.id/"> Covid-19</a>
                </ul>
              </li>

               -->

              

              
              
              <!-- <li><a href="#contact">Kontak</a></li> -->

              <li class="<?=is_active('informasi')?>"><a href="<?=base_url()?>ppid/informasi/<?=encode_url(4)?>">Informasi Setiap Saat</a></li>

              <li class="<?=is_active('informasi')?>"><a href="<?=base_url()?>ppid/informasi/<?=encode_url(2)?>">Informasi Dikecualikan</a></li>
              <ul>
              
             
            </ul>
          </li>

          

         





          </ul>
        </li>


        <li class="drop-down <?=is_active('layanan')?>"><a href="#">Layanan</a>
            <ul>
            <?php $pi = $this->M_portalsz->get_form_id('2'); ?> <li><a href="<?=base_url()?>ppid/open_form/<?=encode_url($pi->x)?>"><?=$pi->judul?></a></li>
            <?php $pi = $this->M_portalsz->get_form_id('4'); ?> <li><a href="<?=base_url()?>ppid/open_form/<?=encode_url($pi->x)?>"><?=$pi->judul?></a></li>
            <?php $pi = $this->M_portalsz->get_form_id('9'); ?> <li><a href="<?=base_url()?>ppid/open_form/<?=encode_url($pi->x)?>"><?=$pi->judul?></a></li>
            <?php $pi = $this->M_portalsz->get_form_id('3'); ?> <li><a href="<?=base_url()?>ppid/open_form/<?=encode_url($pi->x)?>"><?=$pi->judul?></a></li>

            <li class="<?=is_active('informasi')?>"><a href="<?=base_url()?>ppid/informasi/<?=encode_url(12)?>">SOP Pelayanan Informasi</a></li>
              
             
              
             
            </ul>
          </li>

          <li><a target="BLANK" href="https://jdih.jatengprov.go.id/">Regulasi</a></li>

          <li><a href="#contact">Kontak</a></li>




          

        

         

    
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->