<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publik extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
        // is_blok($this->input->ip_address());
        
	}

	public function index()
	{
		$data['allberitabawah'] = $this->M_publik->get_all_berita_bawah();
		$data['allberitaatas'] = $this->M_publik->get_all_berita_atas();
	
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		
		$data['allgaleri'] = $this->M_publik->get_all_galeria();
		$data['allvideo'] = $this->M_publik->get_all_video('3');
		$data['allslide'] = $this->M_publik->get_all_slide();
		$data['alllink'] = $this->M_publik->get_all_linkz();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/beranda',$data);
	
	}

	public function artikel()
	{
		// $data['result'] = $this->db->query("SELECT * FROM turin_download")->result();
		$data['allberitabawah'] = $this->M_publik->get_all_berita();
		$data['allkontak'] = $this->M_publik->get_all_kontak();

		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/list_artikel',$data);
	}

	public function fotos()
	{
		// $data['result'] = $this->db->query("SELECT * FROM turin_download")->result();
		$data['allgaleri'] = $this->M_publik->get_all_infografis();
		$data['allkontak'] = $this->M_publik->get_all_kontak();

		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/list_foto',$data);
	}

	public function videos()
	{
		$data['allvideo'] = $this->M_publik->get_all_video('12');
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/list_video',$data);
	}

	public function sop()
	{
		$uri3 = decode_url($this->uri->segment('3'));

		// echo $uri3;
		$data['result'] = $this->db->query("SELECT * FROM turin_sop_data WHERE id_sop = $uri3")->result();
		$data['judul'] = get_id_sop_all_kol($uri3)->nama;
		$data['url'] = 'sop';
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/list_data_ppid',$data);
	}

	public function infografiss()
	{
		// $data['allinfografis'] = $this->M_publik->get_all_video('6');
		$data['allgaallinfografisleri'] = $this->M_publik->get_all_infografis_all();
		$data['allkontak'] = $this->M_publik->get_all_kontak();
	
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/list_infografis',$data);
	}

	public function informasi()
	{
		$uri3 = decode_url($this->uri->segment('3'));

		// echo $uri3;
		$data['result'] = $this->db->query("SELECT * FROM turin_informasi_data WHERE id_informasi = $uri3")->result();
		$data['judul'] = get_id_informasi_all_kol($uri3)->nama;
		$data['url'] = 'informasi';
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/list_data_ppid',$data);
	}

	public function laporan()
	{
		$uri3 = decode_url($this->uri->segment('3'));

		// echo $uri3;
		$data['result'] = $this->db->query("SELECT * FROM turin_laporan_data WHERE id_laporan = $uri3")->result();
		$data['judul'] = get_id_laporan_all_kol($uri3)->nama;
		$data['url'] = 'laporan';
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/list_data_ppid',$data);
	}

	public function anggaran()
	{
		$uri3 = decode_url($this->uri->segment('3'));

		// echo $uri3;
		$data['result'] = $this->db->query("SELECT * FROM turin_anggaran_data WHERE id_anggaran = $uri3")->result();
		$data['judul'] = get_id_anggaran_all_kol($uri3)->nama;
		$data['url'] = 'anggaran';
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/list_data',$data);
	}

	public function program()
	{
		$uri3 = decode_url($this->uri->segment('3'));

		// echo $uri3;
		$data['result'] = $this->db->query("SELECT * FROM turin_program_data WHERE id_program = $uri3")->result();
		$data['judul'] = get_id_program_all_kol($uri3)->nama;
		$data['url'] = 'program';
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/list_data',$data);
	}

	public function pengadaan()
	{
		$uri3 = decode_url($this->uri->segment('3'));

		// echo $uri3;
		$data['result'] = $this->db->query("SELECT * FROM turin_pengadaan_data WHERE id_pengadaan = $uri3")->result();
		$data['judul'] = get_id_pengadaan_all_kol($uri3)->nama;
		$data['url'] = 'pengadaan';
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/list_data',$data);
	}

	public function regulasi()
	{
		$uri3 = decode_url($this->uri->segment('3'));

		// echo $uri3;
		$data['result'] = $this->db->query("SELECT * FROM turin_regulasi_data WHERE id_regulasi = $uri3")->result();
		$data['judul'] = get_id_ruglasi_all_kol($uri3)->nama;
		$data['url'] = 'regulasi';
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/list_data',$data);
	}

	public function read(){
		$id = decode_url($this->uri->segment('4'));
		$uri = 'turin_'.$this->uri->segment('3');
		// $row = $this->M_publik->get_by_id($uri, $id);
		$row = $this->db->query("SELECT * FROM $uri WHERE id = '$id'")->row();
		if ($row) {
			$data = array(
				'judul' 		=> set_value('judul', $row->judul),
				'file' 			=> set_value('file', $row->file),
				'deskripsi' 	=> set_value('deskripsi', $row->deskripsi),
				);
				$this->load->view('umum/template/head');
				$this->load->view('umum/template/header');
				$this->load->view('umum/read_file', $data);
		} else {
			redirect(site_url('publik'));
		} 
	}

	public function download()
	{
		$data['result'] = $this->db->query("SELECT * FROM turin_download ORDER BY id DESC")->result();
		$data['judul'] = 'Download';
		$data['url'] = 'download';
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/list_download',$data);
	}

	public function infocovid19()
	{
		$data['result'] = $this->db->query("SELECT * FROM turin_covid ORDER BY id DESC")->result();
		$data['judul'] = 'Info Covid';
		$data['url'] = 'covid';
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/list_covid',$data);
	}



	public function info()
	{
		$uri3 = $this->uri->segment('3');
		$data['breadcumb'] = $this->uri->segment('4');
		$data['allberita'] = $this->M_publik->get_berita_by($uri3);
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/berita', $data);
	}


	public function kategori(){
		$uri3 = $this->uri->segment('3');
		$uri5 = decode_url($this->uri->segment('5'));
		$data['result'] = $this->M_publik->get_by_jenis($uri3, $uri5);
		$data['breadcumb'] = $this->uri->segment('4');
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/list', $data);

	}

	public function pejabat(){
		$data['tab1'] = $this->M_publik->get_pejabat_tab('1');
		$data['tab2'] = $this->M_publik->get_pejabat_tab('2');
		$data['tab3'] = $this->M_publik->get_pejabat_tab('3');
		$data['tab4'] = $this->M_publik->get_pejabat_tab('4');
		$data['tab5'] = $this->M_publik->get_pejabat_tab('5');
		$data['tab6'] = $this->M_publik->get_pejabat_tab('6');
		$data['tab7'] = $this->M_publik->get_pejabat_tab('7');
		$data['tab8'] = $this->M_publik->get_pejabat_tab('8');
		$data['tab9'] = $this->M_publik->get_pejabat_tab('9');
		$data['tab10'] = $this->M_publik->get_pejabat_tab('10');
		$data['tab11'] = $this->M_publik->get_pejabat_tab('11');
		$data['tab12'] = $this->M_publik->get_pejabat_tab('12');
		$data['breadcumb'] = 'Profil Pejabat';
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/pejabat2', $data);

	}

	public function daftar(){
		$uri3 = $this->uri->segment('3');
		if($uri3 == 'bidang'){
			$data['result'] = $this->M_publik->get_bidang();
		}else{
			$data['result'] = $this->M_publik->get_uptd();
		}
		$data['breadcumb'] = 'Bidang';
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/uptd_bidang', $data);
	}

	public function downloads(){
		$data['result'] = $this->M_publik->get_download();
		$data['breadcumb'] = 'Download';
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/download', $data);
	}

	public function list_(){
		$id = decode_url($this->uri->segment('4'));
		$uri3 = $this->uri->segment('3');
			if($uri3 == 'bidang'){
				$data['result'] = $this->M_publik->get_data_by_bidang($id);
				$data['breadcumb'] = 'Bidang';
			}else{
				$data['result'] = $this->M_publik->get_data_by_uptd($id);
				$data['breadcumb'] = 'UPTD';
			}
			$data['allkontak'] = $this->M_publik->get_all_kontak();
			$this->load->view('umum/template/head');
			$this->load->view('umum/template/header');
			$this->load->view('umum/list', $data);
	}

	

	public function data(){
		$id = decode_url($this->uri->segment('4'));
		$uri = $this->uri->segment('3');
		$row = $this->M_publik->get_by_id($uri, $id);
		if ($row) {
			$data = array(
				'judul' 		=> set_value('judul', $row->judul),
				'file' 			=> set_value('file', $row->file),
				'deskripsi' 	=> set_value('deskripsi', $row->deskripsi),
				'allkontak'		=> $this->M_publik->get_all_kontak(),
				);
				$this->load->view('umum/template/head');
				$this->load->view('umum/template/header');
				$this->load->view('umum/read', $data);
		} else {
			redirect(site_url('publik'));
		} 
	}

	public function berita(){
		$id = decode_url($this->uri->segment('3'));
		$uri = $this->uri->segment('2');
		$row = $this->M_publik->get_by_id($uri, $id);
		if ($row) {
			$data = array(
				'judul' 		=> set_value('judul', $row->judul),
				'file' 			=> set_value('file', $row->file),
				'deskripsi' 	=> set_value('deskripsi', $row->deskripsi),
				'isi' 			=> set_value('isi', $row->isi),
				'allkontak'		=> $this->M_publik->get_all_kontak(),
				);
				$this->load->view('umum/template/head');
				$this->load->view('umum/template/header');
				$this->load->view('umum/read_b', $data);
		} else {
			redirect(site_url('publik'));
		} 
	}

	public function custom(){
		$id = decode_url($this->uri->segment('3'));
		$uri = 'turin_menu';
		$row = $this->db->query("SELECT * FROM turin_menu WHERE id = '$id'")->row();
		if ($row) {
			$data = array(
				'judul' 		=> set_value('judul', $row->judul),
				'file' 			=> set_value('file', $row->file),
				'deskripsi' 	=> set_value('deskripsi', $row->deskripsi),
				'isi' 			=> set_value('isi', $row->isi),
				'allkontak'		=> $this->M_publik->get_all_kontak(),
				);
				$this->load->view('umum/template/head');
				$this->load->view('umum/template/header');
				$this->load->view('umum/read_b', $data);
		} else {
			redirect(site_url('publik'));
		} 
	}

	public function open_form(){
		$id = decode_url($this->uri->segment('3'));
		$row = $this->db->query("SELECT * FROM turin_form WHERE id = '$id'")->row();
		if ($row) {
			$data = array(
				'judul' 		=> set_value('judul', $row->judul),
				'file' 			=> set_value('file', $row->file),
				'deskripsi' 	=> set_value('deskripsi', $row->deskripsi),
				);
				$this->load->view('umum/template/head');
				$this->load->view('umum/template/header');
				$this->load->view('umum/read_file_ppid', $data);
		} else {
			redirect(site_url('publik'));
		} 
	}

	public function profil(){
		$id = decode_url($this->uri->segment('3'));
		$uri = $this->uri->segment('2');
		$row = $this->M_publik->get_by_id($uri, $id);
		if ($row) {
			$data = array(
				'judul' 		=> set_value('judul', $row->judul),
				'file' 			=> set_value('file', $row->file),
				'deskripsi' 	=> set_value('deskripsi', $row->deskripsi),
				'allkontak'		=> $this->M_publik->get_all_kontak(),
				);
				$this->load->view('umum/template/head');
				$this->load->view('umum/template/header');
				$this->load->view('umum/read', $data);
		} else {
			redirect(site_url('publik'));
		} 
	}

	public function form(){
		$id = decode_url($this->uri->segment('3'));
		$uri = $this->uri->segment('2');
		$row = $this->M_publik->get_by_id($uri, $id);
		if ($row) {
			$data = array(
				'judul' 		=> set_value('judul', $row->judul),
				'file' 			=> set_value('file', $row->file),
				'deskripsi' 	=> set_value('deskripsi', $row->deskripsi),
				'allkontak'		=> $this->M_publik->get_all_kontak(),
				);
				$this->load->view('umum/template/head');
				$this->load->view('umum/template/header');
				$this->load->view('umum/read', $data);
		} else {
			redirect(site_url('publik'));
		} 
	}

	public function struktur(){
		$id = decode_url($this->uri->segment('3'));
		$uri = $this->uri->segment('2');
		$row = $this->M_publik->get_by_id($uri, $id);
		if ($row) {
			$data = array(
				'judul' 		=> set_value('judul', $row->judul),
				'file' 			=> set_value('file', $row->file),
				'deskripsi' 	=> set_value('deskripsi', $row->deskripsi),
				'allkontak'		=> $this->M_publik->get_all_kontak(),
				);
				$this->load->view('umum/template/head');
				$this->load->view('umum/template/header');
				$this->load->view('umum/struktur', $data);
		} else {
			redirect(site_url('publik'));
		} 
	}

	public function video()
	{
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$data['allvideo'] = $this->M_publik->get_all_video('12');
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/video', $data);
	}

	public function foto()
	{
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$data['allgaleri'] = $this->M_publik->get_all_galeri_beranda();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/galeri', $data);
	}

	

	public function foto_galeri()
	{
		//echo decode_url($this->uri->segment('3'));
		$id = decode_url($this->uri->segment('3'));
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$data['allgaleri'] = $this->M_publik->get_by_id_galeri($id);
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/foto', $data);
	}

	public function infografis()
	{
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$data['allgaleri'] = $this->M_publik->get_all_infografis();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header');
		$this->load->view('umum/infografis', $data);
	}


	

}
