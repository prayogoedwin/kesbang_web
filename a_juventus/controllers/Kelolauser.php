<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kelolauser extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_blok($this->input->ip_address());
        is_login();
        $this->load->model('M_kelolauser');
        $this->load->model('M_log');
        
    }

    public function index()
	{
        $data['alldata'] = $this->M_kelolauser->get_all();
        $this->load->view('internal/template/head');
        $this->load->view('internal/template/header');
        $this->load->view('internal/template/sidebar');
        $this->load->view('internal/user/user',$data);
	}



    public function tambah()
    {
        $this->load->view('internal/template/head');
        $this->load->view('internal/template/header');
        $this->load->view('internal/template/sidebar');
        $this->load->view('internal/user/tambah');
    }

    public function aksi_tambah()
    { 

            $ip = $this->input->ip_address();
            $userp = $this->input->post('username', TRUE);
            $passp = $this->input->post('password', TRUE);

            if (filter_input_c($userp) == '1' || filter_input_c($passp) == '1' ){
                $datablok = array(
                    'ips'        => $ip,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->M_log->addblok($datablok);
                $this->session->sess_destroy();
			    redirecting();
            }else{
                    $data = array(
                    'emailx'  => $userp,
                    'passx'  => sha1(md5($passp)),
                    'created_at' => date('Y-m-d H:i:s')
                    );
                    $add = $this->M_kelolauser->insert($data);
                    if($add) {
                        $this->session->set_flashdata('info', 'success');
                        $this->session->set_flashdata('message', 'selamat, berhasil ');
                        redirect(site_url('kelolauser'));
                    }else{
                        $this->session->set_flashdata('info', 'danger');
                        $this->session->set_flashdata('message','Gagal');
                        redirect(site_url('kelolauser'));
                    }

            }
    }

    public function edit($i)
    {
                $id = decode_url($i);
                $row = $this->M_kelolauser->get_by_id($id);
                if ($row) {
                    $data = array(
                        'id'       => set_value('id', $row->id),
                        'username' => set_value('username', $row->emailx),
                        'password' => set_value('password', $row->passx),
                        );
                        $this->load->view('internal/template/head');
                        $this->load->view('internal/template/header');
                        $this->load->view('internal/template/sidebar');
                        $this->load->view('internal/user/edit', $data);
                    
                
                } else {
                    $this->session->set_flashdata('info', 'danger');
                    $this->session->set_flashdata('message','Gagal');
                    redirect(site_url('kelolauser'));
                }

            
        
    }

    public function aksi_edit()
    {
            $ip = $this->input->ip_address();
            $userp = $this->input->post('username', TRUE);

            if (filter_input_c($userp) == '1'){
                $datablok = array(
                    'ips'        => $ip,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->M_log->addblok($datablok);
                $this->session->sess_destroy();
                redirecting();
                
            }else{
                $id = $this->input->post('id_user',TRUE);
                $update = array(
                    'emailx' =>  $userp,
                );
                $data = $this->M_kelolauser->update($id, $update);
                if($data){
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Berhasil Edit ');
                redirect(site_url('kelolauser'));
                }else{
                    $this->session->set_flashdata('info', 'danger');
                    $this->session->set_flashdata('message','Gagal');
                    redirect(site_url('kelolauser'));
                }
            }
          
    }

    public function hapus($i)
    {
        $id = decode_url($i);
        $this->load->helper("file");
        $row = $this->M_kelolauser->get_by_id($id);
        if ($row) {
            $this->M_kelolauser->delete($id);
            $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'selamat, berhasil ');
                redirect(site_url('kelolauser'));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Gagal ');
            redirect(site_url('kelolauser'));
        }
    }

    public function reset($i)
    {
        $id = decode_url($i);
        $row = $this->M_kelolauser->get_by_id($id);
        if ($row) {
                $this->M_kelolauser->reset($id);
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Reset Password Berhasil');
                redirect(site_url('kelolauser'));
        } else {
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Reset Password Gagal');
                redirect(site_url('kelolauser'));
        }
    }



}

/* End of file Banner.php */
/* Location: ./application/controllers/Banner.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-21 07:00:03 */
/* http://harviacode.com */
