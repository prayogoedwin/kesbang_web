<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct()
    {
		parent::__construct();
		// is_blok($this->input->ip_address());
         is_login();
		//  $this->load->model('M_adminsz');
    }
	   

	public function index()
	{
		$this->load->model('M_portalsz');
		$data['alluser'] = $this->M_portalsz->get_user();
		$data['allgagal'] = $this->M_portalsz->get_log_x();
		$data['allblok'] = $this->M_portalsz->get_block();
        $this->load->view('internal/template/head');
        $this->load->view('internal/template/header');
        $this->load->view('internal/template/sidebar');
        $this->load->view('internal/dashboard',$data);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(site_url('welcome/login'));
	}


}

?>