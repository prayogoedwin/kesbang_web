<?php

    class M_portalsz extends CI_Model{

        public function login($user, $pass){
           
            $this->db->select('*');
            $this->db->where('emailx', $user);
            $this->db->where('passx', $pass);
            $log = $this->db->get('turin_admin_xyz')->row();
            if ($log){
                return true;
            }else{
                return false;
            }
        }

        

        function jumlah_user(){
            $this->db->select('id');
            $this->db->from('tbl_user');
            // $this->db->where($your_conditions);
            return $this->db->count_all_results();
        }

        // get all
    function get_regulasi()
    {
        // $this->db->order_by('id';
        $ignore = array(0);
        $this->db->where_not_in('id', $ignore);
        return $this->db->get('turin_regulasi')->result();
    }

       // get all
       function get_regulasi_id($id)
       {
           // $this->db->order_by('id';
        
           $this->db->where_not_in('id', $id);
           return $this->db->get('turin_regulasi')->row();
       }

    // get all
    function get_program()
    {
        // $this->db->order_by('id');
        $ignore = array(23);
        $this->db->where_not_in('id', $ignore);
        return $this->db->get('turin_program')->result();
    }

    // get all
    function get_program_in()
    {
        // $this->db->order_by('id');
        $ignore = array('32','33','34');
        $this->db->where_in('id', $ignore);
        return $this->db->get('turin_program')->result();
    }

    // get all
    function get_program_where_in($array)
    {
        // $this->db->order_by('id');
       //  $ignore = array('12','13','14');
        $order = sprintf('FIELD(id, %s)', implode(', ', $array));
        $this->db->where_in('id', $array);
        $this->db->order_by($order); 
        return $this->db->get('turin_program')->result();
    }

    // get all
    function get_program_id($id)
    {
        // $this->db->order_by('id');
        $this->db->where('id', $id);
        return $this->db->get('turin_program')->row();
    }

    // get all
    function get_sop()
    {
        // $this->db->order_by('id');
        return $this->db->get('turin_sop')->result();
    }

    // get all
    function get_perencanaan()
    {
        // $this->db->order_by('id');
        return $this->db->get('turin_perencanaan')->result();
    }

    // get all
    function get_pengadaan()
    {
        // $this->db->order_by('id');
        return $this->db->get('turin_pengadaan')->result();
    }

     // get all
     function get_pengadaan_where_in($array)
     {
         // $this->db->order_by('id');
        //  $ignore = array('12','13','14');
         $order = sprintf('FIELD(id, %s)', implode(', ', $array));
         $this->db->where_in('id', $array);
         $this->db->order_by($order); 
         return $this->db->get('turin_pengadaan')->result();
     }

    // get all
    function get_sumber()
    {
        // $this->db->order_by('id');
        return $this->db->get('turin_pengadaan')->result();
    }

    // get all
    function get_informasi()
    {
        // $this->db->order_by('id');
        return $this->db->get('turin_informasi')->result();
    }

     // get all
     function get_informasi_where_in($array)
     {
         // $this->db->order_by('id');
        //  $ignore = array('12','13','14');
         $order = sprintf('FIELD(id, %s)', implode(', ', $array));
         $this->db->where_in('id', $array);
         $this->db->order_by($order); 
         return $this->db->get('turin_informasi')->result();
     }

    // get all
    function get_informasi_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('turin_informasi')->result();
    }

    // get all
    function get_laporan()
    {
        // $this->db->order_by('id');
        return $this->db->get('turin_laporan')->result();
    }

     // get all
     function get_laporan_where_in($array)
     {
         // $this->db->order_by('id');
        //  $ignore = array('12','13','14');
         $order = sprintf('FIELD(id, %s)', implode(', ', $array));
         $this->db->where_in('id', $array);
         $this->db->order_by($order); 
         return $this->db->get('turin_laporan')->result();
     }

    // get all
    function get_renstra()
    {
        // $this->db->order_by($this->id, $this->order);
        return $this->db->get('turin_renstra')->result();
    }

     // get all
     function get_anggaran()
     {
        //  $this->db->order_by($this->id, $this->order);
         return $this->db->get('turin_anggaran')->result();
     }

     // get all
    function get_anggaran_in()
    {
        // $this->db->order_by('id');
        $ignore = array('12','13','14');
        $this->db->where_in('id', $ignore);
        return $this->db->get('turin_anggaran')->result();
    }

     // get all
    //  function get_anggaran_where_in($array)
    //  {
    //      // $this->db->order_by('id');
    //     //  $ignore = array('12','13','14');
    //      $this->db->where_in('id', $array);
    //      return $this->db->get('turin_anggaran')->result();
    //  }

     // get all
    function get_anggaran_where_in($array)
    {
        // $this->db->order_by('id');
       //  $ignore = array('12','13','14');
        $order = sprintf('FIELD(id, %s)', implode(', ', $array));
        $this->db->where_in('id', $array);
        $this->db->order_by($order); 
        return $this->db->get('turin_anggaran')->result();
    }

     // get all
    //  function get_profil()
    //  {
    //      $this->db->select('id as x, judul');
    //      $this->db->where('id != 7');
    //      $this->db->where('id != 8');
    //      $this->db->order_by('id', 'ASC');
    //      return $this->db->get('turin_profil')->result();
    //  }
     function get_profil()
         {

             $ignore = array(9, 8);
             $this->db->select("id as x, judul");
             $this->db->where('tipe', 0);
             $this->db->where_not_in('id', $ignore);
             return $this->db->get('turin_profil')->result();
         }


     // get all
     function get_profil_id($id)
     {
         $this->db->select('id as x, judul');
        $this->db->where('id', $id);
        //  $this->db->order_by('id', 'ASC');
         return $this->db->get('turin_profil')->row();
     }

     // get all
     function get_profil_ppid()
     {
         $this->db->select('id as x, judul');
         $this->db->where('tipe', 1);
         return $this->db->get('turin_profil')->result();
     }

     // get all
     function get_struktur()
     {
         $this->db->select('id as x, judul');
         $this->db->where('tipe', 0);
         return $this->db->get('turin_profil_struktur')->result();
     }

     // get all
     function get_struktur_ppid()
     {
         $this->db->select('id as x, judul');
         $this->db->where('tipe', 1);
         return $this->db->get('turin_profil_struktur')->result();
     }

     // get all
     function get_form()
     {
         $this->db->select('id as x, judul');
         return $this->db->get('turin_form')->result();
     }

     // get all
    function get_form_where_in($array)
    {
        // $this->db->order_by('id');
       //  $ignore = array('12','13','14');
        $order = sprintf('FIELD(id, %s)', implode(', ', $array));
        $this->db->where_in('id', $array);
        $this->db->order_by($order); 
        return $this->db->get('turin_form')->result();
    }

     // get all
     function get_form_id($id)
     {

         $this->db->select('id as x, judul');
         $this->db->where('id', $id);
         return $this->db->get('turin_form')->row();
     }

     // get all
     function get_user()
     {
         $this->db->select('count(id) as x');
         return $this->db->get('turin_admin_xyz')->row()->x;
     }

     // get all
     function get_log_x()
     {
         $this->db->select('count(id) as x');
         $this->db->where('status', 'X');
         return $this->db->get('turin_log_activity')->row()->x;
     }

     // get all
     function get_block()
     {
         $this->db->select('count(id) as x');
         return $this->db->get('turin_ip_jahat')->row()->x;
     }


     // get all
     function get_kontak_where_in($array)
     {
         // $this->db->order_by('id');
        //  $ignore = array('12','13','14');
         $order = sprintf('FIELD(id, %s)', implode(', ', $array));
         $this->db->where_in('id', $array);
         $this->db->order_by($order); 
         return $this->db->get('turin_kontak')->result();
     }

    }

?>