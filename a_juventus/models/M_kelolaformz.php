<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_kelolaformz extends CI_Model
{

    public $table = 'turin_form';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // insert data
    function insert($data)
    {
        $add = $this->db->insert($this->table, $data);
        if($add){
            return true;
        }else{
            return false;
        }
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $edit = $this->db->update($this->table, $data);
        if($edit){
            return true;
        }else{
            return false;
        }
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

