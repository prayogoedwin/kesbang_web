<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kelolaprofilpejabat extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_blok($this->input->ip_address());
        is_login();
        $this->load->model('M_kelolaprofilpejabat');
        $this->load->model('M_log');
        
    }

    public function index()
	{
        $data['alldata'] = $this->M_kelolaprofilpejabat->get_all();
        $this->load->view('internal/template/head');
        $this->load->view('internal/template/header');
        $this->load->view('internal/template/sidebar');
        $this->load->view('internal/profilpejabat/profilpejabat',$data);
	}



    public function tambah()
    {
        $data['alldata'] = $this->M_kelolaprofilpejabat->get_all_tab();
        $this->load->view('internal/template/head');
        $this->load->view('internal/template/header');
        $this->load->view('internal/template/sidebar');
        $this->load->view('internal/profilpejabat/tambah',$data);
    }

    public function aksi_tambah()
    { 

            $ip = $this->input->ip_address();
            $nama = $this->input->post('nama', TRUE);
            $jabatan = $this->input->post('jabatan', TRUE);
            $nip = $this->input->post('nip', TRUE);
            $pangol = $this->input->post('pangol', TRUE);
            $pend = $this->input->post('pend', TRUE);
            $diklat = $this->input->post('diklat', TRUE);

            if (filter_input_c($nama) == '1' || filter_input_c($jabatan) == '1' || filter_input_c($nip) == '1' || filter_input_c($pangol) == '1' || filter_input_c($pend) == '1' || filter_input_c($diklat) == '1'){
                $datablok = array(
                    'ips'        => $ip,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->M_log->addblok($datablok);
                $this->session->sess_destroy();
			    redirecting();
            }else{

                //image
                ini_set('max_execution_time', 1000);
                $filename = null;
                $config['upload_path']   = './assets/upload/profilpejabat/' . date('m_Y');
                $config['allowed_types'] = 'png|jpg|jpeg|gif';
                $config['max_size'] = max_upload();
                $config['encrypt_name'] = TRUE;

                if (!file_exists($config['upload_path'])) {
                    mkdir($config['upload_path'], 0777, true);
                }

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('filez') || empty($_FILES['filez']['name'])) {
                    if (!empty($_FILES['filez']['name'])) {
                        $upload = $this->upload->data();
                        $filename = '/assets/upload/profilpejabat/' . date('m_Y') .'/' .$upload['file_name'];
                    }
                    $data = array(
                    'nama'  => $nama,
                    'jabatan'  => $jabatan,
                    'nip'   => $nip,
                    'pangkat_golongan' => $pangol,
                    'pendidikan_akhir' => $pend,
                    'diklat_akhir' => $diklat,
                    'file'  => $upload['file_name'],
                    'deskripsi' => $filename,
                    'created_by' => get_id_user($this->session->userdata('username')),
                    'created_at' => date('Y-m-d H:i:s')
                    );
                    $add = $this->M_kelolaprofilpejabat->insert($data);
                    if($add) {
                        $this->session->set_flashdata('info', 'success');
                        $this->session->set_flashdata('message', 'selamat, berhasil ');
                        redirect(site_url('kelolaprofilpejabat'));
                    }else{
                        $this->session->set_flashdata('info', 'danger');
                        $this->session->set_flashdata('message','Gagal');
                        redirect(site_url('kelolaprofilpejabat'));
                    }
                } else {
                    if (($_FILES['file']['size']/1000)> 500) {
                        $this->session->set_flashdata('info', 'danger');
                        $this->session->set_flashdata('message','Gagal, ukuran file melebihi 500KB');
                        redirect(site_url('kelolaprofilpejabat'));
                        
                    } else {
                        $this->session->set_flashdata('info', 'danger');
                        $this->session->set_flashdata('message','Gagal');
                        redirect(site_url('kelolaprofilpejabat'));
                    }
                }

            }
    }

    public function edit($i)
    {
        $id = decode_url($i);
                $row = $this->M_kelolaprofilpejabat->get_by_id($id);
                if ($row) {
                    $data = array(
                        'id'       => set_value('id', $row->id),
                        'nama' => set_value('nama', $row->nama),
                        'jabatan' => set_value('jabatan', $row->jabatan),
                        'nip' => set_value('nama', $row->nip),
                        'pangol' => set_value('pangol', $row->pangkat_golongan),
                        'pend' => set_value('pend', $row->pendidikan_akhir),
                        'diklat' => set_value('diklat', $row->diklat_akhir),
                        'file' => set_value('file', $row->file),
                        'deskripsi' => set_value('deskripsi', $row->deskripsi),
                        );
                        $this->load->view('internal/template/head');
                        $this->load->view('internal/template/header');
                        $this->load->view('internal/template/sidebar');
                        $this->load->view('internal/profilpejabat/edit', $data);
                    
                
                } else {
                    $this->session->set_flashdata('info', 'danger');
                    $this->session->set_flashdata('message','Gagal');
                    redirect(site_url('kelolaprofilpejabat'));
                }

            
        
    }

    public function aksi_edit()
    {
            $ip = $this->input->ip_address();
            $nama = $this->input->post('nama', TRUE);
            $jabatan = $this->input->post('jabatan', TRUE);
            $nip = $this->input->post('nip', TRUE);
            $pangol = $this->input->post('pangol', TRUE);
            $pend = $this->input->post('pend', TRUE);
            $diklat = $this->input->post('diklat', TRUE);

            if (filter_input_c($nama) == '1' || filter_input_c($jabatan) == '1' || filter_input_c($nip) == '1' || filter_input_c($pangol) == '1' || filter_input_c($pend) == '1' || filter_input_c($diklat) == '1'){
                $datablok = array(
                    'ips'        => $ip,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->M_log->addblok($datablok);
                $this->session->sess_destroy();
			    redirecting();
            }else{
                if(empty($_FILES['filez']['name'])){
                    $id = $this->input->post('id',TRUE);
                    $update = array(
                        'nama'  => $nama,
                        'jabatan'  => $jabatan,
                        'nip'   => $nip,
                        'pangkat_golongan' => $pangol,
                        'pendidikan_akhir' => $pend,
                        'diklat_akhir' => $diklat
                    );
                    $data = $this->M_kelolaprofilpejabat->update($id, $update);
                    if($data){
                        $this->session->set_flashdata('info', 'success');
                        $this->session->set_flashdata('message', 'Berhasil Edit ');
                        redirect(site_url('kelolaprofilpejabat/edit/'.encode_url($id)));
                    }else{
                        $this->session->set_flashdata('info', 'danger');
                        $this->session->set_flashdata('message','Gagal');
                        redirect(site_url('kelolaprofilpejabat/edit/'.encode_url($id)));
                    }
                }else{
                        ini_set('max_execution_time', 1000);
                        $filename = null;
                        $config['upload_path']   = './assets/upload/profilpejabat/' . date('m_Y');
                        $config['allowed_types'] = 'png|jpg|jpeg|gif';
                        $config['max_size'] = max_upload();
                        $config['encrypt_name'] = TRUE;

                        if (!file_exists($config['upload_path'])) {
                            mkdir($config['upload_path'], 0777, true);
                        }
                        $this->load->library('upload', $config);
                        if ($this->upload->do_upload('filez') || empty($_FILES['filez']['name'])) {
                            if (!empty($_FILES['filez']['name'])) {
                                $upload = $this->upload->data();
                                $filename = '/assets/upload/profilpejabat/' . date('m_Y') .'/' .$upload['file_name'];
                            }

                            $id = $this->input->post('id',TRUE);
                            $update = array(
                                'nama'  => $nama,
                                'jabatan'  => $jabatan,
                                'nip'   => $nip,
                                'pangkat_golongan' => $pangol,
                                'pendidikan_akhir' => $pend,
                                'diklat_akhir' => $diklat,
                                'file'  => $upload['file_name'],
                                'deskripsi' => $filename
                              
                            );
                            $data = $this->M_kelolaprofilpejabat->update($id, $update);
                            if($data){
                            $this->session->set_flashdata('info', 'success');
                            $this->session->set_flashdata('message', 'Berhasil Edit ');
                            redirect(site_url('kelolaprofilpejabat/edit/'.encode_url($id)));
                            }else{
                                $this->session->set_flashdata('info', 'danger');
                                $this->session->set_flashdata('message','Gagal');
                                redirect(site_url('kelolaprofilpejabat/edit/'.encode_url($id)));
                            }
                        } else {
                            if (($_FILES['file']['size']/1000)> 500) {
                                $this->session->set_flashdata('info', 'danger');
                                $this->session->set_flashdata('message','Gagal, ukuran file melebihi 500KB');
                                redirect(site_url('kelolaprofilpejabat/edit/'.encode_url($id)));
                                
                            } else {
                                $this->session->set_flashdata('info', 'danger');
                                $this->session->set_flashdata('message','Gagal');
                                redirect(site_url('kelolaprofilpejabat/edit/'.encode_url($id)));
                            }
                        }
            }
        }
          
    }

    public function hapus($i)
    {
        $id = decode_url($i);
        $this->load->helper("file");
        $row = $this->M_kelolaprofilpejabat->get_by_id($id);
        if ($row) {
            $this->M_kelolaprofilpejabat->delete($id);
            $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'selamat, berhasil ');
                redirect(site_url('kelolaprofilpejabat'));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Gagal ');
            redirect(site_url('kelolaprofilpejabat'));
        }
    }

   


}

/* End of file Banner.php */
/* Location: ./application/controllers/Banner.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-21 07:00:03 */
/* http://harviacode.com */
