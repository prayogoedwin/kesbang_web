<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ppid extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
        is_blok($this->input->ip_address());
        
	}

	public function index()
	{
		$data['allberita'] = $this->M_publik->get_all_berita();
		$data['allumk'] = $this->M_publik->get_all_regulasi('8', '5');
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$data['allgaleri'] = $this->M_publik->get_all_galeri_beranda();
		$data['allvideo'] = $this->M_publik->get_all_video('1');
		$data['allslide'] = $this->M_publik->get_all_slide_ppid();
		$data['alllink'] = $this->M_publik->get_all_linkz();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header_ppid');
		$this->load->view('umum/ppid', $data);
    }

    public function info()
	{
		$uri3 = $this->uri->segment('3');
		$data['breadcumb'] = $this->uri->segment('4');
		$data['allberita'] = $this->M_publik->get_berita_by($uri3);
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header_ppid');
		$this->load->view('umum/berita', $data);
	}

	public function kategori(){
		$uri3 = $this->uri->segment('3');
		$uri5 = decode_url($this->uri->segment('5'));
		$data['result'] = $this->M_publik->get_by_jenis($uri3, $uri5);
		$data['breadcumb'] = $this->uri->segment('4');
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header_ppid');
		if($uri3 == 'lhkpn' || $uri3 == 'lhkasn'){
			$this->load->view('umum/lhk', $data);
		}else{
			$this->load->view('umum/list', $data);
		}

	}

	public function lhkcari(){
		$tahun = $this->input->post('tahun', true);
		$uri3 = $this->uri->segment('3');
		$uri5 = decode_url($this->uri->segment('5'));
		$data['result'] = $this->M_publik->get_by_jenis_tahun($uri3, $uri5, $tahun );
		$data['breadcumb'] = $this->uri->segment('4');
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header_ppid');
		if($uri3 == 'lhkpn' || $uri3 == 'lhkasn'){
			$this->load->view('umum/lhk', $data);
		}else{
			$this->load->view('umum/list', $data);
		}

	}

	public function category(){
		$uri3 = $this->uri->segment('3');
		$uri4 = decode_url($this->uri->segment('4'));
		$data['result'] = $this->M_publik->get_category($uri3, $uri4);
		$data['breadcumb'] = $uri4;
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header_ppid');
		$this->load->view('umum/list', $data);
	}


	public function daftar(){
		$uri3 = $this->uri->segment('3');
		if($uri3 == 'bidang'){
			$data['result'] = $this->M_publik->get_bidang();
		}elseif( $uri3 == 'uptd' ){
			$data['result'] = $this->M_publik->get_uptd();
		}elseif( $uri3 == 'pengadaan' ){
			$data['result'] = $this->M_publik->get_pengadaan();
		}elseif( $uri3 == 'perencanaan' ){
			$data['result'] = $this->M_publik->get_perencanaan();
		}else{
			$data['result'] = $this->M_publik->get_bidang();
		}
		$data['breadcumb'] = $uri3;
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header_ppid');
		$this->load->view('umum/uptd_bidang', $data);
	}

	public function list_(){
		$id = decode_url($this->uri->segment('4'));
		$uri3 = $this->uri->segment('3');
			if($uri3 == 'bidang'){
				$data['result'] = $this->M_publik->get_data_by_bidang($id);
				
			}elseif( $uri3 == 'uptd' ){
				$data['result'] = $this->M_publik->get_data_by_uptd($id);
			}elseif( $uri3 == 'pengadaan' ){
				$data['result'] = $this->M_publik->get_data_by_pengadaan($id);
			}elseif( $uri3 == 'perencanaan' ){
				$data['result'] = $this->M_publik->get_data_by_perencanaan($id);
			}else{
				$data['result'] = $this->M_publik->get_data_by_bidang($id);
			}
			$data['breadcumb'] = $uri3;
			$data['allkontak'] = $this->M_publik->get_all_kontak();
			$this->load->view('umum/template/head');
			$this->load->view('umum/template/header_ppid');
			$this->load->view('umum/list', $data);
	}

	

	public function data(){
		$id = decode_url($this->uri->segment('4'));
		$uri = $this->uri->segment('3');
		$row = $this->M_publik->get_by_id($uri, $id);
		if ($row) {
			$data = array(
				'judul' 		=> set_value('judul', $row->judul),
				'file' 			=> set_value('file', $row->file),
				'deskripsi' 	=> set_value('deskripsi', $row->deskripsi),
				'allkontak'		=> $this->M_publik->get_all_kontak(),
				);
				$this->load->view('umum/template/head');
				$this->load->view('umum/template/header_ppid');
				$this->load->view('umum/read', $data);
		} else {
			redirect(site_url('publik'));
		} 
	}

	public function berita(){
		$id = decode_url($this->uri->segment('3'));
		$uri = $this->uri->segment('2');
		$row = $this->M_publik->get_by_id($uri, $id);
		if ($row) {
			$data = array(
				'judul' 		=> set_value('judul', $row->judul),
				'file' 			=> set_value('file', $row->file),
				'deskripsi' 	=> set_value('deskripsi', $row->deskripsi),
				'isi' 			=> set_value('isi', $row->isi),
				'allkontak'		=> $this->M_publik->get_all_kontak(),
				);
				$this->load->view('umum/template/head');
				$this->load->view('umum/template/header_ppid');
				$this->load->view('umum/read', $data);
		} else {
			redirect(site_url('publik'));
		} 
	}

	public function profil(){
		$id = decode_url($this->uri->segment('3'));
		$uri = $this->uri->segment('2');
		$row = $this->M_publik->get_by_id($uri, $id);
		if ($row) {
			$data = array(
				'judul' 		=> set_value('judul', $row->judul),
				'file' 			=> set_value('file', $row->file),
				'deskripsi' 	=> set_value('deskripsi', $row->deskripsi),
				'allkontak'		=> $this->M_publik->get_all_kontak(),
				);
				$this->load->view('umum/template/head');
				$this->load->view('umum/template/header_ppid');
				$this->load->view('umum/read', $data);
		} else {
			redirect(site_url('publik'));
		} 
	}

	public function pejabat(){
		$data['tab1'] = $this->M_publik->get_pejabat_tab('1');
		$data['tab2'] = $this->M_publik->get_pejabat_tab('2');
		$data['tab3'] = $this->M_publik->get_pejabat_tab('3');
		$data['tab4'] = $this->M_publik->get_pejabat_tab('4');
		$data['tab5'] = $this->M_publik->get_pejabat_tab('5');
		$data['tab6'] = $this->M_publik->get_pejabat_tab('6');
		$data['tab7'] = $this->M_publik->get_pejabat_tab('7');
		$data['tab8'] = $this->M_publik->get_pejabat_tab('8');
		$data['tab9'] = $this->M_publik->get_pejabat_tab('9');
		$data['tab10'] = $this->M_publik->get_pejabat_tab('10');
		$data['tab11'] = $this->M_publik->get_pejabat_tab('11');
		$data['tab12'] = $this->M_publik->get_pejabat_tab('12');
		$data['breadcumb'] = 'Profil Pejabat';
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header_ppid');
		$this->load->view('umum/pejabat2', $data);

	}

	public function form(){
		$id = decode_url($this->uri->segment('3'));
		$uri = $this->uri->segment('2');
		$row = $this->M_publik->get_by_id($uri, $id);
		if ($row) {
			$data = array(
				'judul' 		=> set_value('judul', $row->judul),
				'file' 			=> set_value('file', $row->file),
				'deskripsi' 	=> set_value('deskripsi', $row->deskripsi),
				'allkontak'		=> $this->M_publik->get_all_kontak(),
				);
				$this->load->view('umum/template/head');
				$this->load->view('umum/template/header_ppid');
				$this->load->view('umum/read', $data);
		} else {
			redirect(site_url('publik'));
		} 
	}

	public function struktur(){
		$id = decode_url($this->uri->segment('3'));
		$uri = $this->uri->segment('2');
		$row = $this->M_publik->get_by_id($uri, $id);
		if ($row) {
			$data = array(
				'judul' 		=> set_value('judul', $row->judul),
				'file' 			=> set_value('file', $row->file),
				'deskripsi' 	=> set_value('deskripsi', $row->deskripsi),
				'allkontak'		=> $this->M_publik->get_all_kontak(),
				);
				$this->load->view('umum/template/head');
				$this->load->view('umum/template/header_ppid');
				$this->load->view('umum/struktur', $data);
		} else {
			redirect(site_url('publik'));
		} 
	}

	public function video()
	{
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$data['allvideo'] = $this->M_publik->get_all_video('12');
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header_ppid');
		$this->load->view('umum/video', $data);
	}

	public function foto()
	{
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$data['allgaleri'] = $this->M_publik->get_all_galeri_beranda();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header_ppid');
		$this->load->view('umum/galeri', $data);
	}

	

	public function foto_galeri()
	{
		//echo decode_url($this->uri->segment('3'));
		$id = decode_url($this->uri->segment('3'));
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$data['allgaleri'] = $this->M_publik->get_by_id_galeri($id);
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header_ppid');
		$this->load->view('umum/foto', $data);
	}

	public function infografis()
	{
		$data['allkontak'] = $this->M_publik->get_all_kontak();
		$data['allgaleri'] = $this->M_publik->get_all_infografis();
		$this->load->view('umum/template/head');
		$this->load->view('umum/template/header_ppid');
		$this->load->view('umum/infografis', $data);
	}
    
}
?>