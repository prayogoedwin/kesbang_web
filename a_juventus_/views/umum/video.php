
<div id="boxes">
   <div style="top: 50%; left: 50%; display: none;" id="dialog" class="window">
      <div id="san">
         <img src="assets/upload/header/disnakertrans.png" width="450">
      </div>
   </div>
   <div style="width: 2478px; font-size: 32pt; color:white; height: 1202px; display: none; opacity: 0.4;" id="mask"></div>
</div>


   <br/>
<br/>
   <div class="page-header full-content" style="background-color:#D3D3D3">
      <div class="container">
         <div class="row">
            <div class="col-sm-6">

               <h1><?=strtoupper($this->uri->segment('2'))?></h1>
            
              
               
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb">
                  <li><a href="<?=base_url()?>"><i class="fa fa-home"></i></a></li>
                 
                  <li class="active"><?=strtoupper($this->uri->segment('2'))?></li>
                  
               </ol>
            </div> 
         </div>
      </div>
   </div>

<section id="home-video" style="margin-top:-40px">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="section-header11">
							<h2><span>Galeri Video DISNAKERTRANS</span></h2>
						</div>
						<div class="row"> 

						<?php foreach($allvideo AS $videos): ?>
							<div class="col-md-4">
								<div class="show-video">
								<iframe src="https://www.youtube.com/embed/<?=filter_output($videos->deskripsi)?>" width="350" height="200" frameborder="0" allowfullscreen></iframe>
                                    <p><?=filter_output($videos->judul)?> <br/>
                                    <?=date('d-m-Y', strtotime($videos->created_at))?></p>
                                </div>
                                
	                     	</div>
						<?php endforeach; ?>

	                	</div>

	               <div class="col-md-12 lagi">
	               	<!-- <div class="row">
	               		<span class="more"><a href="<?=base_url('publik/galeri')?>" aria-label="Lihat Semua Galeri" class="hint--top hint--large">Semua Galeri</a></span>
	               	</div> -->
	               </div>
					</div>
					

						
		               	
		               	
		               	
		            </div>
		            <!-- <div id="pencet">
		            	<a href="#"><button class="tombol tombol-lg lebar blue">Semua UMK</button></a>
		         	</div> -->
					</div>
				</div>
			</div>
		</section>





   <?php include(__DIR__ . "/template/footer.php"); ?>



</body>
</html>