
 
 <!-- ======= Hero Section ======= -->
  <section id="hero" >
    <div class="hero-container">
      <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          
						
              
              <?php foreach($allslide AS $slides): ?>
                <div class="carousel-item <?=max_id($slides->id)?>" style="background-image: url('<?=$slides->deskripsi?>');">
           
                </div>
              <?php endforeach; ?>
              

        </div>

      

      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Counts Section ======= -->
    <section class="counts section-bg">
      <div class="container">

        <div class="row">

       

        <?php foreach($alllink AS $links): ?>
        
          <div class="col-lg-3 col-md-6 text-center" data-aos="fade-up">
            <div class="count-box">
            <a href="<?=$links->isi?>" target="BLANK">
              <i class="icofont-<?=$links->icon?>" style="color: #F9111E;"></i>
              <!-- <span data-toggle="counter-up">232</span> -->
              <p><?=$links->judul?></p>
              </a>
            </div>
          </div>
        
      <?php endforeach; ?>

          

        </div>

      </div>
    </section><!-- End Counts Section -->

       <!-- ======= Services Section ======= -->
       <section id="services" class="services">
      <div class="container">
 
        <div class="section-title">
          <h2>Seputar Kesbangpol</h2>
        </div> 

        <div class="row">
          <div class="col-lg-3 col-md-4 icon-box" data-aos="fade-up">
            <div class="icon"><i class="icofont-computer"></i></div>
            <h4 class="title"><a href="">KOMITMEN KEBANGSAAN<br/>&nbsp;</a></h4>
            <p class="description isi">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
          </div>
          <div class="col-lg-3 col-md-4 icon-box" data-aos="fade-up" data-aos-delay="100">
            <div class="icon"><i class="icofont-chart-bar-graph"></i></div>
            <h4 class="title"><a href="">NASIONALISME DAN PATRIOTISME</a></h4>
            <p class="description isi">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
          </div>
          <div class="col-lg-3 col-md-4 icon-box" data-aos="fade-up" data-aos-delay="200">
            <div class="icon"><i class="icofont-earth"></i></div>
            <h4 class="title"><a href="">WAWASAN KEBANGSAAN<br/>&nbsp;</a></h4>
            <p class="description isi">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
          </div>
          <div class="col-lg-3 col-md-4 icon-box" data-aos="fade-up" data-aos-delay="300">
            <div class="icon"><i class="icofont-image"></i></div>
            <h4 class="title"><a href="">BHINNEKA TUNGGAL IKA<br/>&nbsp;</a></h4>
            <p class="description isi">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
          </div>
          <!-- <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="400">
            <div class="icon"><i class="icofont-settings"></i></div>
            <h4 class="title"><a href="">Nemo Enim</a></h4>
            <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
          </div>
          <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="500">
            <div class="icon"><i class="icofont-tasks-alt"></i></div>
            <h4 class="title"><a href="">Eiusmod Tempor</a></h4>
            <p class="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
          </div> -->
        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= About Us Section ======= -->
    <!-- <section id="about" class="about">
      <div class="container">

        <div class="row no-gutters">
          <div class="col-lg-6 video-box">
            <img src="assets/img/about.jpg" class="img-fluid" alt="">
            <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
          </div>

          <div class="col-lg-6 d-flex flex-column justify-content-center about-content">

             <div class="section-title">
              <h2>About Us</h2>
              <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.</p>
            </div> 

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-fingerprint"></i></div>
              <h4 class="title"><a href="">Komitmen Kebangsaan</a></h4>
              <p class="description">Adalah cara pandang bangsa Indonesia dalam rangka mengelola kehidupan berbangsa dan bernegara yang dilandasi oleh jati diri bangsa (nation and character) dan kesadaran terhadap sistem nasional (national system) yang bersumber dari Pancasila, UUD NRI tahun 1945, NKRI, dan Bhinneka Tunggal Ika.</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">NASIONALISME DAN PATRIOTISME</a></h4>
              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">WAWASAN KEBANGSAAN</a></h4>
              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">BHINNEKA TUNGGAL IKA</a></h4>
              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div>

          </div>
        </div>

      </div>
    </section> -->
    <!-- End About Us Section -->

    <!-- ======= About Lists Section ======= -->
    <section class="about-lists">
      <div class="container">

        <div class="section-title">
          <h2>Berita Terbaru Kesbangpol</h2>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.</p> -->
        </div> 

        <div class="row no-gutters">

          <style>
            .judul_berita{
              text-align: center;
            }
            .isi{
              text-align: justify;
            }
          </style>

          <?php foreach($allberita AS $beritas): ?>

          <div class="col-lg-3 col-md-4 content-item" data-aos="fade-up">
            <div class="pic"><img src="<?=base_url(filter_output($beritas->deskripsi))?>" class="img-fluid" alt=""></div>
            <br/>
            <h5 class="judul_berita"><?=filter_output($beritas->judul)?></h5>
            <p class="isi"><?=substr(filter_output($beritas->isi),0,200)?></p>
          </div>

          <?php endforeach; ?>

          <!-- <div class="col-lg-3 col-md-4 content-item" data-aos="fade-up" data-aos-delay="100">
            <div class="pic"><img src="assets/img/team/team-1.jpg" class="img-fluid" alt=""></div>
            <h5 class="judul_berita">Lorem Ipsum</h5>
            <p class="isi">Dolorem est fugiat occaecati voluptate velit esse. Dicta veritatis dolor quod et vel dire leno para dest</p>
          </div>

          <div class="col-lg-3 col-md-4 content-item" data-aos="fade-up" data-aos-delay="200">
            <div class="pic"><img src="assets/img/team/team-1.jpg" class="img-fluid" alt=""></div>
            <h5 class="judul_berita">Lorem Ipsum</h5>
            <p class="isi">Molestiae officiis omnis illo asperiores. Aut doloribus vitae sunt debitis quo vel nam quis</p>
          </div>

          <div class="col-lg-3 col-md-4 content-item" data-aos="fade-up" data-aos-delay="300">
            <div class="pic"><img src="assets/img/team/team-1.jpg" class="img-fluid" alt=""></div>
            <h5 class="judul_berita" >Lorem Ipsum</h5>
            <p class="isi">Inventore quo sint a sint rerum. Distinctio blanditiis deserunt quod soluta quod nam mider lando casa</p>
          </div> -->

          

        <hr/>

        </div>
        <br/>

        <div class="section-title">
          
          <a href="#" class=" button btn-get-started animate__animated animate__fadeInUp scrollto"><button class="btn btn-danger">Lihat Semua Berita</button></a>
        </div> 

      </div>
    </section>
    <!-- End About Lists Section -->

    

 

    <!-- ======= Our Portfolio Section ======= -->
    <section id="portfolio" class="portfolio section-bg">
      <div class="container" data-aos="fade-up" data-aos-delay="100">

        <div class="section-title">
          <h2>Galeri Kegiatan</h2>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

        <!-- <div class="row">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-app">App</li>
              <li data-filter=".filter-card">Card</li>
              <li data-filter=".filter-web">Web</li>
            </ul>
          </div>
        </div> -->

        <div class="row portfolio-container">
          

        <?php foreach($allgaleri AS $galeris): ?>
          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="<?=base_url(get_by_id_galeri_foto($galeris->id))?>" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><?=$galeris->judul?></h4>
                <!-- <p>App</p> -->
                <div class="portfolio-links">
                  <a href="<?=base_url(get_by_id_galeri_foto($galeris->id))?>" data-gall="portfolioGallery" class="venobox" title="<?=$galeris->judul?>"><i class="icofont-eye"></i></a>
                  <a href="<?=base_url('publik/foto/'.encode_url($galeris->id))?>" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>
          <?php endforeach; ?>


          <!-- <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <div class="portfolio-wrap">
              <img src="assets/img/portfolio/portfolio-2.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Web 3</h4>
                <p>Web</p>
                <div class="portfolio-links">
                  <a href="assets/img/portfolio/portfolio-2.jpg" data-gall="portfolioGallery" class="venobox" title="Web 3"><i class="icofont-eye"></i></a>
                  <a href="portfolio-details.html" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="assets/img/portfolio/portfolio-3.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>App 2</h4>
                <p>App</p>
                <div class="portfolio-links">
                  <a href="assets/img/portfolio/portfolio-3.jpg" data-gall="portfolioGallery" class="venobox" title="App 2"><i class="icofont-eye"></i></a>
                  <a href="portfolio-details.html" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="assets/img/portfolio/portfolio-4.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Card 2</h4>
                <p>Card</p>
                <div class="portfolio-links">
                  <a href="assets/img/portfolio/portfolio-4.jpg" data-gall="portfolioGallery" class="venobox" title="Card 2"><i class="icofont-eye"></i></a>
                  <a href="portfolio-details.html" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <div class="portfolio-wrap">
              <img src="assets/img/portfolio/portfolio-5.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Web 2</h4>
                <p>Web</p>
                <div class="portfolio-links">
                  <a href="assets/img/portfolio/portfolio-5.jpg" data-gall="portfolioGallery" class="venobox" title="Web 2"><i class="icofont-eye"></i></a>
                  <a href="portfolio-details.html" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="assets/img/portfolio/portfolio-6.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>App 3</h4>
                <p>App</p>
                <div class="portfolio-links">
                  <a href="assets/img/portfolio/portfolio-6.jpg" data-gall="portfolioGallery" class="venobox" title="App 3"><i class="icofont-eye"></i></a>
                  <a href="portfolio-details.html" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div> -->

          

        </div>

        <br/>

        <div class="section-title">
          
          <a href="#" class=" button btn-get-started animate__animated animate__fadeInUp scrollto"><button class="btn btn-danger">Lihat Semua Galeri Kegiatan</button></a>
        </div> 

      </div>
    </section><!-- End Our Portfolio Section -->

    <!-- ======= Our Team Section ======= -->
    <!-- <section id="team" class="team">
      <div class="container">

        <div class="section-title">
          <h2>Our Team</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem.</p>
        </div>

        <div class="row">

          <div class="col-xl-3 col-lg-4 col-md-6" data-aos="fade-up">
            <div class="member">
              <div class="pic"><img src="assets/img/team/team-1.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Walter White</h4>
                <span>Chief Executive Officer</span>
                <div class="social">
                  <a href=""><i class="icofont-twitter"></i></a>
                  <a href=""><i class="icofont-facebook"></i></a>
                  <a href=""><i class="icofont-instagram"></i></a>
                  <a href=""><i class="icofont-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-xl-3 col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
            <div class="member">
              <div class="pic"><img src="assets/img/team/team-2.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Sarah Jhonson</h4>
                <span>Product Manager</span>
                <div class="social">
                  <a href=""><i class="icofont-twitter"></i></a>
                  <a href=""><i class="icofont-facebook"></i></a>
                  <a href=""><i class="icofont-instagram"></i></a>
                  <a href=""><i class="icofont-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-xl-3 col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
            <div class="member">
              <div class="pic"><img src="assets/img/team/team-3.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>William Anderson</h4>
                <span>CTO</span>
                <div class="social">
                  <a href=""><i class="icofont-twitter"></i></a>
                  <a href=""><i class="icofont-facebook"></i></a>
                  <a href=""><i class="icofont-instagram"></i></a>
                  <a href=""><i class="icofont-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-xl-3 col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="300">
            <div class="member">
              <div class="pic"><img src="assets/img/team/team-4.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Amanda Jepson</h4>
                <span>Accountant</span>
                <div class="social">
                  <a href=""><i class="icofont-twitter"></i></a>
                  <a href=""><i class="icofont-facebook"></i></a>
                  <a href=""><i class="icofont-instagram"></i></a>
                  <a href=""><i class="icofont-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section> -->
    <!-- End Our Team Section -->

    <!-- ======= Frequently Asked Questions Section ======= -->
    <!-- <section id="faq" style="background-color: #000;">
      <div class="container">

        <div class="section-title" >
          <h2 style="color:#fff">Pengumuman</h2>
        </div>

        <div class="alert alert-danger" role="alert">Perijinan Penelitian terkait Kesbangpol, meliputi Rekomendasi Penelitian, Rekomendasi Riset, Rekomendasi KKN, Rekomendasi Praktik Kerja Lapangan, Rekomendasi Melaksanakan Survey dapat diurus secara online lho. silahkan akses disini, dan lengkapi persyaratannya. Klik disini.</div>

        <div class="alert alert-warning" role="alert">Siap Siaga dan selalu waspada hadapi Bencana Alam. Hubungi nomer-nomer berikut jika anda ingin memberikan informasi terkait bencana yang terjadi di Jawa Tengah. Klik disini.</div>

        <div class="alert alert-success" role="alert">Men PAN-RB mengeluarkan SE nomor: 137 Tahun 2018 tentang penyebarluasan informasi melalui media sosial bagi ASN, ditujukan untuk para menteri kabinet kerja, Panglima TNI, Kapolri, Jaksa Agung, Sekretaris Kabinet, para kepala lembaga non kementerian, pimpinan kesekretariatan lembaga negara dan non struktural, para gubernur, dan wali kota. Surat tersebut dalam rangka pemanfaatan media sosial sebagai sarana komunikasi untuk penyebarluasan informasi, baik antar individu, individu dan institusi, serta antar institusi dalam menghadapi tantangan dan perubahan lingkungan yang sangat cepat dan dinamis, ASN diharapkan dapat berperan membangun suasana yang kondusif di media sosial. Klik disini.</div>

       

        

      </div>
    </section> -->
    <!-- End Frequently Asked Questions Section -->


        <!-- ======= About Lists Section ======= -->
        <!-- <section class="about-lists">
          <div class="container">
    
            <div class="section-title">
              <h2>Lembaga Mitra</h2>
             
            </div> 
    
            <div class="row no-gutters">
    
              <style>
                .judul_berita{
                  text-align: center;
                }
                .isi{
                  text-align: justify;
                }
              </style>
    
              <div class="col-lg-3 col-md-4 content-item" data-aos="fade-up">
                <div class="pic"><img src="assets/img/team/team-1.jpg" class="img-fluid" alt=""></div>
                <h5 class="judul_berita">Forum Kerukunan Umat Beragama<br/>&nbsp;</h5>
                <p class="isi">Ulamco laboris nisi ut aliquip ex ea commodo consequat. Et consectetur ducimus vero placeat</p>
              </div>
    
              <div class="col-lg-3 col-md-4 content-item" data-aos="fade-up" data-aos-delay="100">
                <div class="pic"><img src="assets/img/team/team-2.jpg" class="img-fluid" alt=""></div>
                <h5 class="judul_berita">Forum Persaudaraan Bangsa Indonesia<br/>&nbsp;</h5>
                <p class="isi">Dolorem est fugiat occaecati voluptate velit esse. Dicta veritatis dolor quod et vel dire leno para dest</p>
              </div>
    
              <div class="col-lg-3 col-md-4 content-item" data-aos="fade-up" data-aos-delay="200">
                <div class="pic"><img src="assets/img/team/team-3.jpg" class="img-fluid" alt=""></div>
                <h5 class="judul_berita">Forum Koordinasi Pencegahan Terorisme</h5>
                <p class="isi">Molestiae officiis omnis illo asperiores. Aut doloribus vitae sunt debitis quo vel nam quis</p>
              </div>
    
              <div class="col-lg-3 col-md-4 content-item" data-aos="fade-up" data-aos-delay="300">
                <div class="pic"><img src="assets/img/team/team-4.jpg" class="img-fluid" alt=""></div>
                <h5 class="judul_berita" >Badan Narkotika Nasional<br/>&nbsp;</h5>
                <p class="isi">Inventore quo sint a sint rerum. Distinctio blanditiis deserunt quod soluta quod nam mider lando casa</p>
              </div>
    
              
    
            <hr/>
    
            </div>
            
    
          </div>
        </section> -->
        <!-- End About Lists Section -->


        <!-- ======= Our Portfolio Section ======= -->
    <section id="portfolio" class="portfolio section-bg">
      <div class="container" data-aos="fade-up" data-aos-delay="100">

        <div class="section-title">
          <h2>Galeri Video</h2>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

        <!-- <div class="row">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-app">App</li>
              <li data-filter=".filter-card">Card</li>
              <li data-filter=".filter-web">Web</li>
            </ul>
          </div>
        </div> -->

        <div class="row portfolio-container">

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <!-- <div class="portfolio-wrap"> -->
              <iframe width="350" height="250" src="https://www.youtube.com/embed/cmzAlBXfsL4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              
              <!-- <div class="portfolio-info">
                <h4>App 1</h4>
                <p>App</p>
                <div class="portfolio-links">
                  <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="icofont-eye"></i></a>
                  <a href="portfolio-details.html" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div> -->
            <!-- </div> -->
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <!-- <div class="portfolio-wrap"> -->
              <iframe width="350" height="250" src="https://www.youtube.com/embed/cmzAlBXfsL4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              
              <!-- <div class="portfolio-info">
                <h4>Web 3</h4>
                <p>Web</p>
                <div class="portfolio-links">
                  <a href="assets/img/portfolio/portfolio-2.jpg" data-gall="portfolioGallery" class="venobox" title="Web 3"><i class="icofont-eye"></i></a>
                  <a href="portfolio-details.html" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div> -->
            <!-- </div> -->
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <!-- <div class="portfolio-wrap"> -->
              <iframe width="350" height="250" src="https://www.youtube.com/embed/cmzAlBXfsL4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              <!-- <div class="portfolio-info">
                <h4>App 2</h4>
                <p>App</p>
                <div class="portfolio-links">
                  <a href="http://www.youtube.com/watch?v=qqXi8WmQ_WM" rel="prettyPhoto" title=""><i class="icofont-eye"></i></a>
                  <a href="portfolio-details.html" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>  -->
            <!-- </div> -->
          </div>


         

        </div>

        <br/>

        <!-- <div class="section-title">
          
          <a href="#" class=" button btn-get-started animate__animated animate__fadeInUp scrollto"><button class="btn btn-danger">Lihat Semua Video</button></a>
        </div>  -->

      </div>
    </section><!-- End Our Portfolio Section -->


           <!-- ======= Services Section ======= -->
           <section id="services" class="services">
      <div class="container">
 
        <div class="section-title">
          <h2>Sosial Media</h2>
        </div>  

        <div class="row">
          <div class="col-lg-4 col-md-4 icon-box" data-aos="fade-up">
            <div class="icon"><i class="icofont-facebook"></i></div>
            <h4 class="title"><a href="">Facebook<br/>&nbsp;</a></h4>
            <!-- <p class="description isi">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p> -->
          </div>
          <div class="col-lg-4 col-md-4 icon-box" data-aos="fade-up" data-aos-delay="100">
            <div class="icon"><i class="icofont-twitter"></i></div>
            <h4 class="title"><a href="">Twitter</a>&nbsp;</h4>
            <!-- <p class="description isi">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p> -->
          </div>
          <div class="col-lg-4 col-md-4 icon-box" data-aos="fade-up" data-aos-delay="200">
            <div class="icon"><i class="icofont-instagram"></i></div>
            <h4 class="title"><a href="">Instagram<br/>&nbsp;</a></h4>
            <!-- <p class="description isi">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p> -->
          </div>
          <!-- <div class="col-lg-3 col-md-4 icon-box" data-aos="fade-up" data-aos-delay="300">
            <div class="icon"><i class="icofont-image"></i></div>
            <h4 class="title"><a href="">BHINNEKA TUNGGAL IKA<br/>&nbsp;</a></h4>
            <p class="description isi">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
          </div> -->
          <!-- <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="400">
            <div class="icon"><i class="icofont-settings"></i></div>
            <h4 class="title"><a href="">Nemo Enim</a></h4>
            <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
          </div>
          <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="500">
            <div class="icon"><i class="icofont-tasks-alt"></i></div>
            <h4 class="title"><a href="">Eiusmod Tempor</a></h4>
            <p class="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
          </div> -->
        </div>

      </div>
    </section><!-- End Services Section -->



<?php $this->load->view('umum/template/kontak')?>

  </main><!-- End #main -->

  <?php $this->load->view('umum/template/footer')?>

  
</body>

</html>