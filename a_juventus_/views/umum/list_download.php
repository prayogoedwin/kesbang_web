
    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="row no-gutters">
          <!-- <div class="col-lg-2 video-box" style="background-image: url('assets/upload/slide/11_2020/fcfd162b2779ed1005ff213ee145bf1a.png);">
              <img src="<?=base_url()?>assets/upload/slide/11_2020/fcfd162b2779ed1005ff213ee145bf1a.png"> -->
            <!-- <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a> 
          </div> -->

          <div class="col-lg-12 d-flex flex-column justify-content-center about-content">

             <div class="section-title">
              <h2><?=$judul?></h2>
                        
            </div> 


            <?php foreach($result as $res): ?>

             
              <h5 class="title"><i class="bx bx-download"></i>&nbsp;&nbsp;<a href="<?=base_url()?>publik/read/<?=$url?>/<?=encode_url($res->id)?>"><?php echo $res->judul;?></a></h5>
             

            <?php endforeach; ?>

            <!-- <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">NASIONALISME DAN PATRIOTISME</a></h4>
              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">WAWASAN KEBANGSAAN</a></h4>
              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">BHINNEKA TUNGGAL IKA</a></h4>
              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div> -->

          </div>
        </div>

      </div>
    </section> 
    <!-- End About Us Section -->

   

    

 



           <!-- ======= Services Section ======= -->
           <section id="services" class="services">
      <div class="container">
 
        <div class="section-title">
          <h2>Sosial Media</h2>
        </div>  

        <div class="row">
          <div class="col-lg-4 col-md-4 icon-box" data-aos="fade-up">
            <div class="icon"><i class="icofont-facebook"></i></div>
            <h4 class="title"><a href="">Facebook<br/>&nbsp;</a></h4>
            <!-- <p class="description isi">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p> -->
          </div>
          <div class="col-lg-4 col-md-4 icon-box" data-aos="fade-up" data-aos-delay="100">
            <div class="icon"><i class="icofont-twitter"></i></div>
            <h4 class="title"><a href="">Twitter</a>&nbsp;</h4>
            <!-- <p class="description isi">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p> -->
          </div>
          <div class="col-lg-4 col-md-4 icon-box" data-aos="fade-up" data-aos-delay="200">
            <div class="icon"><i class="icofont-instagram"></i></div>
            <h4 class="title"><a href="">Instagram<br/>&nbsp;</a></h4>
            <!-- <p class="description isi">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p> -->
          </div>
          <!-- <div class="col-lg-3 col-md-4 icon-box" data-aos="fade-up" data-aos-delay="300">
            <div class="icon"><i class="icofont-image"></i></div>
            <h4 class="title"><a href="">BHINNEKA TUNGGAL IKA<br/>&nbsp;</a></h4>
            <p class="description isi">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
          </div> -->
          <!-- <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="400">
            <div class="icon"><i class="icofont-settings"></i></div>
            <h4 class="title"><a href="">Nemo Enim</a></h4>
            <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
          </div>
          <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="500">
            <div class="icon"><i class="icofont-tasks-alt"></i></div>
            <h4 class="title"><a href="">Eiusmod Tempor</a></h4>
            <p class="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
          </div> -->
        </div>

      </div>
    </section><!-- End Services Section -->

    <?php $this->load->view('umum/template/kontak')?>

  </main><!-- End #main -->

  <?php $this->load->view('umum/template/footer')?>

  
</body>

</html>