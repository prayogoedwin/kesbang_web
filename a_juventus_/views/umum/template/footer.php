 <!-- ======= Footer ======= -->
 <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-info">
            <h3>Badan Kesatuan Bangsa Dan Politik Provinsi Jawa Tengah</h3>
            <p>
              Jl. Ahmad Yani No. 160 Kota Semarang, Jawa Tengah<br><br>
              <strong>Telp:</strong> 024-8454990<br>
              <strong>SMS:</strong> 089690923824<br>
              
            </p>
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="youtube"><i class="bx bxl-youtube"></i></a>
              <!-- <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a> -->
            </div>
          </div>

          <div class="col-lg-5 col-md-6 footer-links">
            <h4>Berita Jatengprov</h4>
            <?php 
            
            $xml = new SimpleXMLElement('https://jatengprov.go.id/feed/', null, true);
            
            
            ?>
            

            <ul>
            <?php
              for($i=0; $i<6; $i++)
              {
               $url = $xml->channel->item[$i]->link;
               $title = $xml->channel->item[$i]->title;
               echo '<li><i class="bx bx-chevron-right"></i><a href="'.$url.'" target="_blank">'.$title.'</a></li>';
              }
            ?> 
            </ul>
            
          </div>

          <!--<div class="col-lg-2 col-md-6 footer-links  ">-->
          <!--   <h4>Berita Jatengprov</h4>-->
          <!--  <ul>-->
          <!--    <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>-->
          <!--    <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>-->
          <!--    <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>-->
          <!--    <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>-->
          <!--    <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>-->
          <!--  </ul> -->
          <!--</div>-->

          <div class="col-lg-4 col-md-6 footer-links">
            <a class="twitter-timeline" data-height="300" href="https://twitter.com/Bakesbangprov?ref_src=twsrc%5Etfw">Tweets by Bakesbangprov</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        2020 &copy; Copyright <strong><span>Badan Kesatuan Bangsa dan Politik Provinsi Jawa Tengah</span></strong>
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/mamba-one-page-bootstrap-template-free/ -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> - Ezrapratama
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?=base_url()?>assets/frontend/vendor/jquery/jquery.min.js"></script>
  <script src="<?=base_url()?>assets/frontend/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url()?>assets/frontend/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?=base_url()?>assets/frontend/vendor/php-email-form/validate.js"></script>
  <script src="<?=base_url()?>assets/frontend/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="<?=base_url()?>assets/frontend/vendor/venobox/venobox.min.js"></script>
  <script src="<?=base_url()?>assets/frontend/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="<?=base_url()?>assets/frontend/vendor/counterup/counterup.min.js"></script>
  <script src="<?=base_url()?>assets/frontend/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="<?=base_url()?>assets/frontend/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?=base_url()?>assets/frontend/vendor/aos/aos.js"></script>

  




  <!-- Template Main JS File -->
  <script src="<?=base_url()?>assets/frontend/js/main.js"></script>

  <script src="<?=base_url()?>assets/frontend/vendor/prettyPhoto/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
  
  <script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
      $("a[rel^='prettyPhoto']").prettyPhoto();
    });
  </script>