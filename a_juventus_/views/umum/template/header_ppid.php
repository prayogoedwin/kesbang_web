<body>

   <!-- ======= Top Bar ======= -->
   <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="icofont-envelope"></i><a href="mailto:<?=get_kontak('Email')->deskripsi?>"><?=get_kontak('Email')->deskripsi?></a>
        <i class="icofont-phone"></i> <?=get_kontak('Telepon')->deskripsi?>
      </div>
      <div class="social-links float-right" >
        <a href="<?=get_kontak('Twitter')->deskripsi?>" class="twitter"><i class="icofont-twitter" style="color:red"></i></a>
        <a href="<?=get_kontak('Facebook')->deskripsi?>" class="facebook"><i class="icofont-facebook" style="color:red"></i></a>
        <a href="<?=get_kontak('Instagram')->deskripsi?>" class="instagram"><i class="icofont-instagram" style="color:red"></i></a>
        <a href="<?=get_kontak('Youtube')->deskripsi?>" class="youtube"><i class="icofont-youtube" style="color:red"></i></a>
        <!-- <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a> -->
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div class="logo float-left">
        <!-- <h1 class="text-light"><a href="index.html"><span>KESBANGPOL</span></a></h1> -->
        <!-- Uncomment below if you prefer to use an image logo  -->
        <a href="<?=base_url('publik')?>"><img src="<?=base_url('assets/kesbang_logo.png')?>" alt="" class="img-fluid"></a>
      </div>


      <nav class="nav-menu float-right d-none d-lg-block">
        <ul>

        <li class="drop-down <?=is_active('profil')?>"><a href="#">Beranda</a>
            <ul>
                  
            <li><a href="<?=base_url('publik')?>">Beranda Web Company Profile</a></li>

            <li><a href="<?=base_url('ppid')?>">Beranda Web PPID</a></li>
            </ul>
          </li>
          

          <li class="drop-down <?=is_active('profil')?>"><a href="#">Profil</a>
            <ul>
                  <?php $a = $this->M_portalsz->get_profil();
                     foreach($a AS $b):
                  ?>
                   <li><a href="<?=base_url()?>publik/profil/<?=encode_url($b->x)?>"><?=$b->judul?></a></li>
                  <?php endforeach; ?>
             
              <!-- <li class="drop-down"><a href="#">Drop Down 2</a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              -->
            </ul>
          </li>

          <li class="drop-down <?=is_active('informasi')?>"><a href="#">Informasi Publik</a>
            <ul>
            <?php $e = $this->M_portalsz->get_informasi();
                  foreach($e AS $f):?>
                   <li><a href="<?=base_url()?>ppid/informasi/<?=encode_url($f->id)?>"><?=$f->nama?></a></li>
                  <?php endforeach; ?>
             
            </ul>
          </li>

          <li class="drop-down <?=is_active('laporan')?>"><a href="#">Laporan</a>
            <ul>
            <?php $g = $this->M_portalsz->get_laporan();
                  foreach($g AS $h):?>
                   <li><a href="<?=base_url()?>ppid/laporan/<?=encode_url($h->id)?>"><?=$h->nama?></a></li>
                  <?php endforeach; ?>
             
            </ul>
          </li>

          <!-- <li class="drop-down <?=is_active('sop')?>"><a href="#">SOP</a>
            <ul>
            <?php $i = $this->M_portalsz->get_sop();
                  foreach($i AS $j):?>
                   <li><a href="<?=base_url()?>ppid/laporan/<?=encode_url($j->id)?>"><?=$j->nama?></a></li>
                  <?php endforeach; ?>
             
            </ul>
          </li> -->

          <li class="drop-down <?=is_active('formulir')?>"><a href="#">Formulir</a>
            <ul>
            <?php $m = $this->M_portalsz->get_form();
                  foreach($m AS $n):?>
                   <li><a href="<?=base_url()?>ppid/open_form/<?=encode_url($n->x)?>"><?=$n->judul?></a></li>
                  <?php endforeach; ?>
             
            </ul>
          </li>

          <li><a href="#contact">Kontak</a></li>

          <?php 
          $custom = get_menu_ppid();
          foreach($custom AS $vv):
          ?>
          <li><a href="<?=base_url()?>publik/custom/<?=encode_url($vv->id)?>"><?=$vv->judul?></a></li>
          <?php endforeach; ?>

    
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->