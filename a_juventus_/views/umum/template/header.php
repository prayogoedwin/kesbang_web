<body>

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="icofont-envelope"></i><a href="mailto:<?=get_kontak('Email')->deskripsi?>"><?=get_kontak('Email')->deskripsi?></a>
        <i class="icofont-phone"></i> <?=get_kontak('Telepon')->deskripsi?>
      </div>
      <div class="social-links float-right" >
        <a href="<?=get_kontak('Twitter')->deskripsi?>" class="twitter"><i class="icofont-twitter" style="color:red"></i></a>
        <a href="<?=get_kontak('Facebook')->deskripsi?>" class="facebook"><i class="icofont-facebook" style="color:red"></i></a>
        <a href="<?=get_kontak('Instagram')->deskripsi?>" class="instagram"><i class="icofont-instagram" style="color:red"></i></a>
        <a href="<?=get_kontak('Youtube')->deskripsi?>" class="youtube"><i class="icofont-youtube" style="color:red"></i></a>
        <!-- <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a> -->
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div class="logo float-left">
        <!-- <h1 class="text-light"><a href="index.html"><span>KESBANGPOL</span></a></h1> -->
        <!-- Uncomment below if you prefer to use an image logo  -->
        <a href="<?=base_url('publik')?>"><img src="<?=base_url('assets/kesbang_logo.png')?>" alt="" class="img-fluid"></a>
      </div>


      <nav class="nav-menu float-right d-none d-lg-block">
        <ul>
          <!-- <li class="active"><a href="#">Profil</a></li> -->

          <li class="drop-down <?=is_active('profil')?>"><a href="#">Profil</a>
            <ul>
                  <?php $a = $this->M_portalsz->get_profil();
                     foreach($a AS $b):
                  ?>
                   <li><a href="<?=base_url()?>publik/profil/<?=encode_url($b->x)?>"><?=$b->judul?></a></li>
                  <?php endforeach; ?>
             
              <!-- <li class="drop-down"><a href="#">Drop Down 2</a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              -->
            </ul>
          </li>
          <!-- <li><a href="#about">About Us</a></li>
          <li><a href="#services">Services</a></li>
          <li><a href="#portfolio">Portfolio</a></li>
          <li><a href="#team">Team</a></li> -->

         
          <li class="drop-down <?=is_active('anggaran')?>"><a href="#">Anggaran</a>
            <ul>
            <?php $i = $this->M_portalsz->get_anggaran();
                                 foreach($i AS $j):?>
                                  <li><a href="<?=base_url()?>publik/anggaran/<?=encode_url($j->id)?>"><?=$j->nama?></a></li>

                  <?php endforeach; ?>
            </ul>
          </li>
          <li class="drop-down <?=is_active('program')?>"><a href="#">Program Kegiatan</a>
            <ul>
            <?php $k = $this->M_portalsz->get_program();
                                 foreach($k AS $l):?>
                                  <li><a href="<?=base_url()?>publik/program/<?=encode_url($l->id)?>"><?=$l->nama?></a></li>

                  <?php endforeach; ?>
            </ul>
          </li>
          <li class="drop-down"><a href="#">Pengadaan</a>
            <ul>
            <?php $m = $this->M_portalsz->get_pengadaan();
                                 foreach($m AS $n):?>
                                  <li><a href="<?=base_url()?>publik/pengadaan/<?=encode_url($n->id)?>"><?=$n->nama?></a></li>

                  <?php endforeach; ?>
             
            </ul>
          </li>

          <li class="drop-down"><a href="#">JDIH</a>
            <ul>
              <li><a target="BLANK" href="https://jdih.jatengprov.go.id/">JDIH Jawa Tengah</a></li>
              <?php $o = $this->M_portalsz->get_regulasi();
                                 foreach($o AS $p):?>
                                  <li><a href="<?=base_url()?>publik/regulasi/<?=encode_url($p->id)?>"><?=$p->nama?></a></li>

                  <?php endforeach; ?>
            </ul>
          </li>

          <!-- <li class="drop-down"><a href="#">Download</a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
            </ul>
          </li> -->

          <!-- <li class="drop-down"><a href="#">PPID</a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
            </ul>
          </li> -->

          <li><a href="<?=base_url('publik/download')?>">Download</a></li>

          <li><a href="<?=base_url('ppid')?>">PPID</a></li>


          <li><a href="#contact">Kontak</a></li>

          <?php 
          $custom = get_menu();
          foreach($custom AS $vv):
          ?>
          <li><a href="<?=base_url()?>publik/custom/<?=encode_url($vv->id)?>"><?=$vv->judul?></a></li>
          <?php endforeach; ?>

        
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->