<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url('dashboard')?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>KBP</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>KESBANGPOL</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <li class="dropdown user user-menu">
          
            <a href="<?php echo base_url() ?>portal/logout/">
              <img src="<?php echo base_url() ?>assets/jateng.png" class="user-image" alt="User Image">
              <span class="hidden-xs">Logout</span>
            </a>
            
          </li>
          
        </ul>
      </div>
    </nav>
  </header>
