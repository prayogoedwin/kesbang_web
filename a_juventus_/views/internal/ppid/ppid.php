  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard 
        <small>Kelola Profil PPID</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 col-xs-12">

        <div class="box">
        <div class="box-header">
        <a href="<?=base_url()?>kelolappid/tambah"><button type="button" class="btn btn-sm btn-primary">Tambah Data</button></a>
        </div>

            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Judul</th>
                  <th>File</th>
                  <th>Tanggal</th>
                  <th>Aksi</th>

                </tr>
                </thead>
                <tbody>
                <?php
                $no = 0;
                foreach($alldata as $all):
                $no++;
                ?>

                  <td><?=$no?></td>
                  <td><?=filter_output($all->judul)?></td>
                  <td><?=filter_output($all->file)?></td>
                  <td><?=date('d-m-Y', strtotime($all->date))?></td>


                <td>
                 <a href="<?=base_url()?>kelolappid/edit/<?=encode_url($all->id)?>"><button type="button" class="btn btn-sm btn-warning"><i class="fa fa-pencil" style="font-size:12px"></i></button></a>

                  <a href="<?=base_url()?>kelolappid/hapus/<?=encode_url($all->id)?>"><button onclick="return confirm('Anda ingin menghapus ?')" type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash" style="font-size:12px"></i></button></a></td>

                </tr>
                <?php endforeach;?>

              </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->



        </div>
    </div>
    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>

  <script>
$(document).ready(function() {
    $('#example2').DataTable()
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        
        buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            'pdfHtml5'
        ]
    } );
} );
</script>
  </body>
</html>