<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Edit Menu</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('menu/aksi_edit');  ?>
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Judul</label>

                  <div class="col-sm-10">
                    <input type="hidden" required name="id" class="form-control" id="inputEmail3" placeholder="Usermame" value="<?=$id?>">
                    <input type="text" required name="judul" class="form-control" id="inputEmail3" placeholder="Judul" value="<?=$judul?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tipe</label>
                  <div class="col-sm-10">
                   <select name="tipe" id=""  class="form-control">
                    <option value="<?=$tipe?>"><?=$tipe?></option>
                    <option value="Utama">Utama</option>
                    <option value="PPID">PPID</option>
                   </select>
                  </div>
                </div>

                <div class="form-group" hidden>
                  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Berita</label>

                  <div class="col-sm-10">
                    <input type="date"  name="tanggal" value="<?=$tanggal?>" class="form-control" id="inputEmail3" placeholder="File">
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Deskripsi</label>

                  <div class="col-sm-10">
                    <textarea class="form-control" name="isi" style="height:200px">  <?=$isi?> </textarea>
                    <script>
                        CKEDITOR.replace( 'isi' );
                </script>
                    
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Gambar</label>

                  <div class="col-sm-10">
                    <input type="file"  name="filez" class="form-control" id="inputEmail3" placeholder="File">
                    <iframe src = "<?=base_url($deskripsi)?>" width='500' height='400' allowfullscreen></iframe>
                    
                  </div>
                </div>   
                
                            
                <!-- <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Level</label>

                  <div class="col-sm-10">
                  <select class="form-control select2" style="width: 100%;">
                  <option>Pilih Level User</option>
                  <option>Super Admionistrator</option>
                  <option>Administrator</option>
                  <option>Updater</option>
                  
                </select>
                  </div>
                </div> -->
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Edit</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>


  <?php include(__DIR__ . "/../template/footer.php"); ?>
  </body>
</html>