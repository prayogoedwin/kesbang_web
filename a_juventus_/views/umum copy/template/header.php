<body class="home4 header4">


<div id="wrapper">
<header id="header" class="site-header">
	<div class="top-header">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<nav class="top-nav">
                  <div class="top-title-nav"><a href="https://www.prakerja.go.id/" target="_blank">Daftar Kartu Prakerja</a></div>
                  <div class="top-title-nav"><a href="https://www.youtube.com/watch?v=s0WKWL0dH-I" target="_blank">Tutorial Daftar Kartu Prakerja</a></div>
					</nav> 
				</div>

				<div class="col-md-6">
					<div class="top-right">
						<div class="header-info">
							<ul class="header-social">
								<li><a href="https://www.facebook.com/nakertrans.jateng" title="facebook" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="https://twitter.com/disnaker_jateng" title="Twitter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="https://www.instagram.com/nakertrans.provjateng/" title="Instagram" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
								<li><a href="https://www.youtube.com/channel/UCWCuB6H5TwwAV64PCOdobtA" title="Youtube" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
							</ul>
						</div><!-- .header-info -->
					</div><!-- .top-right -->
				</div>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- .top-header -->   <div class="mid-header">
      <div class="container">
         <nav class="main-menu">
            <span class="mobile-btn"><i class="fa fa-bars"></i></span>
            <ul>
               <li class=""><a href="<?=base_url()?>">Home</a></li>
               <li class=""><a href="#">PROFIL</a>
                  <ul class="sub-menu">
                  <?php $a = $this->M_portalsz->get_profil();
                     foreach($a AS $b):
                  ?>
                     <li><a href="<?=base_url()?>publik/profil/<?=encode_url($b->x)?>"><?=$b->judul?></a></li>
                  <?php endforeach; ?>

                  <?php $c = $this->M_portalsz->get_struktur();
                     foreach($c AS $d):?>
                     <li><a href="<?=base_url()?>publik/struktur/<?=encode_url($d->x)?>"><?=$d->judul?></a></li>
                  <?php endforeach; ?>
                  
                  <li><a href="<?=base_url()?>publik/pejabat">Profil Pejabat Struktural</a></li>
                  <li><a href="<?=base_url()?>publik/daftar/bidang">Data Bidang</a></li>
                  <li><a href="<?=base_url()?>publik/daftar/uptd">UPTD</a></li>
                  </ul>
               </li>

				<li class=""><a href="#">Regulasi</a>
                <ul class="sub-menu">
                <?php $e = $this->M_portalsz->get_regulasi();
                  foreach($e AS $f):
                  ?>
						<li><a href="<?=base_url()?>publik/kategori/regulasi/<?=$f->url?>/<?=encode_url($f->id)?>"><?=$f->nama?></a></li>
               <?php endforeach; ?>
               </ul>
            </li>

            <li class=""><a href="#">Publikasi</a>
                <ul class="sub-menu">
                <li class=""><a href="<?=base_url()?>publik/downloads">Download Materi</a></li>
                <li class=""><a href="<?=base_url()?>publik/info/berita/">Berita</a></li>
                <li class=""><a href="<?=base_url()?>publik/info/pengumuman/">Pengumuman</a></li>
                <li class=""><a href="<?=base_url()?>publik/kategori/regulasi/du/7">Data Umum</a></li>
                <li class=""><a href="<?=base_url()?>publik/kategori/regulasi/umk/<?=encode_url('8')?>">UMK</a></li>
               </ul>
            </li>

            <li class=""><a href="<?=base_url('ppid')?>">PPID</a></li>
          
			   
			   <li class=""><a href="#">Galeri</a>
                <ul class="sub-menu">
					<li class=""><a href="<?=base_url()?>publik/foto/">Galeri Kegiatan</a></li>
               <li class=""><a href="<?=base_url()?>publik/infografis/">Galeri Infografis</a></li>
					<li><a href="<?=base_url()?>publik/video">Galeri Video</a></li>
                </ul>
            </li>
			   <li class=""><a href="http://data.jatengprov.go.id/organization/dinas-tenaga-kerja-dan-transmigrasi-provinsi-jawa-tengah">OpenData </a></li>
            
            <li class=""><a href="#">Kontak</a>
               <ul class="sub-menu">
					<li class=""><a href="#footer">Kontak Kami</a></li>
					<?php $m = $this->M_portalsz->get_form();
                  foreach($m AS $n):
                  ?>
                  <li><a href="<?=base_url()?>publik/form/<?=encode_url($n->x)?>"><?=$n->judul?></a></li>
               <?php endforeach; ?>
               </ul>
            </li>
            </ul>
         </nav>

         <div class="site-brand">
            <a class="logo" href="index.html">
               <img src="<?=base_url('assets/theme/img/logo4.png')?>" alt="Dinas Perhubungan Provinsi Jawa Tengah" />
            </a>
         </div>
      </div>
   </div>
</header>