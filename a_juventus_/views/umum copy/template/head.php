<!DOCTYPE html>
<html class="no-js" lang="ID">
<head>
<title>Disnakertrans</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Disnakertrans merupakan dinas yang memiliki tugas dan fungsi menyelenggarakan transportasi di Provinsi Jawa Tengah"/>
<meta name="robots" content="index, follow">
<meta name="author" content="Disnakertrans Provinsi Jawa Tengah">
<meta name="keywords" content="disnaker,jateng, prov , jawa tengah ,disnakertrans,provinsi ">
<meta name="author" content="Disnakertrans Provinsi Jawa Tengah">
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="Copyright" content="Disnakertrans Provinsi Jawa Tengah">
<meta name="revisit-after" content="3">
<meta name="webcrawlers" content="all">
<meta name="rating" content="general">
<meta name="spiders" content="all">
<meta property="og:locale" content="id_ID"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Disnakertrans Provinsi Jawa Tengah"/>
<meta property="og:description" content="Disnakertrans"/>
<meta property="og:site_name" content="Disnakertrans Provinsi Jawa Tengah"/>
<meta property="og:image" content="<?=base_url()?>assets/upload/disnaker.jpg"/>
<meta property="og:image:width" content="800"/>
<meta property="og:image:height" content="500"/>


<link href="https://fonts.googleapis.com/css?family=Montserrat:800" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
<link rel="icon" href="<?=base_url()?>assets/theme/img/icon.png" type="image/png">
<link href="<?=base_url()?>assets/theme/css/style.css" rel="stylesheet">
<link href="<?=base_url()?>assets/theme/css/jquery.fancybox.css" rel="stylesheet">
<link href="<?=base_url()?>assets/theme/css/hint.css" rel="stylesheet">
<link href="<?=base_url()?>assets/theme/css/responsive.css" rel="stylesheet">
<link href="<?=base_url()?>assets/admin/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<script type="text/javascript" src="<?=base_url()?>assets/theme/js/jquery-1.11.3.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>



</head>

<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.10&appId=164580804102586";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>