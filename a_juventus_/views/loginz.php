<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">



    <link rel="icon" href="<?=base_url()?>assets/theme/img/icon.png" type="image/png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <title>Login</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light navbar-laravel">
    <div class="container">
    <a class="navbar-brand" href="#">KESBANGPOL</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?=base_url('portal/login')?>">Login</a>
            </li>
           
           
            
             <li class="nav-item">
                <a class="nav-link" href="<?=base_url()?>">Home</a>
            </li>
            
        </ul>

    </div>
    </div>
</nav>

<main class="my-form">
    <div class="cotainer">
        <div class="row justify-content-center">
            <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Login</div>
                        <div class="card-body">
                            <?php
                                $info = $this->session->flashdata('info');
                                $pesan = $this->session->flashdata('message');
                                
                                 ?>
                                
                               
                                
                                   <?php echo form_open('portal/cek_login');   
                                  
                                  if( $info == 'danger'){ ?>
                                      
                                      <span style="color:red"><?=$pesan?> </span>
                                     
                                  <?php } ?>
                                  
                                
                                  
                                  <?php if( $info == 'success'){ ?>
                                      
                                      <span style="color:green"><?=$pesan?> </span>
                                     
                                  <?php } ?>
          

                                    <div class="form-group row">
                                    <label for="full_name" class="col-md-4 col-form-label text-md-right">Email/Username</label>
                                    <div class="col-md-6">
                                        <input type="text" id="full_name" class="form-control" name="username" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email_address" class="col-md-4 col-form-label text-md-right">Password</label>
                                    <div class="col-md-6">
                                        <input type="password" id="email_address" class="form-control" name="password" required>
                                    </div>
                                </div>

                                
                              
                                 
                        
                                
                                <div class="form-group row">
                                    <label for="user_name" class="col-md-4 col-form-label text-md-right">Captcha</label>
                                    <div class="col-md-6">
                                    <input id="satu" style="width:40px"  readonly name="satu" value="<?php echo (rand(1,9)); ?>" > + 
                                    <input style="width:40px" id="dua" readonly name="dua" value="<?php echo (rand(1,9)); ?>" > =
                                    <input style= "width:128px" name="jawaban"  type="number" placeholder="Hasil" required >
                                    </div>
                                </div>
     
                                <div class="form-group row">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                        Login
                                        </button>
                                    </div>
                                </div>
                           <?php echo form_close(); ?>
                        </div>
                       
                    </div>
            </div>
        </div>
    </div>

</main>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>
</html>