<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
        // is_blok($this->input->ip_address());
		$this->load->model('M_log');
		$this->load->model('M_cek');
        
	}
	
	public function index()
	{
		// is_blok($this->input->ip_address());
		$this->login();
	}

	
	public function login()
	{
		$this->load->view('loginz');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(site_url('publik'));
	}

	public function cek_login(){

		$ip = $this->input->ip_address();
		$userp = $this->input->post('username', TRUE);
		$passp = $this->input->post('password', TRUE);

		if(cek_ip_is_gagal_3($ip)){
			redirect('https://polri.go.id/');
		}elseif(cek_email_is_gagal_3($userp)){
			redirect('https://polri.go.id/');
		}else{
			if (filter_input_c($userp) == '1' || filter_input_c($passp) == '1' ){
				$datablok = array(
					'ips'        => $ip,
					'created_at' => date('Y-m-d H:i:s')
				);
				$this->M_log->addblok($datablok);
				redirecting();
			}else{
				$this->load->model('M_cek');
				$login = $this->M_cek->login($userp, sha1(md5($passp)));
				if($login){
					$datalog = array(
						'emaily'     => $userp,
						'passy'      => sha1(md5($passp)),
						'ips'        => $ip,
						'status'	 => 'Y',
						'created_at' => date('Y-m-d H:i:s')
					);
					$this->M_log->addlog($datalog);
					$this->session->set_userdata('status', 1);
					$this->session->set_userdata('username', $userp);
					$this->session->set_userdata('tanggal', date('Y-m-d H:i:s'));
					$this->session->set_flashdata('info', 'success');
					$this->session->set_flashdata('message', 'Selamat, login berhasil');
					redirect(site_url('dashboard'));
				}else{
					$datalog = array(
						'emaily'     => $userp,
						'passy'      => sha1(md5($passp)),
						'ips'        => $ip,
						'status'	 => 'X',
						'created_at' => date('Y-m-d H:i:s')
					);
					$this->M_log->addlog($datalog);
					$this->session->set_flashdata('info', 'danger');
					$this->session->set_flashdata('message', 'Maaf, login gagal');
					redirect(site_url('publik'));
				}
			
			}


		}
					
	}
}
