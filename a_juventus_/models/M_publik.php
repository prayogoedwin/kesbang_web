<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_publik extends CI_Model{


    function __construct()
    {
        parent::__construct();
    }


        // get all
        function get_regulasi()
        {
           
            return $this->db->get('turin_regulasi')->result();
        }
    
        // get all
        function get_program()
        {

           
            return $this->db->get('turin_program')->result();
        }

        // get all
        function get_uptd()
        {

           
            return $this->db->get('turin_uptd')->result();
        }

        // get all
        function get_data_by_uptd($id)
        {
            $this->db->where('id_uptd', $id);
            return $this->db->get('turin_uptd_data')->result();
        }

        // get all
        function get_data_by_perencanaan($id)
        {
            $this->db->where('id_perencanaan', $id);
            return $this->db->get('turin_perencanaan_data')->result();
        }

        // get all
        function get_data_by_pengadaan($id)
        {
            $this->db->where('id_pengadaan', $id);
            return $this->db->get('turin_pengadaan_data')->result();
        }
    

        // get all
        function get_bidang()
        {

           
            return $this->db->get('turin_bidang')->result();
        }

        // get all
        function get_data_by_bidang($id)
        {

            $this->db->where('id_bidang', $id);
            return $this->db->get('turin_bidang_data')->result();
        }
    
    
        // get all
        function get_renstra()
        {

           
            return $this->db->get('turin_renstra')->result();
        }
    
         // get all
         function get_anggaran()
         {

    
             return $this->db->get('turin_anggaran')->result();
         }
    
        
         function get_profil()
         {

             $this->db->select('id as x, judul');
             return $this->db->get('turin_profil')->result();
         }
    
         // get all
         function get_struktur()
         {

             $this->db->select('id as x, judul');
             return $this->db->get('turin_profil_struktur')->result();
         }
    
         // get all
         function get_form()
         {

             $this->db->select('id as x, judul');
             return $this->db->get('turin_form')->result();
         }

         // get all
    function get_sop()
    {
        // $this->db->order_by('id');
        return $this->db->get('turin_sop')->result();
    }

    // get all
    function get_perencanaan()
    {
        // $this->db->order_by('id');
        return $this->db->get('turin_perencanaan')->result();
    }

    // get all
    function get_pengadaan()
    {
        // $this->db->order_by('id');
        return $this->db->get('turin_pengadaan')->result();
    }

    // get all
    function get_sumber()
    {
        // $this->db->order_by('id');
        return $this->db->get('turin_pengadaan')->result();
    }

    // get all
    function get_informasi()
    {
        // $this->db->order_by('id');
        return $this->db->get('turin_informasi')->result();
    }

    // get all
    function get_laporan()
    {
        // $this->db->order_by('id');
        return $this->db->get('turin_laporan')->result();
    }

    function get_all_berita()
    {
        $this->db->order_by('id', 'DESC');
        return $this->db->get('turin_berita')->result();
    }

    function get_all_berita_atas()
    {
        $this->db->order_by('id', 'DESC');
        $this->db->limit('5');
        return $this->db->get('turin_berita')->result();
    }

    function get_all_berita_bawah()
    {
        $this->db->order_by('id', 'DESC');
        $this->db->limit('8');
        return $this->db->get('turin_berita')->result();
    }

    function get_berita_by($tipe)
    {
        $this->db->where('tipe', $tipe);
        $this->db->order_by('id', 'DESC');
        return $this->db->get('turin_berita')->result();
    }

    function get_by_id_berita($id)
    {
    
        $this->db->where('id', $id);
        return $this->db->get('turin_berita')->row();
    }

    function get_all_regulasi($id_regulasi, $limit)
    {
    
        $this->db->where('id_regulasi', $id_regulasi);
        $this->db->order_by('id', 'DESC');
        if($limit != 0){
        
            $this->db->limit($limit);
        }
        return $this->db->get('turin_regulasi_data')->result();
    }

    function get_by_id_regulasi($id)
    {
        
        $this->db->where('id', $id);
        return $this->db->get('turin_regulasi_data')->row();
    }

    function get_all_kontak()
    {
        
        $this->db->order_by('id', 'DESC');
        return $this->db->get('turin_kontak')->result();
    }

    function get_all_slide()
    {
        $this->db->where('ppid', 0);
        $this->db->order_by('id', 'M_publik');
        return $this->db->get('turin_slide')->result();
    }


    function get_all_slide_ppid()
    {
        $this->db->where('ppid', 1);
        $this->db->order_by('id', 'DESC');
        return $this->db->get('turin_slide')->result();
    }


    function get_all_linkz()
    {
        // $this->db->order_by('id', 'DESC');
        return $this->db->get('turin_links')->result();
    }

    function get_download()
    {
        // $this->db->order_by('id', 'DESC');
        return $this->db->get('turin_download')->result();
    }

    function get_download_id($id)
    {
        
        $this->db->where('id', $id);
        return $this->db->get('turin_download')->row();
    }

    function get_all_galeri()
    {
        
        $this->db->order_by('id', 'DESC');
        return $this->db->get('turin_galeri')->result();
    }

    function get_by_id_galeri($id)
    {
        $this->db->where('id_galeri', $id);
        return $this->db->get('turin_galeri_data')->result();
    }

    function get_by_id_foto($id)
    {
        
        $this->db->where('id', $id);
        return $this->db->get('turin_galeri_data')->row();
    }




    function get_all_galeri_beranda()
    {
        $this->db->limit('6');
        $this->db->order_by('id', 'DESC');
        return $this->db->get('turin_galeri')->result();
    }

    function get_all_infografis()
    {
        $this->db->order_by('id', 'DESC');
        $this->db->limit('3');
        return $this->db->get('turin_infografis')->result();
    }

    function get_by_id_infografis($id)
    {
        
        $this->db->where('id', $id);
        return $this->db->get('turin_infografis')->row();
    }



    function get_all_video($limit)
    {
        
        $this->db->order_by('id', 'DESC');
        if($limit != 0){
            $this->db->limit($limit);
        }
        return $this->db->get('turin_galeri_video')->result();
    }


    function get_by_jenis($uri3, $uri5){  
        $tabel = NULL; 
        $tabel = get_nama($uri3);
        $where = 'id_'.$uri3;

        if($tabel == 'turin_maklumat_lhkpn'){
            if($uri3 == 'maklumat'){
                $this->db->where('tipe', 'M');
            }elseif($uri3 == 'lhkpn'){
                $this->db->where('tipe', 'L');
            }else{
                $this->db->where('tipe', 'L2');
            }
            
        }
        if($uri5 != ''){
            $this->db->where($where, $uri5);
        }
        $this->db->order_by('id', 'DESC');
        return $this->db->get($tabel)->result();
    }

    function get_by_jenis_tahun($uri3, $uri5, $tahun){  
        $tabel = NULL; 
        $tabel = get_nama($uri3);
        $where = 'id_'.$uri3;

        if($tabel == 'turin_maklumat_lhkpn'){
            if($uri3 == 'maklumat'){
                $this->db->where('tipe', 'M');
            }elseif($uri3 == 'lhkpn'){
                $this->db->where('tipe', 'L');
                $this->db->where('tahun', $tahun);
            }else{
                $this->db->where('tipe', 'L2');
                $this->db->where('tahun', $tahun);
            }
            
        }
        if($uri5 != ''){
            $this->db->where($where, $uri5);
        }
        $this->db->order_by('id', 'DESC');
        return $this->db->get($tabel)->result();
    }

    function get_pejabat_tab($tab){  
        $this->db->where('tab', $tab);
        return $this->db->get('turin_profil_pejabat')->result();
    }

    function get_by_id($uri, $id){    
        $tabel = get_nama($uri);
        $this->db->where('id', $id);
        return $this->db->get($tabel)->row();
    }

    function get_category($uri3, $uri4){   
        $where = 'id_'.$uri3;
        $tabel = 'turin_'.$uri3.'_data';
        $this->db->where($where, $uri4);
        return $this->db->get($tabel)->result();
    }

    

    

    

}

/* End of file Tbl_banner_model.php */
/* Location: ./application/models/Tbl_banner_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-21 07:00:03 */
/* http://harviacode.com */
