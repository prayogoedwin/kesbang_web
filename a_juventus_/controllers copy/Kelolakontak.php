<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class kelolakontak extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_blok($this->input->ip_address());
        is_login();
        $this->load->model('M_kelolakontak');
        $this->load->model('M_log');
        
    }

    public function index()
	{
        $data['alldata'] = $this->M_kelolakontak->get_all();
        $this->load->view('internal/template/head');
        $this->load->view('internal/template/header');
        $this->load->view('internal/template/sidebar');
        $this->load->view('internal/kontak/kontak',$data);
	}



    public function tambah()
    {
        $this->load->view('internal/template/head');
        $this->load->view('internal/template/header');
        $this->load->view('internal/template/sidebar');
        $this->load->view('internal/kontak/tambah');
    }

    public function aksi_tambah()
    { 

            $ip = $this->input->ip_address();
            $judul = $this->input->post('judul', TRUE);
            $deskripsi = $this->input->post('deskripsi', TRUE);

            if (filter_input_c($judul) == '1' || filter_input_c($deskripsi) == '1' ){
                $datablok = array(
                    'ips'        => $ip,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->M_log->addblok($datablok);
                $this->session->sess_destroy();
			    redirecting();
            }else{
                    $data = array(
                    'judul'  => $judul,
                    'deskripsi'  => $deskripsi,
                    'created_by' => get_id_user($this->session->userdata('username')),
                    'created_at' => date('Y-m-d H:i:s')
                    );
                    $add = $this->M_kelolakontak->insert($data);
                    if($add) {
                        $this->session->set_flashdata('info', 'success');
                        $this->session->set_flashdata('message', 'selamat, berhasil ');
                        redirect(site_url('kelolakontak'));
                    }else{
                        $this->session->set_flashdata('info', 'danger');
                        $this->session->set_flashdata('message','Gagal');
                        redirect(site_url('kelolakontak'));
                    }

            }
    }

    public function edit($i)
    {
        $id = decode_url($i);
                $row = $this->M_kelolakontak->get_by_id($id);
                if ($row) {
                    $data = array(
                        'id'       => set_value('id', $row->id),
                        'judul' => set_value('judul', $row->judul),
                        'deskripsi' => set_value('deskripsi', $row->deskripsi),
                        );
                        $this->load->view('internal/template/head');
                        $this->load->view('internal/template/header');
                        $this->load->view('internal/template/sidebar');
                        $this->load->view('internal/kontak/edit', $data);
                    
                
                } else {
                    $this->session->set_flashdata('info', 'danger');
                    $this->session->set_flashdata('message','Gagal');
                    redirect(site_url('kelolakontak'));
                }

            
        
    }

    public function aksi_edit()
    {
            $ip = $this->input->ip_address();
            $judul = $this->input->post('judul', TRUE);
            $deskripsi = $this->input->post('deskripsi', TRUE);

            if (filter_input_c($userp) == '1'){
                $datablok = array(
                    'ips'        => $ip,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->M_log->addblok($datablok);
                $this->session->sess_destroy();
                redirecting();
                
            }else{

                $id = $this->input->post('id',TRUE);
                $update = array(
                    'judul'  => $judul,
                    'deskripsi'  => $deskripsi,
                );
                $data = $this->M_kelolakontak->update($id, $update);
                if($data){
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Berhasil Edit ');
                redirect(site_url('kelolakontak/edit/'.encode_url($id)));
                }else{
                    $this->session->set_flashdata('info', 'danger');
                    $this->session->set_flashdata('message','Gagal');
                    redirect(site_url('kelolakontak/edit/'.encode_url($id)));
                }
            }
          
    }

    public function hapus($i)
    {
        $id = decode_url($i);
        $this->load->helper("file");
        $row = $this->M_kelolakontak->get_by_id($id);
        if ($row) {
            $this->M_kelolakontak->delete($id);
            $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'selamat, berhasil ');
                redirect(site_url('kelolakontak'));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Gagal ');
            redirect(site_url('kelolakontak'));
        }
    }

   


}

/* End of file Banner.php */
/* Location: ./application/controllers/Banner.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-21 07:00:03 */
/* http://harviacode.com */
