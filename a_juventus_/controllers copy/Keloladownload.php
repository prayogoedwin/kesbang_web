<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Keloladownload extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_blok($this->input->ip_address());
        is_login();
        $this->load->model('M_keloladownload');
        $this->load->model('M_log');
        
    }

    public function index()
	{
        $data['alldata'] = $this->M_keloladownload->get_all();
        $this->load->view('internal/template/head');
        $this->load->view('internal/template/header');
        $this->load->view('internal/template/sidebar');
        $this->load->view('internal/download/download',$data);
	}



    public function tambah()
    {
        $this->load->view('internal/template/head');
        $this->load->view('internal/template/header');
        $this->load->view('internal/template/sidebar');
        $this->load->view('internal/download/tambah');
    }

    public function aksi_tambah(){

        $ip = $this->input->ip_address();
        $judul = $this->input->post('judul', TRUE);

        if (filter_input_c($judul) == '1'){
            $datablok = array(
                'ips'        => $ip,
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->M_log->addblok($datablok);
            $this->session->sess_destroy();
            redirecting();

        }else{
                //image
                ini_set('max_execution_time', 1000);
                $filename = null;
                $config['upload_path']   = './assets/upload/download/' . date('m_Y');
                $config['allowed_types'] = 'pdf';
                $config['max_size'] = max_upload();
                $config['encrypt_name'] = TRUE;

                if (!file_exists($config['upload_path'])) {
                    mkdir($config['upload_path'], 0777, true);
                }

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('filez') || empty($_FILES['filez']['name'])) {
                    if (!empty($_FILES['filez']['name'])) {
                        $upload = $this->upload->data();
                        $filename = '/assets/upload/download/' . date('m_Y') .'/' .$upload['file_name'];
                    }
                    $data = array(
                       
                        'judul'	=> $judul,
                        'file'  => $upload['file_name'],
                        'deskripsi' => $filename,
                        'created_by' => get_id_user($this->session->userdata('username')),
                        'created_at' => date('Y-m-d H:i:s')
                    );

                    $add = $this->M_keloladownload->insert($data);
                    if($add) {
                        $this->session->set_flashdata('info', 'success');
                        $this->session->set_flashdata('message', 'selamat, berhasil ');
                        redirect(site_url('keloladownload'));
                    }else{
                        $this->session->set_flashdata('info', 'danger');
                        $this->session->set_flashdata('message','Gagal');
                        redirect(site_url('keloladownload'));
                    }
                } else {
                    if (($_FILES['file']['size']/1000)> 500) {
                        $this->session->set_flashdata('info', 'danger');
                        $this->session->set_flashdata('message','Gagal, ukuran file melebihi 500KB');
                        redirect(site_url('keloladownload'));
                        
                    } else {
                        $this->session->set_flashdata('info', 'danger');
                        $this->session->set_flashdata('message','Gagal');
                        redirect(site_url('keloladownload'));
                    }
                }
        }
	}

    

    public function edit($i)
    {
        $id = decode_url($i);
                $row = $this->M_keloladownload->get_by_id($id);
                if ($row) {
                    $data = array(
                        'id'       => set_value('id', $row->id),
                        'judul' => set_value('judul', $row->judul),
                        'file' => set_value('file', $row->file),
                        'deskripsi' => set_value('deskripsi', $row->deskripsi),
                        );
                        $this->load->view('internal/template/head');
                        $this->load->view('internal/template/header');
                        $this->load->view('internal/template/sidebar');
                        $this->load->view('internal/download/edit', $data);
                    
                
                } else {
                    $this->session->set_flashdata('info', 'danger');
                    $this->session->set_flashdata('message','Gagal');
                    redirect(site_url('keloladownload'));
                } 
    }

    public function aksi_edit()
    {
        
            $ip = $this->input->ip_address();
            $judul = $this->input->post('judul', TRUE);

            if (filter_input_c($judul) == '1'){
                $datablok = array(
                    'ips'        => $ip,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->M_log->addblok($datablok);
                $this->session->sess_destroy();
                redirecting();

            }else{
                if(empty($_FILES['filez']['name'])){
                    $id = $this->input->post('id',TRUE);
                    $update = array(
                        'judul'	=> $judul,
                    );
                    $data = $this->M_keloladownload->update($id, $update);
                    if($data){
                    $this->session->set_flashdata('info', 'success');
                    $this->session->set_flashdata('message', 'Berhasil Edit ');
                    redirect(site_url('keloladownload/edit/'.encode_url($id)));
                    }else{
                        $this->session->set_flashdata('info', 'danger');
                        $this->session->set_flashdata('message','Gagal');
                        redirect(site_url('keloladownload/edit/'.encode_url($id)));
                    }
                }else{
                    //image
                        ini_set('max_execution_time', 1000);
                        $filename = null;
                        $config['upload_path']   = './assets/upload/download/' . date('m_Y');
                        $config['allowed_types'] = 'jpg|png|jpeg';
                        $config['max_size'] = max_upload();
                        $config['encrypt_name'] = TRUE;

                        if (!file_exists($config['upload_path'])) {
                            mkdir($config['upload_path'], 0777, true);
                        }
                        $this->load->library('upload', $config);
                        if ($this->upload->do_upload('filez') || empty($_FILES['filez']['name'])) {
                            if (!empty($_FILES['filez']['name'])) {
                                $upload = $this->upload->data();
                                $filename = '/assets/upload/download/' . date('m_Y') .'/' .$upload['file_name'];
                            }
                            $update = array(
                                'judul'	=> $judul,
                                'file'  => $upload['file_name'],
                                'deskripsi' => $filename,
                            );
                            $id = $this->input->post('id',TRUE);
                            $data = $this->M_keloladownload->update($id, $update);
                            if($data) {
                                $this->session->set_flashdata('info', 'success');
                                $this->session->set_flashdata('message', 'selamat, berhasil ');
                                redirect(site_url('keloladownload/edit/'.encode_url($id)));
                            }else{
                                $this->session->set_flashdata('info', 'danger');
                                $this->session->set_flashdata('message','Gagal');
                                redirect(site_url('keloladownload/edit/'.encode_url($id)));
                            }
                        } else {
                            if (($_FILES['file']['size']/1000)> 500) {
                                $this->session->set_flashdata('info', 'danger');
                                $this->session->set_flashdata('message','Gagal, ukuran file melebihi 500KB');
                                redirect(site_url('keloladownload/edit/'.encode_url($id)));
                                
                            } else {
                                $this->session->set_flashdata('info', 'danger');
                                $this->session->set_flashdata('message','Gagal');
                                redirect(site_url('keloladownload/edit/'.encode_url($id)));
                            }
                        }
             }


            }
    }

    public function hapus($i)
    {
        $id = decode_url($i);
        $this->load->helper("file");
        $row = $this->M_keloladownload->get_by_id($id);
        if ($row) {
            $this->M_keloladownload->delete($id);
            $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'selamat, berhasil ');
                redirect(site_url('keloladownload'));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Gagal ');
            redirect(site_url('keloladownload'));
        }
    }

    



}

/* End of file Banner.php */
/* Location: ./application/controllers/Banner.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-21 07:00:03 */
/* http://harviacode.com */
