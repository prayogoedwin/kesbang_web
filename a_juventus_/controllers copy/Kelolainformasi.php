<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kelolainformasi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_blok($this->input->ip_address());
        is_login();
        $this->load->model('M_kelolainformasi');
        $this->load->model('M_log');
        
    }

    public function index()
	{
        $uri = $this->uri->segment('3');
        $data['alldata'] = $this->M_kelolainformasi->get_all($uri);
        $this->load->view('internal/template/head');
        $this->load->view('internal/template/header');
        $this->load->view('internal/template/sidebar');
        $this->load->view('internal/informasi/informasi',$data);
    }


    public function tambah()
    {
        $data['alldata'] = $this->M_kelolainformasi->get_informasi();
        $this->load->view('internal/template/head');
        $this->load->view('internal/template/header');
        $this->load->view('internal/template/sidebar');
        $this->load->view('internal/informasi/tambah', $data);
    }

    public function aksi_tambah(){
       
        $ip = $this->input->ip_address();
        $judul = $this->input->post('judul', TRUE);
        $informasi = $this->input->post('informasi', TRUE);

        $uri = get_id_informasi($informasi);

        if (filter_input_c($judul) == '1'){
            $datablok = array(
                'ips'        => $ip,
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->M_log->addblok($datablok);
            $this->session->sess_destroy();
            redirecting();

        }else{
                //image
                ini_set('max_execution_time', 1000);
                $filename = null;
                $config['upload_path']   = './assets/upload/informasi/' . date('m_Y');
                $config['allowed_types'] = 'pdf';
                $config['max_size'] = max_upload();
                $config['encrypt_name'] = TRUE;

                if (!file_exists($config['upload_path'])) {
                    mkdir($config['upload_path'], 0777, true);
                }

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('filez') || empty($_FILES['filez']['name'])) {
                    if (!empty($_FILES['filez']['name'])) {
                        $upload = $this->upload->data();
                        $filename = '/assets/upload/informasi/' . date('m_Y') .'/' .$upload['file_name'];
                    }
                    $data = array(
                       
                        'judul'	=> $judul,
                        'id_informasi' => $informasi,
                        'file'  => $upload['file_name'],
                        'deskripsi' => $filename,
                        'created_by' => get_id_user($this->session->userdata('username')),
                        'created_at' => date('Y-m-d H:i:s')
                    );

                    $add = $this->M_kelolainformasi->insert($data);
                    if($add) {
                        $this->session->set_flashdata('info', 'success');
                        $this->session->set_flashdata('message', 'selamat, berhasil ');
                        redirect(site_url('kelolainformasi/index/'.$uri));
                    }else{
                        $this->session->set_flashdata('info', 'danger');
                        $this->session->set_flashdata('message','Gagal');
                        redirect(site_url('kelolainformasi/index/'.$uri));
                    }
                } else {
                    if (($_FILES['file']['size']/1000)> 500) {
                        $this->session->set_flashdata('info', 'danger');
                        $this->session->set_flashdata('message','Gagal, ukuran file melebihi 500KB');
                        redirect(site_url('kelolainformasi/index/'.$uri));
                        
                    } else {
                        $this->session->set_flashdata('info', 'danger');
                        $this->session->set_flashdata('message','Gagal');
                        redirect(site_url('kelolainformasi/index/'.$uri));
                    }
                }
        }
	}

    

    public function edit($uri, $i)
    {
        $id = decode_url($i);
                $row = $this->M_kelolainformasi->get_by_id($id);
                if ($row) {
                    $data = array(
                        'id'       => set_value('id', $row->id),
                        'judul' => set_value('judul', $row->judul),
                        'informasi'       => set_value('informasi', $row->id_informasi),
                        'file' => set_value('file', $row->file),
                        'deskripsi' => set_value('deskripsi', $row->deskripsi),
                        'alldata' => $this->M_kelolainformasi->get_informasi(),
                        );
                        $this->load->view('internal/template/head');
                        $this->load->view('internal/template/header');
                        $this->load->view('internal/template/sidebar');
                        $this->load->view('internal/informasi/edit', $data);
                    
                
                } else {
                    $this->session->set_flashdata('info', 'danger');
                    $this->session->set_flashdata('message','Gagal');
                    redirect(site_url('kelolainformasi/index/'.$uri));
                } 
    }

    public function aksi_edit()
    {
            $ip = $this->input->ip_address();
            $judul = $this->input->post('judul', TRUE);
            $informasi = $this->input->post('informasi');

            $uri = get_id_informasi($informasi);

            if (filter_input_c($judul) == '1'){
                $datablok = array(
                    'ips'        => $ip,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->M_log->addblok($datablok);
                $this->session->sess_destroy();
                redirecting();

            }else{
                if(empty($_FILES['filez']['name'])){
                    $id = $this->input->post('id',TRUE);
                    $update = array(
                        'judul'	=> $judul,
                        'id_informasi' => $informasi,
                    );
                    $data = $this->M_kelolainformasi->update($id, $update);
                    if($data){
                    $this->session->set_flashdata('info', 'success');
                    $this->session->set_flashdata('message', 'Berhasil Edit ');
                    redirect(site_url('kelolainformasi/edit/'.$uri.'/'.encode_url($id)));
                    }else{
                        $this->session->set_flashdata('info', 'danger');
                        $this->session->set_flashdata('message','Gagal');
                        redirect(site_url('kelolainformasi/edit/'.$uri.'/'.encode_url($id)));
                    }
                }else{
                    //image
                        ini_set('max_execution_time', 1000);
                        $filename = null;
                        $config['upload_path']   = './assets/upload/kelolainformasi/' . date('m_Y');
                        $config['allowed_types'] = 'pdf';
                        $config['max_size'] = max_upload();
                        $config['encrypt_name'] = TRUE;

                        if (!file_exists($config['upload_path'])) {
                            mkdir($config['upload_path'], 0777, true);
                        }
                        $this->load->library('upload', $config);
                        if ($this->upload->do_upload('filez') || empty($_FILES['filez']['name'])) {
                            if (!empty($_FILES['filez']['name'])) {
                                $upload = $this->upload->data();
                                $filename = '/assets/upload/kelolainformasi/' . date('m_Y') .'/' .$upload['file_name'];
                            }
                            $update = array(
                                'judul'	=> $judul,
                                'id_informasi' => $informasi,
                                'file'  => $upload['file_name'],
                                'deskripsi' => $filename,
                            );
                            $id = $this->input->post('id',TRUE);
                            $data = $this->M_kelolainformasi->update($id, $update);
                            if($data) {
                                $this->session->set_flashdata('info', 'success');
                                $this->session->set_flashdata('message', 'selamat, berhasil ');
                                redirect(site_url('kelolainformasi/edit/'.$uri.'/'.encode_url($id)));
                            }else{
                                $this->session->set_flashdata('info', 'danger');
                                $this->session->set_flashdata('message','Gagal');
                                redirect(site_url('kelolainformasi/edit/'.$uri.'/'.encode_url($id)));
                            }
                        } else {
                            if (($_FILES['file']['size']/1000)> 500) {
                                $this->session->set_flashdata('info', 'danger');
                                $this->session->set_flashdata('message','Gagal, ukuran file melebihi 500KB');
                                redirect(site_url('kelolainformasi/edit/'.$uri.'/'.encode_url($id)));
                                
                            } else {
                                $this->session->set_flashdata('info', 'danger');
                                $this->session->set_flashdata('message','Gagal');
                                redirect(site_url('kelolainformasi/edit/'.$uri.'/'.encode_url($id)));
                            }
                        }
             }


            }
    }

    public function hapus($uri, $i)
    {
        $id = decode_url($i);
        $this->load->helper("file");
        $row = $this->M_kelolainformasi->get_by_id($id);
        if ($row) {
            $this->M_kelolainformasi->delete($id);
            $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'selamat, berhasil ');
                redirect(site_url('kelolainformasi/index/'.$uri));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Gagal ');
            redirect(site_url('kelolainformasi/index/'.$uri));
        }
    }

   



}

/* End of file Banner.php */
/* Location: ./application/controllers/Banner.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-21 07:00:03 */
/* http://harviacode.com */
