-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 20, 2021 at 09:25 AM
-- Server version: 10.3.27-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kesbangpoljateng_web2020`
--

-- --------------------------------------------------------

--
-- Table structure for table `turin_admin_xyz`
--

CREATE TABLE `turin_admin_xyz` (
  `id` int(11) UNSIGNED NOT NULL,
  `emailx` varchar(100) DEFAULT NULL,
  `passx` text DEFAULT NULL,
  `role_akses` int(2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_admin_xyz`
--

INSERT INTO `turin_admin_xyz` (`id`, `emailx`, `passx`, `role_akses`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '+admin_2020', '2d15c23bf213dbb2db3a0481ef45d01fd4ffea0d', NULL, NULL, NULL, '2020-04-19 23:07:04', NULL),
(2, 'roby-2', '67bd54ca07247480b884057db184ee04ee78d4e3', NULL, NULL, '2020-12-30 05:22:03', NULL, NULL),
(3, 'feri-2', 'c9235b5c154d229a37800267179e1e206931586e', NULL, NULL, '2020-12-30 05:23:06', '2021-01-18 09:26:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_aduan`
--

CREATE TABLE `turin_aduan` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` text DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `kota` int(11) DEFAULT NULL,
  `kecamatan` int(11) DEFAULT NULL,
  `kelurahan` varchar(11) DEFAULT NULL,
  `alamat` int(11) DEFAULT NULL,
  `telp` varchar(13) DEFAULT NULL,
  `hp` varchar(13) DEFAULT NULL,
  `isi_aduan` text DEFAULT NULL,
  `file_bukti` text DEFAULT NULL,
  `created_by_ip` varchar(15) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `upated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `turin_anggaran`
--

CREATE TABLE `turin_anggaran` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `url` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_anggaran`
--

INSERT INTO `turin_anggaran` (`id`, `nama`, `url`) VALUES
(1, 'Catatan Atas Laporan Keuangan', 'calk'),
(2, 'Dokumen Pelaksana Anggaran', 'dpa'),
(3, 'Laporan Operasional', 'lo'),
(4, 'Laporan Perubahan Ekuitas', 'lpe'),
(5, 'Laporan Realisasi Anggaran', 'lra'),
(6, 'Neraca', 'n'),
(7, 'Rencana & Realisasi Keuangan', 'rrk'),
(8, 'Rencana Kerja Anggaran', 'rka'),
(9, 'Daftar Aset', 'da'),
(10, 'Notulensi Rakor Pengendalian APBD', 'note'),
(11, 'SPJ Fungsional', 'spj');

-- --------------------------------------------------------

--
-- Table structure for table `turin_anggaran_copy`
--

CREATE TABLE `turin_anggaran_copy` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `url` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_anggaran_copy`
--

INSERT INTO `turin_anggaran_copy` (`id`, `nama`, `url`) VALUES
(1, 'SK Keuangan', 'sk'),
(2, 'Rencana & Laporan Realisasi anggaran', 'rlra'),
(3, 'Neraca', 'inv'),
(4, 'Laporan Arus Kas & CALK', 'calk'),
(5, 'Inventerisasi', 'inv'),
(6, 'DPA', 'dpa'),
(7, 'RKA', 'rka'),
(8, 'RKO', 'rko'),
(9, 'Sumber Dana', 'sd');

-- --------------------------------------------------------

--
-- Table structure for table `turin_anggaran_data`
--

CREATE TABLE `turin_anggaran_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_anggaran` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_anggaran_data`
--

INSERT INTO `turin_anggaran_data` (`id`, `id_anggaran`, `judul`, `deskripsi`, `file`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'CATATAN ATAS LAPORAN KEUANGAN TAHUN 2015', 'assets/upload/anggaran/11_2020/6a53ed5b964afb8d2f2a5d3a6baf4bb7.pdf', '6a53ed5b964afb8d2f2a5d3a6baf4bb7.pdf', 1, '2020-11-20 03:25:43', '2020-11-20 10:11:35', NULL),
(2, 1, 'CATATAN ATAS LAPORAN KEUANGAN TAHUN 2016', 'assets/upload/anggaran/11_2020/b1bddfb49bf385f403319097dfb3032d.pdf', 'b1bddfb49bf385f403319097dfb3032d.pdf', 1, '2020-11-20 03:28:42', '2020-11-20 10:13:49', NULL),
(3, 1, 'CATATAN ATAS LAPORAN KEUANGAN TAHUN 2017', '/assets/upload/anggaran/12_2020/068259978bd74c693efc5d4a97718c46.pdf', '068259978bd74c693efc5d4a97718c46.pdf', 1, '2020-12-30 06:01:15', NULL, NULL),
(4, 1, 'CATATAN ATAS LAPORAN KEUANGAN TAHUN 2018', '/assets/upload/anggaran/12_2020/a7e5cb3b67727a5d9fd377ac68426a10.pdf', 'a7e5cb3b67727a5d9fd377ac68426a10.pdf', 3, '2020-12-30 06:06:35', NULL, NULL),
(5, 1, 'CATATAN ATAS LAPORAN KEUANGAN TAHUN 2019', '/assets/upload/anggaran/12_2020/a10a158bbaff67bb5a77704047a673fc.pdf', 'a10a158bbaff67bb5a77704047a673fc.pdf', 3, '2020-12-30 06:08:31', NULL, NULL),
(6, 2, 'DPA BADAN KESBANGPOL DAN LINMAS PROV. JATENG TAHUN 2015', '/assets/upload/anggaran/12_2020/0a67658bbc8fbc5a0325bfb05fddc362.pdf', '0a67658bbc8fbc5a0325bfb05fddc362.pdf', 3, '2020-12-30 06:15:19', NULL, NULL),
(7, 2, 'DPA BADAN KESBANGPOL DAN LINMAS PROV. JATENG TAHUN 2016', '/assets/upload/anggaran/12_2020/640dc104c80386f8403da248116453f4.pdf', '640dc104c80386f8403da248116453f4.pdf', 3, '2020-12-30 06:16:46', NULL, NULL),
(8, 3, 'LAPORAN OPERASIONAL 2014', '/assets/upload/anggaran/12_2020/dd3dde69f71732a4f13a3283a63cd151.pdf', 'dd3dde69f71732a4f13a3283a63cd151.pdf', 3, '2020-12-30 06:31:38', NULL, NULL),
(9, 3, 'LAPORAN-OPERASIONAL-2015', '/assets/upload/anggaran/12_2020/aaf30c3d9e7865549639cc0b48942f1b.pdf', 'aaf30c3d9e7865549639cc0b48942f1b.pdf', 3, '2020-12-30 06:36:48', NULL, NULL),
(10, 3, 'LAPORAN-OPERASIONAL-2016', '/assets/upload/anggaran/12_2020/e769cf6aa4df5f84607844956adf84e8.pdf', 'e769cf6aa4df5f84607844956adf84e8.pdf', 3, '2020-12-30 06:37:08', NULL, NULL),
(11, 3, 'LAPORAN-OPERASIONAL-2017', '/assets/upload/anggaran/12_2020/cc10f12e0e5b8c5150c8ed1a9dd120c9.pdf', 'cc10f12e0e5b8c5150c8ed1a9dd120c9.pdf', 3, '2020-12-30 06:39:04', NULL, NULL),
(12, 3, 'LAPORAN-OPERASIONAL-2018', '/assets/upload/anggaran/12_2020/affc5d21c3a7d57c0cc2e2a52ff20dbc.pdf', 'affc5d21c3a7d57c0cc2e2a52ff20dbc.pdf', 3, '2020-12-30 06:39:22', NULL, NULL),
(13, 3, 'LAPORAN-OPERASIONAL-2019', '/assets/upload/anggaran/12_2020/6ba4a9c9331cb48ad9b2700ac356fccc.pdf', '6ba4a9c9331cb48ad9b2700ac356fccc.pdf', 3, '2020-12-30 06:40:04', NULL, NULL),
(16, 2, 'DPA BADAN KESBANGPOL PROV. JATENG TAHUN 2018', '/assets/upload/anggaran/12_2020/070a6200825e1870ac5d4c9d58daa3eb.pdf', '070a6200825e1870ac5d4c9d58daa3eb.pdf', 1, '2020-12-30 06:44:58', NULL, NULL),
(17, 2, 'DPA BADAN KESBANGPOL PROV. JATENG TAHUN 2019', '/assets/upload/anggaran/12_2020/b532e6fa1435520b756aa9ce11c55c3c.pdf', 'b532e6fa1435520b756aa9ce11c55c3c.pdf', 1, '2020-12-30 06:45:19', NULL, NULL),
(18, 2, 'DPA BADAN KESBANGPOL PROV. JATENG TAHUN 2020', '/assets/upload/anggaran/12_2020/16e53eca68c109f9106f18540049f95b.pdf', '16e53eca68c109f9106f18540049f95b.pdf', 1, '2020-12-30 06:45:43', NULL, NULL),
(19, 4, 'LAPORAN-PERUBAHAN-EKUITAS-TAHUN-2014', '/assets/upload/anggaran/12_2020/36ea1d1dfdde3b23dda419f8d4d2bc8d.pdf', '36ea1d1dfdde3b23dda419f8d4d2bc8d.pdf', 3, '2020-12-30 06:48:56', NULL, NULL),
(20, 4, 'LAPORAN-PERUBAHAN-EKUITAS-TAHUN-2015', '/assets/upload/anggaran/12_2020/b02143963ac709f679bd4a67c4f72ebb.pdf', 'b02143963ac709f679bd4a67c4f72ebb.pdf', 3, '2020-12-30 06:49:19', NULL, NULL),
(21, 4, 'LAPORAN-PERUBAHAN-EKUITAS-TAHUN-2016', '/assets/upload/anggaran/12_2020/97f5b2e563e10dfd87775479925f4af8.pdf', '97f5b2e563e10dfd87775479925f4af8.pdf', 3, '2020-12-30 06:49:34', NULL, NULL),
(22, 4, 'LAPORAN-PERUBAHAN-EKUITAS-TAHUN-2017', '/assets/upload/anggaran/12_2020/ee79f12213be7e00e170fe0071d7ecab.pdf', 'ee79f12213be7e00e170fe0071d7ecab.pdf', 3, '2020-12-30 06:49:56', NULL, NULL),
(23, 4, 'LAPORAN-PERUBAHAN-EKUITAS-TAHUN-2018', '/assets/upload/anggaran/12_2020/c376f2fad2f1d34906cf6c537df7a835.pdf', 'c376f2fad2f1d34906cf6c537df7a835.pdf', 3, '2020-12-30 06:50:12', NULL, NULL),
(24, 4, 'LAPORAN-PERUBAHAN-EKUITAS-TAHUN-2019', '/assets/upload/anggaran/12_2020/5a96ea5916864748f6c29379b935c58c.pdf', '5a96ea5916864748f6c29379b935c58c.pdf', 3, '2020-12-30 06:50:29', NULL, NULL),
(25, 5, 'LAPORAN-REALISASI-ANGGARAN-TAHUN-2015', '/assets/upload/anggaran/12_2020/c08cb4d9d369bf859d8cf9c71c178638.pdf', 'c08cb4d9d369bf859d8cf9c71c178638.pdf', 3, '2020-12-30 06:56:40', NULL, NULL),
(26, 5, 'LAPORAN-REALISASI-ANGGARAN-TAHUN-2016', '/assets/upload/anggaran/12_2020/e58758282f8066057f0d420689d98526.pdf', 'e58758282f8066057f0d420689d98526.pdf', 3, '2020-12-30 06:56:56', NULL, NULL),
(27, 5, 'LAPORAN-REALISASI-ANGGARAN-TAHUN-2017', '/assets/upload/anggaran/12_2020/892bf20e5cb068685979180dd13e624f.pdf', '892bf20e5cb068685979180dd13e624f.pdf', 3, '2020-12-30 06:57:11', NULL, NULL),
(28, 5, 'LAPORAN-REALISASI-ANGGARAN-TAHUN-2018', '/assets/upload/anggaran/12_2020/db35c1558eac1b782f4eec83ca1ee31f.pdf', 'db35c1558eac1b782f4eec83ca1ee31f.pdf', 3, '2020-12-30 06:57:31', NULL, NULL),
(29, 5, 'LAPORAN-REALISASI-ANGGARAN-TAHUN-2019', '/assets/upload/anggaran/12_2020/4a13e27d85a9dede8ce7fc7b5ccfc94d.pdf', '4a13e27d85a9dede8ce7fc7b5ccfc94d.pdf', 3, '2020-12-30 06:57:46', NULL, NULL),
(30, 6, 'NERACA-BADAN-KESBANGPOL-DAN-LINMAS-PROV.-JATENG-TAHUN-2015', '/assets/upload/anggaran/12_2020/b872303f6ec3c5cb61ce52d0f7567aee.pdf', 'b872303f6ec3c5cb61ce52d0f7567aee.pdf', 3, '2020-12-30 07:01:57', NULL, NULL),
(31, 6, 'NERACA-BADAN-KESBANGPOL-DAN-LINMAS-PROV.-JATENG-TAHUN-2016', '/assets/upload/anggaran/12_2020/52180b23a4eca684025116179e4ab6e0.pdf', '52180b23a4eca684025116179e4ab6e0.pdf', 3, '2020-12-30 07:02:30', NULL, NULL),
(32, 6, 'NERACA-BADAN-KESBANGPOL-DAN-LINMAS-PROV.-JATENG-TAHUN-2017', '/assets/upload/anggaran/12_2020/05ee63d1f06d3312d0edf4376c077d73.pdf', '05ee63d1f06d3312d0edf4376c077d73.pdf', 3, '2020-12-30 07:02:48', NULL, NULL),
(33, 6, 'NERACA-BADAN-KESBANGPOL-PROV.-JATENG-TAHUN-2018', '/assets/upload/anggaran/12_2020/83a086573785737927cf36aaaab2c20c.pdf', '83a086573785737927cf36aaaab2c20c.pdf', 3, '2020-12-30 07:03:10', NULL, NULL),
(34, 6, 'NERACA-BADAN-KESBANGPOL-PROV.-JATENG-TAHUN-2019', '/assets/upload/anggaran/12_2020/f5726a2f8a985660d9398d8b748a0a83.pdf', 'f5726a2f8a985660d9398d8b748a0a83.pdf', 3, '2020-12-30 07:03:37', NULL, NULL),
(35, 7, 'LAPORAN-PELAKSANAAN-TAHUN-2015', '/assets/upload/anggaran/12_2020/665fc50bec1f1f85de72a658cf6cf2ba.pdf', '665fc50bec1f1f85de72a658cf6cf2ba.pdf', 3, '2020-12-30 07:21:39', '2020-12-30 14:48:52', NULL),
(36, 7, 'LAPORAN-PELAKSANAAN-APBD-TAHUN-2016', '/assets/upload/anggaran/12_2020/6aa302d309de4300c75d93d4e2c5963f.pdf', '6aa302d309de4300c75d93d4e2c5963f.pdf', 3, '2020-12-30 07:34:47', '2020-12-30 14:47:54', NULL),
(37, 7, 'LAPORAN-PELAKSANAAN-APBD-TAHUN-2017', '/assets/upload/anggaran/12_2020/d56f0b05b705a8df41af480feffab1f5.pdf', 'd56f0b05b705a8df41af480feffab1f5.pdf', 3, '2020-12-30 08:00:38', NULL, NULL),
(38, 7, 'LAPORAN-PELAKSANAAN-APBD-TAHUN-2018', '/assets/upload/anggaran/12_2020/c133488057fc4eaf55087d8918bc3236.pdf', 'c133488057fc4eaf55087d8918bc3236.pdf', 3, '2020-12-30 08:15:10', NULL, NULL),
(39, 8, 'RKA-2016', '/assets/upload/anggaran/12_2020/b35045e96573f3976a6c0c4fa83a1eb2.pdf', 'b35045e96573f3976a6c0c4fa83a1eb2.pdf', 3, '2020-12-30 08:20:03', NULL, NULL),
(40, 8, 'RKA-KESBANGPOL-TAHUN-2020', '/assets/upload/anggaran/12_2020/ad27dbfbb02fc8c3b3a53a580ebf7120.pdf', 'ad27dbfbb02fc8c3b3a53a580ebf7120.pdf', 3, '2020-12-30 08:24:09', NULL, NULL),
(41, 9, 'DAFTAR-ASET-2015', '/assets/upload/anggaran/12_2020/13064d9669af76c3658d75a6990bc365.pdf', '13064d9669af76c3658d75a6990bc365.pdf', 3, '2020-12-30 08:37:57', NULL, NULL),
(42, 9, 'DAFTAR-ASET-2016', '/assets/upload/anggaran/12_2020/411c8c1e57a2d014cfbae419ded79df3.pdf', '411c8c1e57a2d014cfbae419ded79df3.pdf', 3, '2020-12-30 08:38:13', NULL, NULL),
(43, 9, 'DAFTAR-ASET-2017', '/assets/upload/anggaran/12_2020/6e7e56c1f2092503d1ad26ea008be444.pdf', '6e7e56c1f2092503d1ad26ea008be444.pdf', 3, '2020-12-30 08:38:31', NULL, NULL),
(44, 9, 'DAFTAR-ASET-2018', '/assets/upload/anggaran/12_2020/1a346a7d0112b1a3334e2d9f62342a8e.pdf', '1a346a7d0112b1a3334e2d9f62342a8e.pdf', 3, '2020-12-30 08:38:47', NULL, NULL),
(45, 9, 'DAFTAR-ASET-2019', '/assets/upload/anggaran/12_2020/2baccaaee3da1a5b0bdb49ff995bbd61.pdf', '2baccaaee3da1a5b0bdb49ff995bbd61.pdf', 3, '2020-12-30 08:39:00', NULL, NULL),
(46, 10, 'Notulen 2018', '/assets/upload/anggaran/12_2020/152cbabe8f97c06b5dbec1bb5070dbd1.pdf', '152cbabe8f97c06b5dbec1bb5070dbd1.pdf', 3, '2020-12-30 08:48:19', NULL, NULL),
(47, 10, 'Notulen 2019', '/assets/upload/anggaran/01_2021/65d1e607c07f1fb31eb91c8c47758363.pdf', '65d1e607c07f1fb31eb91c8c47758363.pdf', 3, '2021-01-04 03:52:18', NULL, NULL),
(48, 10, 'Notulen 2020', '/assets/upload/anggaran/01_2021/6ba14541d1165a054228603e8820436f.pdf', '6ba14541d1165a054228603e8820436f.pdf', 3, '2021-01-04 04:04:20', NULL, NULL),
(49, 11, 'SPJ-Fungsional 2020', '/assets/upload/anggaran/01_2021/3619bfbe535ed1275c2fdc2d693fb6e2.pdf', '3619bfbe535ed1275c2fdc2d693fb6e2.pdf', 3, '2021-01-04 04:08:08', NULL, NULL),
(50, 7, 'LAPORAN-PELAKSANAAN-APBD TAHUN-2019', '/assets/upload/anggaran/01_2021/b8c97c55233b7337d2e7f77ea472200b.pdf', 'b8c97c55233b7337d2e7f77ea472200b.pdf', 3, '2021-01-04 04:18:55', '2021-01-04 11:19:17', NULL),
(51, 5, 'LAPORAN-PELAKSANAAN-APBD TAHUN-2020', '/assets/upload/anggaran/01_2021/3ce75f2c7d0d5673398cff46c43504d4.pdf', '3ce75f2c7d0d5673398cff46c43504d4.pdf', 3, '2021-01-04 04:21:43', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_anggaran_data_copy`
--

CREATE TABLE `turin_anggaran_data_copy` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_anggaran` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_anggaran_data_copy`
--

INSERT INTO `turin_anggaran_data_copy` (`id`, `id_anggaran`, `judul`, `deskripsi`, `file`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'Tes', '/assets/upload/anggaran/04_2020/e20d4755395fd176c3ca6256403875ce.pdf', 'e20d4755395fd176c3ca6256403875ce.pdf', 1, '2020-04-16 19:34:41', NULL, NULL),
(2, 1, 'Neraca Tahun 2018', '/assets/upload/anggaran/04_2020/f3c62405d58b022aa62bd7089d735f6f.pdf', 'f3c62405d58b022aa62bd7089d735f6f.pdf', 1, '2020-04-18 16:10:39', NULL, NULL),
(3, 1, 'Neraca Tahun 2017', '/assets/upload/anggaran/04_2020/ece16189e367af8f557e564e651656e7.pdf', 'ece16189e367af8f557e564e651656e7.pdf', 1, '2020-04-18 16:12:20', NULL, NULL),
(4, 1, 'LRA Tahun 2017', '/assets/upload/anggaran/04_2020/c6fac5f9b3819ec24fb6464b9c19c3f5.pdf', 'c6fac5f9b3819ec24fb6464b9c19c3f5.pdf', 1, '2020-04-18 16:12:46', NULL, NULL),
(5, 1, ' CALK Tahun 2016', '/assets/upload/anggaran/04_2020/bca7775a7bddb8d393f4904471729c58.pdf', 'bca7775a7bddb8d393f4904471729c58.pdf', 1, '2020-04-18 16:13:33', NULL, NULL),
(6, 1, 'Neraca Tahun 2016', '/assets/upload/anggaran/04_2020/c2e3e01c9fe194be5b5e99297fdb1f15.pdf', 'c2e3e01c9fe194be5b5e99297fdb1f15.pdf', 1, '2020-04-18 16:14:31', NULL, NULL),
(7, 1, 'LRA SKPD Tahun 2016', '/assets/upload/anggaran/04_2020/e7dd580fd2fd6ecdf0f5991e909b2d9c.pdf', 'e7dd580fd2fd6ecdf0f5991e909b2d9c.pdf', 1, '2020-04-18 16:14:57', NULL, NULL),
(8, 1, 'Neraca Tahun 2105', '/assets/upload/anggaran/04_2020/0620fd53dfde8d6e5924756d9ccbaa69.pdf', '0620fd53dfde8d6e5924756d9ccbaa69.pdf', 1, '2020-04-18 16:16:05', NULL, NULL),
(9, 1, 'LRA 2 Tahun 2015', '/assets/upload/anggaran/04_2020/c1bb8f5edbe361893207506c50485a76.pdf', 'c1bb8f5edbe361893207506c50485a76.pdf', 1, '2020-04-18 16:16:36', NULL, NULL),
(10, 1, ' LRA 1 Tahun 2015 ', '/assets/upload/anggaran/04_2020/e8f85a08110d19c434f1dec2fb149f24.pdf', 'e8f85a08110d19c434f1dec2fb149f24.pdf', 1, '2020-04-18 16:16:56', NULL, NULL),
(11, 1, 'CaLK 2016', '/assets/upload/anggaran/04_2020/622853524b0f53892204dedd0d4eff6b.pdf', '622853524b0f53892204dedd0d4eff6b.pdf', 1, '2020-04-18 16:19:11', NULL, NULL),
(12, 3, 'Neraca Keuangan Tahun 2016', '/assets/upload/anggaran/04_2020/4c7262082528506a4455af41f00da73c.pdf', '4c7262082528506a4455af41f00da73c.pdf', 1, '2020-04-18 16:22:21', NULL, NULL),
(13, 3, 'Neraca 2016', '/assets/upload/anggaran/04_2020/904420aee0816faeca31810af1b39dc7.pdf', '904420aee0816faeca31810af1b39dc7.pdf', 1, '2020-04-18 16:23:14', NULL, NULL),
(14, 3, 'NERACA 2018', '/assets/upload/anggaran/04_2020/472156d3a771c65f78658351bb503849.pdf', '472156d3a771c65f78658351bb503849.pdf', 1, '2020-04-18 16:23:42', NULL, NULL),
(15, 3, 'ASET 2018 ', '/assets/upload/anggaran/04_2020/5e5d5889185fb65f1839b968ee09ffda.pdf', '5e5d5889185fb65f1839b968ee09ffda.pdf', 1, '2020-04-18 16:24:06', NULL, NULL),
(16, 3, 'Aset Tahun 2018 ', '/assets/upload/anggaran/04_2020/95783f24fde986b8d3f2aebfb8fad672.pdf', '95783f24fde986b8d3f2aebfb8fad672.pdf', 1, '2020-04-18 16:24:32', NULL, NULL),
(17, 3, 'Neraca Tahun 2018', '/assets/upload/anggaran/04_2020/040ee1af2f025aa18532eca4c9b5d47b.pdf', '040ee1af2f025aa18532eca4c9b5d47b.pdf', 1, '2020-04-18 16:24:54', NULL, NULL),
(18, 3, 'Daftar Aset Tahun 2015', '/assets/upload/anggaran/04_2020/796475c714d1e48efdf7630da2b7ed2c.pdf', '796475c714d1e48efdf7630da2b7ed2c.pdf', 1, '2020-04-18 16:25:19', NULL, NULL),
(19, 3, 'Aset Tahun 2016', '/assets/upload/anggaran/04_2020/975a4b3d1b1626f2d9eda8668838f447.pdf', '975a4b3d1b1626f2d9eda8668838f447.pdf', 1, '2020-04-18 16:25:37', NULL, NULL),
(20, 3, 'Aset Tahun 2017', '/assets/upload/anggaran/04_2020/4a2d24c6aba0a2dd61dbdd13925fe8e9.pdf', '4a2d24c6aba0a2dd61dbdd13925fe8e9.pdf', 1, '2020-04-18 16:26:05', NULL, NULL),
(21, 3, 'Aset Tahun 2018', '/assets/upload/anggaran/04_2020/bf639a34d0674d97b8d8d8bbef668927.pdf', 'bf639a34d0674d97b8d8d8bbef668927.pdf', 1, '2020-04-18 16:26:26', NULL, NULL),
(22, 3, 'Aset Tahun 2019', '/assets/upload/anggaran/04_2020/e15198c94c25f3e25deb61fdec32d238.pdf', 'e15198c94c25f3e25deb61fdec32d238.pdf', 1, '2020-04-18 16:26:45', NULL, NULL),
(23, 4, ' Pagudana dan Realisasi Anggaran s/d Maret 2018', '/assets/upload/anggaran/04_2020/af8d80f6dddd96daf352eff26146ef95.pdf', 'af8d80f6dddd96daf352eff26146ef95.pdf', 1, '2020-04-18 16:28:01', NULL, NULL),
(24, 4, 'PAD TAHUN 2018', '/assets/upload/anggaran/04_2020/e555c572aa9dcd0e24daa5639c88a17b.pdf', 'e555c572aa9dcd0e24daa5639c88a17b.pdf', 1, '2020-04-18 16:28:23', NULL, NULL),
(25, 4, 'Rekap 2.2 DPA 2018 Disnakertrans', '/assets/upload/anggaran/04_2020/c946664362b4b9575b40607cb687fd79.pdf', 'c946664362b4b9575b40607cb687fd79.pdf', 1, '2020-04-18 16:28:49', NULL, NULL),
(26, 4, 'REAL ANGGARAN SD AGT 2018', '/assets/upload/anggaran/04_2020/a5ee32e6cb0fb9cd9b66c33b18279486.pdf', 'a5ee32e6cb0fb9cd9b66c33b18279486.pdf', 1, '2020-04-18 16:30:38', NULL, NULL),
(27, 4, 'Realisasi Anggaran APBD dan APBN Tahun 2018', '/assets/upload/anggaran/04_2020/99c9543be121bd941ac22d12b5a1ca77.pdf', '99c9543be121bd941ac22d12b5a1ca77.pdf', 1, '2020-04-18 16:31:31', NULL, NULL),
(28, 4, 'Anggaran APBD dan APBN Tahun 2019', '/assets/upload/anggaran/04_2020/f2f31b5c0457f17d63dfab5cfbebd3d4.pdf', 'f2f31b5c0457f17d63dfab5cfbebd3d4.pdf', 1, '2020-04-18 16:33:27', NULL, NULL),
(29, 4, 'Realisasi APBD, APBN, PAD TAHUN 2019', '/assets/upload/anggaran/04_2020/2f8f94f4d2916b25fdcdef5c13abaf09.pdf', '2f8f94f4d2916b25fdcdef5c13abaf09.pdf', 1, '2020-04-18 16:33:56', NULL, NULL),
(30, 2, ' Pagu Anggaran APBD dan APBN 2017 ', '/assets/upload/anggaran/04_2020/950da3a0fa7341ba24340baefe12c0ca.pdf', '950da3a0fa7341ba24340baefe12c0ca.pdf', 1, '2020-04-18 16:43:00', NULL, NULL),
(31, 2, 'DPA 2017', '/assets/upload/anggaran/04_2020/7b87a814d0b9a133b25b2e86838bc950.pdf', '7b87a814d0b9a133b25b2e86838bc950.pdf', 1, '2020-04-18 16:43:31', NULL, NULL),
(32, 2, 'Belanja Langsung 2017', '/assets/upload/anggaran/04_2020/44ddda315ed5f3e2d0c56e5d29f60aa0.pdf', '44ddda315ed5f3e2d0c56e5d29f60aa0.pdf', 1, '2020-04-18 16:43:51', NULL, NULL),
(33, 2, 'Dok Perubahan Anggaran 2016', '/assets/upload/anggaran/04_2020/76b412f3785d715d7d84a9ae8cc2734e.pdf', '76b412f3785d715d7d84a9ae8cc2734e.pdf', 1, '2020-04-18 16:44:29', NULL, NULL),
(34, 2, 'Pagu apbd dan apdn ', '/assets/upload/anggaran/04_2020/80139603afda99eb53361623094c407c.pdf', '80139603afda99eb53361623094c407c.pdf', 1, '2020-04-18 16:44:57', NULL, NULL),
(35, 2, ' Pagudana dan Realisasi Anggaran s/d Maret 2018', '/assets/upload/anggaran/04_2020/5cce353d9a9c0b9ecf0420bce3d4210c.pdf', '5cce353d9a9c0b9ecf0420bce3d4210c.pdf', 1, '2020-04-18 16:45:32', NULL, NULL),
(36, 2, 'DPA Disnakertrans Tahun 2019 ', '/assets/upload/anggaran/04_2020/880b1a22ad0bd2574b7dee93b5702d97.pdf', '880b1a22ad0bd2574b7dee93b5702d97.pdf', 1, '2020-04-18 16:46:06', NULL, NULL),
(37, 2, 'Belanja Tidak Langsung Tahun 2019 ', '/assets/upload/anggaran/04_2020/078d96b864c299981568495b7e963220.pdf', '078d96b864c299981568495b7e963220.pdf', 1, '2020-04-18 16:46:30', NULL, NULL),
(38, 2, 'DPA Tahun 2019 ', '/assets/upload/anggaran/04_2020/fc11f4a5ed2ef6fc71477539ebcb8cc8.pdf', 'fc11f4a5ed2ef6fc71477539ebcb8cc8.pdf', 1, '2020-04-18 16:46:54', NULL, NULL),
(39, NULL, NULL, NULL, NULL, 1, '2020-05-08 06:38:52', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_berita`
--

CREATE TABLE `turin_berita` (
  `id` int(11) UNSIGNED NOT NULL,
  `tipe` varchar(11) DEFAULT NULL,
  `judul` text DEFAULT NULL,
  `isi` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_berita`
--

INSERT INTO `turin_berita` (`id`, `tipe`, `judul`, `isi`, `deskripsi`, `file`, `date`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Berita', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididuntBerita 1', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>\r\n', '/assets/upload/berita/04_2020/fbbd1e0c770e0354f27867643bfa222d.png', 'fbbd1e0c770e0354f27867643bfa222d.png', '2020-04-19', 1, '2020-04-17 09:17:38', '2020-12-03 20:44:07', NULL),
(2, 'Berita', 'Berita 2', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>\r\n', '/assets/upload/berita/04_2020/fbbd1e0c770e0354f27867643bfa222d.png', 'fbbd1e0c770e0354f27867643bfa222d.png', '2020-04-19', 1, '2020-04-17 09:17:38', '2020-05-01 14:22:42', NULL),
(3, 'Berita', 'Berita 3', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>\r\n', '/assets/upload/berita/04_2020/fbbd1e0c770e0354f27867643bfa222d.png', 'fbbd1e0c770e0354f27867643bfa222d.png', '2020-04-19', 1, '2020-04-17 09:17:38', '2020-05-01 14:22:46', NULL),
(4, 'Berita', 'Berita 4', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>\r\n', '/assets/upload/berita/04_2020/fbbd1e0c770e0354f27867643bfa222d.png', 'fbbd1e0c770e0354f27867643bfa222d.png', '2020-04-19', 1, '2020-04-17 09:17:38', '2020-05-01 14:22:50', NULL),
(5, 'Berita', 'Berita 5 adalah berita', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>\r\n', '/assets/upload/berita/04_2020/fbbd1e0c770e0354f27867643bfa222d.png', 'fbbd1e0c770e0354f27867643bfa222d.png', '2020-04-19', 1, '2020-04-17 09:17:38', '2020-05-01 14:22:50', NULL),
(6, 'Berita', 'Berita 6 adalah berita', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>\r\n', '/assets/upload/berita/04_2020/fbbd1e0c770e0354f27867643bfa222d.png', 'fbbd1e0c770e0354f27867643bfa222d.png', '2020-04-19', 1, '2020-04-17 09:17:38', '2020-12-03 21:04:05', NULL),
(7, 'Berita', 'Berita 7 adalah berita', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>\r\n', '/assets/upload/berita/04_2020/fbbd1e0c770e0354f27867643bfa222d.png', 'fbbd1e0c770e0354f27867643bfa222d.png', '2020-04-19', 1, '2020-04-17 09:17:38', '2020-12-03 21:04:05', NULL),
(8, 'Berita', 'Berita 8 adalah berita', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>\r\n', '/assets/upload/berita/04_2020/fbbd1e0c770e0354f27867643bfa222d.png', 'fbbd1e0c770e0354f27867643bfa222d.png', '2020-04-19', 1, '2020-04-17 09:17:38', '2020-12-03 21:04:05', NULL),
(9, 'Berita', 'APRESIASI NILAI-NILAI SEJARAH PERJUANGAN BANGSA', '<p>KEGIATAN APRESIASI NILAI-NILAI SEJARAH PERJUANGAN BANGSA dengan tema “Ngrumat Bebrayan untuk Jawa Tengah yang religius, toleran dan guyup dalam menjaga Negara Kesatuan Republik Indonesia” di Kabupaten Wonogiri (23/8/2019).</p>\r\n\r\n<p>.</p>\r\n\r\n<p>Menghadirkan Narasumber :</p>\r\n\r\n<p>1. Dosen UNWAHAS Semarang (Dr. Agus Riyanto, M.Si).</p>\r\n\r\n<p>2. Pengamat Sosial Kemasyarakatan/Budaya (Drs. MT. Arifin, SU).</p>\r\n\r\n<p>3. Kepala Kantor Kesbangpol Kabupaten Wonogiri (Sulardi, S.Sos, MH).</p>\r\n\r\n<p>.</p>\r\n\r\n<p>Dibuka langsung oleh Kepala Badan Kesatuan Bangsa dan Politik Provinsi Jawa Tengah Bpk. Achmad Rofai, M.Si yang dalam sambutanya disampaikan sebagai berikut :</p>\r\n\r\n<p>.</p>\r\n\r\n<p>Kemerdekaan bangsa ini tercapai karena rakyat Indonesia mempunyai semangat perjuangan yang sangat tinggi. Tanpa semangat perjuangan yang sangat tinggi, kita yakin kemerdekaan bangsa ini tidak akan tercapai. Selamanya rakyat Indonesia akan hidup dibawah penindasan penjajah yang bermaksud menguasai pemerintahan dan mengeruk kekayaan bangsa ini.</p>\r\n\r\n<p>.</p>\r\n\r\n<p>Hal lain yang perlu kita teladani sebagai generasi muda yaitu kita harus tangguh seperti Pahlawan yang pantang menyerah untuk menggapai cita-cita mereka. Mereka mau belajar untuk mecapai cita-cita mereka. Para pahlawan tidak hanya mencucurkan keringat tetapi juga dengan segenap jiwa mereka untuk kemerdekaan Indonesia. Semangat rela berkorban para pejuang kita yang begitu besar itu patut kita teladani juga.</p>\r\n\r\n<p>.</p>\r\n\r\n<p>Sudah saatnya generasi muda di kenalkan kembali kepada nilai-nilai kepahlawanan yang sudah mulai luntur dan tidak berarti lagi. Dengan mengenal para pahlawan maka kita pun akan di ajarkan untuk membangun kembali sikap kepahlawanan, patriotisme dan nasionalisme. .</p>\r\n\r\n<p>Kami berharap dalam momentum Hari Jadi Provinsi Jawa Tengah ke-69 ini, peranan generasi muda sebagai generasi bangsa dapat menerapkan nilai-nilai yang terkandung dalam sejarah perjuangan bangsa Indonesia dalam kehidupan sehari-hari. Tanpa adanya peranan generasi muda maka bangsa Indonesia pastinya akan sulit mengalami perubahan dan akan mudah pula kehilangan identitas bangsa Indonesia. </p>\r\n', '/assets/upload/berita/12_2020/b9bf18d75b0a8c7adb01608766b900ca.png', 'b9bf18d75b0a8c7adb01608766b900ca.png', '2019-08-28', 1, '2020-12-30 19:30:04', NULL, NULL),
(10, 'Berita', 'JAWA TENGAH KAMPUNG BHINNEKA', '<p>Bertempat di Gedung Gradhika Bhakti Praja (14/9/2019), Gubernur Jawa Tengah H. Ganjar Pranowo, SH., M.IP membuka acara JAWA TENGAH KAMPUNG BHINNEKA yang dalam inti sambutanya menyampaikan :</p>\r\n\r\n<p>Hoaks, ujaran kebencian, caci maki, politisasi SARA, berpotensi memecah belah Persatuan dan kesatuan Indonesia. Kita harus waspada. Bangsa ini tak boleh terserak, tercabik dan terdegradasi hanya mempersoalkan perbedaan dan keragaman. Kita harus lawan itu semua, jangan biarkan masyarakat Indonesia salaing bermusuhan, kita ini saudara sebangsa dan setanah air. Kita harus bangkit guyub bergandeng tangan menjaga negeri ini menenun spirit kebangsaan.</p>\r\n\r\n<p>Mari kita terus menjunjung tinggi dan meneguhkan nilai kebangsaan serta Pancasila sebagai dasar Negara Indonesia. Kita harus mampu menunjukkan kepribadian sebagai bangsa Indonesia. Kita tunjukkan pada dunia bahwa kita punya kepribadian dalam kebudayaan. Kita memiliki semangat gotong-royong, toleransi, musyawarah untuk mufakat dan Bhinneka Tunggal Ika untuk membangun kebersamaan. Itulah sesungguhnya pondasi jatidiri yang selaras dengan potensi, kompetensi, dan karakteristik Bangsa Indonesia sebagai bangsa pejuang yang selalu ingin bertindak sportif, kreatif dan inovatif dalam menunjukkan karya terbaiknya dalam membangun pulau-pulau harapan yang bernama Indonesia.</p>\r\n\r\n<p>Untuk itu, di Kampung Bhinneka ini mari kita gelorakan semangat merah putih, semangat kebangsaan, semangat persatuan yang penuh daya hidup yang tak pernah menguap. Mari kita tularkan kegembiraan.</p>\r\n', '/assets/upload/berita/12_2020/018e35fe616ed580fb6b5a407a0b33f6.png', '018e35fe616ed580fb6b5a407a0b33f6.png', '2019-10-14', 1, '2020-12-30 19:35:14', NULL, NULL),
(11, 'Berita', 'PEMELIHARAAN KEHARMONISAN DAN KERUKUNAN UMAT BERAGAMA DAN PENGHAYAT KEPERCAYAAN', '<p>Kegiatan ini mengangkat tema “Mencintai Indonesia“ dengan maksud Mensinkronkan program kerja FKUB Provinsi Jawa Tengah dengan FKUB Kab/Kota se Jawa Tengah khususnya dalam pencegahan faham radikalisme bagi generasi muda, yang bertujuan Memberikan pengetahuan dan pengalaman terhadap generasi muda dalam berinteraksi dengan lintas agama dan lintas iman.</p>\r\n\r\n<p>Dilaksanakan paha hari Rabu s/d Kamis tanggal 30-31 Oktober 2019 di Hotel Grand Wahid, Kota Salatiga. Peserta Fasilitasi FKUB dalam rangka Pemeliharaan Keharmonisan dan Kerukunan Antar Umat Beragma dan Penghayat  Kepercayaan   Provinsi  Jawa  Tengah tahun 2019 terdiri dari Pengurus FKUB Provinsi Jawa Tengah, Ketua dan Pengurus FKUB  Kabupaten/Kota se Jawa tengah, sejumlah 80 orang.</p>\r\n\r\n<p>Metode/Mekanisme yang digunakan pada Fasilitasi FKUB Provinsi Jawa Tengah dalam rangka Pemeliharaan Keharmonisan dan Kerukunan Antar Umat Beragama serta Penghayat Kepercayaan Tahun 2019  ini berupa ceramah dan tanya jawab. Berikut materi dan narasumber yang menyampaikan ceramah :</p>\r\n\r\n<ol>\r\n <li>Penguatan Gernerasi Muda FKUB di Jawa Tengah oleh Drs. Taslim Syahlan, M.Si – Ketua FKUB Prov. Jateng.</li>\r\n <li>Penguatan Moderasi Beragama DAlam Perspektif Budha Bagi Generasi Muda FKUB di Jawa Tengah, oleh Romo PMD Angga Dhammo Warto, B.Sc – FKUB Prov. Jateng.</li>\r\n <li>Keberagaman Sebagai Modal Sosial Penguatan Kerukunan Umat Beragama di Jawa Tengah oleh Prof. Dr. H. Mudjahirin Thohir, MA – Akademisi UNDIP Semarang.</li>\r\n <li>Penguatan Moderasi Beragama Bagi Generasi Muda FKUB di Jawa Tengah oleh Drs. H. Abu Hafsin. PhD – Akademisi UIN Walisongo Semarang.</li>\r\n <li>Penguatan Moderasi Beragama Beragama dalam perspektif Kristiani Bagi Generasi Muda di Jawa Tengah oleh Pdt. Bambang Pujianto, M.Min – FKUB Prov. Jateng.</li>\r\n <li>Penguatan Moderasi Beragama dalam Perspektif Hindu Bagi Generasi Muda di Jawa Tengah oleh Eko Pujiyanto, S.Ag – FKUB Prov. Jateng.</li>\r\n</ol>\r\n\r\n<p>          Acara dibuka oleh Kepala Badan Kesatuan Bangsa dan Politik Provinsi Jawa Tengah, Drs. Achmad Rofa’i, M.Si yang dalam sambutannya menyampaikan :</p>\r\n\r\n<p>Mengawali pertemuan ini, marilah senantiasa kita memanjatkan puji syukur kehadirat Tuhan Yang Maha Kuasa, karena hanya atas rahmat dan  karuniaNya  kita semua dapat hadir ditempat ini, dalam rangka melaksanakan Upacara Pembukaan Fasilitasi FKUB dalam rangka Pemeliharaan Keharmonisan dan Kerukunan Antar Umat Beragama serta Penghayat Kepercayaan Provinsi Jawa Tengah Tahun 2019, dalam keadaan sehat wal afiat, semoga seluruh acara dapat berjalan dengan lancar dan sukses.</p>\r\n\r\n<p><em>Hadirin sekalian yang berbahagia</em>, Kegiatan semacam ini saya pandang sangat strategis untuk memantapkan peran FKUB,  dalam  menjaga  dan   memelihara    serta mewujudkan    masyarakat Jawa – Tengah yang toleran dan damai dalam perbedaan keyakinan.</p>\r\n\r\n<p>Pembangunan bidang agama merupakan upaya untuk mewujudkan agenda meningkatkan kesejahteraan rakyat melalui peningkatan kualitas pelayanan dan pemahaman agama serta kehidupan beragama. Selain itu, pembangunan bidang agama juga mencakup dimensi peningkatan kerukunan hidup umat beragama yang mendukung peningkatan saling percaya dan harmonisasi antarkelompok masyarakat. Dimensi kerukunan ini sangat penting dalam rangka membangun masyarakat yang memiliki kesadaran mengenai realitas multikulturalisme dan memahami makna kemajemukan sosial sehingga tercipta suasana kehidupan masyarakat yang penuh toleransi, tenggang rasa, dan harmonis.</p>\r\n\r\n<p>Beberapa perkembangan penting yang dicapai dalam pembangunan bidang agama, antara lain meningkatnya fasilitas berbagai sarana peribadatan untuk memberikan pelayanan kehidupan agama yang lebih bermutu dan merata, penguatan kapasitas lembaga sosial keagamaan dalam pengelolaan dana sosial keagamaan dan pemberdayaan umat, dan terbentuknya berbagai forum komunikasi antarumat beragama untuk mendukung terwujudnya kerukunan intern dan antarumat beragama.</p>\r\n\r\n<p>Dalam peran pembangunan kerukunan antar umat beragama, FKUB    memiliki peran strategis dalam perwujudan pembangunan bidang keagamaan. Komitmen antara Pemerintah dengan FKUB sebagai mitra dalam pembinaan kerukunan umat beragama merupakan modal dasar perwujudan tugas dan fungsi FKUB. Komitmen tersebut akan terwujud  apabila  antar masing – masing dapat saling bermitra dan saling mendukung. </p>\r\n\r\n<p>Kondisi umat beragama saat ini memang tidak sederhana dan teramat kompleks. Persoalan umat beragama kini tidak cukup hanya dilihat dari aspek sosial keagamaan semata, namun juga perlu dilihat dari aspek – aspek lain yang menyangkut letak geografis, kependudukan, pendidikan, hukum dan peradilan, sosial, ekonomi, budaya, politik dan bahkan permasalahan yang menyangkut dunia global. Disinilah sesungguhnya peran strategis FKUB sangat diperlukan karena mengingat bahwa diantara sekian banyak persoalan, minimal dapat diawali dengan dialog/ sarasehan dan negoisasi yang sangat mungkin untuk dikembangkan dan dilakukan oleh FKUB baik ditingkat Provinsi maupun ditingkat Kota dan Kabupaten. Proses – proses tersebut akan terwujud dengan baik ketika kelembagaan FKUB berjalan dengan baik pula.</p>\r\n\r\n<p><em>Hadirin yang berbahagia, </em>Berkenaan dengan hal tersebut, pada kesempatan ini saya mengajak seluruh peserta kegiatan ini untuk menjaga komitmen,  sinergi,   keterpaduan dan saling   menjaga keharmonisan hubungan demi terwujudnya masyarakat yang lebih sejahtera lahir dan batin. Kedepankan upaya bermusyawarah dalam setiap penyelesaian masalah dan konflik yang timbul sehingga masing-masing pihak yang bertikai dapat saling menghormati dan menghargai.</p>\r\n', '/assets/upload/berita/12_2020/295af9b0278b6cbd226cb8757cf37bb5.png', '295af9b0278b6cbd226cb8757cf37bb5.png', '2019-11-13', 1, '2020-12-30 19:36:57', NULL, NULL),
(12, 'Berita', 'PENGUATAN DAN PEMANTAPAN IDEOLOGI PANCASILA (KADER PANCASILA)', '<p>Kegiatan dengan maksud mensosialisasikan dan reedukasi pentingnya menja Pancasila sebagai konsensus nasional dan payung kebangsaan serta menumbuh kembangkan pemahaman segenap elemen masyarakat terhadap Pancasila sebagai ideologi Negara, pandangan hidup, dan falsafah bangsa Indonesia yang kemudian bertujuan terbentuknya Kader-kader Pancasila guna menjaga Ideologi Pancasila dan keutuhan Negara Indonesia serta memasyarakatkan lima sila Pancasila dan mengimplementasikan dalam kehidupan sehari-hari.</p>\r\n\r\n<p>Dilaksanakan pada hari Selasa dan Rabu tanggal 5-6 November 2019 di Hotel Riyadi Palace, Kota Surakarta. Peserta teridi dari Kwarda dan Kwarcab Pramuka Kab/Kota se-Jawa Tengah yang berjumlah 80 orang.</p>\r\n\r\n<p>Metode/Mekanisme yang digunakan pada Kegiatan Penguatan dan Pemantapan Ideologi Pancasila (Kader Pancasila) Angkatan IV berupa ceramah dan tanya jawab. Berikut materi dan narasumber yang menyampaikan ceramah :</p>\r\n\r\n<ol>\r\n <li>Pancasila Sebagai Solusi Problem Bangsa oleh Prof. Dr. Warsino, M.Hum</li>\r\n <li>Strtategi Membumikan Pancasila Dikalangan Generasi Milenial oleh Kwarda Provinsi Jawa Tengah.</li>\r\n <li>Pancasila Sebagai Benteng Penjaga Pancasila oleh Kwada Provinsi Jawa Tengah.</li>\r\n <li>Menangkal Faham Radikalisme dan Terorisme Dikalangan Generasi Muda oleh Drs. Budiyanto, SH., M.Hum (Ketua FKPT Jawa Tengah).</li>\r\n</ol>\r\n\r\n<p>Acara tersebut dibuka oleh Kepala Badan Kesatuan Bangsa dan Politik Provinsi Jawa Tengah, Drs. Achmad Rofa’i, M.Si yang dalam sambutannya menyampaikan :</p>\r\n\r\n<p>Era demokrasi yang terus berjalan menuju fase yang semakin baik merupakan berkah tersendiri bagi bangsa dan negara kita. Era keterbukaan semakin memberikan jaminan pemenuhan hak berpendapat dan terlibat dalam proses pembangunan. Ini men­jadi poin penting pencapaian kualitas demokrasi di Indonesia. Negara Indonesia sendiri telah menjadi salah satu negara di dunia yang sangat mengedepankan demokrasi dalam pelaksanaan pembangun­annya.</p>\r\n\r\n<p>Banyak kemajuan yang telah dicapai dari perkembangan demokrasi kita hari ini, namun banyak pula perbaikan yang masih harus dibuat. Kita semua harus optimis bahwa catatan-catatan tersebut akan menjadi penting bagi perbaikan sistem demokrasi di Indonesia. Prioritas kita hari ini adalah mengawal demokrasi agar semakin substansial dan memberikan manfaat bagi perkembangan kesejahteraan masyarakat.</p>\r\n\r\n<p>Tanpa mengurangi optimisme dan keyakinan kita atas masa depan demokrasi di Indonesia, Beberapa catatan tetap harus kita garis bawahi sebagai bentuk concern dan kewaspadaan kita bersama dalam pelaksanaan pengawalan proses pembangunan demokrasi di Indonesia. Maraknya hoak bernuansa politik, budaya nyinyir, perang di sosial media, buzzer dan influenzer, serta berbagai bentuk black campaign, menjurus pada praktik tidak baik dalam berdemokrasi.</p>\r\n\r\n<p>Ditengah jaman yang semakin modern, komunikasi informasi yang semakin mengglobal dan terbuka, alur informasi menjadi semakin tidak tersekat. Perbincangan isu politik nasional di tingkatan elit menjadi perbincangan biasa dan nyata di berbagai warung kopi dan tongkrongan pos kampling. Jadi jangan heran juga jika anak anda yang masih duduk di bangku SMA bahkan SMP sudah mengetahui sepenggal informasi tentang Revisi UU KPK, RKUHP dan sebagainya.</p>\r\n\r\n<p>Bukan hanya itu,  ajakan demo dari kakak-kakak mahasiswanya, seolah juga menjadi ajakan bagi anak-anak kita. Padahal secara usia dan referensi, mereka belum cukup mendapatkan gambaran data, dan belum relatif dewasa untuk bisa mengambil keputusan dengan bijak.</p>\r\n\r\n<p>Fenomena demo adik-adik SMA dan STM bisa menjadi gambaran. Betapa sekat informasi yang terbuka jika tidak disertai dengan pemahaman dan penjelasan yang memadai bisa menjebak generasi kita pada kesimpulan sikap seketika yang terkadang kurang bijak. Di Jakarta muncul korban. Di Magelang berujung pada kericuhan.</p>\r\n\r\n<p>Ini adalah PR kita semua bapak ibu. Keluarga dan orang tua, bersama seluruh komponen bangsa punya tanggungjawab moral dan tanggung jawab sosial untuk menyertai perjalanan demokrasi ini, agar bijak dan terarah.</p>\r\n\r\n<p>Hadirin yang berbahagia, Jika kita berbicara tentang proses pendidikan dan penyadaran karakter bangsa, tentu tidak bisa dipisahkan dari Pramuka. Gerakan Pramuka merupakan kesatuan entitas pembangun bangsa Indonesia hingga sekarang.  Pramuka sejak awal didirikan, hadir untuk melatih generasi muda agar memaksimalkan setiap potensi yang ada di dalam dirinya, baik itu intelektual, spiritual, sosial, dan fisik. Tentu ini menjadi poin penting dimana generasi muda adalah pemegang tongkat estafet perjalanan bangsa ke depannya.</p>\r\n\r\n<p>Sekali lagi saya ingin menyampaikan apresiasi dan penghargaan kepada para pegiat Pramuka di seluruh pelosok tanah air, khususnya para pegiat Pramuka yang berada di ruangan ini, yang tanpa lelah dan dengan penuh semangat telah melaksanakan berbagai kegiatan yang bermanfaat bagi generasi muda, masyarakat dan bangsa.</p>\r\n\r\n<p>Pramuka merupakan kekuatan yang setiap saat mendampingi pemuda yang akan menjadi pemimpin bangsa nantinya. Kekuatan inti dari masa depan bangsa ini. Ini yang kadang tidak banyak kita sadari. Oleh sebab itu secara khusus saya ingin menyampaikan sekali lagi penghargaan, apresiasi kepada pegiat Pramuka, atas kerja keras dan capaian-capaian yang selama ini telah dikerjakan. Ke depan saya ingin melihat Pramuka tumbuh sebagai sebuah gerakan dan lebih berperan lagi jadi ujung tombak dalam melaksanakan program-program prioritas pemerintah. Di bidang kesehatan, di bidang pendidikan, program-program kesejahteraan rakyat, dan pembangunan karakter dan mentalitas generasi bangsa.</p>\r\n\r\n<p>Hadirin yang berbahagia,Selanjutnya saya ingin menyampaikan sedikit pesan. Pancasila sebagai dasar negara, ideologi negara, dan pandangan hidup bangsa yang digali oleh para pendiri bangsa merupakan anugerah yang tiada tara dari Tuhan Yang Maha Esa buat bangsa Indonesia.</p>\r\n\r\n<p>Pancasila sebagai suatu keyakinan dan pendirian yang asasi harus terus diperjuangkan. Keberagaman kondisi geografis, flora, fauna hingga aspek antropologis dan sosiologis masyarakat hanya dapat dirajut dalam bingkai kebangsaan yang inklusif. Proses internalisasi sekaligus pengamalan nilai-nilai Pancasila harus dilakukan dan diperjuangkan secara terus menerus. Pancasila harus tertanam dalam hati yang suci dan diamalkan dalam kehidupan sehari-hari.</p>\r\n\r\n<p>Gerakan Pramuka bersama seluruh komponen bangsa punya tanggungjawab moral dan tanggung jawab sosial untuk menyertai perjalanan bangsa ini, agar bijak dan terarah. Mari kita bersama-sama kedepan lebih massif bekerjasama melaksanakan program-program penguatan dan pemantapan ideologi Pancasila, sampai ke tingkat akar rumput, sampai ke tingkat terkecil yakni keluarga. Mari kita lahirkan Kader-kader Pancasila, mulai dari generasi muda, dimotori oleh Pramuka, Demi Indonesia di masa depan yang semakin maju dan sejahtera.</p>\r\n', '/assets/upload/berita/12_2020/65f4967c8ad90be9731c59725b9b08fc.png', '65f4967c8ad90be9731c59725b9b08fc.png', '2019-11-13', 1, '2020-12-30 19:38:36', NULL, NULL),
(13, 'Berita', 'KEGIATAN PENGEMBANGAN & PENINGKATAN WAWASAN KEBANGSAAN & NASIONALISME', '<p>Grobogan (17/11) – Merawat Keragaman, Membangun Toleransi Dalam Rangka Penguatan Wawasan Kebangsaan.</p>\r\n\r\n<p>Memperhatikan protokol kesehatan Covid-19, kegiatan dilaksanakan pada hari Selasa, 17 Nopember 2020 bertempat di Hotel Kyriad Grand Master, Grobogan.</p>\r\n\r\n<p>Peserta pada kegiatan kali ini seluruh elemen masyarakat, yakni Forum Kerukuran Umat Beragama (FKUB) se Kabupaten Grobogan.</p>\r\n\r\n<p>Dihadiri oleh Haerudin, Ka. Kesbangpol Jateng yang saat ini sebagai Pjs. Bupati Grobogan; Sulistyo Yuli Utomo, Plh. Ka. Kesbangpol Jateng; Daru Wisakti, Ka. Kesbangpol Grobogan; Taslim Syahlan, Ketua FKUB Jateng; dan M. Toha, Ketua FKUB Grobogan.</p>\r\n\r\n<p>Pembukaan diawali dengan menyanyikan lagu Indonesia Raya, Pancasila, Doa dan selanjutnya laporan panitia penyelenggara oleh Agus Setiawan selaku Plh. Kasubbag Umum & Kepegawaian Kesbangpol Jateng.</p>\r\n\r\n<p>Kemudian acara dibuka oleh Sulistyo Yuli Utomo, Plh. Ka. Kesbangpol Jateng.</p>\r\n\r\n<p>kegiatan dilakukan dengan ceramah dan diskusi tanya jawab.</p>\r\n\r\n<p>Usai pembukaan dilanjutkan dengan penyampaian materi oleh Taslim Syahlan, Ketua Forum Kerukunan Umat Beragama (FKUB) Jateng dengan materi menguatkan toleransi dan semangat kebangsaan dimasa pandemi covid-19.</p>\r\n\r\n<p>“Apakah kita sudah toleransi atau masih intoleransi” Kata Taslim. Taslim menerangkan bagaimana menguatkan toleransi dan semangat kebangsaan.</p>\r\n\r\n<p>Dalam perspektif FKUB, Taslim mengatakan “toleransi itu sejauh mana bisa saling menghargai agama masing-masing” .</p>\r\n\r\n<p>Untuk menumbuhkan semangat kebangsaan dalam menguatkan toleransi, Taslim menegaskan harus berdasarkan 4 pilar kebangsaan yakni Pancasila, UUD 1945, Bhinneka Tunggal Ika, dan NKRI harga mati.</p>\r\n\r\n<p>Taslim menjelaskan sebagai penguatan toleransi beragama harus dengan menumbuhkan sikap kedewasaan/moderasi beragama, solidaritas, sinergitas, dan integritas toga-toma sebagai perekat bangsa.</p>\r\n\r\n<p>FKUB Jateng sendiri selama pandemi selalu melakukan kegiatan sosial untuk membantu sesama, Taslim mengatakan niat baik pasti akan selalu mendapat kemudahan.</p>\r\n\r\n<p>Dilanjutkan pemateri kedua Haerudin, Ka. Kesbangpol Jateng yang saat ini sebagai Pjs. Bupati Grobogan dengan materi Pengamalan nilai-nilai Pancasila di era disrupsi.</p>\r\n\r\n<p>Era disrupsi sebagai fenomena beralihnya aktivitas di dunia nyata ke dunia maya dengan perubahan yang luar biasa, menuntut kita harus beradaptasi dengan fenomena tersebut.</p>\r\n\r\n<p>“Era disrupsi hari ini, kita dituntut mengikuti perubahan tersebut tentu dengan berdasarkan nilai-nilai Pancasila” kata Haerudin.</p>\r\n\r\n<p>Karena di era disrupsi ini telah menempatkan geopolitik secara terbuka dan secara cepat, baik dari bidang sosialbudaya, ekonomi dan politik.</p>\r\n\r\n<p>Hal tersebut menuntut masyarakat khususnya generasi muda bisa memfilter era disrupsi dimana pengaruhnya dapat mempengaruhi karakter mereka.</p>\r\n\r\n<p>Haerudin juga menyampaikan beberapa persoalan terhadap penanganan Covid-19 yang ada di Grobogan.</p>\r\n\r\n<p>Menurut Haerudin angka kematian di Grobogan meningkat dikarenakan penanganan yang lambat. “Kultur kita masih kebiasaan, kalau belum parah itu diobati sendiri” kata Haerudin.</p>\r\n\r\n<p>Haerudin menjelaskan, jika sudah gejala untuk segera di periksa ke Rumah Sakit dan nantinya penanganan akan lebih baik.</p>\r\n\r\n<p>Penutup acara, dilanjutkan penyerahan Piagam Watugong yang telah disepakati beberapa hari lalu tanggal 10 Oktober 2020 di Viharra Buddhagaya Watugong Semarang.</p>\r\n\r\n<p>Piagam tersebut sebagai roadmap dan pilot project nasional kerukunan umat beragama dannkepercayaan untuk menjaga stabilitas NKRI, kebebasan beragama, mencegah radikalisme serta intoleransi.</p>\r\n\r\n<p>Rumusan Piagam Watugong sebagai berikut :</p>\r\n\r\n<ol>\r\n <li>Penguatan nilai-nilai Pancasila, UUD 1945, Bhinneka Tunggal Ika, Keutuhan NKRI.</li>\r\n <li>Penguatan moderasi beragama dan kepercayaan terhadap Tuhan YME.</li>\r\n <li>Penguatan toleransi, solidaritas, dan silaturahmi kebangsaan.</li>\r\n</ol>\r\n\r\n<p>Disebut Piagam Watugong karena merujuk tempat diadakanya kesepakatan.</p>\r\n', '/assets/upload/berita/12_2020/fb1f1f3945eb0937e7e9b44c3af63a67.png', 'fb1f1f3945eb0937e7e9b44c3af63a67.png', '2020-11-18', 1, '2020-12-30 19:41:17', '2020-12-31 03:02:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_bidang`
--

CREATE TABLE `turin_bidang` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_bidang`
--

INSERT INTO `turin_bidang` (`id`, `nama`) VALUES
(1, 'BIDANG PELATIHAN KERJA DAN PRODUKTIVITAS'),
(2, 'BIDANG PENEMPATAN TENAGA KERJA DAN TRANSMIGRASI'),
(3, 'BIDANG HUBUNGAN INDUSTRI DAN JAMINAN SOSIAL'),
(4, 'BIDANG PENGAWASAN KETENAGAKERJAAN');

-- --------------------------------------------------------

--
-- Table structure for table `turin_bidang_data`
--

CREATE TABLE `turin_bidang_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_bidang` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `file` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_bidang_data`
--

INSERT INTO `turin_bidang_data` (`id`, `id_bidang`, `judul`, `file`, `deskripsi`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 'Jadwal Pertandingans', '1_(4).pdf', '/assets/upload/keloladatabidang/04_2020/1_(4).pdf', 1, '2020-04-15 20:00:36', '2020-04-16 01:15:19', NULL),
(3, 1, 'latas', 'STATISTIK_I_2017_web_latas21.pdf', '/assets/upload/databidang/04_2020/STATISTIK_I_2017_web_latas21.pdf', 1, '2020-04-17 17:26:14', '2020-04-17 17:29:24', NULL),
(4, 2, 'Pelatihan Kerja & Produktivitas', 'Penempatan_Tenaga_Kerja.pdf', '/assets/upload/databidang/04_2020/Penempatan_Tenaga_Kerja.pdf', 1, '2020-04-17 17:28:45', '2020-04-17 17:29:07', NULL),
(5, 2, 'Penempatan Tenaga Kerja 2018 ', 'bidang_penempatan_tenaga_kerja_2018.pdf', '/assets/upload/databidang/04_2020/bidang_penempatan_tenaga_kerja_2018.pdf', 1, '2020-04-17 17:34:41', '2020-04-17 17:34:50', NULL),
(6, 2, 'PENTA ', 'STATISTIK_I_2017_web_PENTA.pdf', '/assets/upload/databidang/04_2020/STATISTIK_I_2017_web_PENTA.pdf', 1, '2020-04-17 17:35:29', '2020-04-17 17:35:39', NULL),
(7, 2, 'data statistik penta 2016', 'buku_STATISTIK_triwulan_IV_2016_penta.pdf', '/assets/upload/databidang/04_2020/buku_STATISTIK_triwulan_IV_2016_penta.pdf', 1, '2020-04-17 17:36:25', '2020-04-17 17:36:33', NULL),
(8, 3, 'Hubungan Industri dan Jaminan Sosial Tahun 2018 ', 'hubungan_industri_dan_jamsos.pdf', '/assets/upload/databidang/04_2020/hubungan_industri_dan_jamsos.pdf', 1, '2020-04-17 17:52:45', '2020-04-17 17:52:55', NULL),
(9, 3, 'Hubungan Industrial & Jamsos 2018', 'bidang_hub_industrial_dan_jamsos_2018.pdf', '/assets/upload/databidang/04_2020/bidang_hub_industrial_dan_jamsos_2018.pdf', 1, '2020-04-17 17:53:28', '2020-04-17 17:53:40', NULL),
(10, 3, 'HI ', 'STATISTIK_I_2017_web_HI.pdf', '/assets/upload/databidang/04_2020/STATISTIK_I_2017_web_HI.pdf', 1, '2020-04-17 17:54:13', '2020-04-17 17:54:21', NULL),
(11, 4, ' Pengawasan Ketenagakerjaan Tahun 2018 ', 'pengawas_tenaga_kerja.pdf', '/assets/upload/databidang/04_2020/pengawas_tenaga_kerja.pdf', 1, '2020-04-17 17:56:41', '2020-04-17 17:56:48', NULL),
(12, 4, 'Bidang Pengawasan Tenaga Kerja 2018', 'bidang_pengawasan_TK_2018.pdf', '/assets/upload/databidang/04_2020/bidang_pengawasan_TK_2018.pdf', 1, '2020-04-17 17:57:30', '2020-04-17 17:57:35', NULL),
(13, 4, 'PENGAWAS', 'STATISTIK_I_2017_PENGAWAS_web.pdf', '/assets/upload/databidang/04_2020/STATISTIK_I_2017_PENGAWAS_web.pdf', 1, '2020-04-17 17:58:23', '2020-04-17 17:58:30', NULL),
(15, 1, 'Pelatihan & Produktifitas 2018', '6f860361ee39c0304b988729cba15b87.pdf', '/assets/upload/databidang/04_2020/6f860361ee39c0304b988729cba15b87.pdf', 1, '2020-04-18 12:27:46', NULL, NULL),
(16, 1, 'Data Statistik', '19278512877f4b2d740f6d9a95db1543.pdf', '/assets/upload/keloladatabidang/04_2020/19278512877f4b2d740f6d9a95db1543.pdf', 1, '2020-04-18 13:19:59', '2020-04-18 13:21:25', NULL),
(17, 4, ' Pelatihan Kerja & Produktivitas ', 'c87858db7c70b6a8473d0c6ca47c25d3.pdf', '/assets/upload/databidang/04_2020/c87858db7c70b6a8473d0c6ca47c25d3.pdf', 1, '2020-04-18 13:21:40', NULL, NULL),
(18, 2, 'Penempatan Tenaga Kerja dan Ketransmigrasian 2018', 'aaebe9bcca54876f8b7639694d0ac7f3.pdf', '/assets/upload/databidang/04_2020/aaebe9bcca54876f8b7639694d0ac7f3.pdf', 1, '2020-04-18 13:23:20', NULL, NULL),
(19, 4, 'Peran LKS Bipartit Dalam Kesiapan Pekerja Mengikuti Uji Kompetensi', '26968f5923f6176decc74d73a65a0631.pdf', '/assets/upload/databidang/04_2020/26968f5923f6176decc74d73a65a0631.pdf', 1, '2020-04-18 13:27:23', NULL, NULL),
(20, 4, 'Peran dan Fungsi LKS Bipartit Dalam Meningkatkan Kualitas PKB', 'ca04d1db977c43574ff3a244873763d5.pdf', '/assets/upload/databidang/04_2020/ca04d1db977c43574ff3a244873763d5.pdf', 1, '2020-04-18 13:28:13', NULL, NULL),
(21, 4, ' Hubungan Industri & Jaminan Sosial', '509b4db445537cd1dba846e77b894192.pdf', '/assets/upload/databidang/04_2020/509b4db445537cd1dba846e77b894192.pdf', 1, '2020-04-18 13:29:43', NULL, NULL),
(22, 4, 'Pengawasan Ketenagakerjaan', 'db2cc1a0ec1196957b8409d3cb1e71bd.pdf', '/assets/upload/databidang/04_2020/db2cc1a0ec1196957b8409d3cb1e71bd.pdf', 1, '2020-04-18 13:30:54', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_download`
--

CREATE TABLE `turin_download` (
  `id` int(11) UNSIGNED NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_download`
--

INSERT INTO `turin_download` (`id`, `judul`, `deskripsi`, `file`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 'Makalah-Pilkada-dan-Agama-Prof-Dr-Mujahirin-Thohir 2018', '/assets/upload/download/01_2021/374ee68d83bcaa27b8d83beb9670aabe.pdf', '374ee68d83bcaa27b8d83beb9670aabe.pdf', 3, '2021-01-08 04:49:45', NULL, NULL),
(7, 'Kesiapan-PAM-Pilgub-dan-Pilkada-2018-oleh-Kapolda-Jateng', '/assets/upload/download/01_2021/bc32710e4de1facde28473f7295a464e.pdf', 'bc32710e4de1facde28473f7295a464e.pdf', 3, '2021-01-08 04:50:08', NULL, NULL),
(8, 'Peran-DPRD-Dalam-Mensukseskan-Pilgub-dan-Pilkada-Serentak-oleh-Ketua-DPRD-Jawa-Tengah 2018', '/assets/upload/download/01_2021/598073c7a798b9e0ad3614b066b34a08.pdf', '598073c7a798b9e0ad3614b066b34a08.pdf', 3, '2021-01-08 04:50:30', NULL, NULL),
(9, 'PERAN-KEJAKSAAN-TINGGI-JAWA-TENGAH-DALAM-PENEGAKAN-HUKUM-TERHADAP-PELANGGARAN-PILGUB-DAN-PILKADA-SER', '/assets/upload/download/01_2021/d2024e4035934006968d73178cc5a86c.pdf', 'd2024e4035934006968d73178cc5a86c.pdf', 3, '2021-01-08 04:50:50', NULL, NULL),
(10, 'Peran-Serta-Masyarakat-Dalam-P4GN-oleh-Atiek-Surniati 2018', '/assets/upload/download/01_2021/205c318cca49ad88a4efe4913f48927e.pdf', '205c318cca49ad88a4efe4913f48927e.pdf', 3, '2021-01-08 04:51:06', NULL, NULL),
(12, 'FORM B DAN C BUKU PANDUAN HARI JADI 69 PROVINSI JAWA TENGAH', '/assets/upload/download/01_2021/1569e9fc214947a3f919f5e843538d8e.pdf', '1569e9fc214947a3f919f5e843538d8e.pdf', 3, '2021-01-08 04:58:25', NULL, NULL),
(13, 'FORM A BUKU PANDUAN HARI JADI 69 PROVINSI JAWA TENGAH', '/assets/upload/download/01_2021/e4a7d6071b4786a4b74bff8eac96d8d6.pdf', 'e4a7d6071b4786a4b74bff8eac96d8d6.pdf', 3, '2021-01-08 04:58:45', NULL, NULL),
(14, 'SAMBUTAN GUBERNUR JAWA TENGAH', '/assets/upload/download/01_2021/dc6bbaddb12a154615d3cd9648610f1c.pdf', 'dc6bbaddb12a154615d3cd9648610f1c.pdf', 3, '2021-01-08 05:01:17', NULL, NULL),
(15, 'RANTAMAN TITI LAKSANA UPACARA', '/assets/upload/download/01_2021/652acf5e94ff0d15e1eded33a7687d94.pdf', '652acf5e94ff0d15e1eded33a7687d94.pdf', 3, '2021-01-08 05:01:59', NULL, NULL),
(16, 'DOA HARI JADI PROVINSI JAWA TENGAH', '/assets/upload/download/01_2021/305cb6b0fb35a96672ef9858d9c0a5a0.pdf', '305cb6b0fb35a96672ef9858d9c0a5a0.pdf', 3, '2021-01-08 05:03:25', NULL, NULL),
(17, 'SEJARAH PROVINSI JAWA TENGAH (bahasa jawa)', '/assets/upload/download/01_2021/3f3e38ef5917059901126c6c4af48715.pdf', '3f3e38ef5917059901126c6c4af48715.pdf', 3, '2021-01-08 05:04:31', NULL, NULL),
(18, 'SEJARAH PROVINSI JAWA TENGAH (bahasa indonesia)', '/assets/upload/download/01_2021/713a5d8e4a9f783e6ac6a6e389417455.pdf', '713a5d8e4a9f783e6ac6a6e389417455.pdf', 3, '2021-01-08 05:04:50', NULL, NULL),
(19, 'TANTANGAN DEMOKRASI INDONESIA KEDEPAN (Drs. Syarmadani, M.Si)', '/assets/upload/download/01_2021/ccc1b968cda486fcdf854159af546232.pdf', 'ccc1b968cda486fcdf854159af546232.pdf', 3, '2021-01-08 05:06:52', NULL, NULL),
(20, 'Peran Pemerintah Kabupaten/Kota dalam Pengembangan Demokrasi di Jawa Tengah (Dr. Laila Kholid Alfird', '/assets/upload/download/01_2021/3727f570185db1c6f286b2405f83938d.pdf', '3727f570185db1c6f286b2405f83938d.pdf', 3, '2021-01-08 05:08:11', NULL, NULL),
(21, 'PERKEMBANGAN INDEKS DEMOKRASI INDONESIA (IDI) PROVINSI JAWA TENGAH ( Muh. Saichudin)', '/assets/upload/download/01_2021/72d9eb27d1d80628b3b355041c76ae6a.pdf', '72d9eb27d1d80628b3b355041c76ae6a.pdf', 3, '2021-01-08 05:09:05', NULL, NULL),
(22, 'INDEKS DEMOKRASI INDONESIA PROVINSI JAWA TENGAH TAHUN 2019', '/assets/upload/download/01_2021/dd746a714103414048d7b51517e7018d.pdf', 'dd746a714103414048d7b51517e7018d.pdf', 3, '2021-01-08 05:09:48', NULL, NULL),
(23, 'LAPORAN PANITIA', '/assets/upload/download/01_2021/eec00b7283660a23540e912d7507e670.pdf', 'eec00b7283660a23540e912d7507e670.pdf', 3, '2021-01-08 05:10:56', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_form`
--

CREATE TABLE `turin_form` (
  `id` int(11) UNSIGNED NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_form`
--

INSERT INTO `turin_form` (`id`, `judul`, `deskripsi`, `file`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Permohonan Informasi', '', 'https://docs.google.com/forms/d/e/1FAIpQLSe7zCPr6kQmV9752jLK1FxqctOfULd31ISfT1CxEHFlXdHWMg/viewform?usp=sf_link', NULL, NULL, NULL, NULL),
(3, 'Pengaduan Layanan Publik', '', 'https://docs.google.com/forms/d/e/1FAIpQLScyy5Al29hqrKZL20OGgeWqFRaziMV8_HcY0vE88JPIjeGZlg/viewform?usp=sf_link', NULL, NULL, NULL, NULL),
(4, 'Pengajuan Keberatan', '', 'https://docs.google.com/forms/d/e/1FAIpQLScyy5Al29hqrKZL20OGgeWqFRaziMV8_HcY0vE88JPIjeGZlg/viewform?usp=sf_link', 1, '2020-05-08 05:37:19', NULL, NULL),
(5, 'Pengaduan ASN', '', 'https://docs.google.com/forms/d/e/1FAIpQLScNVC3SKpPmXG5jbXkeEyTblqjXbo3g6P3beXlKhr_Re1ZF8w/viewform?usp=sf_link', 1, '2020-05-08 05:37:45', NULL, NULL),
(6, 'Indeks Kepuasan Masyarakat ', '', 'https://docs.google.com/forms/d/e/1FAIpQLSctyvN48L0t8FhekdhrHy7Z-UqROPeL-p9Mfhkv30m4c_kBUQ/viewform', 1, '2020-05-08 05:38:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_galeri`
--

CREATE TABLE `turin_galeri` (
  `id` int(11) UNSIGNED NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_galeri`
--

INSERT INTO `turin_galeri` (`id`, `judul`, `deskripsi`, `created_by`, `created_at`, `updated_by`, `deleted_at`) VALUES
(6, 'Galeri 1', '0', 1, '2020-04-17 10:46:58', NULL, NULL),
(7, 'Galeri 2', '0', 1, '2020-04-17 10:46:58', '2020-04-17 15:48:05', NULL),
(8, 'Galeri 3', '0', 1, '2020-04-17 10:46:58', '2020-04-17 15:48:08', NULL),
(9, 'Galeri 4', '0', 1, '2020-04-17 10:46:58', '2020-04-17 15:48:10', NULL),
(10, 'Galeri 5', '0', 1, '2020-04-17 10:46:58', '2020-04-17 15:48:12', NULL),
(11, 'Galeri 6', '0', 1, '2020-04-17 10:46:58', '2020-04-17 15:48:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_galeri_data`
--

CREATE TABLE `turin_galeri_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_galeri` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `file` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_galeri_data`
--

INSERT INTO `turin_galeri_data` (`id`, `id_galeri`, `judul`, `file`, `deskripsi`, `created_by`, `created_at`, `updated_by`, `deleted_at`) VALUES
(31, 5, NULL, 'c85462827d0c338ac130c0b89d1cea56.png', '/assets/upload/foto/04_2020/c85462827d0c338ac130c0b89d1cea56.png', 1, '2020-04-16 20:04:41', NULL, NULL),
(32, 6, NULL, '764574cd399ea529f60061f5440eed76.png', '/assets/upload/foto/04_2020/764574cd399ea529f60061f5440eed76.png', 1, '2020-04-17 10:47:11', NULL, NULL),
(33, 6, NULL, '4e1643b0c40caad7f8057a40c3d077b1.png', '/assets/upload/foto/04_2020/4e1643b0c40caad7f8057a40c3d077b1.png', 1, '2020-04-17 10:47:19', NULL, NULL),
(34, 6, NULL, 'e3d53cfe35479ad03515a4791b01d3af.png', '/assets/upload/foto/04_2020/e3d53cfe35479ad03515a4791b01d3af.png', 1, '2020-04-17 10:47:28', NULL, NULL),
(35, 7, NULL, 'e3d53cfe35479ad03515a4791b01d3af.png', '/assets/upload/foto/04_2020/e3d53cfe35479ad03515a4791b01d3af.png', 1, '2020-04-17 10:47:28', NULL, NULL),
(36, 8, NULL, 'e3d53cfe35479ad03515a4791b01d3af.png', '/assets/upload/foto/04_2020/e3d53cfe35479ad03515a4791b01d3af.png', 1, '2020-04-17 10:47:28', NULL, NULL),
(37, 9, NULL, 'e3d53cfe35479ad03515a4791b01d3af.png', '/assets/upload/foto/04_2020/e3d53cfe35479ad03515a4791b01d3af.png', 1, '2020-04-17 10:47:28', NULL, NULL),
(38, 10, NULL, 'e3d53cfe35479ad03515a4791b01d3af.png', '/assets/upload/foto/04_2020/e3d53cfe35479ad03515a4791b01d3af.png', 1, '2020-04-17 10:47:28', NULL, NULL),
(39, 11, NULL, 'e3d53cfe35479ad03515a4791b01d3af.png', '/assets/upload/foto/04_2020/e3d53cfe35479ad03515a4791b01d3af.png', 1, '2020-04-17 10:47:28', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_galeri_video`
--

CREATE TABLE `turin_galeri_video` (
  `id` int(11) UNSIGNED NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_galeri_video`
--

INSERT INTO `turin_galeri_video` (`id`, `judul`, `deskripsi`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'Kegiatan Pengembangan dan Peningkatan Wawasan Kebangsaan dan Nasionalisme', 'cmzAlBXfsL4', 1, '2020-04-17 11:06:45', '2020-12-04 01:10:44', NULL),
(4, 'Wawasan Kebangsaan PPWK Kesbangpol Jateng', 'lQ5NZ3lRop8', 1, NULL, '2020-12-04 01:11:58', NULL),
(5, 'Penguatan Ideologi Pancasila Kesbangpol Jateng 2017', 'qGmlx7jaB58', 1, NULL, '2020-12-04 01:11:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_header`
--

CREATE TABLE `turin_header` (
  `id` int(11) UNSIGNED NOT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `turin_infografis`
--

CREATE TABLE `turin_infografis` (
  `id` int(11) UNSIGNED NOT NULL,
  `judul` text DEFAULT NULL,
  `isi` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_infografis`
--

INSERT INTO `turin_infografis` (`id`, `judul`, `isi`, `deskripsi`, `file`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Penguatan Pendidikan Politik', '<p>0</p>\r\n', '/assets/upload/infografis/01_2021/cc0237de8e35c7d39bda9cc85342639e.png', 'cc0237de8e35c7d39bda9cc85342639e.png', 1, '2020-05-01 16:12:51', '2021-01-08 01:40:11', NULL),
(2, 'Penguatan Harmonisasi Kerukunan Antar Etnis', '<p>0</p>\r\n', '/assets/upload/infografis/01_2021/6ac5caef7ea165c38044882315fb746b.png', '6ac5caef7ea165c38044882315fb746b.png', 1, '2020-05-01 16:12:51', '2021-01-08 01:39:06', NULL),
(3, 'Pengembangan & Peningkatan Wawasan Kebangsaan', '<p>0</p>\r\n', '/assets/upload/infografis/01_2021/df5c34d994edffdf48a6a5f4e6071350.png', 'df5c34d994edffdf48a6a5f4e6071350.png', 1, '2020-05-01 16:12:51', '2021-01-08 01:37:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_informasi`
--

CREATE TABLE `turin_informasi` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `url` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_informasi`
--

INSERT INTO `turin_informasi` (`id`, `nama`, `url`) VALUES
(1, 'Informasi Publik', 'ip'),
(2, 'Informasi Dikecualikan', 'id'),
(3, 'Informasi Berkala', 'ib'),
(4, 'Informasi Setiap Saat', 'iss'),
(5, 'Informasi Serta Merta', 'ism');

-- --------------------------------------------------------

--
-- Table structure for table `turin_informasi_data`
--

CREATE TABLE `turin_informasi_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_informasi` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_informasi_data`
--

INSERT INTO `turin_informasi_data` (`id`, `id_informasi`, `judul`, `deskripsi`, `file`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(9, 1, 'Daftar Informasi Publik Tahun 2018', '/assets/upload/informasi/01_2021/5cf761dc7e97b16d5fe35dc7d72a4927.pdf', '5cf761dc7e97b16d5fe35dc7d72a4927.pdf', 2, '2021-01-05 02:18:04', NULL, NULL),
(10, 1, 'SK Daftar Informasi Publik Tahun 2019', '/assets/upload/informasi/01_2021/40b3b24001e4ab0b8eec3518355e423c.pdf', '40b3b24001e4ab0b8eec3518355e423c.pdf', 2, '2021-01-05 02:18:28', NULL, NULL),
(11, 1, 'Daftar Informasi Publik Tahun 2019', '/assets/upload/informasi/01_2021/d49ad1053388b672df35e5096a6df7cb.pdf', 'd49ad1053388b672df35e5096a6df7cb.pdf', 2, '2021-01-05 02:18:55', NULL, NULL),
(12, 1, 'Daftar Informasi Publik Tahun 2020', '/assets/upload/informasi/01_2021/32dac7bb9c4860fb1625534db14ddd71.pdf', '32dac7bb9c4860fb1625534db14ddd71.pdf', 2, '2021-01-05 02:19:11', NULL, NULL),
(13, 2, 'SK Klasifikasi Informasi Dikecualikan Tahun 2018', '/assets/upload/informasi/01_2021/693d6ed9f9297ef810ea4e2bee67a10a.pdf', '693d6ed9f9297ef810ea4e2bee67a10a.pdf', 2, '2021-01-05 02:24:10', NULL, NULL),
(14, 2, 'Daftar Informasi Dikecualikan Tahun 2018', '/assets/upload/informasi/01_2021/a575a9bd2457c280e12ce92dff472ec8.pdf', 'a575a9bd2457c280e12ce92dff472ec8.pdf', 2, '2021-01-05 02:24:44', NULL, NULL),
(15, 2, ' Notulen Rapat PPID 22 MEI 2018', '/assets/upload/informasi/01_2021/fa2a7cd98f91de6861fefdf1d7416067.pdf', 'fa2a7cd98f91de6861fefdf1d7416067.pdf', 2, '2021-01-05 02:25:05', NULL, NULL),
(16, 2, 'SK Informasi Dikecualikan Tahun 2019', '/assets/upload/informasi/01_2021/508a713198052193ca45860d96ec1418.pdf', '508a713198052193ca45860d96ec1418.pdf', 2, '2021-01-05 02:28:08', NULL, NULL),
(17, 2, 'Daftar Informasi Dikecualikan Tahun 2019', '/assets/upload/informasi/01_2021/f02eb0d1446102ce679ea83e64f41e81.pdf', 'f02eb0d1446102ce679ea83e64f41e81.pdf', 2, '2021-01-05 02:28:45', NULL, NULL),
(18, 2, 'Notulen Rapat PPID 7 MEI 2019', '/assets/upload/informasi/01_2021/46d9b562b4ae59e71d8a5cb942f6a522.pdf', '46d9b562b4ae59e71d8a5cb942f6a522.pdf', 2, '2021-01-05 02:29:12', NULL, NULL),
(19, 2, 'Naskah Pertimbangan Uji Konsekuensi Informasi Tahun 2019', '/assets/upload/informasi/01_2021/9bb8515af8e37dd99a8b13fae802001d.pdf', '9bb8515af8e37dd99a8b13fae802001d.pdf', 2, '2021-01-05 02:29:37', NULL, NULL),
(20, 2, 'Informasi Dikecualikan Tahun 2020', '/assets/upload/informasi/01_2021/32e563d3b0d4a5539459f27f3e9d4043.pdf', '32e563d3b0d4a5539459f27f3e9d4043.pdf', 2, '2021-01-05 02:31:50', NULL, NULL),
(21, 3, 'Daftar Informasi Berkala Tahun 2018', '/assets/upload/informasi/01_2021/f354179efb21fa3e5af41eb585c69b8e.pdf', 'f354179efb21fa3e5af41eb585c69b8e.pdf', 2, '2021-01-05 02:57:48', NULL, NULL),
(22, 3, 'Daftar Informasi Berkala Tahun 2019', '/assets/upload/informasi/01_2021/00c6f4a15c1e9b6da5d710405d98cd3d.pdf', '00c6f4a15c1e9b6da5d710405d98cd3d.pdf', 2, '2021-01-05 02:58:01', NULL, NULL),
(23, 3, 'Daftar Informasi Berkala Tahun 2020', '/assets/upload/informasi/01_2021/90799ea28e03f9230b6ac1f1c49c1940.pdf', '90799ea28e03f9230b6ac1f1c49c1940.pdf', 2, '2021-01-05 02:58:14', NULL, NULL),
(24, 4, 'Daftar Informasi Tersedia Setiap Saat Tahun 2018', '/assets/upload/informasi/01_2021/c58c3cff0120535f481841407ee0ac0d.pdf', 'c58c3cff0120535f481841407ee0ac0d.pdf', 2, '2021-01-05 03:01:10', NULL, NULL),
(25, 4, 'Daftar Informasi Tersedia Setiap Saat Tahun 2019', '/assets/upload/informasi/01_2021/43d9470020a7c9a0738541f60d2f86ef.pdf', '43d9470020a7c9a0738541f60d2f86ef.pdf', 2, '2021-01-05 03:01:23', NULL, NULL),
(26, 4, 'Daftar Informasi Tersedia Setiap Saat Tahun 2020', '/assets/upload/informasi/01_2021/9e7805d279b60a10f3ca9edc54a8b7c8.pdf', '9e7805d279b60a10f3ca9edc54a8b7c8.pdf', 2, '2021-01-05 03:01:37', NULL, NULL),
(27, 5, 'Daftar Informasi Serta Merta Tahun 2018', '/assets/upload/informasi/01_2021/26158706297592b0480ef69ba7f1e43e.pdf', '26158706297592b0480ef69ba7f1e43e.pdf', 2, '2021-01-05 03:05:16', NULL, NULL),
(28, 5, 'Daftar Informasi Serta Merta Tahun 2019', '/assets/upload/informasi/01_2021/035010c0412dad596167894251a1687d.pdf', '035010c0412dad596167894251a1687d.pdf', 2, '2021-01-05 03:05:50', NULL, NULL),
(29, 5, 'Daftar Informasi Serta Merta Tahun 2020', '/assets/upload/informasi/01_2021/8abb2350f5b5912c77ee1dc362ae974c.pdf', '8abb2350f5b5912c77ee1dc362ae974c.pdf', 2, '2021-01-05 03:06:04', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_ip_jahat`
--

CREATE TABLE `turin_ip_jahat` (
  `id` int(11) UNSIGNED NOT NULL,
  `ips` varchar(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `turin_jabatan`
--

CREATE TABLE `turin_jabatan` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama_jabatan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `turin_kontak`
--

CREATE TABLE `turin_kontak` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_kontak`
--

INSERT INTO `turin_kontak` (`id`, `judul`, `deskripsi`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(13, 'Instagram', 'https://www.instagram.com/kesbangpoljateng/', 0, NULL, '2020-12-03 23:56:05', NULL),
(10, 'Youtube', 'https://www.youtube.com/channel/UC4LLBBmkMj-KJXtgjxiK0vA', 0, NULL, '2020-12-03 23:56:26', NULL),
(11, 'Facebook', 'https://www.facebook.com/pages/Kantor-kesbangpolinmas-provjawa-tengah/277520415644137', 0, NULL, '2020-12-03 23:55:19', NULL),
(12, 'Twitter', 'https://twitter.com/bakesbangprov', 0, NULL, '2020-12-03 23:53:13', NULL),
(15, 'Telepon', '024-8454990', 0, NULL, '2020-12-04 00:03:08', NULL),
(17, 'Email', 'info@kesbangpol.jatengprov.go.id\r\n', 0, NULL, '2020-12-04 00:02:57', NULL),
(18, 'Alamat', 'Jl. Ahmad Yani No. 160 Kota Semarang, Jawa Tengah\r\n', 0, NULL, '2020-12-04 00:03:17', NULL),
(16, 'SMS', '089690923824\r\n', 0, '2020-11-20 05:17:47', '2020-12-04 00:03:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_laporan`
--

CREATE TABLE `turin_laporan` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `url` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_laporan`
--

INSERT INTO `turin_laporan` (`id`, `nama`, `url`) VALUES
(1, 'Akses Informasi Publik', 'aip'),
(2, 'Ringkasan Informasi Publik', 'rip'),
(3, 'Rekap Tindak Lanjut Aduan', 'rtla'),
(4, 'Ringkasan Laporan/Pengaduan', 'rlp');

-- --------------------------------------------------------

--
-- Table structure for table `turin_laporan_data`
--

CREATE TABLE `turin_laporan_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_laporan` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_laporan_data`
--

INSERT INTO `turin_laporan_data` (`id`, `id_laporan`, `judul`, `deskripsi`, `file`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 'TLA', '/assets/upload/laporan/05_2020/9583004a7a1d8b7eaf522a924eaae196.pdf', '9583004a7a1d8b7eaf522a924eaae196.pdf', 1, '2020-05-08 05:32:58', NULL, NULL),
(2, 1, 'Register Pemohon Informasi Publik', '/assets/upload/laporan/01_2021/a1b0956cf7a6f229cba3a5dd16339078.pdf', 'a1b0956cf7a6f229cba3a5dd16339078.pdf', 2, '2021-01-08 02:17:49', NULL, NULL),
(3, 1, 'Laporan Layanan Informasi Tahun Publik 2018', '/assets/upload/laporan/01_2021/40785a1bb23b9b94c59059afcbd662f4.pdf', '40785a1bb23b9b94c59059afcbd662f4.pdf', 2, '2021-01-08 02:18:56', NULL, NULL),
(4, 1, 'Akses Informasi Publik Tahun 2018', '/assets/upload/laporan/01_2021/42feda813683a1ea4dd8ec57639169ea.pdf', '42feda813683a1ea4dd8ec57639169ea.pdf', 2, '2021-01-08 02:19:31', NULL, NULL),
(5, 1, 'Laporan Layanan Informasi Publik Tahun 2019', '/assets/upload/laporan/01_2021/8721f87a3a5039a163ae418193bc1145.pdf', '8721f87a3a5039a163ae418193bc1145.pdf', 2, '2021-01-08 02:20:28', NULL, NULL),
(6, 1, 'Akses Informasi Publik Tahun 2019', '/assets/upload/laporan/01_2021/7ca3d7062a94e64881de3e1a29c6cbfe.pdf', '7ca3d7062a94e64881de3e1a29c6cbfe.pdf', 2, '2021-01-08 02:20:57', NULL, NULL),
(7, 2, 'Laporan Tahunan PPID 2015', '/assets/upload/laporan/01_2021/df904de630b75c4129af3c967a2f6b1c.pdf', 'df904de630b75c4129af3c967a2f6b1c.pdf', 2, '2021-01-11 02:58:40', NULL, NULL),
(8, 2, 'Laporan Tahunan PPID 2016', '/assets/upload/laporan/01_2021/0dd6bddb143e8190d40e514e347ed28a.pdf', '0dd6bddb143e8190d40e514e347ed28a.pdf', 2, '2021-01-11 02:59:00', NULL, NULL),
(9, 2, 'Laporan Pelaksanaan PPID 2016', '/assets/upload/laporan/01_2021/d4331924d0923240616a44ae7034d6e2.pdf', 'd4331924d0923240616a44ae7034d6e2.pdf', 2, '2021-01-11 02:59:32', NULL, NULL),
(10, 2, 'Laporan Tahunan PPID 2017', '/assets/upload/laporan/01_2021/f6d2adf82d9a18dea15776f06f7067b4.pdf', 'f6d2adf82d9a18dea15776f06f7067b4.pdf', 2, '2021-01-11 02:59:52', NULL, NULL),
(11, 2, 'Laporan Pelaksanaan PPID 2018', '/assets/upload/laporan/01_2021/b06d2ea491626d1e0aecc0295e2c8d7e.pdf', 'b06d2ea491626d1e0aecc0295e2c8d7e.pdf', 2, '2021-01-11 03:00:15', NULL, NULL),
(12, 4, 'Laporan Penanganan Pengaduan', '/assets/upload/laporan/01_2021/be880a32193565b165c436001139e343.pdf', 'be880a32193565b165c436001139e343.pdf', 2, '2021-01-11 06:26:54', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_links`
--

CREATE TABLE `turin_links` (
  `id` int(11) UNSIGNED NOT NULL,
  `judul` text DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `isi` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_links`
--

INSERT INTO `turin_links` (`id`, `judul`, `icon`, `isi`, `deskripsi`, `file`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Website Provinsi Jawa Tengah', 'globe', 'http://www.jatengprov.go.id/', '/assets/upload/linkz/04_2020/7e176671eece3aa4f7cb7e9d6270156e.jpg', '7e176671eece3aa4f7cb7e9d6270156e.jpg', 1, '2020-04-19 17:49:43', '2020-11-20 12:40:46', NULL),
(3, 'Portal PPID Jawa Tengah', 'network', 'https://ppid.jatengprov.go.id/', '/assets/upload/linkz/04_2020/02691f5a04e66109412b74bd129ba710.jpg', '02691f5a04e66109412b74bd129ba710.jpg', 1, '2020-04-19 17:50:54', '2020-11-20 12:40:03', NULL),
(4, 'LaporGub', 'basket', 'https://laporgub.jatengprov.go.id/', '/assets/upload/linkz/04_2020/d22b16c66ba3301d8453080d4f0abb97.jpg', 'd22b16c66ba3301d8453080d4f0abb97.jpg', 1, '2020-04-19 17:51:18', '2020-11-19 16:17:16', NULL),
(5, 'Open Data', 'database', 'http://data.jatengprov.go.id/organization/badankesatuanbangsadanpolitikprovinsijawatengah', '/assets/upload/linkz/04_2020/7b0a1d5e2f166b1a2aea8f41afe497e7.jpg', '7b0a1d5e2f166b1a2aea8f41afe497e7.jpg', 1, '2020-04-19 17:51:47', '2020-11-20 12:39:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_log_activity`
--

CREATE TABLE `turin_log_activity` (
  `id` int(11) UNSIGNED NOT NULL,
  `emaily` text DEFAULT NULL,
  `passy` text DEFAULT NULL,
  `ips` varchar(11) DEFAULT NULL,
  `status` varchar(5) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_log_activity`
--

INSERT INTO `turin_log_activity` (`id`, `emaily`, `passy`, `ips`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(150, 'roby-2', '67bd54ca07247480b884057db184ee04ee78d4e3', '103.47.133.', 'Y', '2021-01-19 06:37:04', NULL, NULL),
(151, 'roby-2', '67bd54ca07247480b884057db184ee04ee78d4e3', '103.47.133.', 'Y', '2021-01-19 06:37:37', NULL, NULL),
(152, 'roby-2', '67bd54ca07247480b884057db184ee04ee78d4e3', '103.47.133.', 'Y', '2021-01-19 06:39:11', NULL, NULL),
(154, '+admin_2020', '2d15c23bf213dbb2db3a0481ef45d01fd4ffea0d', '103.102.14.', 'Y', '2021-01-19 06:41:19', NULL, NULL),
(155, 'roby-2', '67bd54ca07247480b884057db184ee04ee78d4e3', '103.47.133.', 'Y', '2021-01-19 06:42:42', NULL, NULL),
(156, 'roby-2', '67bd54ca07247480b884057db184ee04ee78d4e3', '103.47.133.', 'Y', '2021-01-20 02:02:21', NULL, NULL),
(157, '+admin_2020', '2d15c23bf213dbb2db3a0481ef45d01fd4ffea0d', '103.102.14.', 'Y', '2021-01-20 09:40:47', NULL, NULL),
(158, 'superadmin', 'adcd7048512e64b48da55b027577886ee5a36350', '182.2.39.10', 'X', '2021-01-22 16:11:52', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_maklumat_lhkpn`
--

CREATE TABLE `turin_maklumat_lhkpn` (
  `id` int(11) UNSIGNED NOT NULL,
  `tipe` char(11) DEFAULT NULL COMMENT 'M=maklumat, L=lhkpn',
  `judul` varchar(100) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_maklumat_lhkpn`
--

INSERT INTO `turin_maklumat_lhkpn` (`id`, `tipe`, `judul`, `tahun`, `deskripsi`, `file`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 'M', 'Alur Pelayanan Informasi Publik', NULL, '/assets/upload/maklumat/04_2020/7c07830fe69195bebbfb2a9846c263f7.pdf', '7c07830fe69195bebbfb2a9846c263f7.pdf', 1, '2020-04-17 20:59:29', NULL, NULL),
(6, 'M', 'TATA CARA PENGAJUAN KEBERATAN', NULL, '/assets/upload/maklumat/04_2020/b54749847a50c78f8fff0fce00fbe95d.pdf', 'b54749847a50c78f8fff0fce00fbe95d.pdf', 1, '2020-04-17 20:59:52', NULL, NULL),
(7, 'M', 'LAP PENGADUAN 2018', 2018, '/assets/upload/maklumat/04_2020/69fa3b4720d5aec0c46991730f221a47.pdf', '69fa3b4720d5aec0c46991730f221a47.pdf', 1, '2020-04-17 21:00:16', NULL, NULL),
(8, 'M', 'PERNYATAAN KEBERATAN ATAS PERMOHONAN INFORMASI ', NULL, '/assets/upload/maklumat/04_2020/1416afcb3bd5022f30d2ce714017b096.pdf', '1416afcb3bd5022f30d2ce714017b096.pdf', 1, '2020-04-17 21:00:44', NULL, NULL),
(9, 'M', 'RFK agt 2018', 2018, '/assets/upload/maklumat/04_2020/0a6a99f8bcd1b3f10784d73cb0aea9cf.pdf', '0a6a99f8bcd1b3f10784d73cb0aea9cf.pdf', 1, '2020-04-17 21:01:14', NULL, NULL),
(10, 'M', 'Cara Menyampaikan Pengaduan ke Disnakertrans Provinsi Jawa Tengah', NULL, '/assets/upload/maklumat/04_2020/fd75df7badb0ac7643f76e875a98295b.pdf', 'fd75df7badb0ac7643f76e875a98295b.pdf', 1, '2020-04-17 21:01:38', NULL, NULL),
(11, 'L', 'SUSI HANDAYANIE (Plt. Kadis / SEKRETARIS)', NULL, '/assets/upload/lhkpn/04_2020/8dd424e271ccfd5e3812bd95fc8d5ba2.pdf', '8dd424e271ccfd5e3812bd95fc8d5ba2.pdf', 1, '2020-04-17 21:06:23', NULL, NULL),
(12, 'L', 'KRISTONO (KEPALA BIDANG HUBUNGAN INDUSTRIAL DAN JAMINAN SOSIAL)', NULL, '/assets/upload/lhkpn/04_2020/a28c466e0754c858689b7e3232cf0dec.pdf', 'a28c466e0754c858689b7e3232cf0dec.pdf', 1, '2020-04-17 21:07:24', NULL, NULL),
(13, 'L', 'AHMAD AZIZ (KEPALA BIDANG PENEMPATAN TENAGA KERJA DAN TRANSMIGRASI)', NULL, '/assets/upload/lhkpn/04_2020/fa9c73ffa305d6582e1e76e6941de06d.pdf', 'fa9c73ffa305d6582e1e76e6941de06d.pdf', 1, '2020-04-17 21:08:00', NULL, NULL),
(14, 'L', 'MUMPUNIATI (KEPALA BALAI KESELAMATAN KERJA)', NULL, '/assets/upload/lhkpn/04_2020/efb4a743f8ca29da608e118d6a4f386a.pdf', 'efb4a743f8ca29da608e118d6a4f386a.pdf', 1, '2020-04-17 21:08:28', NULL, NULL),
(15, 'L', 'MOHAMMAD WISNU SURYA BARATHA (KEPALA BALAI PELAYANAN PENYELESAIAN PERSELISIHAN TENAGA KERJA)', NULL, '/assets/upload/lhkpn/04_2020/2a2ca666d27547006d619ae41f128d6c.pdf', '2a2ca666d27547006d619ae41f128d6c.pdf', 1, '2020-04-17 21:10:35', NULL, NULL),
(16, 'L', 'GUNTORO SEJATI (KEPALA BALAI LATIHAN KERJA CILACAP)', NULL, '/assets/upload/lhkpn/04_2020/c8d74500bc62c3c6bba29d195205f591.pdf', 'c8d74500bc62c3c6bba29d195205f591.pdf', 1, '2020-04-17 21:11:05', NULL, NULL),
(17, 'L', 'EDY PURWANTONO (Kepala Balai Balai Latihan Kerja Semarang 2)', NULL, '/assets/upload/lhkpn/04_2020/413c2695f79c3aa99c32e889b8a39b2d.pdf', '413c2695f79c3aa99c32e889b8a39b2d.pdf', 1, '2020-04-17 21:11:33', NULL, NULL),
(18, 'L', 'UMI HANI\' (KASI KELEMBAGAAN DAN HUBUNGAN INDUSTRIAL) ', NULL, '/assets/upload/lhkpn/04_2020/19380aa4c052e8f5f842870d897c0848.pdf', '19380aa4c052e8f5f842870d897c0848.pdf', 1, '2020-04-17 21:11:56', NULL, NULL),
(19, 'L', 'TUTIK TUSMIYATI (KEPALA SUBBAGIAN TATA USAHA BALAI KESELAMATAN KERJA) ', NULL, '/assets/upload/lhkpn/04_2020/4fb6769173acb8afc2f8ecf618838cb4.pdf', '4fb6769173acb8afc2f8ecf618838cb4.pdf', 1, '2020-04-17 21:12:19', NULL, NULL),
(20, 'L', 'TRI RETNO CAHYAWATI (KEPALA SATUAN PENGAWASAN KETENAGAKERJAAN WILAYAH PATI)', NULL, '/assets/upload/lhkpn/04_2020/75104b7eeb5599a7dda76eab8d756a7e.pdf', '75104b7eeb5599a7dda76eab8d756a7e.pdf', 1, '2020-04-17 21:12:53', NULL, NULL),
(21, 'L', ' TRI KARTIKA MARTIJANA (KASI PERLUASAN KESEMPATAN KERJA)', NULL, '/assets/upload/lhkpn/04_2020/94432ce514ab9207f126c46d3f16b343.pdf', '94432ce514ab9207f126c46d3f16b343.pdf', 1, '2020-04-17 21:13:24', NULL, NULL),
(22, 'L', 'TRI ENDROYONO (KEPALA SUBBAGIAN TATA USAHA)', NULL, '/assets/upload/lhkpn/04_2020/ff1dcdc929254849db56f826fbb723a4.pdf', 'ff1dcdc929254849db56f826fbb723a4.pdf', 1, '2020-04-17 21:14:12', NULL, NULL),
(23, 'L', 'TITIK SUWARTI (KEPALA SUBBAGIAN TATA USAHA BALAI LATIHAN KERJA SEMARANG)', NULL, '/assets/upload/lhkpn/04_2020/eebd9150cb0ebf8a41e0485398980083.pdf', 'eebd9150cb0ebf8a41e0485398980083.pdf', 1, '2020-04-17 21:14:54', NULL, NULL),
(24, 'L', 'TEJO MADYANTO, SH (KEPALA SEKSI PELATIHAN BALAI LATIHAN KERJA SEMARANG 1)', NULL, '/assets/upload/lhkpn/04_2020/388347ca22373b0fe8d2015bcb1d3df0.pdf', '388347ca22373b0fe8d2015bcb1d3df0.pdf', 1, '2020-04-17 21:16:31', NULL, NULL),
(25, 'L', 'SUDALMA (KEPALA SEKSI PELAYANAN TEKNIS BALAI KESELAMATAN KERJA)', NULL, '/assets/upload/lhkpn/04_2020/c51848f8ca590f986302765ebd9905bc.pdf', 'c51848f8ca590f986302765ebd9905bc.pdf', 1, '2020-04-17 21:28:23', NULL, NULL),
(26, 'L', 'SUBIANTO (KASI PELATIHAN DAN PEMAGANGAN BIDANG PELATIHAN KERJA DAN PRODUKTIVITAS) ', NULL, '/assets/upload/lhkpn/04_2020/c8c1c91c5f2b4ce713922baa68d0c5ed.pdf', 'c8c1c91c5f2b4ce713922baa68d0c5ed.pdf', 1, '2020-04-17 21:28:52', NULL, NULL),
(27, 'L', 'SITI WAHYUNI (KEPALA SUBBAGIAN KEUANGAN)', NULL, '/assets/upload/lhkpn/04_2020/cdc8233f989f1bd1c98187851ca74181.pdf', 'cdc8233f989f1bd1c98187851ca74181.pdf', 1, '2020-04-17 21:29:23', NULL, NULL),
(28, 'L', ' ROSYATI (KEPALA SEKSI PEMASARAN BALAI LATIHAN KERJA SEMARANG 2)', NULL, '/assets/upload/lhkpn/04_2020/12049c6303b20cdf2c21aa1b0d7f1eae.pdf', '12049c6303b20cdf2c21aa1b0d7f1eae.pdf', 1, '2020-04-17 21:30:05', NULL, NULL),
(29, 'L', 'DRA. PRASETYOWATI HARYANI (KASI SYARAT KERJA DAN JAMINAN SOSIAL)', NULL, '/assets/upload/lhkpn/04_2020/80a8bb84272068ec9b44176ba18c6cb0.pdf', '80a8bb84272068ec9b44176ba18c6cb0.pdf', 1, '2020-04-17 21:30:59', NULL, NULL),
(30, 'L', 'NUR HIDAYATI (KASI PENEMPATAN TENAGA KERJA)', NULL, '/assets/upload/lhkpn/04_2020/8a6dba10bd84413cbeca5ad38a5adc51.pdf', '8a6dba10bd84413cbeca5ad38a5adc51.pdf', 1, '2020-04-17 21:31:39', NULL, NULL),
(31, 'L', 'NUR AFIF (KASI PENGUPAHAN DAN KESEJAHTERAAN TENAGA KERJA)', NULL, '/assets/upload/lhkpn/04_2020/724d072ae0b6918b926c68e6b112528f.pdf', '724d072ae0b6918b926c68e6b112528f.pdf', 1, '2020-04-17 21:32:08', NULL, NULL),
(32, 'L', 'MUSLIKHUDIN (KASI PENGAWASAN NORMA KESELAMATAN DAN KESEHATAN KERJA) ', NULL, '/assets/upload/lhkpn/04_2020/9fe5994c8bbe100825144e54d7bc5a14.pdf', '9fe5994c8bbe100825144e54d7bc5a14.pdf', 1, '2020-04-17 21:32:49', NULL, NULL),
(33, 'L', 'IKE MARDIANA (SUBBAG TU SATUAN PENGAWASAN KETENAGAKERJAAN WILAYAH SURAKARTA)', NULL, '/assets/upload/lhkpn/04_2020/558e25c31771418f596fad75f2fc773e.pdf', '558e25c31771418f596fad75f2fc773e.pdf', 1, '2020-04-17 21:33:26', NULL, NULL),
(34, 'L', 'HERI PURNOMO (KEPALA SUBBAGIAN UMUM DAN KEPEGAWAIAN)', NULL, '/assets/upload/lhkpn/04_2020/6038135397dacec6d9442f4cb0a95675.pdf', '6038135397dacec6d9442f4cb0a95675.pdf', 1, '2020-04-17 21:33:57', NULL, NULL),
(35, 'L', ' HARARET HERTIN UTAMI (KEPALA SUBBAGIAN TATA USAHA SATUAN PENGAWASAN KETENAGAKERJAAN WILAYAH SEMARAN', NULL, '/assets/upload/lhkpn/04_2020/0b56089bd70dd99319f273dda60ddf2c.pdf', '0b56089bd70dd99319f273dda60ddf2c.pdf', 1, '2020-04-17 21:34:33', NULL, NULL),
(36, 'L', 'ERRY DYAH NURHIDAYAH (KASI PENEGAKAN HUKUM KETENAGAKERJAAN BIDANG PENGAWASAN KETENAGAKERJAAN)', NULL, '/assets/upload/lhkpn/04_2020/1b9477c4a629d4ad13409d4123024dfe.pdf', '1b9477c4a629d4ad13409d4123024dfe.pdf', 1, '2020-04-17 21:34:59', NULL, NULL),
(37, 'L', 'ENIK NURHAYATINI WIDIYASTUTI (KASI PENYELESAIAN PERSELISIHAN HUBUNGAN INDUSTRIAL BP3TK)', NULL, '/assets/upload/lhkpn/04_2020/31bb5bf84c1d4c1e695b77b0d50c7581.pdf', '31bb5bf84c1d4c1e695b77b0d50c7581.pdf', 1, '2020-04-17 21:36:45', NULL, NULL),
(38, 'L', 'ENDAH SRI NURNANINGSIH, SP (KEPALA SUBBAGIAN TATA USAHA BALAI LATIHAN KERJA PERTANIAN DAN TRANSMIGRA', NULL, '/assets/upload/lhkpn/04_2020/85af2981eb49dd3ba1395a1ca6ecd757.pdf', '85af2981eb49dd3ba1395a1ca6ecd757.pdf', 1, '2020-04-17 21:37:16', NULL, NULL),
(39, 'L', 'EDY PURWANTONO (KEPALA BALAI LATIHAN KERJA SEMARANG 2) ', NULL, '/assets/upload/lhkpn/04_2020/4cf65cb5b2430fa699c13a8a2ce9dfda.pdf', '4cf65cb5b2430fa699c13a8a2ce9dfda.pdf', 1, '2020-04-17 21:37:48', NULL, NULL),
(40, 'L', 'DARWIJI (Kepala Seksi Pelatihan Balai Latihan Kerja Semarang 2)', NULL, '/assets/upload/lhkpn/04_2020/28d1fcae98d0c736a8b5357a5b33edc1.pdf', '28d1fcae98d0c736a8b5357a5b33edc1.pdf', 1, '2020-04-17 21:38:21', NULL, NULL),
(41, 'L', 'BAMBANG SETIARTO (Kepala Subbagian Tata Usaha Balai Latihan Kerja Cilacap)', NULL, '/assets/upload/lhkpn/04_2020/f78b4c4bc123be884cd77f85e705f260.pdf', 'f78b4c4bc123be884cd77f85e705f260.pdf', 1, '2020-04-17 21:40:02', NULL, NULL),
(42, 'L', 'ASKURI (Kepala Subbagian Tata Usaha Balai Pelayanan Penyelesaian Perselisihan Tenaga Kerja)', NULL, '/assets/upload/lhkpn/04_2020/d5c658a9df43f9aeffbb6797443e39d3.pdf', 'd5c658a9df43f9aeffbb6797443e39d3.pdf', 1, '2020-04-17 21:40:26', NULL, NULL),
(43, 'L', 'ANTONIUS MULYANTO HARIYONO (Kepala satuan pengawasan ketenagakerjaan wilayah Surakarta) ', NULL, '/assets/upload/lhkpn/04_2020/fe255c045f32c624bba71eb8fb9e10dd.pdf', 'fe255c045f32c624bba71eb8fb9e10dd.pdf', 1, '2020-04-17 21:40:51', NULL, NULL),
(44, 'L', 'ANTON DWIYANTO SANTOSO (Kepala Seksi Pemasaran)', NULL, '/assets/upload/lhkpn/04_2020/09a2ffddc2be47990f7fae535c90d032.pdf', '09a2ffddc2be47990f7fae535c90d032.pdf', 1, '2020-04-17 21:41:16', NULL, NULL),
(45, 'L', 'DRS. AGUS SUPRIYANTO, MM (Kasi Penyelesaian Perselisihan Penempatan Tenaga Kerja)', NULL, '/assets/upload/lhkpn/04_2020/71c1d3f954e1ec965c2b744eb4b68c03.pdf', '71c1d3f954e1ec965c2b744eb4b68c03.pdf', 1, '2020-04-17 21:41:55', NULL, NULL),
(46, 'L', 'LHKPN', NULL, '/assets/upload/lhkpn/04_2020/51aac828553940324e3d2538f6e1c13d.pdf', '51aac828553940324e3d2538f6e1c13d.pdf', 1, '2020-04-17 21:42:18', NULL, NULL),
(47, 'L', 'LHKPN', NULL, '/assets/upload/lhkpn/04_2020/b73de7f34402d5665784822643ae30a5.pdf', 'b73de7f34402d5665784822643ae30a5.pdf', 1, '2020-04-17 21:42:45', NULL, NULL),
(48, 'L', 'LHKPN', NULL, '/assets/upload/lhkpn/04_2020/2441f4cb872bf26738b22a6e3b0a524b.pdf', '2441f4cb872bf26738b22a6e3b0a524b.pdf', 1, '2020-04-17 21:43:07', NULL, NULL),
(49, 'L', 'LHKPN', NULL, '/assets/upload/lhkpn/04_2020/0769505a8f2925056a6d31e86a3be0ef.pdf', '0769505a8f2925056a6d31e86a3be0ef.pdf', 1, '2020-04-17 21:43:25', NULL, NULL),
(50, 'L', 'LHKPN', 2022, '/assets/upload/lhkpn/04_2020/53d5a9ca5fe3ecccff774b27d72ea6e6.pdf', '53d5a9ca5fe3ecccff774b27d72ea6e6.pdf', 1, '2020-04-17 21:43:52', NULL, NULL),
(51, 'L2', 'LHKASN Tahun 2019', 2019, '/assets/upload/lhkpn/04_2020/ad3b1b9837cc7d7e2bc0cf1c33217020.pdf', 'ad3b1b9837cc7d7e2bc0cf1c33217020.pdf', 1, '2020-04-18 15:43:11', NULL, NULL),
(54, 'L2', 'Jadwal Pertandingans', 2016, '/assets/upload/lhkasn/05_2020/44f85b5396ca7c41e8809dc70516d9dc.pdf', '44f85b5396ca7c41e8809dc70516d9dc.pdf', 1, '2020-05-11 00:35:06', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_menu`
--

CREATE TABLE `turin_menu` (
  `id` int(11) UNSIGNED NOT NULL,
  `tipe` varchar(11) DEFAULT NULL,
  `judul` text DEFAULT NULL,
  `isi` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `turin_pengadaan`
--

CREATE TABLE `turin_pengadaan` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `url` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_pengadaan`
--

INSERT INTO `turin_pengadaan` (`id`, `nama`, `url`) VALUES
(1, 'Di tenderkan', 'tdr'),
(2, 'Swakelola', 'swa'),
(3, 'Dokumen PPK', 'dppk'),
(4, 'Lain-lain', 'll');

-- --------------------------------------------------------

--
-- Table structure for table `turin_pengadaan_data`
--

CREATE TABLE `turin_pengadaan_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_pengadaan` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `turin_perencanaan`
--

CREATE TABLE `turin_perencanaan` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `url` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_perencanaan`
--

INSERT INTO `turin_perencanaan` (`id`, `nama`, `url`) VALUES
(1, 'Renstra', 'renstra'),
(2, 'Renja', 'renja'),
(3, 'LKJiP', 'lkjip');

-- --------------------------------------------------------

--
-- Table structure for table `turin_perencanaan_data`
--

CREATE TABLE `turin_perencanaan_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_perencanaan` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_perencanaan_data`
--

INSERT INTO `turin_perencanaan_data` (`id`, `id_perencanaan`, `judul`, `deskripsi`, `file`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'rejna', '/assets/upload/perencanaan/05_2020/3a4f1ed9c4080d7823f8d67690a78395.pdf', '3a4f1ed9c4080d7823f8d67690a78395.pdf', 1, '2020-05-08 05:33:35', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_ppid`
--

CREATE TABLE `turin_ppid` (
  `id` int(11) UNSIGNED NOT NULL,
  `tipe` varchar(11) DEFAULT NULL,
  `judul` text DEFAULT NULL,
  `isi` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `turin_profil`
--

CREATE TABLE `turin_profil` (
  `id` int(11) UNSIGNED NOT NULL,
  `tipe` int(11) DEFAULT NULL COMMENT '0 profil, 1 ppid',
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_profil`
--

INSERT INTO `turin_profil` (`id`, `tipe`, `judul`, `deskripsi`, `file`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 'Selayang Pandang', '<h2>Selayang Pandang Kesbangpol</h2>\r\n\r\n<p>Alamat : Jl. Ahmad Yani No. 160 Kota Semarang, Jawa Tengah</p>\r\n\r\n<p>Telp : 024-8454990, Fax : 024-8414205, dan SMS : 089690923824</p>\r\n\r\n<p>E-mail : bakesbangpol@jatengprov.go.id</p>\r\n\r\n<p>Berdasarkan Peraturan Daerah Provinsi Jawa Tengah Nomor 7 Tahun 2008 tentang Organisasi dan Tata Kerja Badan Perencanaan Pembangunan Daerah, Inspektorat dan Lembaga Teknis Daerah Provinsi Jawa Tengah, Badan Kesatuan Bangsa, Politik (Kesbangpol) merupakan unsur pendukung tugas Gubernur di bidang kesatuan bangsa, politik dan perlindungan masyarakat, yang berkedudukan di bawah dan bertanggung jawab kepada Gubernur melalui Sekretaris Daerah (Sekda).</p>\r\n', NULL, 1, '2020-05-01 09:42:07', NULL, NULL),
(2, 0, 'Visi & Misi', '<h2>Visi dan Misi</h2>\r\n\r\n<p><strong>VISI </strong></p>\r\n\r\n<p>Terwujudnya Kehidupan masyarakat Jawa Tengah yang kondusif, demokratis dan partisipatif</p>\r\n\r\n<p><strong>MISI </strong></p>\r\n\r\n<ol>\r\n <li>Mewujudkan kesadaran, pemahaman dan pengamalan nilai-nilai Pancasila, wawasan kebangsaan dan nasionalisme di masyarakat guna memperkokoh persatuan dan kesatuan bangsa dalam bingkai Negara Kesatuan Republik Indonesia.</li>\r\n <li>Mewujudkan kewaspadaan nasional dengan peningkatan partisipasi masyarakat dan penguatan peran kelembagaan masyarakat.</li>\r\n <li>Mewujudkan kualitas kehidupan politik yang demokratis berdasarkan Pancasila dan UUD 1945 dengan peningkatan partisipasi masyarakat.</li>\r\n <li>Mewujudkan stabilitas masyarakat yang kondusif untuk mendukung terciptanya ketahanan bangsa.</li>\r\n <li>Mewujudkan peran lembaga penggerak masyarakat di bidang kesatuan bangsa dan politik.</li>\r\n</ol>\r\n', NULL, 1, '2020-05-01 09:41:51', NULL, NULL),
(3, 0, 'Tupoksi Kesbangpol', '<p>Berdasarkan Peraturan Daerah Provinsi Jawa Tengah Nomor 7 Tahun 2008 tentang Organisasi dan Tata Kerja Badan Perencanaan Pembangunan Daerah, Inspektorat dan Lembaga Teknis Daerah Provinsi Jawa Tengah, Badan Kesatuan Bangsa, Politik (Kesbangpol) merupakan unsur pendukung tugas Gubernur di bidang kesatuan bangsa dan politik, yang berkedudukan di bawah dan bertanggung jawab kepada Gubernur melalui Sekretaris Daerah (Sekda).</p>\r\n\r\n<p>Pada Perda Nomor 7 Tahun 2008 Pasal 20 dinyatakan bahwa Badan Kesbangpol Provinsi Jawa Tengah mempunyai tugas pokok melaksanakan penyusunan dan pelaksanaan kebijakan daerah di bidang kesatuan bangsa, politik. Untuk melaksanakan tugas pokok tersebut, Badan Kesbangpol menyelenggarakan fungsi :</p>\r\n\r\n<ol>\r\n <li>Perumusan kebijakan teknis bidang kesatuan bangsa dan politik;</li>\r\n <li>Penyelenggaraan urusan pemerintahan dan pelayanan umum bidang kesatuan bangsa dan politik;</li>\r\n <li>Pembinaan, fasilitasi dan pelaksanaan tugas di bidang ideologi dan kewaspadaan, ketahanan bangsa, politik dalam negeri di lingkup provinsi dan kabupaten/kota</li>\r\n <li>Pemantauan, evaluasi dan pelaporan bidang kesatuan bangsa dan politik;</li>\r\n <li>Pelaksanaan kesekretariatan badan;</li>\r\n</ol>\r\n\r\n<ol>\r\n <li>Kepala Badan;</li>\r\n <li>Sekretariat, membawahkan:\r\n <ol>\r\n  <li>Subbagian Program;</li>\r\n  <li>Subbagian Keuangan;</li>\r\n  <li>Subbagian Umum dan Kepegawaian.</li>\r\n </ol>\r\n </li>\r\n <li>Bidang Ideologi dan Kewaspadaan, membawahkan:\r\n <ol>\r\n  <li>Subbidang Ideologi dan Wawasan Kebangsaan;</li>\r\n  <li>Subbidang Kewaspadaan Nasional.</li>\r\n </ol>\r\n </li>\r\n <li>Bidang Ketahanan Bangsa, membawahkan:\r\n <ol>\r\n  <li>Subbidang Ketahanan Seni dan Budaya, Agama dan Kemasyarakatan;</li>\r\n  <li>Subbidang Ketahanan Ekonomi.Pelaksanaan tugas lain yang diberikan oleh Gubernur sesuai dengan tugas dan fungsinya.</li>\r\n </ol>\r\n </li>\r\n <li>Bidang Politik Dalam Negeri, membawahkan:\r\n <ol>\r\n  <li>Subbidang Sistem, Implementasi dan Kelembagaan Politik;</li>\r\n  <li>Subbidang Pemilu, Pendidikan dan Budaya Politik.</li>\r\n </ol>\r\n </li>\r\n <li>Kelompok Jabatan Fungsional.</li>\r\n</ol>\r\n', NULL, NULL, '2020-04-15 17:35:50', NULL, NULL),
(4, 0, 'Struktur Organisasi', '<h2> </h2>\r\n\r\n<p><strong>1. Sekretariat</strong></p>\r\n\r\n<ul>\r\n <li>Mempunyai tugas melaksanakan penyiapan perumusan kebijakan teknis, pembinaan, pengkoordinasian penyelenggaraan tugas secara terpadu, pelayanan administrasi, dan pelaksanaan di bidang program, keuangan, umum dan kepegawaian.</li>\r\n <li>Fungsi :<br>\r\n Penyiapan bahan perumusan kebijakan teknis, pembinaan, pengkoordinasian penyelenggaraan tugas secara terpadu, pelayanan administrasi, dan pelaksanaan di bidang program.<br>\r\n Penyiapan bahan perumusan kebijakan teknis, pembinaan, pengkoordinasian penyelenggaraan tugas secara terpadu, pelayanan administrasi, dan pelaksanaan di bidang keuangan.<br>\r\n Penyiapan bahan perumusan kebijakan teknis, pembinaan, pengkoordinasian penyelenggaraan tugas secara terpadu, pelayanan administrasi, dan pelaksanaan di bidang umum dan kepegawaian. Pelaksanaan tugas lain yang diberikan oleh Kepala Badan sesuai dengan tugas dan fungsinya.</li>\r\n <li>Sekretariat, membawahkan :<br>\r\n Subbag Program, mempunyai tugas melakukan penyiapan bahan perumusan kebijakan teknis, pembinaan, pengkoordinasian penyelenggaraan tugas secara terpadu, pelayanan administrasi dan pelaksanaan di bidang program, meliputi : koordinasi perencanaan, pemantauan, evaluasi dan pelaporan serta pengelolaan sistem informsi dilingkungan Badan<br>\r\n Subbag Keuangan, mempunyai tugas melakukan penyiapan bahan perumu-san kebijakan teknis, pembinaan, pengkoordinasian penyelenggaraan tugas secara terpadu, pelayanan administrasi dan pelaksanaan di bidang program, meliputi : pengelolaan keuangan, verifikasi, pembukuan dan akuntasi di lingkungan Badan.<br>\r\n Subbag Umum dan Kepegawaian, mempunyai tugas melakukan penyiapan bahan perumusan kebijakan teknis, pembinaan, pengkoordinasian penyelenggaraan tugas secara terpadu, pelayanan administrasi dan pelaksanaan di bidang program, meliputi : pengelolaan administrasi kepegawaian, hukum, humas, organisasi dan tatalaksana, ketatausahaan, rumah tangga dan perlengkapan di lingkungan Badan</li>\r\n</ul>\r\n\r\n<p><strong>2. Bidang Ideologi Dan Kewaspadaan.</strong></p>\r\n\r\n<ul>\r\n <li>Mempunyai tugas melaksanakan penyiapan perumusan kebijakan teknis, pembinaan, dan pelaksanaan di bidang ideologi dan wawasan kebangsaan, dan kewaspadaan nasional.</li>\r\n <li>Fungsi :<br>\r\n Penyiapan bahan perumusan kebijakan teknis, pembinaan dan pelaksanaan di bidang ideologi dan wawasan kebangsaan.</li>\r\n <li>Penyiapan bahan perumusan kebijakan teknis, pembinaan dan pelaksanaan di bidang kewaspadaan nasional. Pelaksanaan tugas lain yang diberikan oleh Kepala Badan sesuai dengan tugas dan fungsinya.</li>\r\n <li>Bidang Ideologi dan Kewaspadaan, membawahkan :<br>\r\n Subbid Ideologi dan Wasbang, mempunyai tugas melakukan penyiapan bahan perumusan kebijakan teknis, pembinaan, dan pelaksanaan di bidang ideologi dan wawasan kebangsaan, meliputi : pelaksanaan, fasilitasi, pengawasan dan koordinasi kegiatan serta peningkatan kapasitas aparatur Kesbangpol di bidang ketahanan ideologi negara, wawasan kebangsaan, bela negara, nilai-nilai sejarah kebangsaan dan penghargaan kebangsaan.<br>\r\n Subbid Kewaspadaan Nasional, mempunyai tugas melakukan penyiapan bahan perumusan kebijakan teknis, pembinaan, dan pelaksanaan di bidang kewaspadaan nasional, meliputi : pelaksanaan, fasilitasi, pengawasan dan koordinasi kegiatan serta peningkatan kapasitas aparatur Kesbangpol di bidang kewaspadaan dini, kerjasama intelkam, bina masyarakat perbatasan dan tenaga kerja, penanganan konflik pemerintahan, penanganan konflik sosial, pengawasan orang asing, dan lembaga asing.</li>\r\n</ul>\r\n\r\n<p><strong>3. Bidang Ketahanan Bangsa.</strong></p>\r\n\r\n<ul>\r\n <li>Mempunyai tugas melaksanakan penyiapan perumusan kebijakan teknis, pembinaan, dan pelaksanaan di bidang ketahanan seni dan budaya, agama dan kemasyarakatan, ketahanan ekonomi.</li>\r\n <li>Fungsi : Penyiapan bahan perumusan kebijakan teknis, pembinaan dan pelaksanaan di bidang ketahanan seni dan budaya, agama dan kemasyarakatan. Penyiapan bahan perumusan kebijakan teknis, pembinaan dan pelaksanaan di bidang ketahanan ekonomi. Pelaksanaan tugas lain yang diberikan oleh Kepala Badan sesuai dengan tugas dan fungsinya.</li>\r\n <li>Bidang Ketahanan Bangsa, membawahkan :<br>\r\n Subbid Ketahanan Seni dan Budaya, mempunyai tugas melakukan penyiapan bahan perumusan kebijakan teknis, pembinaan, dan pelaksanaan di bidang ketahanan seni dan budaya, agama dan kemasyarakatan, meliputi : pelaksanaan, fasilitasi, pengawasan dan koordinasi kegiatan serta peningkatan kapasitas aparatur Kesbangpol di bidang ketahanan seni dan budaya, agama dan kepercayaan, pembauran dan akulturasi budaya, organisasi kemasyarakatan, dan penanganan masalah sosial kemasyarakatan.<br>\r\n Subbid Ketahanan Ekonomi, mempunyai tugas melakukan penyiapan bahan perumusan kebijakan teknis, pembinaan, dan pelaksanaan di bidang ketahanan ekonomi, meliputi : pelaksanaan, fasilitasi, pengawasan dan koordinasi kegiatan serta peningkatan kapasitas aparatur Kesbangpol di bidang ketahanan sumber daya alam, ketahanan perdagangan, investasi, fiskal dan moneter, perilaku masyarakat, kebijakan dan ketahanan lembaga usaha ekonomi, kebijakan dan ketahanan ormas perekonomian.</li>\r\n</ul>\r\n\r\n<p><strong>4. Bidang Politik Dalam Negeri.</strong></p>\r\n\r\n<ul>\r\n <li>Mempunyai tugas melaksanakan penyiapan perumuan kebijakan teknis, pembinaan, dan pelaksanaan di bidang sistem, implementasi dan kelembagaan politik, pemilu, pendidikan dan budaya politik.</li>\r\n <li>Fungsi :Penyiapan bahan perumusan kebijakan teknis, pembinaan dan pelaksanaan di bidang sistem, implementasi dan kelembagaan politik. Penyiapan bahan perumusan kebijakan teknis, pembinaan dan pelaksanaan di bidang pemilu, pendidikan dan budaya politik. Pelaksanaan tugas lain yang diberikan oleh Kepala Badan sesuai dengan tugas dan fungsinya.</li>\r\n <li>Bidang Politik Dalam Negeri, membawahkan :<br>\r\n Subbid Sistem, Implementasi Dan Kelembagaan Politik, mempunyai tugas melakukan penyiapan bahan perumusan kebijakan teknis, pembinaan, dan pelaksanaan di bidang system, implementasi dan kelembagaan politik, meliputi : pelaksanaan, fasilitasi, pengawasan dan koordinasi kegiatan serta peningkatan kapasitas aparatur Kesbangpol di bidang sistem, implementasi dan kelembagaan partai politik.<br>\r\n Subbid Pemilu, Pendidikan Dan Budaya Politik, mempunyai tugas melakukan penyiapan bahan perumusan kebijakan teknis, pembinaan, dan pelaksanaan di bidang pemilu, pendidikan dan budaya politik, meliputi : pelaksanaan, fasilitasi, pengawasan dan koordinasi kegiatan serta peningkatan kapasitas aparatur Kesbangpol di bidang sistem dan implementasi pemilu, pilpres, pilkada, pendidikan dan budaya politik.</li>\r\n</ul>\r\n', NULL, 1, '2020-11-20 02:55:01', NULL, NULL),
(5, 0, 'Kepegawaian', '<p><img alt=\"\" src=\"https://i2.wp.com/kesbangpol.jatengprov.go.id/wp-content/uploads/2020/09/Screenshot_2020-09-17-Struktur-Organisasi.png?w=668&ssl=1\" xss=removed></p>\r\n\r\n<p> </p>\r\n\r\n<p><img alt=\"\" src=\"https://i2.wp.com/kesbangpol.jatengprov.go.id/wp-content/uploads/2018/05/kepegawaian2.jpg?zoom=0.8999999761581421&fit=960,720&ssl=1\" xss=removed></p>\r\n', NULL, 1, '2020-11-20 02:56:28', NULL, NULL),
(8, 0, 'Profil Pimpinan', '<p><img alt=\"\" src=\"https://i0.wp.com/kesbangpol.jatengprov.go.id/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-14-at-13.04.42-1.jpeg?resize=1024,604&ssl=1\" xss=removed></p>\r\n\r\n<p>?Bersama Membangun Bangsa Bersatu Membela Negara? menjadi Spirit dan Tekad kami untuk semaksimal mungkin menciptakan kondusifitas dan kemananan Jawa Tengah, dalam rangka mewujudkan Jawa Tengah yang Sejahtera dan Berdikari, yakni kesejahteraan yang setara bagi segenap komponen masyarakat Jawa Tengah dan mewujudkan kondisi Jawa Tengah yang berdaulat secara politik, berdikari secara ekonomi, dan berkepribadian secara sosial budaya, yang dapat dimanifestasikan dalam bentuk sikap maupun perbuatan.</p>\r\n\r\n<p>Untuk mewujudkan visi tersebut, komitmen kami senantiasa melangkah berbasis nilai pada Trisakti Bung Karno, Berdaulat di bidang politik, Berdikari di bidang ekonomi dan Berkepribadian di bidang Kebudayaan.</p>\r\n\r\n<p>Kami juga berkomitmen untuk Memperkuat Kelembagaan Sosial Masyaratat untuk meningkatkan Persatuan dan Kesatuan, serta Memperkuat Partisipasi masyarakat dalam pengambilan keputusan dan proses pembangunan yang menyangkut hajat hidup orang banyak.</p>\r\n\r\n<p>Dengan sinergitas Pemerintah Pusat, Seluruh pemangku kepentingan di Daerah, Kabupaten/ Kota serta seluruh komponen bangsa, Jawa Tengah akan benar-benar Sejahtera dan Berdikari.</p>\r\n\r\n<table>\r\n <tbody>\r\n  <tr>\r\n   <td>Nama Lengkap</td>\r\n   <td>:</td>\r\n   <td>HAERUDIN, SH, MH</td>\r\n  </tr>\r\n  <tr>\r\n   <td>Tempat /Tanggal Lahir</td>\r\n   <td>:</td>\r\n   <td>MABOLU MUNA/ 29 JULI 1970</td>\r\n  </tr>\r\n  <tr>\r\n   <td>Jenis Kelamin</td>\r\n   <td>:</td>\r\n   <td>LAKI-LAKI</td>\r\n  </tr>\r\n  <tr>\r\n   <td>Alamat</td>\r\n   <td>:</td>\r\n   <td>DK. JL. SERIMPI RAYA B-57</td>\r\n  </tr>\r\n  <tr>\r\n   <td>Pendidikan Terakhir</td>\r\n   <td>:</td>\r\n   <td>Pasca Sarjana Magister Hukum</td>\r\n  </tr>\r\n  <tr>\r\n   <td>Agama</td>\r\n   <td>:</td>\r\n   <td>ISLAM</td>\r\n  </tr>\r\n  <tr>\r\n   <td>Status Pernikahan</td>\r\n   <td>:</td>\r\n   <td>NIKAH</td>\r\n  </tr>\r\n  <tr>\r\n   <td>Jabatan saat ini</td>\r\n   <td>:</td>\r\n   <td>KEPALA BADAN KESBANGPOL PROV. JATENG</td>\r\n  </tr>\r\n  <tr>\r\n   <td>Pangkat</td>\r\n   <td>:</td>\r\n   <td>PEMBINA TINGKAT I (IV/b)</td>\r\n  </tr>\r\n  <tr>\r\n   <td>Diklat Perjenjangan</td>\r\n   <td>:</td>\r\n   <td>SEPADA, DIKLATPIM TK III</td>\r\n  </tr>\r\n  <tr>\r\n   <td>Alamat Kantor</td>\r\n   <td>:</td>\r\n   <td>JL. A. YANI NO : 160 SEMARANG</td>\r\n  </tr>\r\n  <tr>\r\n   <td>Alamat Email</td>\r\n   <td>:</td>\r\n   <td>haerudinpoldagri@yahoo.com</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p>Kami siap menerima masukan dan saran dalam rangka mewujudkan Jawa Tengah yang kondusif dan aman.</p>\r\n', NULL, 1, '2020-11-20 02:59:36', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_profil_pejabat`
--

CREATE TABLE `turin_profil_pejabat` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tab` int(11) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `nip` varchar(100) DEFAULT NULL,
  `pangkat_golongan` varchar(100) DEFAULT NULL,
  `pendidikan_akhir` varchar(100) DEFAULT NULL,
  `diklat_akhir` varchar(100) DEFAULT NULL,
  `file` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_profil_pejabat`
--

INSERT INTO `turin_profil_pejabat` (`id`, `nama`, `tab`, `jabatan`, `nip`, `pangkat_golongan`, `pendidikan_akhir`, `diklat_akhir`, `file`, `deskripsi`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ir. SAKINA ROSELLASARI, M.si,M.Sc', 1, 'Kepala Dinas ', '196608211993032006', 'Penata Tk. I', 'S2', '\'', '79ce07ee2085a56da6eaf05a705c029d.jpeg', '/assets/upload/profilpejabat/04_2020/79ce07ee2085a56da6eaf05a705c029d.jpeg', 1, '2020-04-17 17:20:23', NULL, NULL),
(2, 'Pyang Yourn', 2, 'Sekertaris', '123', '123', '213', '123', '0941ca9ebc40fc33adf8eac5b47dd7ef.jpeg', '/assets/upload/profilpejabat/05_2020/0941ca9ebc40fc33adf8eac5b47dd7ef.jpeg', 1, '2020-05-04 18:29:08', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_profil_pejabat_tab`
--

CREATE TABLE `turin_profil_pejabat_tab` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_profil_pejabat_tab`
--

INSERT INTO `turin_profil_pejabat_tab` (`id`, `nama`) VALUES
(1, 'KEPALA DINAS'),
(2, 'SEKRETARIAT'),
(3, 'BIDANG PELATIHAN KERJA DAN PRODUKTIVITAS'),
(4, 'BIDANG PENEMPATAN TENAGA KERJA DAN TRANSMIGRASI'),
(5, 'BIDANG HUBUNGAN INDUSTRIAL DAN JAMINAN SOSIAL'),
(6, 'BIDANG PENGAWASAN KETENAGAKERJAAN'),
(7, 'BALAI PELATIHAN KERJA TRANSMIGRASI\n'),
(8, 'BALAI PELAYANAN PENYELESAIAN PERSELISIHAN TENAGA KERJA'),
(9, 'BALAI KESELAMATAN KERJA'),
(10, 'BALAI LATIHAN KERJA SEMARANG 1'),
(11, 'BALAI LATIHAN KERJA SEMARANG 2'),
(12, 'BALAI LATIHAN KERJA CILACAP');

-- --------------------------------------------------------

--
-- Table structure for table `turin_profil_struktur`
--

CREATE TABLE `turin_profil_struktur` (
  `id` int(11) UNSIGNED NOT NULL,
  `tipe` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_profil_struktur`
--

INSERT INTO `turin_profil_struktur` (`id`, `tipe`, `judul`, `deskripsi`, `file`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 'Struktur Organisasi Dinas', '/assets/upload/struktur/05_2020/5eb2cc336c955c08b3c2dca72ecfefa3.jpeg', '5eb2cc336c955c08b3c2dca72ecfefa3.jpeg', NULL, NULL, NULL, NULL),
(2, 1, 'Struktur Organisasi PPID', '/assets/upload/struktur/05_2020/5eb2cc336c955c08b3c2dca72ecfefa3.jpeg', '5eb2cc336c955c08b3c2dca72ecfefa3.jpeg', NULL, NULL, NULL, NULL),
(3, 1, 'SK PPID', '/assets/upload/struktur/05_2020/149b880086cf785310a108d06c552191.jpeg', '149b880086cf785310a108d06c552191.jpeg', 1, '2020-05-13 18:12:37', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_program`
--

CREATE TABLE `turin_program` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `url` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_program`
--

INSERT INTO `turin_program` (`id`, `nama`, `url`) VALUES
(20, 'Indikator Kerja Utama', 'iku'),
(21, 'Kerangka Acuan Kerja', 'kak'),
(22, 'LAKIP', 'lakip'),
(23, 'Pelaksana Program Kegiatan', 'ppk'),
(24, 'Perjanjian Kerja', 'pk'),
(25, 'RENAKSI', 'renaksi'),
(26, 'Rencana Kerja', 'rk'),
(27, 'Rencana Kerja Operasional', 'rko'),
(28, 'Rencana Kerja Pemerintah Daerah', 'rkpd'),
(30, 'RENSTRA', 'renstra'),
(31, 'RKT', 'rkt');

-- --------------------------------------------------------

--
-- Table structure for table `turin_program_copy`
--

CREATE TABLE `turin_program_copy` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `url` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_program_copy`
--

INSERT INTO `turin_program_copy` (`id`, `nama`, `url`) VALUES
(10, 'Nama Program & Kegiatan', 'sedang'),
(11, 'Penanggung Jawab', 'pj'),
(12, 'Target Capaian', 'targetcapai'),
(13, 'Jadwal Pelaksanaan', 'jadwal'),
(14, 'Anggaran', 'anggaran'),
(15, 'Agenda Kegiatan', 'agenda'),
(16, 'Agenda Kepala Dinas', 'lingkup');

-- --------------------------------------------------------

--
-- Table structure for table `turin_program_data`
--

CREATE TABLE `turin_program_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_program` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `file` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_program_data`
--

INSERT INTO `turin_program_data` (`id`, `id_program`, `judul`, `file`, `deskripsi`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 20, 'IKU KESBANGPOL 2013-2018', '877e39837b53e56d102d6005ad9f3424.pdf', '/assets/upload/program/12_2020/877e39837b53e56d102d6005ad9f3424.pdf', 2, '2020-12-30 06:09:12', NULL, NULL),
(4, 20, 'IKU KESBANGPOL 2018-2023', '24b35d4250703cba3c3adcf429d53e02.pdf', '/assets/upload/program/12_2020/24b35d4250703cba3c3adcf429d53e02.pdf', 2, '2020-12-30 06:14:07', NULL, NULL),
(5, 21, 'KERANGKA ACUAN KERJA 2017', '8a76beae9f31b7cfc3618962c3299833.pdf', '/assets/upload/program/12_2020/8a76beae9f31b7cfc3618962c3299833.pdf', 2, '2020-12-30 06:15:56', NULL, NULL),
(6, 21, 'KERANGKA ACUAN KERJA 2018', 'c28f29bf895e29c5e0405762c9f69c3c.pdf', '/assets/upload/program/12_2020/c28f29bf895e29c5e0405762c9f69c3c.pdf', 2, '2020-12-30 06:16:37', NULL, NULL),
(7, 21, 'KERANGKA ACUAN KERJA 2019', 'c2da14c0a3a23aec6843447ed7df5cef.pdf', '/assets/upload/program/12_2020/c2da14c0a3a23aec6843447ed7df5cef.pdf', 2, '2020-12-30 06:18:32', NULL, NULL),
(8, 22, 'LAKIP 2013', '4512a0c3d67a3ae168825a44714195b5.pdf', '/assets/upload/program/12_2020/4512a0c3d67a3ae168825a44714195b5.pdf', 2, '2020-12-30 06:19:40', NULL, NULL),
(9, 22, 'LAKIP 2014', '1e51b9e307a884a3920d5f63b914a3c7.pdf', '/assets/upload/program/12_2020/1e51b9e307a884a3920d5f63b914a3c7.pdf', 2, '2020-12-30 06:20:16', NULL, NULL),
(10, 22, 'LAKIP 2015', 'ac6b87ea9f1c0e0e75d9fd9fa4329bb5.pdf', '/assets/upload/program/12_2020/ac6b87ea9f1c0e0e75d9fd9fa4329bb5.pdf', 2, '2020-12-30 06:21:48', NULL, NULL),
(11, 22, 'LAKIP 2016', '928811850cda76727f276a2085704298.pdf', '/assets/upload/program/12_2020/928811850cda76727f276a2085704298.pdf', 2, '2020-12-30 06:23:15', NULL, NULL),
(12, 22, 'LAKIP 2017', 'b15b49d6e2f4448c13dd7ba7dfaad46f.pdf', '/assets/upload/program/12_2020/b15b49d6e2f4448c13dd7ba7dfaad46f.pdf', 2, '2020-12-30 06:24:23', NULL, NULL),
(13, 22, 'LAKIP 2018', '867403baa640b673217cf83fa15f12b0.pdf', '/assets/upload/program/12_2020/867403baa640b673217cf83fa15f12b0.pdf', 2, '2020-12-30 06:40:38', NULL, NULL),
(14, 23, 'JADWAL PELAKSANAAN KEGIATAN TAHUN 2016', '09b9b1dfd39d4939bb40c0dba42d6540.pdf', '/assets/upload/program/12_2020/09b9b1dfd39d4939bb40c0dba42d6540.pdf', 2, '2020-12-30 06:43:47', NULL, NULL),
(15, 23, 'SUMBER DAN JUMLAH DANA TAHUN 2016', '8a867672411c5eb40a513dbd6a2d41ed.pdf', '/assets/upload/program/12_2020/8a867672411c5eb40a513dbd6a2d41ed.pdf', 2, '2020-12-30 06:44:30', NULL, NULL),
(16, 23, 'JADWAL PELAKSANAAN KEGIATAN TAHUN 2017', '6f5867abab354f5bf6256c2822bb549c.pdf', '/assets/upload/program/12_2020/6f5867abab354f5bf6256c2822bb549c.pdf', 2, '2020-12-30 06:46:34', NULL, NULL),
(17, 23, 'SUMBER DAN JUMLAH DANA TAHUN 2017', '466e4a4d21b27b8336a3b8d67749b2b6.pdf', '/assets/upload/program/12_2020/466e4a4d21b27b8336a3b8d67749b2b6.pdf', 2, '2020-12-30 06:48:14', NULL, NULL),
(18, 23, 'JADWAL PELAKSANAAN KEGIATAN TAHUN 2018', 'ed7989c2113b35bdf73c53151d251a2b.pdf', '/assets/upload/program/12_2020/ed7989c2113b35bdf73c53151d251a2b.pdf', 2, '2020-12-30 06:51:44', NULL, NULL),
(19, 23, 'SUMBER DAN JUMLAH DANA TAHUN 2018', '6274b0cb3535f8e957f2552604543312.pdf', '/assets/upload/program/12_2020/6274b0cb3535f8e957f2552604543312.pdf', 2, '2020-12-30 06:53:12', NULL, NULL),
(20, 23, 'JADWAL PELAKSANAAN KEGIATAN TAHUN 2019', '93ce306c4cf37e9e59b4702329969473.pdf', '/assets/upload/program/12_2020/93ce306c4cf37e9e59b4702329969473.pdf', 2, '2020-12-30 06:56:27', NULL, NULL),
(21, 23, 'SUMBER DAN JUMLAH DANA TAHUN 2019', 'ecb5e5b1f3a51368099d2c1a8770da11.pdf', '/assets/upload/program/12_2020/ecb5e5b1f3a51368099d2c1a8770da11.pdf', 2, '2020-12-30 06:57:07', NULL, NULL),
(22, 23, 'JADWAL PELAKSANAAN KEGIATAN TAHUN 2020', '07b0ff2dcb09b3e19ba9a2396bd5af7a.pdf', '/assets/upload/program/12_2020/07b0ff2dcb09b3e19ba9a2396bd5af7a.pdf', 2, '2020-12-30 06:57:54', NULL, NULL),
(23, 23, 'SUMBER DAN JUMLAH DANA TAHUN 2020', 'c548e1c3491ee85493299f5573147f52.pdf', '/assets/upload/program/12_2020/c548e1c3491ee85493299f5573147f52.pdf', 2, '2020-12-30 07:05:38', NULL, NULL),
(35, 25, 'Rencana Aksi sakip Bakesbangpol 2017', '63d6b57f03124b0946d0ffc22cb2f478.pdf', '/assets/upload/program/12_2020/63d6b57f03124b0946d0ffc22cb2f478.pdf', 2, '2020-12-30 08:47:01', NULL, NULL),
(36, 25, 'Rencana Aksi sakip Bakesbangpol 2018', 'f71126cdd79df7a43692de6358d57b40.pdf', '/assets/upload/program/12_2020/f71126cdd79df7a43692de6358d57b40.pdf', 2, '2020-12-30 08:47:16', NULL, NULL),
(37, 25, 'Rencana Aksi sakip Bakesbangpol 2019', 'a24e2e70f0c88dd616e38910672e710d.pdf', '/assets/upload/program/12_2020/a24e2e70f0c88dd616e38910672e710d.pdf', 2, '2020-12-30 08:47:30', NULL, NULL),
(38, 26, 'RENCANA KERJA TAHUN 2014', '0b9e10b4374f02dc023a049a01c121af.pdf', '/assets/upload/program/12_2020/0b9e10b4374f02dc023a049a01c121af.pdf', 2, '2020-12-30 08:58:56', NULL, NULL),
(39, 26, 'RENCANA KERJA TAHUN 2015', 'c9ae9effd364c844b35482698d26bf3d.pdf', '/assets/upload/program/12_2020/c9ae9effd364c844b35482698d26bf3d.pdf', 2, '2020-12-30 09:07:04', NULL, NULL),
(40, 31, 'RKT 2018', 'f7f98e4a81cef5649596ed08d520f520.pdf', '/assets/upload/program/01_2021/f7f98e4a81cef5649596ed08d520f520.pdf', 2, '2021-01-04 05:54:25', NULL, NULL),
(41, 31, 'RKT 2019', 'd7015fcfbb023f247f479c0e0eea87a6.pdf', '/assets/upload/program/01_2021/d7015fcfbb023f247f479c0e0eea87a6.pdf', 2, '2021-01-04 05:54:43', NULL, NULL),
(42, 31, 'RKT 2020', '19975f32b33896d62711b83ef8133c35.pdf', '/assets/upload/program/01_2021/19975f32b33896d62711b83ef8133c35.pdf', 2, '2021-01-04 05:54:56', NULL, NULL),
(43, 30, 'RENSTRA KESBANGPOLLINMAS 2013-2018', '7210eb89627ab7ce0356d3e7b7f9ecba.pdf', '/assets/upload/program/01_2021/7210eb89627ab7ce0356d3e7b7f9ecba.pdf', 2, '2021-01-04 05:58:17', NULL, NULL),
(44, 30, 'RENSTRA KESBANGPOL 2018-2023', 'd70a529b064e41814fe8b0a53c3949fa.pdf', '/assets/upload/program/01_2021/d70a529b064e41814fe8b0a53c3949fa.pdf', 2, '2021-01-04 05:58:55', NULL, NULL),
(45, 24, 'Perjanjian Kinerja Tahun 2017', '30dd0e12de23613a2661b619a6c1dea5.pdf', '/assets/upload/program/01_2021/30dd0e12de23613a2661b619a6c1dea5.pdf', 3, '2021-01-04 06:13:15', NULL, NULL),
(48, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN I TAHUN 2015', '4515ec015dd10163f8702a610c6f63e6.pdf', '/assets/upload/program/01_2021/4515ec015dd10163f8702a610c6f63e6.pdf', 2, '2021-01-04 06:31:48', NULL, NULL),
(49, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN II TAHUN 2015', 'cc2c4e9218759f0069b9105d80117c4a.pdf', '/assets/upload/program/01_2021/cc2c4e9218759f0069b9105d80117c4a.pdf', 2, '2021-01-04 06:32:28', NULL, NULL),
(50, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN III TAHUN 2015', '5d51f4f5981d1847bd50069ec87df1e5.pdf', '/assets/upload/program/01_2021/5d51f4f5981d1847bd50069ec87df1e5.pdf', 2, '2021-01-04 06:33:13', NULL, NULL),
(51, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN IV TAHUN 2015', '777a01da307bb5ddc5a03643ba27e20d.pdf', '/assets/upload/program/01_2021/777a01da307bb5ddc5a03643ba27e20d.pdf', 2, '2021-01-04 06:33:42', NULL, NULL),
(52, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN I TAHUN 2016', '27aca90204b926e3d989a6eb0ab1bf6e.pdf', '/assets/upload/program/01_2021/27aca90204b926e3d989a6eb0ab1bf6e.pdf', 2, '2021-01-04 06:34:27', NULL, NULL),
(53, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN II TAHUN 2016', 'f4b20b85b406b9dcd3fdcdb2c552a75f.pdf', '/assets/upload/program/01_2021/f4b20b85b406b9dcd3fdcdb2c552a75f.pdf', 2, '2021-01-04 06:35:02', NULL, NULL),
(54, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN III TAHUN 2016', 'c1a1d898e31500584b8f9ac675aa6f21.pdf', '/assets/upload/program/01_2021/c1a1d898e31500584b8f9ac675aa6f21.pdf', 2, '2021-01-04 06:35:23', NULL, NULL),
(55, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN IV TAHUN 2016', 'b0990b31ce6178c56061a224a33ea0ef.pdf', '/assets/upload/program/01_2021/b0990b31ce6178c56061a224a33ea0ef.pdf', 2, '2021-01-04 06:35:48', NULL, NULL),
(56, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN I TAHUN 2017', '96e6b3ec6ff4340b7bea45bd95d2f5ce.pdf', '/assets/upload/program/01_2021/96e6b3ec6ff4340b7bea45bd95d2f5ce.pdf', 2, '2021-01-04 06:41:55', NULL, NULL),
(57, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN II TAHUN 2017', '6b191eea0cefef42556477096bf9387a.pdf', '/assets/upload/program/01_2021/6b191eea0cefef42556477096bf9387a.pdf', 2, '2021-01-04 06:42:15', NULL, NULL),
(58, 24, 'Perjanjian Kinerja Tahun 2018', '7369348d3dbf2a53af018d6f7a606227.pdf', '/assets/upload/program/01_2021/7369348d3dbf2a53af018d6f7a606227.pdf', 3, '2021-01-04 06:42:31', NULL, NULL),
(59, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN III TAHUN 2017', '6083d988859a145a01dbc4fbf890dda1.pdf', '/assets/upload/program/01_2021/6083d988859a145a01dbc4fbf890dda1.pdf', 2, '2021-01-04 06:42:41', NULL, NULL),
(60, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN IV TAHUN 2017', '166f3d15e80aecd149fd7d5cdc98bad9.pdf', '/assets/upload/kelolaprogram/01_2021/166f3d15e80aecd149fd7d5cdc98bad9.pdf', 2, '2021-01-04 06:43:12', '2021-01-04 13:48:30', NULL),
(61, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN I TAHUN 2018', 'c2bb7575966212e527fdd87daa7dac3a.pdf', '/assets/upload/program/01_2021/c2bb7575966212e527fdd87daa7dac3a.pdf', 2, '2021-01-04 06:51:43', NULL, NULL),
(62, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN II TAHUN 2018', '0d7be0a038fe0534616f14f039f911c5.pdf', '/assets/upload/program/01_2021/0d7be0a038fe0534616f14f039f911c5.pdf', 2, '2021-01-04 06:52:00', NULL, NULL),
(63, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN II TAHUN 2018', 'ba99bf505ad85a01f11ce80716fb5c06.pdf', '/assets/upload/program/01_2021/ba99bf505ad85a01f11ce80716fb5c06.pdf', 2, '2021-01-04 06:52:16', NULL, NULL),
(64, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN IV TAHUN 2018', '61d8242f4a23b0fcf662f7f00ee8a92d.pdf', '/assets/upload/program/01_2021/61d8242f4a23b0fcf662f7f00ee8a92d.pdf', 2, '2021-01-04 06:52:38', NULL, NULL),
(65, 26, 'RENCANA-KERJA-TAHUN-2016', '0b9c44a3e9f0cba0c6cd4ea83aa6d75a.pdf', '/assets/upload/program/01_2021/0b9c44a3e9f0cba0c6cd4ea83aa6d75a.pdf', 3, '2021-01-04 06:57:27', NULL, NULL),
(66, 26, 'RENCANA-KERJA-TAHUN-2017', '94b425625b021cd27d2590f59d62550b.pdf', '/assets/upload/program/01_2021/94b425625b021cd27d2590f59d62550b.pdf', 3, '2021-01-04 06:58:23', NULL, NULL),
(67, 26, 'RENCANA-KERJA-TAHUN-2018', '9257d0a3bf2f5dcfed7f9cc663426a77.pdf', '/assets/upload/program/01_2021/9257d0a3bf2f5dcfed7f9cc663426a77.pdf', 3, '2021-01-04 06:58:39', NULL, NULL),
(68, 26, 'RENCANA-KERJA-TAHUN-2019', '81e5740d5507a686460261f745e9df8f.pdf', '/assets/upload/program/01_2021/81e5740d5507a686460261f745e9df8f.pdf', 3, '2021-01-04 06:59:03', NULL, NULL),
(69, 26, 'RENCANA-KERJA-TAHUN-2020', 'e0f4f5e0d0da41731de81fea89c42790.pdf', '/assets/upload/program/01_2021/e0f4f5e0d0da41731de81fea89c42790.pdf', 3, '2021-01-04 06:59:22', NULL, NULL),
(70, 27, 'RENCANA KERJA OPERASIONAL TAHUN 2016', '86d6aed33e1c4c0b049a1d50f5dc06d4.pdf', '/assets/upload/program/01_2021/86d6aed33e1c4c0b049a1d50f5dc06d4.pdf', 3, '2021-01-04 07:03:23', '2021-01-04 14:34:18', NULL),
(71, 27, 'RENCANA KERJA OPERASIONAL TAHUN 2020', 'd87341d5305206f89edff75776b3bdc7.pdf', '/assets/upload/program/01_2021/d87341d5305206f89edff75776b3bdc7.pdf', 3, '2021-01-04 07:09:58', '2021-01-04 14:33:28', NULL),
(72, 27, 'RENCANA KERJA OPERASIONAL TAHUN 2017', 'f357333693c7d41f5d70600767e933dc.pdf', '/assets/upload/program/01_2021/f357333693c7d41f5d70600767e933dc.pdf', 3, '2021-01-04 07:12:08', '2021-01-04 14:34:01', NULL),
(73, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN I TAHUN 2019', '73e3906bcebd07fa2b69e139f51efb91.pdf', '/assets/upload/program/01_2021/73e3906bcebd07fa2b69e139f51efb91.pdf', 2, '2021-01-04 07:14:47', NULL, NULL),
(74, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN II TAHUN 2019', '9b92b89ec27b11b6dbd677a3d92af921.pdf', '/assets/upload/program/01_2021/9b92b89ec27b11b6dbd677a3d92af921.pdf', 2, '2021-01-04 07:15:12', NULL, NULL),
(75, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN III TAHUN 2019', '8b2d34601316786e94926f299a5516f6.pdf', '/assets/upload/program/01_2021/8b2d34601316786e94926f299a5516f6.pdf', 2, '2021-01-04 07:15:38', NULL, NULL),
(76, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN IV TAHUN 2019', '5f687339075ded8152c2348592a2ddbc.pdf', '/assets/upload/program/01_2021/5f687339075ded8152c2348592a2ddbc.pdf', 2, '2021-01-04 07:15:53', NULL, NULL),
(77, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN I TAHUN 2020', '39850f33dfd5da80e98064762d23f27c.pdf', '/assets/upload/program/01_2021/39850f33dfd5da80e98064762d23f27c.pdf', 2, '2021-01-04 07:18:48', NULL, NULL),
(78, 28, 'RENCANA KERJA PEMERINTAH DAERAH TRIWULAN II TAHUN 2020', 'c582af8632a278a4f8798b46e3699c9a.pdf', '/assets/upload/program/01_2021/c582af8632a278a4f8798b46e3699c9a.pdf', 2, '2021-01-04 07:19:06', NULL, NULL),
(79, 27, 'RENCANA KERJA OPERASIONAL TAHUN 2018', 'de0ae28d13978481f61ccc32fc1b7b66.pdf', '/assets/upload/program/01_2021/de0ae28d13978481f61ccc32fc1b7b66.pdf', 2, '2021-01-04 07:29:31', '2021-01-04 14:33:47', NULL),
(80, 27, 'RENCANA KERJA OPERASIONAL TAHUN 2019', '5acec79cd65c3bf9cd4f09e0fc9b1571.pdf', '/assets/upload/program/01_2021/5acec79cd65c3bf9cd4f09e0fc9b1571.pdf', 2, '2021-01-04 07:34:39', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_program_data_copy`
--

CREATE TABLE `turin_program_data_copy` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_program` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `file` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_program_data_copy`
--

INSERT INTO `turin_program_data_copy` (`id`, `id_program`, `judul`, `file`, `deskripsi`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 'Jadwal Pertandingans', '1_(4).pdf', '/assets/upload/kelolaprogram/04_2020/1_(4).pdf', 1, '2020-04-15 22:49:11', '2020-04-16 03:49:24', NULL),
(2, 2, 'Jadwal Pertandingan', '4f12bc05653a4eabea3e6f234c8e5e67.pdf', '/assets/upload/program/04_2020/4f12bc05653a4eabea3e6f234c8e5e67.pdf', 1, '2020-04-15 22:53:38', NULL, NULL),
(3, 11, 'Penanggung Jawab Anggaran 2019', '45d409940da442a13dab1a2c05f5d916.pdf', '/assets/upload/program/04_2020/45d409940da442a13dab1a2c05f5d916.pdf', 1, '2020-04-17 21:45:53', '2020-05-08 09:29:06', NULL),
(4, 1, 'FORM DAFTAR INFORMASI PUBLIK TAHUN 2018', 'ac38ba67bb6c85986e71017bf2372144.pdf', '/assets/upload/program/04_2020/ac38ba67bb6c85986e71017bf2372144.pdf', 1, '2020-04-17 21:46:57', NULL, NULL),
(5, 11, 'Penanggungjawan Kegiatan Tahun 2017', '2b0c7cf027c83e35b9749ec45c0ec198.pdf', '/assets/upload/program/04_2020/2b0c7cf027c83e35b9749ec45c0ec198.pdf', 1, '2020-04-17 21:47:21', '2020-05-08 09:29:18', NULL),
(6, 2, 'RKO Jadwal Pelaksanaan Tahun 2019', '3e19fbf2f2f762c508af0f81b8822ec1.pdf', '/assets/upload/program/04_2020/3e19fbf2f2f762c508af0f81b8822ec1.pdf', 1, '2020-04-17 21:48:53', NULL, NULL),
(7, 2, 'Target Pelaksanaan Fisik Tahun 2019', '781a4f762899aed86027b15a9c2ed1bc.pdf', '/assets/upload/program/04_2020/781a4f762899aed86027b15a9c2ed1bc.pdf', 1, '2020-04-17 21:49:18', NULL, NULL),
(8, 13, 'Jadwal Kegiatan / Program Tahun 2018 ', '746ff31e95bcdff9c894c41dbbb4200e.pdf', '/assets/upload/program/04_2020/746ff31e95bcdff9c894c41dbbb4200e.pdf', 1, '2020-04-17 21:49:45', '2020-05-08 09:30:36', NULL),
(9, 3, 'Ruang Lingkup Kerja', '8cf0e83a8f9c820cf1f6f37e0ac0f9c3.pdf', '/assets/upload/program/04_2020/8cf0e83a8f9c820cf1f6f37e0ac0f9c3.pdf', 1, '2020-04-17 21:53:25', NULL, NULL),
(10, 4, 'Pekerjaan yg sedang dilaksanakan tahun 2017', '2e824fec0f596a97aa441dfec41902f9.pdf', '/assets/upload/program/04_2020/2e824fec0f596a97aa441dfec41902f9.pdf', 1, '2020-04-17 21:54:23', NULL, NULL),
(11, 5, 'SPM dan SP2D 2018', 'eabacabe6d0ced76304c1620e7f40322.pdf', '/assets/upload/program/04_2020/eabacabe6d0ced76304c1620e7f40322.pdf', 1, '2020-04-17 21:56:31', NULL, NULL),
(12, 5, 'Dokumen Pengadaan Pelaksanaan Perencanaan dari PPK 2018', 'a85deecf35ee7e6726682f428dd44a96.pdf', '/assets/upload/program/04_2020/a85deecf35ee7e6726682f428dd44a96.pdf', 1, '2020-04-17 21:57:59', NULL, NULL),
(13, 5, 'Pengadaan tahun 2019', 'a52ebd81068625a75ca9529acc3a32d7.pdf', '/assets/upload/program/04_2020/a52ebd81068625a75ca9529acc3a32d7.pdf', 1, '2020-04-17 21:58:24', NULL, NULL),
(14, 5, 'PERENCANAAN DAN PELAKSANAAN PROSES PENGADAAN BARANG / JASA TAHUN 2019', '7917c5cb78e98245f437cfaddfd4b6e3.pdf', '/assets/upload/program/04_2020/7917c5cb78e98245f437cfaddfd4b6e3.pdf', 1, '2020-04-17 21:58:46', NULL, NULL),
(15, 5, 'Matrik Rencana Aksi SDGs Disnakertrans ', 'ab992c34e6c25c33a86eb19d148f488e.pdf', '/assets/upload/program/04_2020/ab992c34e6c25c33a86eb19d148f488e.pdf', 1, '2020-04-17 21:59:14', NULL, NULL),
(16, 5, 'Rencana Aksi Pangan Gizi Thn 2019', '1f0da5416f74465399b1a424b62139ff.pdf', '/assets/upload/program/04_2020/1f0da5416f74465399b1a424b62139ff.pdf', 1, '2020-04-17 22:01:07', NULL, NULL),
(17, 5, 'Rfk Agustus Tahun 2019', '68c458f561b4acf929872766496c3dd4.pdf', '/assets/upload/program/04_2020/68c458f561b4acf929872766496c3dd4.pdf', 1, '2020-04-17 22:01:27', NULL, NULL),
(18, 5, 'Nama dan Jumlah Program Kegiatan', '332900a57f967922a5f21d8ea2bb8493.pdf', '/assets/upload/program/04_2020/332900a57f967922a5f21d8ea2bb8493.pdf', 1, '2020-04-17 22:01:50', NULL, NULL),
(19, 5, 'Pengendalian Operasional Kegiatan Agustus 2019 ', '3962a6ed7459363cc269bc3c988a520c.pdf', '/assets/upload/program/04_2020/3962a6ed7459363cc269bc3c988a520c.pdf', 1, '2020-04-17 22:02:15', NULL, NULL),
(20, 5, 'Realisasi Anggaran Per Program Tahun 2018', '2715bb85a4ce0b9d6280d68d050cda61.pdf', '/assets/upload/program/04_2020/2715bb85a4ce0b9d6280d68d050cda61.pdf', 1, '2020-04-17 22:02:55', NULL, NULL),
(21, 5, 'DPA 2017', 'd30c046d67f1076b64e68535742f94e9.pdf', '/assets/upload/program/04_2020/d30c046d67f1076b64e68535742f94e9.pdf', 1, '2020-04-17 22:04:09', NULL, NULL),
(22, 6, 'PENCAPAIAN TARGET RPJMD 2013 - 2018 ', '63b32db7595c23b52586c8d22a160597.pdf', '/assets/upload/program/04_2020/63b32db7595c23b52586c8d22a160597.pdf', 1, '2020-04-17 22:05:43', NULL, NULL),
(23, 6, 'Sumber Anggaran APBD dan APBN Tahun 2018 ', '88cd4fac4c243c9311f97870e863a42f.pdf', '/assets/upload/program/04_2020/88cd4fac4c243c9311f97870e863a42f.pdf', 1, '2020-04-17 22:06:06', NULL, NULL),
(24, 12, 'Target Capaian dan Penyerapan Anggaran Tahun 2018 ', '534cd7486d2de6397f358b9a951d1ed0.pdf', '/assets/upload/program/04_2020/534cd7486d2de6397f358b9a951d1ed0.pdf', 1, '2020-04-17 22:06:27', '2020-05-08 09:31:19', NULL),
(25, 6, 'Indikator Kinerja', 'f97e696e55e862f2df47be7973cefe13.pdf', '/assets/upload/program/04_2020/f97e696e55e862f2df47be7973cefe13.pdf', 1, '2020-04-17 22:06:49', NULL, NULL),
(26, 12, 'Target Realisasi Kegiatan Tahun 2017 ', '346fe1b5299c5bbb28d5996b00a5c76e.pdf', '/assets/upload/program/04_2020/346fe1b5299c5bbb28d5996b00a5c76e.pdf', 1, '2020-04-17 22:07:22', '2020-05-08 09:31:21', NULL),
(27, 6, 'IKU', 'b7e1afe686b03eb2bc1fb6db5433758b.pdf', '/assets/upload/program/04_2020/b7e1afe686b03eb2bc1fb6db5433758b.pdf', 1, '2020-04-17 22:07:42', NULL, NULL),
(28, 14, 'Realisasi Anggaran Tahun 2018 ', 'cebf5acea0e1b2129eddd3a3cc391087.pdf', '/assets/upload/program/04_2020/cebf5acea0e1b2129eddd3a3cc391087.pdf', 1, '2020-04-18 14:43:36', '2020-05-08 09:31:40', NULL),
(29, 5, 'Dokumen Pengadaan Pelaksanaan Perencanaan dari PPK 2019', 'b2b724479554be5153ffa25503734e65.pdf', '/assets/upload/program/04_2020/b2b724479554be5153ffa25503734e65.pdf', 1, '2020-04-18 14:45:13', NULL, NULL),
(30, 5, 'PELATIHAN KEJURUAN PERIKANAN APBD DI DS.BRECONG, KAB. KEBUMEN', 'bdf96e917c3adb8d5ccd3f7793455759.pdf', '/assets/upload/program/04_2020/bdf96e917c3adb8d5ccd3f7793455759.pdf', 1, '2020-04-18 14:46:45', NULL, NULL),
(31, 14, 'Realisasi Anggaran Th. 2018', '4b9ba8684f3e90a01d13e5d39ab3efae.pdf', '/assets/upload/program/04_2020/4b9ba8684f3e90a01d13e5d39ab3efae.pdf', 1, '2020-04-18 14:47:19', '2020-05-08 09:31:43', NULL),
(32, 16, 'Agenda Kerja Kepala Dinas tahun 2017', '8aaf35c69a32d48cff5a9173a6ffddd0.pdf', '/assets/upload/kelolaprogram/04_2020/8aaf35c69a32d48cff5a9173a6ffddd0.pdf', 1, '2020-04-18 14:49:57', '2020-05-08 09:31:45', NULL),
(33, 13, ' Jadwal Pelaksanaan 2017', '3c758badcd654c42409efd336ccb56a0.pdf', '/assets/upload/program/04_2020/3c758badcd654c42409efd336ccb56a0.pdf', 1, '2020-04-18 15:46:14', '2020-05-08 09:30:03', NULL),
(34, 16, 'Agenda Kerja Kepala Dinas tahun 2019', 'b81f8234847fdde6e5f1e23c3ac99784.pdf', '/assets/upload/program/04_2020/b81f8234847fdde6e5f1e23c3ac99784.pdf', 1, '2020-04-18 15:49:44', '2020-05-08 09:29:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_regulasi`
--

CREATE TABLE `turin_regulasi` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(255) DEFAULT '',
  `url` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_regulasi`
--

INSERT INTO `turin_regulasi` (`id`, `nama`, `url`) VALUES
(2, 'Undang-Undang', 'uu'),
(3, 'Peraturan Presiden', 'pp'),
(4, 'Peraturan Gubernur', 'pg'),
(5, 'Peraturan Mentri', 'pm'),
(6, 'Peraturan Daerah', 'pd'),
(7, 'SK Dinas', 'sk'),
(8, 'Lainnya', 'du'),
(9, 'Peraturan Pemerintah', 'ppem'),
(10, 'Peraturan Internal Lembaga', 'pil');

-- --------------------------------------------------------

--
-- Table structure for table `turin_regulasi_`
--

CREATE TABLE `turin_regulasi_` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(255) DEFAULT '',
  `url` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_regulasi_`
--

INSERT INTO `turin_regulasi_` (`id`, `nama`, `url`) VALUES
(1, 'Undang-Undang', 'perizinan'),
(2, 'Peraturan Pemerintah', 'uu'),
(3, 'Peraturan Presiden', 'pp'),
(4, 'Peraturan Gubernur', 'pg'),
(5, 'Peraturan Daerah', 'pd'),
(6, 'SK Dinas', 'sk'),
(7, 'Data Umum', 'du'),
(8, 'UMK', 'umk');

-- --------------------------------------------------------

--
-- Table structure for table `turin_regulasi_data`
--

CREATE TABLE `turin_regulasi_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_regulasi` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_regulasi_data`
--

INSERT INTO `turin_regulasi_data` (`id`, `id_regulasi`, `judul`, `deskripsi`, `file`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 9, '2015-PP-Nomor-2-Tahun-2015-tentang-Peraturan-Pelaksanan-UU-no-7-tahun-2012-tentang-Penanganan-Konfli', '/assets/upload/regulasi/01_2021/1492e048c013842cd2e25656b0731935.pdf', 2147483647, 3, '2021-01-04 08:39:14', NULL, NULL),
(3, 9, '2016-PP-Nomor-58-Tahun-2016-tentang-Pelaksanaan-UU-No-17-Tahun-2013-tentang-Ormas-1', '/assets/upload/regulasi/01_2021/b4265d84457f95a1c8b6bdec915056eb.pdf', 0, 3, '2021-01-04 08:43:55', NULL, NULL),
(4, 9, '2016-PP-Nomor-59-Tahun-2016-tentang-Ormas-yang-didirikan-oleh-WNA-1', '/assets/upload/regulasi/01_2021/bb806539dc3743c2d7c0fc4f82dbc095.pdf', 0, 3, '2021-01-04 08:45:21', NULL, NULL),
(5, 9, 'PP_5-2009-TENTANG-BANTUAN-KEUANGAN-KEPADA-PARTAI-POLITIK-1', '/assets/upload/regulasi/01_2021/513a6d728c104608a966f583c2d2bec7.pdf', 513, 3, '2021-01-04 08:46:16', NULL, NULL),
(6, 9, 'PP_18-1986-TENTANG-PELAKSANAAN-UNDANG-UNDANG-NOMOR-8-TAHUN-1985-TENTANG-ORGANISASI-KEMASYARAKATAN-1', '/assets/upload/regulasi/01_2021/53a8f9ebf3e643a2b93d090978d49f79.pdf', 53, 3, '2021-01-04 08:46:35', NULL, NULL),
(7, 9, 'pp_no.18-2013-TENTANG-TATA-CARA-PENGUNDURAN-DIRI-KEPALA-DAERAH-WAKIL-KEPALA-DAERAH-DAN-PEGAWAI-NEGER', '/assets/upload/regulasi/01_2021/66ee63368ea01f82a244a46d2e0c9620.pdf', 66, 3, '2021-01-04 08:47:08', NULL, NULL),
(8, 9, 'pp_no.29-2013_-1', '/assets/upload/regulasi/01_2021/540e139989e9ae949d88216d84e42379.pdf', 2147483647, 3, '2021-01-04 08:48:19', NULL, NULL),
(9, 9, 'pp_no.30-2013_-1', '/assets/upload/regulasi/01_2021/ac0036280bd2cd1116b113a2be27d94f.pdf', 0, 3, '2021-01-04 08:48:38', NULL, NULL),
(10, 9, 'pp_no.31-2013_-1', '/assets/upload/regulasi/01_2021/1f676fe1d22c704a52694462bba19c93.pdf', 1, 3, '2021-01-04 08:49:00', NULL, NULL),
(11, 9, 'pp_no.32-2013_-1', '/assets/upload/regulasi/01_2021/d675989ef529378d1228f37ee3de1c31.pdf', 0, 3, '2021-01-04 08:49:21', NULL, NULL),
(12, 9, 'PP_no.20-2007-TENTANG-PARTAI-POLITIK-LOKAL-DI-ACEH-1', '/assets/upload/regulasi/01_2021/0daf1caab6bc707d23003b8d413d7dad.pdf', 0, 3, '2021-01-04 08:49:41', NULL, NULL),
(13, 9, 'PP_no.20-2007-TENTANG-PARTAI-POLITIK-LOKAL-DI-ACEH-penjelasan-1', '/assets/upload/regulasi/01_2021/b6693974d64795a006b14227ab3620ec.pdf', 0, 3, '2021-01-04 08:50:11', NULL, NULL),
(14, 9, 'PP_no.83-2012_-1', '/assets/upload/regulasi/01_2021/2eeab439b65e82a96fa6c3f1e8202084.pdf', 2, 3, '2021-01-04 08:50:48', NULL, NULL),
(15, 9, '2015-PP-Nomor-2-Tahun-2015-tentang-Peraturan-Pelaksanan-UU-no-7-tahun-2012-tentang-Penanganan-Konfli', '/assets/upload/regulasi/01_2021/cf4bd81ffd997fd705a519735a53bbf5.pdf', 0, 3, '2021-01-04 08:53:36', NULL, NULL),
(16, 9, '2016-PP-Nomor-58-Tahun-2016-tentang-Pelaksanaan-UU-No-17-Tahun-2013-tentang-Ormas-1', '/assets/upload/regulasi/01_2021/1699496801972bc52e7fdd72aa805ce9.pdf', 2147483647, 3, '2021-01-04 08:53:57', NULL, NULL),
(17, 9, '2016-PP-Nomor-59-Tahun-2016-tentang-Ormas-yang-didirikan-oleh-WNA-1', '/assets/upload/regulasi/01_2021/e16043d8408c545cbd25724464f49a06.pdf', 0, 3, '2021-01-04 08:54:15', NULL, NULL),
(18, 9, 'PP_5-2009-TENTANG-BANTUAN-KEUANGAN-KEPADA-PARTAI-POLITIK-1', '/assets/upload/regulasi/01_2021/ef4dbee5536f185c28bfd4fde5c99d86.pdf', 0, 3, '2021-01-04 08:54:32', NULL, NULL),
(19, 9, 'PP_18-1986-TENTANG-PELAKSANAAN-UNDANG-UNDANG-NOMOR-8-TAHUN-1985-TENTANG-ORGANISASI-KEMASYARAKATAN-1', '/assets/upload/regulasi/01_2021/2c577e6d162aa8f005d4d678a3ec17bf.pdf', 2, 3, '2021-01-04 08:54:58', NULL, NULL),
(20, 9, 'pp_no.18-2013-TENTANG-TATA-CARA-PENGUNDURAN-DIRI-KEPALA-DAERAH-WAKIL-KEPALA-DAERAH-DAN-PEGAWAI-NEGER', '/assets/upload/regulasi/01_2021/73c0319b45327ad6a4f831a5596faed8.pdf', 73, 3, '2021-01-04 08:55:18', NULL, NULL),
(21, 9, 'pp_no.29-2013_-1', '/assets/upload/regulasi/01_2021/5a0b7e3b44fafc6825bb28d84b2e23d9.pdf', 5, 3, '2021-01-05 03:20:36', NULL, NULL),
(22, 9, 'pp_no.30-2013_-1', '/assets/upload/regulasi/01_2021/828ee8833871aefca2fcda851ae64060.pdf', 828, 3, '2021-01-05 03:21:00', NULL, NULL),
(23, 9, 'pp_no.31-2013_-1', '/assets/upload/regulasi/01_2021/a1a7ec02ae8d530da88a096d6e41fd67.pdf', 0, 3, '2021-01-05 03:21:19', NULL, NULL),
(24, 9, 'pp_no.32-2013_-1', '/assets/upload/regulasi/01_2021/139a50792961d4e44d1e0501c6609df0.pdf', 139, 3, '2021-01-05 03:21:47', NULL, NULL),
(25, 9, 'PP_no.20-2007-TENTANG-PARTAI-POLITIK-LOKAL-DI-ACEH-1', '/assets/upload/regulasi/01_2021/530798febfb76f8f67e637a17e37cce9.pdf', 530798, 3, '2021-01-05 03:22:12', NULL, NULL),
(26, 9, 'PP_no.20-2007-TENTANG-PARTAI-POLITIK-LOKAL-DI-ACEH-penjelasan-1', '/assets/upload/regulasi/01_2021/fa330cd530b6487364e8064407929d34.pdf', 0, 3, '2021-01-05 03:22:38', NULL, NULL),
(27, 9, 'PP_no.83-2012_-1', '/assets/upload/regulasi/01_2021/a2e8c4438ffd084a5acda6d13366937a.pdf', 0, 3, '2021-01-05 03:23:08', NULL, NULL),
(28, 2, '2007-UU-nomor-24-tahun-2007-tentang-Penanggulangan-Bencana-1', '/assets/upload/regulasi/01_2021/358b1f64bb5c6b544517dca4cf26c5b7.pdf', 358, 3, '2021-01-05 03:32:05', NULL, NULL),
(29, 2, '2008-UU-nomor-2-tahun-2008-tentang-Partai-Politik-1', '/assets/upload/regulasi/01_2021/71d848a7bb2b73bf9fd2226f28ee9578.pdf', 71, 3, '2021-01-05 03:32:47', NULL, NULL),
(30, 2, '2008-UU-NOMOR-42-TAHUN-2008-TENTANG-PEMILIHAN-UMUM-PRESIDEN-DAN-WAKIL-PRESIDEN-1', '/assets/upload/regulasi/01_2021/0d2e43f9f03cf281b7b1359fd72886fe.pdf', 0, 3, '2021-01-05 03:36:37', NULL, NULL),
(31, 2, '2009-UU-nomor-27-tahun-2009-tentang-MAJELIS-PERMUSYAWARATAN-RAKYAT-DEWAN-PERWAKILAN-RAKYAT-DEWAN-PER', '/assets/upload/regulasi/01_2021/4bb6a06ab0ac01331b169752c0b45e97.pdf', 4, 3, '2021-01-05 03:37:06', NULL, NULL),
(32, 2, '2011-UU-nomor-2-tahun-2011-tentang-Perubahan-Atas-UNDANG-UNDANG-NOMOR-2-TAHUN-2008-TENTANG-PARTAI-PO', '/assets/upload/regulasi/01_2021/1fcd875e5858c1fd28903420c80ca46f.pdf', 1, 3, '2021-01-05 03:37:44', NULL, NULL),
(33, 2, '2012-UU-nomor-7-tahun-2012-tentang-Penanganan-Konflik-Sosial-1', '/assets/upload/regulasi/01_2021/4c43a9564f9ca21591b7abdea7e1a14f.pdf', 4, 3, '2021-01-05 03:38:01', NULL, NULL),
(34, 2, '2013-UU-NOMOR-09-TAHUN-2013-TENTANG-PENCEGAHAN-DAN-PEMBERANTASAN-TINDAK-PIDANA-PENDANAAN-TERORISME-1', '/assets/upload/regulasi/01_2021/1592332db1c864d9c2af213201e96bf3.pdf', 1592332, 3, '2021-01-05 03:38:17', NULL, NULL),
(35, 2, '2013-UU-NOMOR-17-TAHUN-2013-TENTANG-ORGANISASI-KEMASYARAKATAN-1', '/assets/upload/regulasi/01_2021/76c3950d3dd7bcd50f01bcc78c5c8388.pdf', 76, 3, '2021-01-05 03:38:34', NULL, NULL),
(36, 2, '2014-UU-NOMOR-8-TAHUN-2015-TENTANG-PERUBAHAN-ATAS-UU-NOMOR-1-TAHUN-2015-TENTANG-PENETAPAN-PERATURAN-', '/assets/upload/regulasi/01_2021/639c5eb63eda80dcc53e10af5f6468ad.pdf', 639, 3, '2021-01-05 03:38:54', NULL, NULL),
(37, 2, '2014-UU-NOMOR-9-TAHUN-2015-TENTANG-PERUBAHAN-KEDUA-ATAS-UNDANG-UNDANG-NOMOR-23-TAHUN-2014-1', '/assets/upload/regulasi/01_2021/c8413aa09c68400a3a77e981204e367c.pdf', 0, 3, '2021-01-05 03:39:13', NULL, NULL),
(38, 2, '2015-UU-NOMOR-2-TAHUN-2015-TENTANG-PENETAPAN-PERATURAN-PEMERINTAH-PENGGANTI-UU-NOMOR-2-TAHUN-2014-TE', '/assets/upload/regulasi/01_2021/71c9bfbb399f3855e81eac5b6bc9e61e.pdf', 71, 3, '2021-01-05 03:39:43', NULL, NULL),
(39, 2, '2015-UU-NOMOR-2-TAHUN-2015-TENTANG-PENETAPAN-PERATURAN-PEMERINTAH-PENGGANTI-UU-NOMOR-2-TAHUN-2014-TE', '/assets/upload/regulasi/01_2021/ae9e738142dbc0e071801f8ee4387c04.pdf', 0, 3, '2021-01-05 03:39:57', NULL, NULL),
(40, 2, '2007-UU-nomor-24-tahun-2007-tentang-Penanggulangan-Bencana', '/assets/upload/regulasi/01_2021/dc4b37efd845531628560263ffe00278.pdf', 0, 3, '2021-01-05 04:19:09', NULL, NULL),
(41, 2, '2008-UU-nomor-2-tahun-2008-tentang-Partai-Politik', '/assets/upload/regulasi/01_2021/efa01d3ddb44c7dcde3099971a228470.pdf', 0, 3, '2021-01-05 04:19:33', NULL, NULL),
(42, 2, '2008-UU-NOMOR-42-TAHUN-2008-TENTANG-PEMILIHAN-UMUM-PRESIDEN-DAN-WAKIL-PRESIDEN', '/assets/upload/regulasi/01_2021/a44a62c17d1d18f5cd187685133fd607.pdf', 0, 3, '2021-01-05 04:19:53', NULL, NULL),
(43, 2, '2009-UU-nomor-27-tahun-2009-tentang-MAJELIS-PERMUSYAWARATAN-RAKYAT-DEWAN-PERWAKILAN-RAKYAT-DEWAN-PER', '/assets/upload/regulasi/01_2021/8b2d7849cdbaed72bef40ce446d4a7d4.pdf', 8, 3, '2021-01-05 04:20:25', NULL, NULL),
(44, 2, '2011-UU-nomor-2-tahun-2011-tentang-Perubahan-Atas-UNDANG-UNDANG-NOMOR-2-TAHUN-2008-TENTANG-PARTAI-PO', '/assets/upload/regulasi/01_2021/ce2b7662daf78773cc169908236fcdbb.pdf', 0, 3, '2021-01-05 04:20:42', NULL, NULL),
(45, 2, '2012-UU-nomor-7-tahun-2012-tentang-Penanganan-Konflik-Sosial', '/assets/upload/regulasi/01_2021/07ec89b36d2c8201a92062eaeacfd6a8.pdf', 7, 3, '2021-01-05 04:21:01', NULL, NULL),
(46, 2, '2013-UU-NOMOR-09-TAHUN-2013-TENTANG-PENCEGAHAN-DAN-PEMBERANTASAN-TINDAK-PIDANA-PENDANAAN-TERORISME', '/assets/upload/regulasi/01_2021/11e9f1eedc4a2e9deb8fba359060966f.pdf', 2147483647, 3, '2021-01-05 04:21:23', NULL, NULL),
(47, 2, '2013-UU-NOMOR-17-TAHUN-2013-TENTANG-ORGANISASI-KEMASYARAKATAN', '/assets/upload/regulasi/01_2021/a790114366c46ee20681d3bc782159d8.pdf', 0, 3, '2021-01-05 04:21:45', NULL, NULL),
(48, 2, '2014-UU-NOMOR-8-TAHUN-2015-TENTANG-PERUBAHAN-ATAS-UU-NOMOR-1-TAHUN-2015-TENTANG-PENETAPAN-PERATURAN-', '/assets/upload/regulasi/01_2021/351846f62c0c27421d5db886d6f95c27.pdf', 351846, 3, '2021-01-05 04:22:15', NULL, NULL),
(49, 2, '2014-UU-NOMOR-9-TAHUN-2015-TENTANG-PERUBAHAN-KEDUA-ATAS-UNDANG-UNDANG-NOMOR-23-TAHUN-2014', '/assets/upload/regulasi/01_2021/715538c889581adb1f04ad03164f85a6.pdf', 715538, 3, '2021-01-05 04:22:35', NULL, NULL),
(50, 2, '2014-UU-NOMOR-17-TAHUN-2014-TENTANG-MAJELIS-PERMUSYAWARATAN-RAKYAT-DEWAN-PERWAKILAN-RAKYAT-DEWAN-PER', '/assets/upload/regulasi/01_2021/30b500a595446d9d89d76301c681ab55.pdf', 30, 3, '2021-01-05 04:22:55', NULL, NULL),
(51, 2, '2015-UU-NOMOR-2-TAHUN-2015-TENTANG-PENETAPAN-PERATURAN-PEMERINTAH-PENGGANTI-UU-NOMOR-2-TAHUN-2014-TE', '/assets/upload/regulasi/01_2021/9ae81a23ffbdde3c7f8cbebd24f8a4f4.pdf', 9, 3, '2021-01-05 04:23:11', NULL, NULL),
(52, 4, 'Pergub_13_th_2018', '/assets/upload/regulasi/01_2021/25bab07c1be118afa2514e7d60fbebde.pdf', 25, 3, '2021-01-05 04:32:40', NULL, NULL),
(53, 4, 'Pergub_12_th_2018', '/assets/upload/regulasi/01_2021/80f00ac59516673875173e3198322019.pdf', 80, 3, '2021-01-05 04:32:55', NULL, NULL),
(54, 4, 'Pergub_11_th_2018', '/assets/upload/regulasi/01_2021/8656fca482da7bda645df0b9f8909db4.pdf', 8656, 3, '2021-01-05 04:33:14', NULL, NULL),
(55, 4, 'Pergub_10_th_2018', '/assets/upload/regulasi/01_2021/7789d1647df9cb4710bb88a60b4f0fa9.pdf', 7789, 3, '2021-01-05 04:33:34', NULL, NULL),
(56, 4, 'Pergub_7_th_2018', '/assets/upload/regulasi/01_2021/e72bce9cd8c756f1496c374bb94777e0.pdf', 0, 3, '2021-01-05 04:33:49', NULL, NULL),
(57, 4, 'pergub_72_th_2015', '/assets/upload/regulasi/01_2021/4aac2939374ac73129e62a3e942ae364.pdf', 4, 3, '2021-01-05 04:34:12', NULL, NULL),
(58, 4, 'pergub_68_th_2015', '/assets/upload/regulasi/01_2021/24562687d0ccae1f9609f09ace00e30b.pdf', 24562687, 3, '2021-01-05 04:34:31', NULL, NULL),
(59, 4, 'pergub_22_th_2015', '/assets/upload/regulasi/01_2021/045e83019b8dd14f852c4589800e83fa.pdf', 2147483647, 3, '2021-01-05 04:34:44', NULL, NULL),
(60, 6, 'Perda-Prov-Jateng-No-6-Tahun-2012-tentang-Pelayanan-Informasi-Publik-Penyelenggaraan-Pemerintah-Daer', '/assets/upload/regulasi/01_2021/708a80e41c570dccb997154a42c87ab8.pdf', 708, 3, '2021-01-05 04:43:30', NULL, NULL),
(61, 6, 'perda_10_th_2017', '/assets/upload/regulasi/01_2021/6c34f2c8f30a06cf8900b70303079ec1.pdf', 6, 3, '2021-01-05 04:43:53', NULL, NULL),
(62, 6, 'perda_9_th_2017', '/assets/upload/regulasi/01_2021/98bd6ae1fdf09f7db728eb20c3011c3a.pdf', 98, 3, '2021-01-05 04:44:22', NULL, NULL),
(63, 6, 'perda_6_th_2015', '/assets/upload/regulasi/01_2021/17bc0c325e1b1bf2d6c096ca6cacc7be.pdf', 17, 3, '2021-01-05 04:44:48', NULL, NULL),
(64, 6, 'perda_5_th_2016', '/assets/upload/regulasi/01_2021/37fe2ca25774b84276ffe1cc95fd156c.pdf', 37, 3, '2021-01-05 04:45:10', NULL, NULL),
(65, 6, 'perda_3_th_2017', '/assets/upload/regulasi/01_2021/373883d7776d69c137fb698890ec1d9b.pdf', 373883, 3, '2021-01-05 04:45:30', NULL, NULL),
(66, 6, 'perda_3_th_2017', '/assets/upload/regulasi/01_2021/ce1f09e070e4e2f28bc00f0fd432c156.pdf', 0, 3, '2021-01-05 04:45:44', NULL, NULL),
(67, 6, 'perda_2_th_2016', '/assets/upload/regulasi/01_2021/6891a6c63579678629c54e036b9ff700.pdf', 6891, 3, '2021-01-05 04:46:05', NULL, NULL),
(68, 6, 'perda_1_th_2016', '/assets/upload/regulasi/01_2021/2f4be84ea6ded9136dad4d9905f41085.pdf', 2, 3, '2021-01-05 04:46:26', NULL, NULL),
(69, 10, 'P-PENYUSUNAN-RKA', '/assets/upload/regulasi/01_2021/422d2f45ee0a8d213f7d48f246db92c4.pdf', 422, 3, '2021-01-05 06:18:52', NULL, NULL),
(70, 10, 'P-PENYUSUNAN-RKO', '/assets/upload/regulasi/01_2021/2ee63ede5561d1df499c57a408a46a79.pdf', 2, 3, '2021-01-05 06:19:06', NULL, NULL),
(71, 10, 'P-PENYUSUNAN-SAKIP', '/assets/upload/regulasi/01_2021/051350b1e8e87306bdc0395a831f0056.pdf', 51350, 3, '2021-01-05 06:19:20', NULL, NULL),
(72, 10, 'SK-Penujukan-Agen-Perubahan', '/assets/upload/regulasi/01_2021/575bb2fed6ad2c8276f5ebdb048276f6.pdf', 575, 3, '2021-01-05 06:19:35', NULL, NULL),
(73, 10, 'SK-Tim-PMPRB-2020', '/assets/upload/regulasi/01_2021/ddfe531412721f9cbb2f3b3e27decf52.pdf', 0, 3, '2021-01-05 06:20:20', NULL, NULL),
(74, 5, 'se_permendagri_57_oke', '/assets/upload/regulasi/01_2021/9c978ea0a9a154772f6ec687bbf6232d.pdf', 9, 3, '2021-01-05 06:51:21', NULL, NULL),
(75, 5, 'Permen_No.1-2012-lampiran2_', '/assets/upload/regulasi/01_2021/8a5d5f4f41627d79d7c092048db26087.pdf', 8, 3, '2021-01-05 06:52:19', NULL, NULL),
(76, 5, 'Permen_No.1-2012-lampiran1__', '/assets/upload/regulasi/01_2021/3ee60f355992499079b5eb64787b7a2d.pdf', 3, 3, '2021-01-05 06:55:39', NULL, NULL),
(77, 5, 'Permen_No.1-2012_', '/assets/upload/regulasi/01_2021/9776e3d497e0efbd8b3da71b62e5649c.pdf', 9776000, 3, '2021-01-05 06:55:56', NULL, NULL),
(78, 5, 'Permen_no.61-2011_', '/assets/upload/regulasi/01_2021/46165b9560ac0dd7213ca649045c991d.pdf', 46165, 3, '2021-01-05 06:59:44', NULL, NULL),
(79, 5, 'Permen_no.50-2010_', '/assets/upload/regulasi/01_2021/7e712e1cc1fa1f4b09d78d89b251063b.pdf', 2147483647, 3, '2021-01-05 07:00:47', NULL, NULL),
(80, 5, 'Permen_no.49-2010_', '/assets/upload/regulasi/01_2021/1d991e814e3bea597fda2410b8b2430b.pdf', 1, 3, '2021-01-05 07:01:48', NULL, NULL),
(81, 5, 'Permen_no.44-2009_', '/assets/upload/regulasi/01_2021/ae1dbb4d05469a908826e4818f3bac88.pdf', 0, 3, '2021-01-05 07:06:32', NULL, NULL),
(82, 5, 'Permen_no.40-2007_', '/assets/upload/regulasi/01_2021/81db6a370efcbb729331377343c877dc.pdf', 81, 3, '2021-01-05 07:07:42', NULL, NULL),
(83, 5, 'Permen_no.39-2007_', '/assets/upload/regulasi/01_2021/db51d5dea09f4cea28f129a4c9b80b8b.pdf', 0, 3, '2021-01-05 07:08:05', NULL, NULL),
(84, 5, 'Permen_no.38-2008_', '/assets/upload/regulasi/01_2021/b56f9f26ccdd0137ded1f570c89097c6.pdf', 0, 3, '2021-01-05 07:08:35', NULL, NULL),
(85, 5, 'Permen_no.36-2010_', '/assets/upload/regulasi/01_2021/4e74ceca9b8d7176b65402b970e56728.pdf', 2147483647, 3, '2021-01-05 07:08:56', NULL, NULL),
(86, 5, 'Permen_no.34-2006_', '/assets/upload/regulasi/01_2021/19c8d21d59fbad381f47a4c795333a18.pdf', 19, 3, '2021-01-05 07:09:20', NULL, NULL),
(87, 5, 'Permen_no.29-2011_', '/assets/upload/regulasi/01_2021/5a6aa14ac5da2237d7fe2aae73ecc119.pdf', 5, 3, '2021-01-05 07:09:41', NULL, NULL),
(88, 5, 'Permen_no.26-2013_', '/assets/upload/regulasi/01_2021/2b95af598e86f9f4079fc87f8c233250.pdf', 2, 3, '2021-01-05 07:19:12', NULL, NULL),
(89, 5, 'Permen_no.24-2009_', '/assets/upload/regulasi/01_2021/714e4d7c5dc5d08211a05ada7b719778.pdf', 7140000, 3, '2021-01-05 07:22:09', NULL, NULL),
(90, 5, 'Permen_no.20-2013_', '/assets/upload/regulasi/01_2021/0d4af79f6b9b8b7bf5c5d6a2f72a9271.pdf', 0, 3, '2021-01-05 07:23:13', NULL, NULL),
(91, 5, 'Permen_no.16-2011_', '/assets/upload/regulasi/01_2021/36a8cd4dbb7ab89eab358f6ad0da3428.pdf', 36, 3, '2021-01-05 07:23:34', NULL, NULL),
(92, 5, 'Permen_no.11-2006_', '/assets/upload/regulasi/01_2021/c711c687644b297e8bc212eb7246b142.pdf', 0, 3, '2021-01-05 07:23:51', NULL, NULL),
(93, 5, 'Permen_no.07A-2007_', '/assets/upload/regulasi/01_2021/e47aba08ce1ab1912ad131e1ad543da8.pdf', 0, 3, '2021-01-05 07:24:06', NULL, NULL),
(94, 5, 'Permen_no._12-2006_', '/assets/upload/regulasi/01_2021/320344df6fb94d3af401ad491f7349d5.pdf', 320344, 3, '2021-01-05 07:24:20', NULL, NULL),
(95, 5, 'permen_No.20-2013_lampiran_', '/assets/upload/regulasi/01_2021/43f449c23b1035e45ee645ea360e65c8.pdf', 43, 3, '2021-01-05 07:24:38', NULL, NULL),
(96, 5, 'permen_no.71-2012_', '/assets/upload/regulasi/01_2021/f769cc80e6bf0c58bbecb138c02c29e7.pdf', 0, 3, '2021-01-05 07:24:56', NULL, NULL),
(97, 5, 'permen_no.64-2011_', '/assets/upload/regulasi/01_2021/474fc9348232f8eaa5f294b16158edff.pdf', 474, 3, '2021-01-05 07:27:32', NULL, NULL),
(98, 5, 'permen_no.33-2012-lampiran_', '/assets/upload/regulasi/01_2021/6d02d0443674d1438089ac21d3d747c8.pdf', 6, 3, '2021-01-05 07:27:48', NULL, NULL),
(99, 5, 'permen_no.33-2012_', '/assets/upload/regulasi/01_2021/d6b474e48b8c304e6d5dca19e3407e50.pdf', 0, 3, '2021-01-05 07:30:22', NULL, NULL),
(100, 5, 'permen_no.21-2013_', '/assets/upload/regulasi/01_2021/1861c0e50d3b880c201bc46f724279d3.pdf', 1861, 3, '2021-01-05 07:31:44', NULL, NULL),
(101, 5, 'permen_no._39_2011_', '/assets/upload/regulasi/01_2021/ff692bed7f5193722eeaa92c9e4dc743.pdf', 0, 3, '2021-01-05 07:35:48', NULL, NULL),
(102, 5, 'Permendagri_No_6_Tahun_2017_tentang_Perubahan_Permendagri_No_77_Tahun_2014_Tdd', '/assets/upload/regulasi/01_2021/fd4a56292c437d460a6a156df35d9987.pdf', 0, 3, '2021-01-05 07:36:13', NULL, NULL),
(103, 5, 'Permendagri_no.57-2011_', '/assets/upload/regulasi/01_2021/0a7785ec5938c6e3b9417a2a0586120d.pdf', 0, 3, '2021-01-05 07:36:31', NULL, NULL),
(104, 5, 'Permendagri_77_Tahun_2014', '/assets/upload/regulasi/01_2021/8fd54e73fac7757394539d090de72c39.pdf', 8, 3, '2021-01-05 07:36:52', NULL, NULL),
(105, 5, 'Permendagri_44_Tahun_2015', '/assets/upload/regulasi/01_2021/e42470f7523cf726c870c9aa80b7025a.pdf', 0, 3, '2021-01-05 07:37:48', NULL, NULL),
(106, 5, 'Permendagri_42_Tahun_20151', '/assets/upload/regulasi/01_2021/c0cb815071f6f2591af81a5abc57b312.pdf', 0, 3, '2021-01-05 07:38:18', NULL, NULL),
(107, 5, 'Permendagri_39_Tahun_2016', '/assets/upload/regulasi/01_2021/c9f29ace5e179410a8937d0b5ff04327.pdf', 0, 3, '2021-01-05 07:38:38', NULL, NULL),
(108, 5, 'Permendagri_31_Tahun_2016', '/assets/upload/regulasi/01_2021/cc1bb90ca24da75de70dee5a43fce8b3.pdf', 0, 3, '2021-01-05 07:41:13', NULL, NULL),
(109, 5, 'Pemendagri_No._28_Tahun_2014_', '/assets/upload/regulasi/01_2021/4bc6c416ad3a318a9b72ac8b6895c91f.pdf', 4, 3, '2021-01-05 07:43:19', NULL, NULL),
(110, 5, 'Lampiran_Permendagri_No_6_Tahun_2017_tentang_Perubahan_Permendagri_No_77_Tahun_2014_Tdd', '/assets/upload/regulasi/01_2021/159e9d4ba214150d110634e1aa9831dd.pdf', 2147483647, 3, '2021-01-05 07:44:56', NULL, NULL),
(111, 5, 'Lampiran_Permendagri_no.26-2013_auf_', '/assets/upload/regulasi/01_2021/7c62b213faa0923d501c69875f2eae63.pdf', 7, 3, '2021-01-05 07:46:47', NULL, NULL),
(112, 5, '2013-Permendagri-nomor-21-tahun-2013-tentang-fasilitasi-pencegahan-penyalahgunaan-narkotika', '/assets/upload/regulasi/01_2021/8a4903f2aeeeb1d5822f84022fea0cc5.pdf', 8, 3, '2021-01-05 07:48:42', NULL, NULL),
(113, 5, '2006-PERMENDAGRI-Nomor-34-TAHUN-2006-PEDOMAN-PENYELENGGARAAN-PEMBAURAN-KEBANGSAAN', '/assets/upload/regulasi/01_2021/44a731789ffa776e8b70ffc5bcd08735.pdf', 44, 3, '2021-01-05 07:49:42', NULL, NULL),
(114, 5, '2006-Permendagri-nomor-12-tahun-2016-tentang-Kewaspadaan-Dini-Masyarakat', '/assets/upload/regulasi/01_2021/875f11e2fc9d2590c3f584cda30b5756.pdf', 875, 3, '2021-01-05 07:53:34', NULL, NULL),
(115, 5, '2012-Permendagri-nomor-71-tahun-2012-tentang-Pedoman-pendidikan-Wawasan-kebangsaan', '/assets/upload/regulasi/01_2021/59766a4655a6b295d7cb85996504764c.pdf', 59766, 3, '2021-01-05 07:54:18', NULL, NULL),
(116, 10, 'P-PENYUSUNAN-RKT', '/assets/upload/regulasi/01_2021/b75cd411af661476976d95f112dd032b.pdf', 0, 3, '2021-01-05 08:16:42', NULL, NULL),
(117, 10, 'SOP-PELAKSANAAN-EVALUASI-KEGITAN', '/assets/upload/regulasi/01_2021/0f39e58cd709bf213be528b8915ac2f4.pdf', 0, 3, '2021-01-05 08:18:03', NULL, NULL),
(118, 10, 'SOP-PELAKSANAAN-MONITORING-KEGIATAN', '/assets/upload/regulasi/01_2021/45c0de00d06a96e4d3e7f8a9204352d4.pdf', 45, 3, '2021-01-05 08:19:15', NULL, NULL),
(119, 10, 'SOP-PELAKSANAAN-PELAYANAAN-INFORMASI-PUBLIK', '/assets/upload/regulasi/01_2021/f808acf5eade72c35b0e621e285b5c81.pdf', 0, 3, '2021-01-05 08:20:23', NULL, NULL),
(120, 10, 'SOP-PELAKSANAAN-PENGELOLAAN-DATA-DAN-INFORMASI', '/assets/upload/regulasi/01_2021/ba89aa56966e7d5fc208f17f2db2adc1.pdf', 0, 3, '2021-01-05 08:21:10', NULL, NULL),
(121, 10, 'SOP-PELAKSANAAN-RAPAT-INTERNAL', '/assets/upload/regulasi/01_2021/31b3a1817280c954e8108df37dc0c6f9.pdf', 31, 3, '2021-01-05 08:22:40', NULL, NULL),
(122, 10, 'SOP-PELAKSANAAN-SISTEM-INFORMASI', '/assets/upload/regulasi/01_2021/527779c076f001809f3f76e5757218d5.pdf', 527779, 3, '2021-01-05 08:23:57', NULL, NULL),
(123, 10, 'SOP-PENGUMPULAN-DATA-ATAU-PENDATAAN-PROGRAM-KEGIATAN', '/assets/upload/regulasi/01_2021/83bd7967ef270865d09d478cde993049.pdf', 83, 3, '2021-01-05 08:25:40', NULL, NULL),
(124, 10, 'SOP-PENYUSUNAN-DPA', '/assets/upload/regulasi/01_2021/e337c87518abb7b5b1e837eae51cb05c.pdf', 0, 3, '2021-01-05 08:27:13', NULL, NULL),
(125, 10, 'SOP-PENYUSUNAN-INDIKATOR-KINERJA-UTAMA', '/assets/upload/regulasi/01_2021/b129c6fd7e9df45318a64bdbc52c8e6f.pdf', 0, 3, '2021-01-05 08:28:22', NULL, NULL),
(126, 10, 'SOP-PENYUSUNAN-LAKIP', '/assets/upload/regulasi/01_2021/0f17e09bf5a2c2ce5778525d16cb43a9.pdf', 0, 3, '2021-01-05 08:42:39', NULL, NULL),
(127, 10, 'SOP-PENYUSUNAN-LAPORAN-BULANAN-TRIWULAN-DAN-SEMESTERAN', '/assets/upload/regulasi/01_2021/86d47653b455c0134bcc8b41c016f122.pdf', 86, 3, '2021-01-05 08:43:01', NULL, NULL),
(128, 10, 'SOP-PENYUSUNAN-LKPJ', '/assets/upload/regulasi/01_2021/c5281bcfc3e38c804af8c953ab438289.pdf', 0, 3, '2021-01-05 08:43:27', NULL, NULL),
(129, 10, 'SOP-PENYUSUNAN-LPPD', '/assets/upload/regulasi/01_2021/c0b7157981b88a997135d8c67a54fa73.pdf', 0, 3, '2021-01-05 08:43:45', NULL, NULL),
(130, 10, 'SOP-PENYUSUNAN-PERJANJIAN-KINERJA', '/assets/upload/regulasi/01_2021/4fd4a261fe509a5ac1deca016666978d.pdf', 4, 3, '2021-01-05 08:44:04', NULL, NULL),
(131, 10, 'SOP-PENYUSUNAN-PROGRAM-BADAN', '/assets/upload/regulasi/01_2021/86858bdd8de49da7fba4330dc220237c.pdf', 86858, 3, '2021-01-05 08:44:26', NULL, NULL),
(132, 10, 'SOP-PENYUSUNAN-RENJA', '/assets/upload/regulasi/01_2021/d82b6523df80721e52dc7c488c44de24.pdf', 0, 3, '2021-01-05 08:44:45', NULL, NULL),
(133, 10, 'SOP-PENYUSUNAN-RENSTRA', '/assets/upload/regulasi/01_2021/cc8dd17c36fb5fd39c77e579ad173da5.pdf', 0, 3, '2021-01-05 08:45:08', NULL, NULL),
(134, 10, 'SOP-PENYUSUNAN-RKA', '/assets/upload/regulasi/01_2021/0aa99ebe9bab934dcd530ae641c18b09.pdf', 0, 3, '2021-01-05 08:45:26', NULL, NULL),
(135, 10, 'SOP-PENYUSUNAN-RKO', '/assets/upload/regulasi/01_2021/a949b18d088e7264c42ec4f5e1e6e94f.pdf', 0, 3, '2021-01-05 08:45:43', NULL, NULL),
(136, 10, 'SOP-PENYUSUNAN-RKT', '/assets/upload/regulasi/01_2021/bcac0b4995e86511902c30770cf8d330.pdf', 0, 3, '2021-01-05 08:45:58', NULL, NULL),
(137, 10, 'SOP-PENYUSUNAN-SAKIP', '/assets/upload/regulasi/01_2021/09efcd1f20702bf6689c2ca9fb570d9b.pdf', 9, 3, '2021-01-05 08:46:13', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_regulasi_data_copy`
--

CREATE TABLE `turin_regulasi_data_copy` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_regulasi` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_regulasi_data_copy`
--

INSERT INTO `turin_regulasi_data_copy` (`id`, `id_regulasi`, `judul`, `deskripsi`, `file`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 1, 'Jadwal Pertandingan', '/assets/upload/regulasi/04_2020/1_(1)1.pdf', 1, 1, '2020-04-15 22:23:21', '2020-04-16 03:24:11', NULL),
(3, 6, 'Jadwal Pertandingan', '/assets/upload/regulasi/04_2020/1_(1)2.pdf', 1, 1, '2020-04-15 22:24:25', NULL, NULL),
(5, 2, 'Jadwal Pertandingan', '/assets/upload/regulasi/04_2020/1_(1)3.pdf', 1, 1, '2020-04-15 22:51:04', NULL, NULL),
(6, 1, 'Jadwal Pertandingan', '/assets/upload/regulasi/04_2020/1_(4).pdf', 1, 1, '2020-04-15 22:52:43', NULL, NULL),
(7, 8, 'UMK 2019', '/assets/upload/regulasi/04_2020/31a2ac8ef615aeb0da95415b4f584959.pdf', 31, 1, '2020-04-17 10:12:12', '2020-04-17 15:12:28', NULL),
(8, 8, 'UMK 2020', '/assets/upload/regulasi/04_2020/31a2ac8ef615aeb0da95415b4f584959.pdf', 31, 1, '2020-04-17 10:12:12', NULL, NULL),
(9, 1, 'Syarat izin Pendirian Cabang', '/assets/upload/regulasi/04_2020/ff235b568ae7ab4f5339b0db2ddaad44.pdf', 0, 1, '2020-04-17 18:11:14', NULL, NULL),
(10, 1, 'Syarat izin Penampungan', '/assets/upload/regulasi/04_2020/4d757f7ca31d17228f9efb53754ef5ea.pdf', 4, 1, '2020-04-17 18:15:42', NULL, NULL),
(11, 1, 'Data Layanan Izin Penampungan', '/assets/upload/regulasi/04_2020/981000ed6765adde2248e761f48c7fa4.pdf', 981000, 1, '2020-04-17 19:49:08', NULL, NULL),
(12, 1, 'LAPORAN PELAYANAN KANTOR CABANG P3MI DI JAWA TENGAH', '/assets/upload/regulasi/04_2020/cb158fbc7ad795d0f70c3d7a456e32c0.pdf', 0, 1, '2020-04-17 19:49:39', NULL, NULL),
(13, 1, 'SPP AKL', '/assets/upload/regulasi/04_2020/410d04c9096b2beb9bcff365e7a2f1ed.pdf', 410, 1, '2020-04-17 19:51:12', NULL, NULL),
(14, 1, 'Rekomendasi Persetujuan Penempatan TK AKAD ', '/assets/upload/regulasi/04_2020/9c659bcf1a72b22c3dfe319af1ffd20a.pdf', 9, 1, '2020-04-17 19:51:57', NULL, NULL),
(15, 1, 'Rekomendasi Pendirian LPTKS AKL', '/assets/upload/regulasi/04_2020/5ecbf53feef2c39c72882bf3565e31a0.pdf', 5, 1, '2020-04-17 19:52:23', NULL, NULL),
(16, 1, 'Rekomendasi Pendirian LPTKS AKAD', '/assets/upload/regulasi/04_2020/e4c72921b1bd7f42fb92b3752feb1982.pdf', 0, 1, '2020-04-17 19:52:47', NULL, NULL),
(17, 1, 'Rekomendasi Pendirian LPPRT', '/assets/upload/regulasi/04_2020/632a4de9f7b0fb2f71e5245460bd912c.pdf', 632, 1, '2020-04-17 19:53:32', NULL, NULL),
(18, 1, 'SOP kantor cabang pptkis', '/assets/upload/regulasi/04_2020/9db9ed03408e38270683112e305357d8.pdf', 9, 1, '2020-04-17 19:54:07', NULL, NULL),
(19, 1, 'SOP Surat Pengantar Rekrut', '/assets/upload/regulasi/04_2020/978127f478591cf1a3f99f9d1235d5fe.pdf', 978127, 1, '2020-04-17 19:54:27', NULL, NULL),
(20, 1, 'SOP Penampungan', '/assets/upload/regulasi/04_2020/0ac7bf9b19807c21cab92f7f9b2108f2.pdf', 0, 1, '2020-04-17 19:54:57', NULL, NULL),
(21, 1, 'SOP perpanjangan ijin cabang', '/assets/upload/regulasi/04_2020/7ca7fd4ad439e8ba729cfb58a5bdb3cc.pdf', 7, 1, '2020-04-17 19:55:32', NULL, NULL),
(22, 1, ' Struktur Organisasi Disnakertrans', '/assets/upload/regulasi/04_2020/ed1da3fffa0e9ef4d7b9eafd56144141.pdf', 0, 1, '2020-04-17 19:56:00', NULL, NULL),
(23, 2, 'UU 29 TAHUN 2009', '/assets/upload/regulasi/04_2020/d5199a1790faef0a8740a1ef3f8ee717.pdf', 0, 1, '2020-04-17 19:58:16', NULL, NULL),
(25, 2, 'UU 15 TAHUN 2015', '/assets/upload/regulasi/04_2020/c4ebeb4015df4addf41783b05e6ec4f5.pdf', 0, 1, '2020-04-17 19:58:43', NULL, NULL),
(26, 2, 'UU Nomor 15 Tahun 2016 ', '/assets/upload/regulasi/04_2020/2aee9294ad0abc137eae477ad37cae2c.pdf', 2, 1, '2020-04-17 19:59:56', NULL, NULL),
(27, 3, 'Keputusan Presiden RI No. 15 tahun 2018 ttg Hari Pemungutan Suara Tahun 2018 sebagai Hari Libur Nasi', '/assets/upload/regulasi/04_2020/ef42823b9428154157f78b099b5f6767.pdf', 0, 1, '2020-04-17 20:02:48', NULL, NULL),
(28, 3, 'PERPRES 45 TAHUN 2013', '/assets/upload/regulasi/04_2020/2d991425c1b28ba57458bf036628146e.pdf', 2, 1, '2020-04-17 20:03:14', NULL, NULL),
(29, 3, 'Perpres Nomor 45 Tahun 2013', '/assets/upload/regulasi/04_2020/a7ecaabd8ae462a233bd58b43bbb08ec.pdf', 0, 1, '2020-04-17 20:03:45', NULL, NULL),
(30, 4, 'Pergub no.33 Tahun 2016 Ttg Kode Etik ASN', '/assets/upload/regulasi/04_2020/be5459a78973735a8c8a674d369b282e.pdf', 0, 1, '2020-04-17 20:15:19', NULL, NULL),
(31, 6, 'SK ppid PEMBANTU', '/assets/upload/regulasi/04_2020/dfad3ed10583b1344ef9fa67960e2fa3.pdf', 0, 1, '2020-04-17 20:26:46', NULL, NULL),
(32, 7, 'Tindak Lanjut Produk LKS Tripartit Provinsi Jateng', '/assets/upload/regulasi/04_2020/14cb503fd31d646974a404cf5387c536.pdf', 14, 1, '2020-04-17 20:35:17', NULL, NULL),
(33, 7, 'Tata cara Pendaftaran Kartu Prakerja', '/assets/upload/regulasi/04_2020/480cc62a7659203c338128d7cd250ed0.pdf', 480, 1, '2020-04-17 20:36:36', NULL, NULL),
(34, 7, 'PENGUMUMAN HASIL SELEKSI PELAYANAN PUBLIK DISNAKERTRANS PROV JATENG 2020', '/assets/upload/regulasi/04_2020/32adfd559354ec52fe86045ef9ff7aa1.pdf', 32, 1, '2020-04-17 20:38:36', NULL, NULL),
(35, 7, 'PENGUMUMAN REKRUTMENT PETUGAS PELAYANAN PUBLIK 2020 ', '/assets/upload/regulasi/04_2020/2851d300194c6789a904831fd7ba02d0.pdf', 2851, 1, '2020-04-17 20:39:16', NULL, NULL),
(36, 7, ' PENGUMUMAN REKRUTMENT PETUGAS PELAYANAN PUBLIK 2020', '/assets/upload/regulasi/04_2020/9598b33ac61fedca961311b44b434a25.pdf', 9598, 1, '2020-04-17 20:40:37', NULL, NULL),
(37, 7, 'LAPORAN TAHUNAN LAYANAN INFORMASI PUBLIK TAHUN 2018', '/assets/upload/regulasi/04_2020/585b5e54aaaa7b3838f3d4d94100ae4f.pdf', 585, 1, '2020-04-17 20:41:03', NULL, NULL),
(38, 7, 'Pengumuman Hasil Seleksi Tanaga Outsourcing (Pelayanan Publik) Disnakertrans Prov. Jateng Tahun 2019', '/assets/upload/regulasi/04_2020/e275a479030824c92a123c931eee53f7.pdf', 0, 1, '2020-04-17 20:41:35', NULL, NULL),
(39, 7, 'SELEKSI PENGADAAN OUTSOURCING (PELAYANAN PUBLIK) TAHUN 2019', '/assets/upload/regulasi/04_2020/2b46e2c29e807150e7919bca1fc3c8a4.pdf', 2, 1, '2020-04-17 20:42:08', NULL, NULL),
(40, 7, 'RUP TAHUN 2018', '/assets/upload/regulasi/04_2020/08096c76fbf317f321f9f518a4685e91.pdf', 8096, 1, '2020-04-17 20:45:14', NULL, NULL),
(41, 7, 'Liga Pekerja Indonesia ? Form4', '/assets/upload/regulasi/04_2020/08a600e935da150ddf7d30be7ae57456.pdf', 8, 1, '2020-04-17 20:46:07', NULL, NULL),
(42, 7, 'Liga Pekerja Indonesia ? Form3 ', '/assets/upload/regulasi/04_2020/26a04c0abb8ce5a21c39dd5290e499c3.pdf', 26, 1, '2020-04-17 20:46:41', NULL, NULL),
(43, 7, 'Liga Pekerja Indonesia ? Form2', '/assets/upload/regulasi/04_2020/fb2e7f73c04642e19fc2a80a7fcfa8d8.pdf', 0, 1, '2020-04-17 20:47:10', NULL, NULL),
(44, 7, 'Liga Pekerja Indonesia ? Form1', '/assets/upload/regulasi/04_2020/a11f9b3307e80f97d0d7cd982e6169da.pdf', 0, 1, '2020-04-17 20:47:36', NULL, NULL),
(45, 7, ' PENGUMUMAN HASIL SELEKSI ADMINISTRASI HAKIM AD HOC PENGADILAN HI TAHUN 2017', '/assets/upload/regulasi/04_2020/3f9e9fcd145bf1f027e8bf7130de4ed1.pdf', 3, 1, '2020-04-17 20:51:11', NULL, NULL),
(46, 7, 'RUP 2017 ', '/assets/upload/regulasi/04_2020/8796896c4b5b703be03438c4ca163de4.pdf', 8796896, 1, '2020-04-17 20:51:39', NULL, NULL),
(47, 7, 'Pengumuman Lelang Angkutan Trans', '/assets/upload/regulasi/04_2020/a202f20795003d8943a386e436a69247.pdf', 0, 1, '2020-04-17 20:52:34', NULL, NULL),
(48, 3, 'PP PELAYANAN PERIZINAN BERUSAHA TERINTEGRASI SECARA ELEKTRONIK', '/assets/upload/regulasi/04_2020/9926dc1fae5fec74553e25a9d4897def.pdf', 9926, 1, '2020-04-18 13:55:09', NULL, NULL),
(49, 2, 'UU NOMOR 18 TAHUN 2017 TTG PELINDUNGAN PEKERJA MIGRAN INDONESIA', '/assets/upload/regulasi/04_2020/c3e21feb21dfb42c9613489f9c7556dc.pdf', 0, 1, '2020-04-18 14:07:06', NULL, NULL),
(50, 4, 'Keputusan Gubernur Jateng Tentang UMK Tahun 2018 ', '/assets/upload/regulasi/04_2020/6b8a315fc70dc59ab395f2bee5cf0ec3.pdf', 6, 1, '2020-04-18 14:13:01', NULL, NULL),
(51, 6, 'adwa', '/assets/upload/regulasi/04_2020/b46eff5f70c201d2a5fa19788d71a512.pdf', 0, 1, '2020-04-18 14:16:34', NULL, NULL),
(52, 6, 'SK PPID Pembantu tahun 2019', '/assets/upload/regulasi/04_2020/9add8438b2f3cb1f5859b512f0dc3671.pdf', 9, 1, '2020-04-18 14:18:00', NULL, NULL),
(53, 6, 'Tim Pengelola PPID', '/assets/upload/regulasi/04_2020/96353c0fc8bc163a9060ca1aba78b40b.pdf', 96353, 1, '2020-04-18 14:23:22', NULL, NULL),
(54, 6, ' SK PPID Pembantu tahun 2018', '/assets/upload/regulasi/04_2020/4e57c926bdb0ccbb208ef670965e37a3.pdf', 2147483647, 1, '2020-04-18 14:24:06', NULL, NULL),
(55, 6, ' Struktur Organisasi Disnaker 2019 ', '/assets/upload/regulasi/04_2020/f890fad6c0b78e5d270bede08bdce1ef.pdf', 0, 1, '2020-04-18 14:28:09', NULL, NULL),
(56, 6, 'Struktur Organisasi Disnaker', '/assets/upload/regulasi/04_2020/02c0df021c16c00ee9a75bba6c1124de.pdf', 2, 1, '2020-04-18 14:28:35', NULL, NULL),
(57, 6, ' SK', '/assets/upload/regulasi/04_2020/a1ce8490523b02af0af035591a257556.pdf', 0, 1, '2020-04-18 14:30:59', NULL, NULL),
(58, 7, 'Apa dan bagaimana Kartu Prakerja', '/assets/upload/regulasi/04_2020/e178de99ab4482aeb700d01ea6b0917c.pdf', 0, 1, '2020-04-18 14:35:58', '2020-04-18 14:37:50', NULL),
(59, 7, 'Buku Statistik (DataTahun 2016- 2018)', '/assets/upload/regulasi/04_2020/d8c52b08b9d0655ffb8667d4da475def.pdf', 0, 1, '2020-04-18 14:37:03', '2020-04-18 14:37:33', NULL),
(60, 7, 'LKjIP Tahun 2018', '/assets/upload/regulasi/04_2020/a1596da1263894ecea9f431713f49253.pdf', 0, 1, '2020-04-18 14:40:11', NULL, NULL),
(61, 7, 'Data Pegawai Disnakertrans Jateng Tahun 2018', '/assets/upload/regulasi/04_2020/6cc33f4ad4261564d2ea9fe8d2e2add5.pdf', 6, 1, '2020-04-18 15:10:40', NULL, NULL),
(62, 7, ' Daftar Pegawai Disnakertrans Tahun 2017', '/assets/upload/regulasi/04_2020/124a8217c0e6f5e276c3389a985ca575.pdf', 124, 1, '2020-04-18 15:12:07', NULL, NULL),
(63, 7, 'MoU Sharing Transmigran dengan Prov Kaltara', '/assets/upload/regulasi/04_2020/e6c30a5737e06c18881cccf61ec36fcd.pdf', 0, 1, '2020-04-18 15:17:54', NULL, NULL),
(64, 7, 'LKS TRIPARTIT & HARMONISASI KOMUNIKASI TENAGA KERJA DENGAN PERUSAHAAN', '/assets/upload/regulasi/04_2020/c4485c1130f054fb17238a665e1890e1.pdf', 0, 1, '2020-04-18 15:21:54', NULL, NULL),
(65, 7, 'PROTOKOL PENANGANAN COVID 19 DI LINGKUNGAN DISNAKERTRANS', '/assets/upload/regulasi/04_2020/c32c005cd4692378eecc8a14ef620aee.pdf', 0, 1, '2020-04-18 15:28:38', NULL, NULL),
(66, 7, 'SKKNI 2014', '/assets/upload/regulasi/04_2020/d74f0910d342d9029ec33810fa608eaf.pdf', 0, 1, '2020-04-18 15:31:24', NULL, NULL),
(67, 7, 'Surat Edaran No:800/09711/2018 ', '/assets/upload/regulasi/04_2020/3999e2e5e4bfb3a93b2e1cf9a09f43ec.pdf', 399900, 1, '2020-04-18 15:32:09', NULL, NULL),
(68, 7, 'Pelatihan Kerja GRATIS', '/assets/upload/regulasi/04_2020/f3d34a49257e7cebe27c2d1d5f9df909.pdf', 0, 1, '2020-04-18 15:33:24', NULL, NULL),
(69, 7, 'PELATIHAN KERJA DAN SERTIFIKASI .... GRATIS ', '/assets/upload/regulasi/04_2020/be673d928a4244e808deae85f6085815.pdf', 0, 1, '2020-04-18 15:36:18', NULL, NULL),
(70, 7, 'Magang Jepang', '/assets/upload/regulasi/04_2020/a2445e83aade27318ef4efdd69467910.pdf', 0, 1, '2020-04-18 15:37:26', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_renstra`
--

CREATE TABLE `turin_renstra` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `url` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_renstra`
--

INSERT INTO `turin_renstra` (`id`, `nama`, `url`) VALUES
(1, 'KAK', 'kak'),
(2, 'RKA', 'rka'),
(3, 'RKT', 'rkt'),
(4, 'DPA', 'dpa'),
(5, 'Kegiatan Publik', 'kp'),
(6, 'Renstra & Renja Tahunan', 'rrt'),
(7, 'Penetapan Kerja', 'pk');

-- --------------------------------------------------------

--
-- Table structure for table `turin_renstra_data`
--

CREATE TABLE `turin_renstra_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_renstra` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `file` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_renstra_data`
--

INSERT INTO `turin_renstra_data` (`id`, `id_renstra`, `judul`, `file`, `deskripsi`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 'Jadwal Pertandingan', '049754fc502a0614d6b76c15d2df109c.pdf', '/assets/upload/renstra/04_2020/049754fc502a0614d6b76c15d2df109c.pdf', 1, '2020-04-16 19:05:41', NULL, NULL),
(2, 1, 'KAK Kegiatan Pelatihan Kerja BLK Semarang 2 Tahun 2019 ', '1e5bffa4db55ed8683080adfab6fa9da.pdf', '/assets/upload/renstra/04_2020/1e5bffa4db55ed8683080adfab6fa9da.pdf', 1, '2020-04-18 15:52:09', NULL, NULL),
(3, 1, 'KAK Emisi Tahun 2019 ', '19d9f45098da2cfedfb5d9addc2aac1f.pdf', '/assets/upload/renstra/04_2020/19d9f45098da2cfedfb5d9addc2aac1f.pdf', 1, '2020-04-18 15:52:34', NULL, NULL),
(4, 1, 'KAK Pelatihan DBHCHT Tahun 2019', '7fa75725c2548987c5540aa0cb0cd4cb.pdf', '/assets/upload/renstra/04_2020/7fa75725c2548987c5540aa0cb0cd4cb.pdf', 1, '2020-04-18 15:52:59', NULL, NULL),
(5, 1, 'KAK Pemasaran Tahun 2019', '22dacc8d318df1430a195a4dcf2895f0.pdf', '/assets/upload/renstra/04_2020/22dacc8d318df1430a195a4dcf2895f0.pdf', 1, '2020-04-18 15:53:25', NULL, NULL),
(6, 1, 'KAK Kegiatan Pelatiha Kerja Tahun 2018', '6b7bf5cd445ebfcba43fe5b9165d2c96.pdf', '/assets/upload/renstra/04_2020/6b7bf5cd445ebfcba43fe5b9165d2c96.pdf', 1, '2020-04-18 15:53:57', NULL, NULL),
(7, 1, 'KAK PENINGKATAN PENEMPATAN MELALUI MEKANISME AKAD 2017', 'e6d35430282ca58f06061235ebba838d.pdf', '/assets/upload/renstra/04_2020/e6d35430282ca58f06061235ebba838d.pdf', 1, '2020-04-18 15:54:20', NULL, NULL),
(8, 1, 'KAK PENGENDALIAN PENGGUNAAN TENAGA KERJA ASING (TKA) 2017', 'ccad496823a3e1c195d1535ace0b6a6d.pdf', '/assets/upload/renstra/04_2020/ccad496823a3e1c195d1535ace0b6a6d.pdf', 1, '2020-04-18 15:54:42', NULL, NULL),
(9, 2, 'RKA Perubahan Tahun 2019', '8460193ee6f62b1b4a04c14410693c9f.pdf', '/assets/upload/renstra/04_2020/8460193ee6f62b1b4a04c14410693c9f.pdf', 1, '2020-04-18 15:55:35', NULL, NULL),
(10, 2, 'RKA Tahun 2018', '80d75f25b9fd6251f40a82fc4d3f8f5b.pdf', '/assets/upload/renstra/04_2020/80d75f25b9fd6251f40a82fc4d3f8f5b.pdf', 1, '2020-04-18 15:55:58', NULL, NULL),
(11, 2, 'RKA Pelayanan Penempatan Dalam dan Luar Negeri', 'c4dafbaee790c32c1827c6ee192cb272.pdf', '/assets/upload/renstra/04_2020/c4dafbaee790c32c1827c6ee192cb272.pdf', 1, '2020-04-18 15:56:21', NULL, NULL),
(12, 4, 'DPA Perubahan Tahun 2019', '907921b4601088f6424110de181808b6.pdf', '/assets/upload/renstra/04_2020/907921b4601088f6424110de181808b6.pdf', 1, '2020-04-18 15:58:32', NULL, NULL),
(13, 4, 'DPA Tahun 2019', '71db302f015bcb4eff4317408018c7bc.pdf', '/assets/upload/renstra/04_2020/71db302f015bcb4eff4317408018c7bc.pdf', 1, '2020-04-18 15:59:00', NULL, NULL),
(14, 4, 'DPA 2017 ', '980aac5271d89ce39302a560e037ea6e.pdf', '/assets/upload/renstra/04_2020/980aac5271d89ce39302a560e037ea6e.pdf', 1, '2020-04-18 15:59:25', NULL, NULL),
(15, 4, 'DPA 2017 ', 'bbcef2bd3e1b1e1a0c048cba4a6a88a4.pdf', '/assets/upload/renstra/04_2020/bbcef2bd3e1b1e1a0c048cba4a6a88a4.pdf', 1, '2020-04-18 15:59:54', NULL, NULL),
(16, 4, 'DPA 2017 ', '7355ced19b243aa358597fa01e224913.pdf', '/assets/upload/renstra/04_2020/7355ced19b243aa358597fa01e224913.pdf', 1, '2020-04-18 16:00:29', NULL, NULL),
(17, 6, 'Rencana Kerja Tahun 2018 ', 'e752159dd15cd3ffce865efb1c029c36.pdf', '/assets/upload/renstra/04_2020/e752159dd15cd3ffce865efb1c029c36.pdf', 1, '2020-04-18 16:04:14', NULL, NULL),
(18, 6, ' Renstra 2017', '82d326987186251ca765b5adbfdf2b6d.pdf', '/assets/upload/renstra/04_2020/82d326987186251ca765b5adbfdf2b6d.pdf', 1, '2020-04-18 16:05:38', NULL, NULL),
(19, 6, 'Renstra 2017', '67910be58f58f72672674577a5388bdc.pdf', '/assets/upload/renstra/04_2020/67910be58f58f72672674577a5388bdc.pdf', 1, '2020-04-18 16:07:21', NULL, NULL),
(20, 7, 'Perjanjian Kirenja 2017', 'cbf2b86ae35b0831a53bc31385cc1546.pdf', '/assets/upload/renstra/04_2020/cbf2b86ae35b0831a53bc31385cc1546.pdf', 1, '2020-04-18 16:08:04', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_slide`
--

CREATE TABLE `turin_slide` (
  `id` int(11) UNSIGNED NOT NULL,
  `front` varchar(11) DEFAULT NULL,
  `judul` text DEFAULT NULL,
  `ppid` int(11) DEFAULT NULL,
  `isi` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_slide`
--

INSERT INTO `turin_slide` (`id`, `front`, `judul`, `ppid`, `isi`, `deskripsi`, `file`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(7, NULL, 'KESABNGPOL', 0, '<p>0</p>\r\n', 'assets/upload/slide/11_2020/fcfd162b2779ed1005ff213ee145bf1a.png', 'fcfd162b2779ed1005ff213ee145bf1a.png', 1, '2020-11-20 02:24:18', '2020-11-20 08:30:15', NULL),
(9, NULL, 'BHINNEKA', 0, '<p>0</p>\r\n', 'assets/upload/slide/11_2020/3ec4a15cca87173df59c0cdd429880aa.png', '3ec4a15cca87173df59c0cdd429880aa.png', 1, '2020-11-20 02:27:30', NULL, NULL),
(10, NULL, 'PPID', 1, '<p>0</p>\r\n', 'assets/upload/slide/11_2020/f3be6b366f8fd7cba2efe93896c3483d.png', 'f3be6b366f8fd7cba2efe93896c3483d.png', 1, '2020-11-20 02:28:48', NULL, NULL),
(11, NULL, 'PPID KESBANGPOL LOVE INDONESIA', 1, '<p>0</p>\r\n', 'assets/upload/slide/11_2020/47c5ae723b264c0bd6e64c5c7e83a70a.png', '47c5ae723b264c0bd6e64c5c7e83a70a.png', 1, '2020-11-20 02:29:39', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turin_sop`
--

CREATE TABLE `turin_sop` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `url` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_sop`
--

INSERT INTO `turin_sop` (`id`, `nama`, `url`) VALUES
(1, 'SOP Permohonan Informasi', 'spi'),
(2, 'SOP Pengaduan', 'sp'),
(3, 'SOP Pengajuan Keberatan', 'spk'),
(4, 'SOP Penetapan DIP', 'spd');

-- --------------------------------------------------------

--
-- Table structure for table `turin_sop_data`
--

CREATE TABLE `turin_sop_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_sop` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `turin_sumber`
--

CREATE TABLE `turin_sumber` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `url` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_sumber`
--

INSERT INTO `turin_sumber` (`id`, `nama`, `url`) VALUES
(1, 'PAD', 'pad'),
(2, 'APBD', 'apbd'),
(3, 'APBN', 'apbn');

-- --------------------------------------------------------

--
-- Table structure for table `turin_sumber_data`
--

CREATE TABLE `turin_sumber_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_sumber` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `turin_tamu`
--

CREATE TABLE `turin_tamu` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `instansi` text DEFAULT NULL,
  `created_by_ip` varchar(15) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `turin_umk`
--

CREATE TABLE `turin_umk` (
  `id` int(11) UNSIGNED NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `file` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `turin_uptd`
--

CREATE TABLE `turin_uptd` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_uptd`
--

INSERT INTO `turin_uptd` (`id`, `nama`) VALUES
(1, 'BLK SEMARANG 1'),
(2, 'BLK SEMARANG 2'),
(3, 'BP3TK'),
(4, 'BALAI K2'),
(5, 'BLK CILACAP'),
(6, 'BPK TRANS');

-- --------------------------------------------------------

--
-- Table structure for table `turin_uptd_data`
--

CREATE TABLE `turin_uptd_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_uptd` int(11) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `file` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turin_uptd_data`
--

INSERT INTO `turin_uptd_data` (`id`, `id_uptd`, `judul`, `file`, `deskripsi`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'BP2TK ', 'profil_dinas_2017_upload_website.pdf', '/assets/upload/uptd/04_2020/profil_dinas_2017_upload_website.pdf', 1, '2020-04-18 13:33:14', NULL, NULL),
(2, 3, ' BP3TK', 'profil_bp3tk_upload_website.pdf', '/assets/upload/uptd/04_2020/profil_bp3tk_upload_website.pdf', 1, '2020-04-18 13:33:56', '2020-04-18 13:34:05', NULL),
(3, 4, 'Balai K2', 'profil_dinas_2017_upload_website1_compressed.pdf', '/assets/upload/uptd/04_2020/profil_dinas_2017_upload_website1_compressed.pdf', 1, '2020-04-18 13:35:09', NULL, NULL),
(4, 1, ' BLKDLN', 'profil_blkdln_upload_web.pdf', '/assets/upload/uptd/04_2020/profil_blkdln_upload_web.pdf', 1, '2020-04-18 13:35:59', NULL, NULL),
(5, 6, ' klampok', 'profil_klampok_upload_website.pdf', '/assets/upload/uptd/04_2020/profil_klampok_upload_website.pdf', 1, '2020-04-18 13:36:59', NULL, NULL),
(6, 5, 'blki', 'profil_blki_upload_website4_compressed.pdf', '/assets/upload/uptd/04_2020/profil_blki_upload_website4_compressed.pdf', 1, '2020-04-18 13:37:56', NULL, NULL),
(7, 5, 'blki', 'profil_blki_upload_website3_compressed.pdf', '/assets/upload/uptd/04_2020/profil_blki_upload_website3_compressed.pdf', 1, '2020-04-18 13:38:35', '2020-04-18 13:38:45', NULL),
(8, 5, 'blki', 'profil_blki_upload_website2_compressed.pdf', '/assets/upload/uptd/04_2020/profil_blki_upload_website2_compressed.pdf', 1, '2020-04-18 13:39:48', NULL, NULL),
(9, 5, 'blki', 'profil_blki_upload_website1_compressed.pdf', '/assets/upload/uptd/04_2020/profil_blki_upload_website1_compressed.pdf', 1, '2020-04-18 13:40:27', NULL, NULL),
(10, 5, 'Profil BLK Industri Cilacap ', 'profil_blki_upload_website_compressed.pdf', '/assets/upload/uptd/04_2020/profil_blki_upload_website_compressed.pdf', 1, '2020-04-18 13:41:17', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `turin_admin_xyz`
--
ALTER TABLE `turin_admin_xyz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_aduan`
--
ALTER TABLE `turin_aduan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_anggaran`
--
ALTER TABLE `turin_anggaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_anggaran_copy`
--
ALTER TABLE `turin_anggaran_copy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_anggaran_data`
--
ALTER TABLE `turin_anggaran_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_anggaran_data_copy`
--
ALTER TABLE `turin_anggaran_data_copy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_berita`
--
ALTER TABLE `turin_berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_bidang`
--
ALTER TABLE `turin_bidang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_bidang_data`
--
ALTER TABLE `turin_bidang_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_download`
--
ALTER TABLE `turin_download`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_form`
--
ALTER TABLE `turin_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_galeri`
--
ALTER TABLE `turin_galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_galeri_data`
--
ALTER TABLE `turin_galeri_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_galeri_video`
--
ALTER TABLE `turin_galeri_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_header`
--
ALTER TABLE `turin_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_infografis`
--
ALTER TABLE `turin_infografis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_informasi`
--
ALTER TABLE `turin_informasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_informasi_data`
--
ALTER TABLE `turin_informasi_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_ip_jahat`
--
ALTER TABLE `turin_ip_jahat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_jabatan`
--
ALTER TABLE `turin_jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_kontak`
--
ALTER TABLE `turin_kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_laporan`
--
ALTER TABLE `turin_laporan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_laporan_data`
--
ALTER TABLE `turin_laporan_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_links`
--
ALTER TABLE `turin_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_log_activity`
--
ALTER TABLE `turin_log_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_maklumat_lhkpn`
--
ALTER TABLE `turin_maklumat_lhkpn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_menu`
--
ALTER TABLE `turin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_pengadaan`
--
ALTER TABLE `turin_pengadaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_pengadaan_data`
--
ALTER TABLE `turin_pengadaan_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_perencanaan`
--
ALTER TABLE `turin_perencanaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_perencanaan_data`
--
ALTER TABLE `turin_perencanaan_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_ppid`
--
ALTER TABLE `turin_ppid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_profil`
--
ALTER TABLE `turin_profil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_profil_pejabat`
--
ALTER TABLE `turin_profil_pejabat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_profil_pejabat_tab`
--
ALTER TABLE `turin_profil_pejabat_tab`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_profil_struktur`
--
ALTER TABLE `turin_profil_struktur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_program`
--
ALTER TABLE `turin_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_program_copy`
--
ALTER TABLE `turin_program_copy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_program_data`
--
ALTER TABLE `turin_program_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_program_data_copy`
--
ALTER TABLE `turin_program_data_copy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_regulasi`
--
ALTER TABLE `turin_regulasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_regulasi_`
--
ALTER TABLE `turin_regulasi_`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_regulasi_data`
--
ALTER TABLE `turin_regulasi_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_regulasi_data_copy`
--
ALTER TABLE `turin_regulasi_data_copy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_renstra`
--
ALTER TABLE `turin_renstra`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_renstra_data`
--
ALTER TABLE `turin_renstra_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_slide`
--
ALTER TABLE `turin_slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_sop`
--
ALTER TABLE `turin_sop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_sop_data`
--
ALTER TABLE `turin_sop_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_sumber`
--
ALTER TABLE `turin_sumber`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_sumber_data`
--
ALTER TABLE `turin_sumber_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_tamu`
--
ALTER TABLE `turin_tamu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_umk`
--
ALTER TABLE `turin_umk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_uptd`
--
ALTER TABLE `turin_uptd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turin_uptd_data`
--
ALTER TABLE `turin_uptd_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `turin_admin_xyz`
--
ALTER TABLE `turin_admin_xyz`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `turin_aduan`
--
ALTER TABLE `turin_aduan`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `turin_anggaran`
--
ALTER TABLE `turin_anggaran`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `turin_anggaran_copy`
--
ALTER TABLE `turin_anggaran_copy`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `turin_anggaran_data`
--
ALTER TABLE `turin_anggaran_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `turin_anggaran_data_copy`
--
ALTER TABLE `turin_anggaran_data_copy`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `turin_berita`
--
ALTER TABLE `turin_berita`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `turin_bidang`
--
ALTER TABLE `turin_bidang`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `turin_bidang_data`
--
ALTER TABLE `turin_bidang_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `turin_download`
--
ALTER TABLE `turin_download`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `turin_form`
--
ALTER TABLE `turin_form`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `turin_galeri`
--
ALTER TABLE `turin_galeri`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `turin_galeri_data`
--
ALTER TABLE `turin_galeri_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `turin_galeri_video`
--
ALTER TABLE `turin_galeri_video`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `turin_header`
--
ALTER TABLE `turin_header`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `turin_infografis`
--
ALTER TABLE `turin_infografis`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `turin_informasi`
--
ALTER TABLE `turin_informasi`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `turin_informasi_data`
--
ALTER TABLE `turin_informasi_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `turin_ip_jahat`
--
ALTER TABLE `turin_ip_jahat`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `turin_jabatan`
--
ALTER TABLE `turin_jabatan`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `turin_kontak`
--
ALTER TABLE `turin_kontak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `turin_laporan`
--
ALTER TABLE `turin_laporan`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `turin_laporan_data`
--
ALTER TABLE `turin_laporan_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `turin_links`
--
ALTER TABLE `turin_links`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `turin_log_activity`
--
ALTER TABLE `turin_log_activity`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `turin_maklumat_lhkpn`
--
ALTER TABLE `turin_maklumat_lhkpn`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `turin_menu`
--
ALTER TABLE `turin_menu`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `turin_pengadaan`
--
ALTER TABLE `turin_pengadaan`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `turin_pengadaan_data`
--
ALTER TABLE `turin_pengadaan_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `turin_perencanaan`
--
ALTER TABLE `turin_perencanaan`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `turin_perencanaan_data`
--
ALTER TABLE `turin_perencanaan_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `turin_ppid`
--
ALTER TABLE `turin_ppid`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `turin_profil`
--
ALTER TABLE `turin_profil`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `turin_profil_pejabat`
--
ALTER TABLE `turin_profil_pejabat`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `turin_profil_pejabat_tab`
--
ALTER TABLE `turin_profil_pejabat_tab`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `turin_profil_struktur`
--
ALTER TABLE `turin_profil_struktur`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `turin_program`
--
ALTER TABLE `turin_program`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `turin_program_copy`
--
ALTER TABLE `turin_program_copy`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `turin_program_data`
--
ALTER TABLE `turin_program_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `turin_program_data_copy`
--
ALTER TABLE `turin_program_data_copy`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `turin_regulasi`
--
ALTER TABLE `turin_regulasi`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `turin_regulasi_`
--
ALTER TABLE `turin_regulasi_`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `turin_regulasi_data`
--
ALTER TABLE `turin_regulasi_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `turin_regulasi_data_copy`
--
ALTER TABLE `turin_regulasi_data_copy`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `turin_renstra`
--
ALTER TABLE `turin_renstra`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `turin_renstra_data`
--
ALTER TABLE `turin_renstra_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `turin_slide`
--
ALTER TABLE `turin_slide`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `turin_sop`
--
ALTER TABLE `turin_sop`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `turin_sop_data`
--
ALTER TABLE `turin_sop_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `turin_sumber`
--
ALTER TABLE `turin_sumber`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `turin_sumber_data`
--
ALTER TABLE `turin_sumber_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `turin_tamu`
--
ALTER TABLE `turin_tamu`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `turin_umk`
--
ALTER TABLE `turin_umk`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `turin_uptd`
--
ALTER TABLE `turin_uptd`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `turin_uptd_data`
--
ALTER TABLE `turin_uptd_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
